# Collaborative AR App

## Experimentation and Data Analysis
To assess the effectiveness of the VCs under analysis (and of their combinations), a user study involving 40 volunteers (11 females and 29 males, ranging from 18 to 34 years old) was performed. Details on the result can be accessed at: 

The collected data, the performed analysis and the results can be consulted in an [excell workbook](https://politoit-my.sharepoint.com/:x:/g/personal/francesco_strada_polito_it/EYifwgpTAZJHvdHhEcOvYw4BOnOz80_C7Tz8o1Yl1Y-8rw?e=YoKPna) and [google collab notebook](https://colab.research.google.com/drive/1UfhUVZXj-8-Vm4q_vt94ffI5CUpTkw3S?usp=sharing).


