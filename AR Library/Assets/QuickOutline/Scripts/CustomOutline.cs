﻿//
//  Outline.cs
//  QuickOutline
//
//  Created by Chris Nolet on 3/30/18.
//  Copyright © 2018 Chris Nolet. All rights reserved.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[DisallowMultipleComponent]

public class CustomOutline : MonoBehaviour
{
    private static HashSet<Mesh> registeredMeshes = new HashSet<Mesh>();

    public enum Type
    {
        Overlapping,
        Original,
        Simple
    }

    public Type OutlineType
    {
        get { return outlineType; }
        set
        {
            outlineType = value;
            needsUpdate = true;
        }
    }

    public enum Mode
    {
        OutlineAll,
        OutlineVisible,
        OutlineHidden,
        OutlineAndSilhouette,
        SilhouetteOnly
    }

    public Mode OutlineMode
    {
        get { return outlineMode; }
        set
        {
            outlineMode = value;
            needsUpdate = true;
        }
    }

    public Color OutlineColor
    {
        get { return outlineColor; }
        set
        {
            outlineColor = value;
            needsUpdate = true;
        }
    }

    public float OutlineWidth
    {
        get { return outlineWidth; }
        set
        {
            outlineWidth = value;
            needsUpdate = true;
        }
    }

    public float OutlineFadeInSpeed
    {
        get { return outlineFadeInSpeed; }
        set { outlineFadeInSpeed = value; }
    }

    public float OutlineFadeOutSpeed
    {
        get { return outlineFadeOutSpeed; }
        set { outlineFadeOutSpeed = value; }
    }

    public bool FadeIn { get; set; }

    [Serializable]
    private class ListVector3
    {
        public List<Vector3> data;
    }

    [SerializeField]
    private Type outlineType;

    [SerializeField]
    private Mode outlineMode;

    [SerializeField]
    private Color outlineColor = Color.white;

    [SerializeField, Range(0f, 10f)]
    private float outlineWidth = 2f;

    [SerializeField]
    private float outlineFadeInSpeed = 4f;

    [SerializeField]
    private float outlineFadeOutSpeed = 4f;

    [Header("Optional")]

    [SerializeField, Tooltip("Precompute enabled: Per-vertex calculations are performed in the editor and serialized with the object. "
    + "Precompute disabled: Per-vertex calculations are performed at runtime in Awake(). This may cause a pause for large meshes.")]
    private bool precomputeOutline;

    [SerializeField, HideInInspector]
    private List<Mesh> bakeKeys = new List<Mesh>();

    [SerializeField, HideInInspector]
    private List<ListVector3> bakeValues = new List<ListVector3>();

    private Renderer[] renderers;
    private Material outlineOverlappingMaterial;
    private Material outlineSimpleMaterial;
    private Material outlineMaskMaterial;
    private Material outlineFillMaterial;

    private bool needsUpdate;

    private Coroutine fadeInCoroutine;
    private Coroutine fadeOutCoroutine;

    public void FadeOutAndDestroy()
    {
        if(!this.enabled) Destroy(this);
        fadeOutCoroutine = StartCoroutine(FadeOutAndDestroyCoroutine());
    }
    public void FadeOutAndDisable()
    {
        if (!this.enabled) return;
        fadeOutCoroutine = StartCoroutine(FadeOutAndDisableCoroutine());
    }

    void Awake()
    {
        // Cache renderers 
        Renderer[] renderersInChildren = GetComponentsInChildren<Renderer>();
        List<Renderer> renderersList = new List<Renderer>();

        //If this gameobject is static interactive or dynamic, consider all children's renderer
        //otherwise consider only this gameobject renderer
        if ((gameObject.layer == GameUtils.Layer.StaticObjects && gameObject.CompareTag("Interactive")) ||
            gameObject.layer == GameUtils.Layer.DynamicObjects || gameObject.layer == GameUtils.Layer.Players)
        {
            for (int i = 0; i < renderersInChildren.Length; i++)
            {
                if (renderersInChildren[i].gameObject.layer == GameUtils.Layer.StaticObjects ||
                    renderersInChildren[i].gameObject.layer == GameUtils.Layer.DynamicObjects)
                {
                    renderersList.Add(renderersInChildren[i]);
                }
            }
        }
        else if(GetComponent<Renderer>() != null)
        {
            renderersList.Add(this.GetComponent<Renderer>());
        }

        renderers = renderersList.ToArray();

        // Instantiate outline materials
        outlineOverlappingMaterial = Instantiate(Resources.Load<Material>(@"Materials/Outline"));

        outlineSimpleMaterial = Instantiate(Resources.Load<Material>(@"Materials/OutlineSimple"));

        outlineMaskMaterial = Instantiate(Resources.Load<Material>(@"Materials/OutlineMask"));
        outlineFillMaterial = Instantiate(Resources.Load<Material>(@"Materials/OutlineFill"));

        outlineOverlappingMaterial.name = "Outline (Instance)";
        outlineSimpleMaterial.name = "OutlineSimple (Instance)";

        outlineMaskMaterial.name = "OutlineMask (Instance)";
        outlineFillMaterial.name = "OutlineFill (Instance)";

        // Retrieve or generate smooth normals
        LoadSmoothNormals();

        // Apply material properties immediately
        needsUpdate = true;

        FadeIn = true;

        this.enabled = false;
    }

    void OnEnable()
    {
        if (FadeIn)
            fadeInCoroutine = StartCoroutine(FadeInCoroutine());

        AddMaterials();
        UpdateMaterialProperties();
    }

    void OnValidate()
    {
        // Update material properties
        needsUpdate = true;

        // Clear cache when baking is disabled or corrupted
        if (!precomputeOutline && bakeKeys.Count != 0 || bakeKeys.Count != bakeValues.Count)
        {
            bakeKeys.Clear();
            bakeValues.Clear();
        }

        // Generate smooth normals when baking is enabled
        if (precomputeOutline && bakeKeys.Count == 0)
        {
            Bake();
        }
    }

    void Update()
    {
        if (needsUpdate)
        {
            needsUpdate = false;

            UpdateMaterialProperties();
        }
    }

    void OnDisable()
    {
        RemoveMaterials();
        fadeInCoroutine = null;
    }

    void OnDestroy()
    {

        // Destroy material instances
        Destroy(outlineOverlappingMaterial);
        Destroy(outlineSimpleMaterial);
        Destroy(outlineMaskMaterial);
        Destroy(outlineFillMaterial);
    }

    void Bake()
    {

        // Generate smooth normals for each mesh
        var bakedMeshes = new HashSet<Mesh>();

        foreach (var meshFilter in GetComponentsInChildren<MeshFilter>())
        {
            //Skip meshes not in these layers: StaticObjects, DynamicObjects, FocalPoints
            if (meshFilter.gameObject.layer != GameUtils.Layer.StaticObjects &&
                meshFilter.gameObject.layer != GameUtils.Layer.DynamicObjects &&
                meshFilter.gameObject.layer != GameUtils.Layer.FocalPoints)
            {
                continue;
            }

            // Skip duplicates
            if (!bakedMeshes.Add(meshFilter.sharedMesh))
            {
                continue;
            }

            // Serialize smooth normals
            var smoothNormals = SmoothNormals(meshFilter.sharedMesh);

            bakeKeys.Add(meshFilter.sharedMesh);
            bakeValues.Add(new ListVector3() { data = smoothNormals });
        }
    }

    void LoadSmoothNormals()
    {

        // Retrieve or generate smooth normals
        foreach (var meshFilter in GetComponentsInChildren<MeshFilter>())
        {
            if (meshFilter.sharedMesh == null)
            {
                continue;
            }

            //Skip meshes not in these layers: StaticObjects, DynamicObjects, FocalPoints
            if (meshFilter.gameObject.layer != GameUtils.Layer.StaticObjects &&
                meshFilter.gameObject.layer != GameUtils.Layer.DynamicObjects &&
                meshFilter.gameObject.layer != GameUtils.Layer.FocalPoints)
            {
                continue;
            }

            // Skip if smooth normals have already been adopted
            if (!registeredMeshes.Add(meshFilter.sharedMesh))
            {
                continue;
            }

            // Retrieve or generate smooth normals
            var index = bakeKeys.IndexOf(meshFilter.sharedMesh);
            var smoothNormals = (index >= 0) ? bakeValues[index].data : SmoothNormals(meshFilter.sharedMesh);

            // Store smooth normals in UV3
            meshFilter.sharedMesh.SetUVs(3, smoothNormals);
        }

        // Clear UV3 on skinned mesh renderers
        foreach (var skinnedMeshRenderer in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            //Skip renderers not in these layers: StaticObjects, DynamicObjects, FocalPoints
            if (skinnedMeshRenderer.gameObject.layer != GameUtils.Layer.StaticObjects &&
                skinnedMeshRenderer.gameObject.layer != GameUtils.Layer.DynamicObjects &&
                skinnedMeshRenderer.gameObject.layer != GameUtils.Layer.FocalPoints)
            {
                continue;
            }

            if (registeredMeshes.Add(skinnedMeshRenderer.sharedMesh))
            {
                skinnedMeshRenderer.sharedMesh.uv4 = new Vector2[skinnedMeshRenderer.sharedMesh.vertexCount];
            }
        }
    }

    List<Vector3> SmoothNormals(Mesh mesh)
    {

        // Group vertices by location
        var groups = mesh.vertices.Select((vertex, index) => new KeyValuePair<Vector3, int>(vertex, index)).GroupBy(pair => pair.Key);

        // Copy normals to a new list
        var smoothNormals = new List<Vector3>(mesh.normals);

        // Average normals for grouped vertices
        foreach (var group in groups)
        {

            // Skip single vertices
            if (group.Count() == 1)
            {
                continue;
            }

            // Calculate the average normal
            var smoothNormal = Vector3.zero;

            foreach (var pair in group)
            {
                smoothNormal += mesh.normals[pair.Value];
            }

            smoothNormal.Normalize();

            // Assign smooth normal to each vertex
            foreach (var pair in group)
            {
                smoothNormals[pair.Value] = smoothNormal;
            }
        }

        return smoothNormals;
    }

    void AddMaterials()
    {
        foreach (var rend in renderers)
        {
            // Append outline shaders
            var materials = rend.sharedMaterials.ToList();

            materials.Remove(outlineOverlappingMaterial);
            materials.Remove(outlineSimpleMaterial);
            materials.Remove(outlineMaskMaterial);
            materials.Remove(outlineFillMaterial);

            switch (OutlineType)
            {
                case Type.Overlapping:
                    materials.Add(outlineOverlappingMaterial);
                    break;
                case Type.Original:
                    materials.Add(outlineMaskMaterial);
                    materials.Add(outlineFillMaterial);
                    break;
                case Type.Simple:
                    materials.Add(outlineSimpleMaterial);
                    break;
            }

            rend.materials = materials.ToArray();
        }
    }

    void RemoveMaterials()
    {
        foreach (var rend in renderers)
        {
            // Remove outline shaders
            var materials = rend.sharedMaterials.ToList();

            materials.Remove(outlineOverlappingMaterial);
            materials.Remove(outlineSimpleMaterial);
            materials.Remove(outlineMaskMaterial);
            materials.Remove(outlineFillMaterial);

            rend.materials = materials.ToArray();
        }
    }

    void UpdateMaterialProperties()
    {
        AddMaterials();

        switch (OutlineType)
        {
            case Type.Overlapping:
                outlineOverlappingMaterial.SetColor("_OutlineColor", outlineColor);
                switch (outlineMode)
                {
                    case Mode.OutlineAll:
                        outlineOverlappingMaterial.SetFloat("_ZTestMask", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineOverlappingMaterial.SetFloat("_ZTestFill", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineOverlappingMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineVisible:
                        outlineOverlappingMaterial.SetFloat("_ZTestMask", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineOverlappingMaterial.SetFloat("_ZTestFill", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineOverlappingMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineHidden:
                        outlineOverlappingMaterial.SetFloat("_ZTestMask", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineOverlappingMaterial.SetFloat("_ZTestFill", (float)UnityEngine.Rendering.CompareFunction.Greater);
                        outlineOverlappingMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineAndSilhouette:
                        outlineOverlappingMaterial.SetFloat("_ZTestMask", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineOverlappingMaterial.SetFloat("_ZTestFill", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineOverlappingMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.SilhouetteOnly:
                        outlineOverlappingMaterial.SetFloat("_ZTestMask", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineOverlappingMaterial.SetFloat("_ZTestFill", (float)UnityEngine.Rendering.CompareFunction.Greater);
                        outlineOverlappingMaterial.SetFloat("_OutlineWidth", 0);
                        break;
                }
                break;
            case Type.Original:
                outlineFillMaterial.SetColor("_OutlineColor", outlineColor);
                switch (outlineMode)
                {
                    case Mode.OutlineAll:
                        outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineVisible:
                        outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineHidden:
                        outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
                        outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineAndSilhouette:
                        outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineFillMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.SilhouetteOnly:
                        outlineMaskMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineFillMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
                        outlineFillMaterial.SetFloat("_OutlineWidth", 0);
                        break;
                }
                break;
            case Type.Simple:
                outlineSimpleMaterial.SetColor("_OutlineColor", outlineColor);
                switch (outlineMode)
                {
                    case Mode.OutlineAll:
                        outlineSimpleMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineSimpleMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineVisible:
                        outlineSimpleMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.LessEqual);
                        outlineSimpleMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineHidden:
                        outlineSimpleMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
                        outlineSimpleMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.OutlineAndSilhouette:
                        outlineSimpleMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Always);
                        outlineSimpleMaterial.SetFloat("_OutlineWidth", outlineWidth);
                        break;

                    case Mode.SilhouetteOnly:
                        outlineSimpleMaterial.SetFloat("_ZTest", (float)UnityEngine.Rendering.CompareFunction.Greater);
                        outlineSimpleMaterial.SetFloat("_OutlineWidth", 0);
                        break;
                }
                break;
        }

    }

    private IEnumerator FadeInCoroutine()
    {
        if (fadeOutCoroutine != null) StopCoroutine(fadeOutCoroutine);
        if (fadeInCoroutine == null)
        {
            OutlineColor = new Color(OutlineColor.r, OutlineColor.g, OutlineColor.b, 0);
            while (OutlineColor.a < 1)
            {
                yield return 0;
                OutlineColor = new Color(OutlineColor.r, OutlineColor.g, OutlineColor.b,
                    Mathf.Clamp01(OutlineColor.a + Time.deltaTime * OutlineFadeInSpeed));
            }
        }
    }

    private IEnumerator FadeOutAndDestroyCoroutine()
    {
        if(fadeInCoroutine != null) StopCoroutine(fadeInCoroutine);

        while (OutlineColor.a > 0)
        {
            yield return 0;
            OutlineColor = new Color(OutlineColor.r, OutlineColor.g, OutlineColor.b,
                Mathf.Clamp01(OutlineColor.a - Time.deltaTime * OutlineFadeOutSpeed));
        }
        Destroy(this);

        fadeOutCoroutine = null;
    }

    private IEnumerator FadeOutAndDisableCoroutine()
    {
        if(fadeInCoroutine != null) StopCoroutine(fadeInCoroutine);

        while (OutlineColor.a > 0)
        {
            yield return 0;
            OutlineColor = new Color(OutlineColor.r, OutlineColor.g, OutlineColor.b,
                Mathf.Clamp01(OutlineColor.a - Time.deltaTime * OutlineFadeOutSpeed));
        }

        this.enabled = false;

        fadeOutCoroutine = null;
    }
}
