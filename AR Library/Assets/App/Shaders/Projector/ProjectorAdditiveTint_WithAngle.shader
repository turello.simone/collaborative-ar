﻿// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'
// Upgrade NOTE: replaced '_ProjectorClip' with 'unity_ProjectorClip'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Projector/Additive AngleCutoff" {
	Properties{
		_Color("Tint Color", Color) = (1,1,1,1)
		_Attenuation("Falloff", Range(0.0, 1.0)) = 1.0
		_ShadowTex("Cookie", 2D) = "gray" {}
		_AngleLimit("Angle Limit (deg)", Float) = 0
	}
	Subshader{
		Tags {"Queue" = "Transparent"}
		Pass {
			ZWrite Off
			ColorMask RGB
			Blend SrcAlpha One // Additive blending
			Offset -1, -1

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f {
				float4 uvShadow : TEXCOORD0;
				float4 pos : SV_POSITION;
				half projAngle : TEXCOORD2;
			};

			float4x4 unity_Projector;
			float4x4 unity_ProjectorClip;
			half3 projNormal;

			inline half angleBetween(half3 vector1, half3 vector2)
			{
				return acos(dot(vector1, vector2) / (length(vector1) * length(vector2)));
			}

			v2f vert(float4 vertex : POSITION, float3 normal : NORMAL)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(vertex);
				o.uvShadow = mul(unity_Projector, vertex);
				projNormal = mul(unity_Projector, normal);
				o.projAngle = abs(angleBetween(half3(0, 0, -1), projNormal));
				return o;
			}

			sampler2D _ShadowTex;
			fixed4 _Color;
			float _Attenuation;
			half _AngleLimit;

			fixed4 frag(v2f i) : SV_Target
			{
				// Apply alpha mask
				fixed4 texCookie = tex2Dproj(_ShadowTex, UNITY_PROJ_COORD(i.uvShadow));
				fixed4 outColor = _Color * texCookie.a;
				// Attenuation
				float depth = i.uvShadow.z; // [-1 (near), 1 (far)]

				return outColor * clamp(1.0 - abs(depth) + _Attenuation, 0.0, 1.0) * (1.0 - step(radians(_AngleLimit), i.projAngle));
			}
			ENDCG
		}
	}
}