﻿Shader "Custom/Proximity Dissolve" 
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {} // Regular object texture 
		//_Position1("Position1", vector) = (0,0,0,0) // World space position of the spherical mask center
		//_Position2("Position2", vector) = (0,0,0,0) // World space position of the spherical mask center
		//_Position3("Position3", vector) = (0,0,0,0) // World space position of the spherical mask center
		//_SphereRadius("Radius", float) = 0.5 // Radius of the spherical mask
		_SphereSoftness("Softness", float) = 0.5 // Softness of the sphere edge
		_DissolveTex("Dissolve texture", 2D) = "white" {}
		//_DissolveThreshold("Dissolve threshold", float) = 1 //How much is dissolved
		_DissolveThreshold1("Dissolve threshold 1", float) = 0.3 
		_DissolveThreshold2("Dissolve threshold 2", float) = 0.6
		_DissolveThreshold3("Dissolve threshold 3", float) = 1
		_DissolveSoftness("Dissolve Softness", float) = 0.5
		_DissolveAnimTex("Dissolve Animated texture", 2D) = "white" {}
		_DissolveAnimThreshold("Dissolve Animated threshold", float) = 1 //How much is dissolved
		_DissolveAnimTransparency("Dissolve Animated transparency", float) = 0.5
		_DissolveAnimSoftness("Dissolve Animated Softness", float) = 0.5
		_ScrollSpeedU("Scroll U Speed",float) = 2
		_ScrollSpeedV("Scroll V Speed",float) = 0
	}

	SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 200

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"


			sampler2D _MainTex;
			sampler2D _DissolveTex;
			sampler2D _DissolveAnimTex;
			fixed4 _MainTex_ST;
			fixed4 _DissolveTex_ST;
			fixed4 _DissolveAnimTex_ST;
			float4 _Position1, _Position2, _Position3;
			half _Sphere1Radius, _Sphere2Radius, _Sphere3Radius, _SphereSoftness;
			half _DissolveThreshold, _DissolveThreshold1, _DissolveThreshold2, _DissolveThreshold3, _DissolveSoftness;
			half _DissolveAnimThreshold, _DissolveAnimTransparency, _DissolveAnimSoftness;
			fixed _ScrollSpeedU, _ScrollSpeedV;

			// Input to vertex shader
			struct VertIn {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			// Input to fragment shader
			struct VertOut {
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				float2 uv : TEXCOORD1;
				float2 uvDissolve : TEXCOORD2;
				float2 uvDissolveAnim : TEXCOORD3;
			};

			// VERTEX SHADER
			VertOut vert(VertIn v)
			{
				VertOut o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvDissolve = TRANSFORM_TEX(v.uv, _DissolveTex);
				o.uvDissolveAnim = TRANSFORM_TEX(v.uv, _DissolveAnimTex);
				//scroll uv
				o.uvDissolveAnim.x += _Time * _ScrollSpeedU;
				o.uvDissolveAnim.y += _Time * _ScrollSpeedV;
				return o;
			}

			// FRAGMENT SHADER
			float4 frag(VertOut i) : COLOR
			{
				// Get texture color
				fixed4 col = tex2D(_MainTex, i.uv);

				// Calculate distance from the spherical mask center
				half d1 = distance(i.worldPos, _Position1);
				half d2 = distance(i.worldPos, _Position2);
				half d3 = distance(i.worldPos, _Position3);

				half sphere1 = saturate((d1 - _Sphere1Radius) / -_SphereSoftness);
				half sphere2 = saturate((d2 - _Sphere2Radius) / -_SphereSoftness);
				half sphere3 = saturate((d3 - _Sphere3Radius) / -_SphereSoftness);
				//clip(sphere1);
				//clip(sphere2);
				//clip(sphere3);
				half spheres = max(max(sphere1, sphere2), sphere3);
				if (spheres <= 0) discard;

				_DissolveThreshold = _DissolveThreshold1;
				if (sphere1 > 0 && sphere2 > 0 && sphere3 > 0) {
					_DissolveThreshold = _DissolveThreshold3;
				}
				else if ((sphere1 > 0 && sphere2 > 0 && sphere3 <= 0) ||
					(sphere1 > 0 && sphere2 <= 0 && sphere3 > 0) ||
					(sphere1 <= 0 && sphere2 > 0 && sphere3 > 0)) {
					_DissolveThreshold = _DissolveThreshold2;
				}

				//Get dissolve textures
				half dissolve_value = tex2D(_DissolveTex, i.uvDissolve);
				half dissolve_anim_value = tex2D(_DissolveAnimTex, i.uvDissolveAnim);
				dissolve_value = saturate((dissolve_value - _DissolveThreshold) / -_DissolveSoftness);
				dissolve_anim_value = _DissolveAnimTransparency + saturate((dissolve_anim_value - _DissolveAnimThreshold) / -_DissolveAnimSoftness);
				//dissolve_value = saturate(-dissolve_value + _DissolveThreshold);
				//dissolve_value = saturate(dissolve_value);
				//clip(dissolve_value);
				dissolve_value = min(dissolve_value, dissolve_anim_value);

				col.a *= min(spheres, dissolve_value);
				return col;

			}

			ENDCG
		}
	}
}