﻿Shader "Custom/Three Light Reveal"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_BumpMap("Normal Map", 2D) = "bump" {}

		_DissolveTex("Dissolve texture", 2D) = "white" {}
		//_DissolveThreshold("Dissolve threshold", Range(0,1)) = 1
		_DissolveThreshold1("Dissolve threshold 1", float) = 0.3
		_DissolveThreshold2("Dissolve threshold 2", float) = 0.6
		_DissolveThreshold3("Dissolve threshold 3", float) = 1
		_DissolveSoftness("Dissolve Softness", float) = 0.5

		_DissolveAnimTex("Dissolve Animated texture", 2D) = "white" {}
		_DissolveAnimThreshold("Dissolve Animated threshold", float) = 1
		_DissolveAnimTransparency("Dissolve Animated transparency", float) = 0.5
		_DissolveAnimSoftness("Dissolve Animated Softness", float) = 0.5
		_ScrollSpeedU("Scroll U Speed",float) = 2
		_ScrollSpeedV("Scroll V Speed",float) = 0

		_Light1Direction("Light 1 Direction", Vector) = (0,-1,0,0)
		_Light1Position("Light 1 Position", Vector) = (0,1,0,0)
		_Light1Enabled("Light 1 Enabled", int) = 0

		_Light2Direction("Light 2 Direction", Vector) = (0,-1,0,0)
		_Light2Position("Light 2 Position", Vector) = (0,1,0,0)
		_Light2Enabled("Light 2 Enabled", int) = 0

		_Light3Direction("Light 3 Direction", Vector) = (0,-1,0,0)
		_Light3Position("Light 3 Position", Vector) = (0,1,0,0)
		_Light3Enabled("Light 3 Enabled", int) = 0

		_LightAngle("Light Angle", Range(0,180)) = 30
		_LightStrength("Light Strength", Float) = 2.5
		_LightRange("Light Range", Float) = 2
		
    }
    SubShader
    {
		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:fade

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex, _BumpMap, _DissolveTex, _DissolveAnimTex;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_BumpMap;
			float2 uv_DissolveTex;
			float2 uv_DissolveAnimTex;
			float3 worldPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		float _BumpScale;

		half _DissolveThreshold, _DissolveThreshold1, _DissolveThreshold2, _DissolveThreshold3, _DissolveSoftness;

		half _DissolveAnimThreshold, _DissolveAnimTransparency, _DissolveAnimSoftness;
		fixed _ScrollSpeedU, _ScrollSpeedV;

		float4 _Light1Position, _Light1Direction;
		float4 _Light2Position, _Light2Direction;
		float4 _Light3Position, _Light3Direction;
		int _Light1Enabled, _Light2Enabled, _Light3Enabled;

		float _LightAngle, _LightStrength, _LightRange;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
        // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

			float cosAngle = cos(radians(_LightAngle)*0.5);

			float reveal1;
			half d1 = distance(_Light1Position, IN.worldPos);
			if (_Light1Enabled == 0) reveal1 = 0;
			else if (d1 > _LightRange) reveal1 = 0;
			else {
				float3 direction1 = normalize(_Light1Position - IN.worldPos);
				reveal1 = dot(direction1, -_Light1Direction); //range [-1 ; 1]
				reveal1 = saturate(reveal1 - cosAngle); // => range [-1-cosAngle ; 1-cosAngle] => range [0 ; 1-cosAngle]
				reveal1 = saturate(reveal1 * (1 / (1 - cosAngle)) * _LightStrength); //rescale range to [0 ; 1] * _LightStrength => range to [0 ; 1]
			}

			float reveal2;
			half d2 = distance(_Light2Position, IN.worldPos);
			if (_Light2Enabled == 0) reveal2 = 0;
			else if (d2 > _LightRange) reveal2 = 0;
			else {
				float3 direction2 = normalize(_Light2Position - IN.worldPos);
				reveal2 = dot(direction2, -_Light2Direction); //range [-1 ; 1]
				reveal2 = saturate(reveal2 - cosAngle); // => range [-1-cosAngle ; 1-cosAngle] => range [0 ; 1-cosAngle]
				reveal2 = saturate(reveal2 * (1 / (1 - cosAngle)) * _LightStrength); //rescale range to [0 ; 1] * _LightStrength => range to [0 ; 1]
			}

			float reveal3;
			half d3 = distance(_Light3Position, IN.worldPos);
			if (_Light3Enabled == 0) reveal3 = 0;
			else if (d3 > _LightRange) reveal3 = 0;
			else {
				float3 direction3 = normalize(_Light3Position - IN.worldPos);
				reveal3 = dot(direction3, -_Light3Direction); //range [-1 ; 1]
				reveal3 = saturate(reveal3 - cosAngle); // => range [-1-cosAngle ; 1-cosAngle] => range [0 ; 1-cosAngle]
				reveal3 = saturate(reveal3 * (1 / (1 - cosAngle)) * _LightStrength); //rescale range to [0 ; 1] * _LightStrength => range to [0 ; 1]
			}

			half reveal = max(max(reveal1, reveal2), reveal3);
			clip(reveal);

			_DissolveThreshold = _DissolveThreshold1;
			if (reveal1 > 0 && reveal2 > 0 && reveal3 > 0) {
				_DissolveThreshold = _DissolveThreshold3;
			}
			else if ((reveal1 > 0 && reveal2 > 0 && reveal3 <= 0) ||
				(reveal1 > 0 && reveal2 <= 0 && reveal3 > 0) ||
				(reveal1 <= 0 && reveal2 > 0 && reveal3 > 0)) {
				_DissolveThreshold = _DissolveThreshold2;
			}

			half dissolve_value = tex2D(_DissolveTex, IN.uv_DissolveTex);
			dissolve_value = saturate((dissolve_value - _DissolveThreshold) / -_DissolveSoftness);

			//Dissolve animated texture calculations
			IN.uv_DissolveAnimTex.x += _Time * _ScrollSpeedU;
			IN.uv_DissolveAnimTex.y += _Time * _ScrollSpeedV;
			half dissolve_anim_value = tex2D(_DissolveAnimTex, IN.uv_DissolveAnimTex);
			dissolve_anim_value = _DissolveAnimTransparency + saturate((dissolve_anim_value - _DissolveAnimThreshold) / -_DissolveAnimSoftness);
			
			dissolve_value = min(dissolve_value, dissolve_anim_value);

			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = min(reveal, dissolve_value)  * c.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
        ENDCG
    }
    FallBack "Diffuse"
}
