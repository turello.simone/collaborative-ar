﻿Shader "Custom/Proximity" 
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {} // Regular object texture 
		_Position("Position", vector) = (0,0,0,0) // World space position of the spherical mask center
		_Radius("Radius", float) = 0.5 // Radius of the spherical mask
		_Softness("Softness", float) = 0.5 // Softness of the sphere edge
	}

	SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
		Pass 
		{
			Blend SrcAlpha OneMinusSrcAlpha
			LOD 200

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _MainTex;
			float4 _Position;
			half _Radius;
			half _Softness;

			// Input to vertex shader
			struct VertIn {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			// Input to fragment shader
			struct VertOut {
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD0;
				float2 uv : TEXCOORD1;
			};

			// VERTEX SHADER
			VertOut vert(VertIn v)
			{
				VertOut o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.uv = v.uv;
				return o;
			}

			// FRAGMENT SHADER
			float4 frag(VertOut i) : COLOR
			{
				// Get texture color
				fixed4 col = tex2D(_MainTex, i.uv);

				// Calculate distance from the spherical mask center
				half d = distance(i.worldPos, _Position);

				half sphere = saturate((d - _Radius) / -_Softness);
				clip(sphere);
				col.a *= sphere;
				return col;

			}

			ENDCG
		}
	}
}