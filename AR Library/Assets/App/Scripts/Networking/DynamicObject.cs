﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using Photon.Pun;
using UnityEditor;
using UnityEngine;
using Random = System.Random;

public abstract class DynamicObject : MarkerTransformSync, IPoolable
{
    public GameUtils.DynamicObjectState DynamicObjectState
    {
        get { return _dynamicObjectState; }
        protected set { _dynamicObjectState = value; }
    }

    /// <summary>
    /// Player who is helding this object, null is object is placed.
    /// </summary>
    public PlayerInfo Owner
    {
        get
        {
            if (DynamicObjectState == GameUtils.DynamicObjectState.Placed)
            {
                return null;

            }
            return _owner;
        }
        protected set
        {
            if (_owner == value) return;
            if (_owner == null)
            {
                if (value == _networkManager.LocalPlayer)
                {
                    _networkManager.LocalPlayer.ObjectHeld = this.gameObject;
                }
                else
                {
                    value.ObjectHeld = this.gameObject;
                }
            }
            else if (_owner == _networkManager.LocalPlayer)
            {
                if (value == null)
                {
                    _networkManager.LocalPlayer.ObjectHeld = null;
                }
            }
            else if (_owner != _networkManager.LocalPlayer)
            {
                if (value == null)
                {
                    _owner.ObjectHeld = null;
                }
            }
            _owner = value;
        }
    }

    [Header("Dynamic Object properties")]
    public float MinDistanceFromCamera = 0.1f;
    public float MaxDistanceFromCamera = 1.0f;

    private bool Enabled
    {
        get => _enabled;
        set => _enabled = value;
    }

    public string PlacedObjectAnchorId { get; private set; } //Anchor name which this object is anchored to, when it is placed

    [Tooltip("Relative (local) position of the center of encumbrance mesh")]
    public Vector3 RotationCenter = Vector3.zero;
    public Vector3 GrabStartLocalRotation = Vector3.zero;
    public float EncumbranceRadius = 0.1f;
    [ReadOnly] public bool IsFreezed = false;
    [ReadOnly] protected bool AnchorOnSpawn = true;

    protected Transform Dummy; //This gameobject will be used to place the DynamicObject onto local surfaces
    protected bool ChangeRotationOnGrab = true;
    private GameObject _objectHit; //last gameobject hit (child of a static object)
    private RaycastHit _staticObjHitInfo; //last hit on a static object
    public RaycastHit SurfaceHitInfo; //last hit on a detected surface
    private Vector3 _surfaceHitNormal; 
    private Matrix4x4 _worldToObjectSpaceMatrix; //Roto-translation matrix from world space to _objectHit space
    protected float _distanceFromCamera; //Distance from camera; the object can snap before this distance, but it will place itself at this distance if it can.
    private PlayerInfo _owner;
    [SerializeField, ReadOnly] private GameUtils.DynamicObjectState _dynamicObjectState;
    private Coroutine _anchorPromotionCoroutine;
    private bool _checkAnchorPromotion;
    private Vector3 _startPosition;
    private Quaternion _startRotation;
    private bool _wasSynchronizedOnNetworkSpawn = false;
    private float _xRotation = 0;
    private float _yRotation = 0;
    private float _zRotation = 0;
    private bool _enabled;
    private bool _firstTimeMine = true;

    protected override void Awake()
    {
        if (Dummy == null) Dummy = transform.Find("Dummy");
        if (Dummy == null)
        {
            Debug.LogError("Dummy was not found, set it in the editor or use the correct DynamicObject structure.");
        }

        base.Awake();

        Enabled = true;

        PlacedObjectAnchorId = this.name + "_" + this.gameObject.GetInstanceID();

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;
        _distanceFromCamera = (MaxDistanceFromCamera + MinDistanceFromCamera) / 2;

        //CustomOutline outline = this.gameObject.AddComponent<CustomOutline>();
        //Destroy(outline);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (Dummy != null)
        {
            Gizmos.DrawWireSphere(Dummy.TransformPoint(RotationCenter), EncumbranceRadius);
        }
        else
        {
            Gizmos.DrawWireSphere(transform.TransformPoint(RotationCenter), EncumbranceRadius);
        }
    }

    protected override void OnEnable() { }

    public void OnSpawn() { }
    public void OnDespawn() { }

    public virtual void OnNetworkSpawn()
    {
        _startPosition = transform.position;
        _startRotation = transform.rotation;

        //At the creation of a dynamic object, the object is necessarily put on a detected surface

        //If this object was created before marker sync (when player State is Synchronizing or Error)
        //[that is i'm connecting after initial preparation done by master client],
        //it may be necessary to update its position when marker anchor is created (when player State becomes Synchronized).
        //If, when State is Synchronized, this dynamic object is still Placed,
        //the anchor operation must be replicated to update position and change the anchor.

        if (_networkManager.LocalPlayer.State == GameUtils.PlayerState.Error ||
            _networkManager.LocalPlayer.State == GameUtils.PlayerState.Synchronizing)
        {
            _wasSynchronizedOnNetworkSpawn = false;
            _networkManager.LocalPlayer.PlayerStateChangedEvent += OnPlayerStateChanged;
        }
        else
        {
            _wasSynchronizedOnNetworkSpawn = true;

            //its position is correct, anchor immediately
            if (DynamicObjectState == GameUtils.DynamicObjectState.Placed && AnchorOnSpawn) Anchor();
        }
    }

    public virtual void Reset()
    {
        _photonView.RPC("ResetRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public virtual void ResetRPC()
    {
        Enable(true);

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;
        IsFreezed = false;

        _xRotation = 0;
        _yRotation = 0;
        _zRotation = 0;

        if (_wasSynchronizedOnNetworkSpawn)
        {
            this.transform.position = _startPosition;
            this.transform.rotation = _startRotation;
        }
        else
        {
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            this.transform.position = _worldToMarkerSpaceMatrix.MultiplyPoint3x4(_startPosition);
            this.transform.rotation = _anchorPose.rotation * _startRotation;
        }

        if (AnchorOnSpawn)
            Anchor();
    }

    public virtual void Enable(bool objEnabled)
    {
        if (Enabled != objEnabled)
        {
            Enabled = objEnabled;
            this.gameObject.RenderersEnabled(objEnabled);
            this.gameObject.CollidersEnabled(objEnabled);
        }
    }

    protected override void Update ()
    {
        if (_photonView == null || PhotonNetwork.IsConnectedAndReady == false ||
            DynamicObjectState == GameUtils.DynamicObjectState.Placed)
        {
            return;
        }

        _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
        _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

        if (Owner == _networkManager.LocalPlayer)
        {
            if (!_photonView.IsMine)
            {
                return;
            }

            if (IsFreezed) return;

            Ray screenRay = Camera.main.ScreenPointToRay(new Vector3((float)(Screen.width / 2.0), (float)(Screen.height / 2.0), 0));

            //Check collision with static objects and detected surfaces
            bool staticObjHit = Physics.SphereCast(screenRay, EncumbranceRadius, out _staticObjHitInfo, _distanceFromCamera,
                GameUtils.Mask.StaticObjects);
            bool surfaceHit = Physics.SphereCast(screenRay, EncumbranceRadius, out SurfaceHitInfo, _distanceFromCamera,
                GameUtils.Mask.DetectedSurfaces);

            //if both an object and a surface were hit, decide which is closer
            if (staticObjHit && surfaceHit)
            {
                if (_staticObjHitInfo.distance < SurfaceHitInfo.distance)
                {
                    surfaceHit = false;
                }
                else
                {
                    staticObjHit = false;
                }
            }

            if (staticObjHit)
            {
                DynamicObjectState = GameUtils.DynamicObjectState.HeldOnObject;
                _objectHit = _staticObjHitInfo.collider.gameObject;
                this.transform.position = screenRay.origin + (screenRay.direction * _staticObjHitInfo.distance);
            }
            else if (surfaceHit)
            {
                DynamicObjectState = GameUtils.DynamicObjectState.HeldOnSurface;
                this.transform.position = screenRay.origin + (screenRay.direction * SurfaceHitInfo.distance);
                this.transform.rotation = Quaternion.LookRotation(
                    Vector3.ProjectOnPlane(this.transform.forward, this.SurfaceHitInfo.normal), SurfaceHitInfo.normal);
            }
            else
            {
                //No collision found, object is held in air

                DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;
                //this.transform.position = Camera.main.transform.position + Camera.main.transform.forward * _distanceFromCamera;
                this.transform.position = Vector3.MoveTowards(transform.position,
                    Camera.main.transform.position + Camera.main.transform.forward * _distanceFromCamera,
                    Time.deltaTime);
            }

        }
        else
        {
            if (_photonView.IsMine)
            {
                return;
            }
            switch (DynamicObjectState)
            {
                case GameUtils.DynamicObjectState.HeldInAir:
                    base.Update();
                    Dummy.localPosition = -RotationCenter;
                    break;
                case GameUtils.DynamicObjectState.HeldOnSurface:
                    base.Update();
                    Dummy.localPosition = -RotationCenter;

                    Ray ray = new Ray(transform.position, _surfaceHitNormal);
                    Ray inverseRay = new Ray(transform.position, -_surfaceHitNormal);
                    RaycastHit hitInfo;
                    if (_arLibrary.Raycast(inverseRay, out hitInfo, EncumbranceRadius + 0.25f))
                    {
                        Dummy.position += hitInfo.point + hitInfo.normal * EncumbranceRadius - transform.position;
                    }
                    else if (_arLibrary.Raycast(ray, out hitInfo, EncumbranceRadius + 0.25f))
                    {
                        Dummy.position += hitInfo.point + hitInfo.normal * EncumbranceRadius - transform.position;
                    }
                    break;
                case GameUtils.DynamicObjectState.HeldOnObject:
                    if (_objectHit == null) return;
                    Dummy.localPosition = -RotationCenter;

                    _worldToObjectSpaceMatrix.SetTRS(_objectHit.transform.position, _objectHit.transform.rotation, Vector3.one);

                    this.UpdatePosition(_worldToObjectSpaceMatrix);
                    this.UpdateRotation(_objectHit.transform.rotation);
                    this.UpdateScale();
                    break;
            }
        }
    }

    public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(DynamicObjectState);

            if (DynamicObjectState == GameUtils.DynamicObjectState.HeldInAir)
            {
                base.OnPhotonSerializeView(stream, info);
            }
            else if (DynamicObjectState == GameUtils.DynamicObjectState.HeldOnSurface)
            {
                //Send surface hit normal
                stream.SendNext(SurfaceHitInfo.normal);

                base.OnPhotonSerializeView(stream, info);
            }
            else if (DynamicObjectState == GameUtils.DynamicObjectState.HeldOnObject)
            {
                _worldToObjectSpaceMatrix.SetTRS(_objectHit.transform.position, _objectHit.transform.rotation, Vector3.one);

                //Send _objectHit's viewID
                stream.SendNext(_objectHit.transform.GetComponent<PhotonView>().ViewID);
                //Synchronize position and rotation of the hitPoint relative to _objectHit position
                m_PositionControl.OnPhotonSerializeView(transform.position, _worldToObjectSpaceMatrix, stream, info);
                m_RotationControl.OnPhotonSerializeView(transform.rotation, _objectHit.transform.rotation, stream, info);
                m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

                ForceInitialData(stream, _worldToObjectSpaceMatrix, _objectHit.transform.rotation);
            }
            /*else if (DynamicObjectState == GameUtils.DynamicObjectState.Placed)
            {
                base.OnPhotonSerializeView(stream, info);
            }*/
        }
        else if(stream.IsReading)
        {
            DynamicObjectState = (GameUtils.DynamicObjectState)stream.ReceiveNext();

            if (DynamicObjectState == GameUtils.DynamicObjectState.HeldInAir)
            {
                base.OnPhotonSerializeView(stream, info);
            }
            else if (DynamicObjectState == GameUtils.DynamicObjectState.HeldOnSurface)
            {
                //Receive surface hit normal
                _surfaceHitNormal = (Vector3)stream.ReceiveNext();

                base.OnPhotonSerializeView(stream, info);
            }
            else if (DynamicObjectState == GameUtils.DynamicObjectState.HeldOnObject)
            {
                //Receive _objectHit's viewID
                int viewID = (int)stream.ReceiveNext();
                if (PhotonView.Find(viewID) == null)
                {
                    Debug.Log("DynamicObject: No objecthit found with specified viewID (" + viewID + ").");
                }
                else
                {
                    _objectHit = PhotonView.Find(viewID).gameObject;
                }

                _worldToObjectSpaceMatrix.SetTRS(_objectHit.transform.position, _objectHit.transform.rotation,
                    Vector3.one);

                m_PositionControl.OnPhotonSerializeView(transform.position, _worldToObjectSpaceMatrix, stream, info);
                m_RotationControl.OnPhotonSerializeView(transform.rotation, _objectHit.transform.rotation, stream, info);
                m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

                ForceInitialData(stream, _worldToObjectSpaceMatrix, _objectHit.transform.rotation);
            }
            /*else if (DynamicObjectState == GameUtils.DynamicObjectState.Placed)
            {
                base.OnPhotonSerializeView(stream, info);
            }*/
        }
    }

    public virtual void Place(Vector3 restPosition, Quaternion restRotation)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        Debug.Log("DynamicObjectTransformSync: " + PlacedObjectAnchorId + " placed.");

        if (Owner == _networkManager.LocalPlayer && IsFreezed)
            FocalPointManager.Instance.DestroyFreezedObjectFocalPoint();

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;

        this.transform.parent = null;
        this.transform.position = restPosition;
        this.transform.up = SurfaceHitInfo.normal;
        IsFreezed = false;

        this.transform.rotation = restRotation;

        Anchor();

        //Call RPC to place the object remotely.
        _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
        _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

        Debug.Log("PlaceDynamicObject: calling PlaceRPC.");
        _photonView.RPC("PlaceRPC", RpcTarget.OthersBuffered,
            _worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(transform.position),
            Quaternion.Inverse(_anchorPose.rotation) * transform.rotation);

    }

    /// <summary>
    /// Force place this object at its current position and rotation.
    /// Unlike Place, everyone can call this function, even if you are not the owner.
    /// </summary>
    public virtual void ForcePlace()
    {
        Debug.Log("DynamicObjectTransformSync: " + PlacedObjectAnchorId + " placed.");

        //Take ownership on the photonview
        if (!_photonView.IsMine)
        {
            _photonView.RequestOwnership();
        }

        if (Owner == _networkManager.LocalPlayer && IsFreezed)
            FocalPointManager.Instance.DestroyFreezedObjectFocalPoint();

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;

        this.transform.parent = null;

        Anchor();

        //Call RPC to place the object remotely.
        _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
        _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

        Debug.Log("PlaceDynamicObject: calling PlaceRPC.");
        _photonView.RPC("PlaceRPC", RpcTarget.OthersBuffered,
            _worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(transform.position), 
            Quaternion.Inverse(_anchorPose.rotation) * transform.rotation);
    }

    [PunRPC]
    public virtual void PlaceRPC(Vector3 restPosition, Quaternion restRotation)
    {
        Debug.Log("DynamicObject:" + PlacedObjectAnchorId + " PlaceRPC called.");

        if(Owner == _networkManager.LocalPlayer && IsFreezed)
            FocalPointManager.Instance.DestroyFreezedObjectFocalPoint();

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;
        this.transform.parent = null;
        IsFreezed = false;

        _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
        _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

        this.transform.position = _worldToMarkerSpaceMatrix.MultiplyPoint3x4(restPosition);
        this.transform.rotation = _anchorPose.rotation * restRotation;

        Anchor();
    }


    public virtual void Grab()
    {
        if (Owner != null)
        {
            //The object is already held by someone
            return;
        }

        //Call RPC to decide who will grab this object, position and rotation are useful to set the starting position for all the clients
        _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
        _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

        _distanceFromCamera = (MaxDistanceFromCamera + MinDistanceFromCamera) / 2;
        Vector3 startPosition = Camera.main.transform.position + Camera.main.transform.forward * _distanceFromCamera;
        //Quaternion startRotation = Quaternion.LookRotation(Camera.main.transform.up , - Camera.main.transform.forward);
        Quaternion startRotation = transform.rotation;
        if (ChangeRotationOnGrab)
            startRotation = Camera.main.transform.rotation * Quaternion.Euler(GrabStartLocalRotation);

        //The target of this RPC is AllBufferedViaServer, so that, in case of concurrency, everyone will receive the rpcs
        //in the same order. The first call will set the owner, the subsequent ones will be discarded.
        Debug.Log("GrabDynamicObject: calling GrabRPC.");
        _photonView.RPC("GrabRPC", RpcTarget.AllBufferedViaServer,
            _worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(startPosition),
            Quaternion.Inverse(_anchorPose.rotation) * startRotation);

    }

    [PunRPC]
    public virtual void GrabRPC(Vector3 startPosition, Quaternion startRotation, PhotonMessageInfo info)
    {
        Debug.Log("DynamicObject:" + PlacedObjectAnchorId + " GrabRPC called.");

        if (Owner != null)
        {
            //The object is already held by someone
            return;
        }

        _xRotation = 0;
        _yRotation = 0;
        _zRotation = 0;

        if (info.Sender == null)
        {
            //The Owner is not in the room anymore

            //Change State
            DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;

            //Destroy anchor which this object is attached to
            Unanchor();

            //Update object position
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            this.transform.position = _worldToMarkerSpaceMatrix.MultiplyPoint3x4(startPosition);
            this.transform.rotation = _anchorPose.rotation * startRotation;
            //Dummy.localPosition = -RotationCenter;
        }
        else if (_networkManager.LocalPlayer.ActorNumber == info.Sender.ActorNumber)
        {
            //I will be the owner

            //Take ownership on the photonview
            if (!_photonView.IsMine)
            {
                _photonView.RequestOwnership();
            }
            
            //if(!_photonView.IsMine) Debug.LogError("Photon view not mine after RequestOwnership");

            //Change State
            DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;

            //Destroy anchor which this object is attached to
            Unanchor();

            //Place the object and anchor it to the camera

            //this.transform.position = _worldToMarkerSpaceMatrix.MultiplyPoint3x4(startPosition);
            //this.transform.rotation = _anchorPose.rotation * startRotation;

            _distanceFromCamera = (MaxDistanceFromCamera + MinDistanceFromCamera) / 2;
            this.transform.position = Camera.main.transform.position +
                                      Camera.main.transform.forward * _distanceFromCamera;
            //this.transform.rotation = Quaternion.LookRotation(Camera.main.transform.up, -Camera.main.transform.forward);
            if (ChangeRotationOnGrab)
                this.transform.rotation = Camera.main.transform.rotation * Quaternion.Euler(GrabStartLocalRotation);

            this.transform.parent = Camera.main.transform;
            Dummy.localPosition = -RotationCenter;

            //Set Owner and ObjectHeld
            Owner = _networkManager.LocalPlayer;
        }
        else
        {
            //I'm not the owner

            //Change State
            DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;

            //Destroy anchor which this object is attached to
            Unanchor();

            //Update object position
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            //this.transform.position = _worldToMarkerSpaceMatrix.MultiplyPoint3x4(startPosition);
            //this.transform.rotation = _anchorPose.rotation * startRotation;
            //Dummy.localPosition = -RotationCenter;

            //Set Owner and ObjectHeld
            Owner = _networkManager.OtherPlayers[info.Sender.ActorNumber];
        }
    }

    public void Freeze()
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        IsFreezed = true;
        this.transform.parent = null;

    }

    public void Unfreeze()
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (IsFreezed == false)
        {
            //The object is not freezed
            return;
        }

        IsFreezed = false;

        this.transform.position = Camera.main.transform.position +
                                  Camera.main.transform.forward * _distanceFromCamera;
        this.transform.parent = Camera.main.transform;
        //Rotate(GameUtils.RotAxis.Right, 0);
        transform.localRotation = Quaternion.Euler(GrabStartLocalRotation);
    }

    public void Rotate(GameUtils.RotAxis axis, float angle)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (IsFreezed) return;

        Vector3 axisVector3 = new Vector3();
        Vector3 directionVector3 = new Vector3();
        switch (axis)
        {
            case GameUtils.RotAxis.Right:
                _xRotation += angle;
                _xRotation = Mathf.Clamp(_xRotation, -90, 90);
                axisVector3 = Camera.main.transform.right;
                directionVector3 = Camera.main.transform.up;
                break;
            case GameUtils.RotAxis.Up:
                _yRotation += angle;
                //_yRotation = Mathf.Clamp(_yRotation, -180, 180);
                axisVector3 = Camera.main.transform.up;
                directionVector3 = Camera.main.transform.right;
                break;
            case GameUtils.RotAxis.Forward:
                _zRotation += angle;
                axisVector3 = Camera.main.transform.forward;
                //directionVector3 = Camera.main.transform.right;
                break;
        }


        if (DynamicObjectState == GameUtils.DynamicObjectState.HeldOnSurface)
        {
            Debug.LogFormat("Rotate: rotaxis {0}, angle {1}, crossp {2}", axis.ToString(), (float) angle,
                Vector3.Cross(transform.up, directionVector3).sqrMagnitude);

            transform.Rotate(transform.up, angle * Vector3.Cross(transform.up, directionVector3).sqrMagnitude, Space.World);

            /*if (axis == GameUtils.RotAxis.Up)
                transform.Rotate(transform.up, angle * Vector3.Cross(transform.up, axisVector3).sqrMagnitude, Space.World);
            else if (axis == GameUtils.RotAxis.Right)
                transform.Rotate(transform.up, angle * -Vector3.Cross(transform.up, axisVector3).sqrMagnitude, Space.World);*/
        }
        else
        {
            this.transform.Rotate(axisVector3, angle, Space.World);
        }

        /*transform.localRotation = Quaternion.Euler(GrabStartLocalRotation);
        this.transform.Rotate(Camera.main.transform.up, _yRotation, Space.World);
        this.transform.Rotate(Camera.main.transform.right, _xRotation, Space.World);
        this.transform.Rotate(Camera.main.transform.forward, _zRotation, Space.World);*/
    }
    
    public void Rotate(Vector2 screenPos, Vector2 deltaMovement)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (IsFreezed) return;

        if (DynamicObjectState == GameUtils.DynamicObjectState.HeldOnSurface)
        {
            Vector2 rotCenterScreenPos = Camera.main.WorldToScreenPoint(transform.position);
            Vector2 startScreenPos = screenPos - deltaMovement;
            Vector2 destScreenPos = screenPos;

            Vector2 startVector = startScreenPos - rotCenterScreenPos;
            Vector2 destVector = destScreenPos - rotCenterScreenPos;

            float angle = Vector2.SignedAngle(startVector, destVector);
            //Debug.LogFormat("Rotate: rotCenterScreenPos {0}, startScreenPos {1}, destScreenPos {2}, angle {3}", rotCenterScreenPos, startScreenPos, destScreenPos, angle);
            
            transform.Rotate(transform.up, -2 * angle, Space.World);
        }
        else
        {
            this.transform.Rotate(Camera.main.transform.up, -deltaMovement.x * ObjectManipulation.Instance.DynamicObjectRotationSpeed, Space.World);
            this.transform.Rotate(Camera.main.transform.right, deltaMovement.y * ObjectManipulation.Instance.DynamicObjectRotationSpeed, Space.World);
        }
    }



    /*public void Rotate(Vector3 axis, float angle)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (IsFreezed) return;

        this.transform.Rotate(axis, angle, Space.World);
    }*/

    public void TranslateTowardsCameraForward(float translation)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (IsFreezed) return;

        //newPosition: new center position of the object
        Vector3 newPosition = this.transform.position + Camera.main.transform.forward * translation;
        //distanceVector: distance vector between camera and object new position
        Vector3 distanceVector = newPosition - Camera.main.transform.position;

        //Check if camera forward vector and distance vector have the same direction, that is the object new position is not behind the camera
        if (Math.Abs(Vector3.Dot(Camera.main.transform.forward, distanceVector.normalized) - 1) < 0.1f)
        {
            if (distanceVector.magnitude >= Math.Max(MinDistanceFromCamera, RotationCenter.magnitude) &&
                distanceVector.magnitude <= MaxDistanceFromCamera)
            {
                //this.transform.position = newPosition;
                _distanceFromCamera = distanceVector.magnitude;
            }
        }
    }

    public void Unanchor()
    {
        if (_anchorPromotionCoroutine != null) StopCoroutine(_anchorPromotionCoroutine);
        _arLibrary.SurfaceDetectedEvent -= SetAnchorPromotion;
        _arLibrary.SurfaceUpdatedEvent -= SetAnchorPromotion;
        _checkAnchorPromotion = false;

        _arLibrary.DestroyAnchor(PlacedObjectAnchorId);
    }

    /// <summary>
    /// Try to anchor this object to a near surface (placing the dummy directly on local surface), otherwise anchor to session.
    /// </summary>
    public void Anchor()
    {
        if (_networkManager.LocalPlayer.State == GameUtils.PlayerState.Error ||
            _networkManager.LocalPlayer.State == GameUtils.PlayerState.Synchronizing)
            return;

        _arLibrary.DestroyAnchor(PlacedObjectAnchorId);
        //Try to anchor this object to a surface, otherwise anchor to session
        Pose hitPose = new Pose();
        if (_arLibrary.CreateAnchorFromPose(PlacedObjectAnchorId, new Pose(transform.position, transform.rotation), out hitPose, 0.25f))
        {
            Dummy.position = hitPose.position;
            _arLibrary.AttachGameObjectToAnchor(PlacedObjectAnchorId, gameObject, true);
        }
        else
        {
            Dummy.localPosition = Vector3.zero;
            _arLibrary.CreateSessionAnchor(PlacedObjectAnchorId, new Pose(transform.position, transform.rotation));
            _arLibrary.AttachGameObjectToAnchor(PlacedObjectAnchorId, gameObject, true);

            //Promote this session anchor to a trackable one if necessary
            _arLibrary.SurfaceDetectedEvent += SetAnchorPromotion;
            _arLibrary.SurfaceUpdatedEvent += SetAnchorPromotion;
            if(_anchorPromotionCoroutine != null) StopCoroutine(_anchorPromotionCoroutine);
            _anchorPromotionCoroutine = StartCoroutine(PromoteAnchorCoroutine());
        }
    }

    private void OnPlayerStateChanged(PlayerInfo player, GameUtils.PlayerState newValue)
    {
        if (newValue >= GameUtils.PlayerState.Synchronized)
        {
            OnPlayerSynchronization();
            _networkManager.LocalPlayer.PlayerStateChangedEvent -= OnPlayerStateChanged;
        }
    }

    private void SetAnchorPromotion(GameObject surfaceGameObject)
    {
        //A new surface was found or an existing one was updated
        _checkAnchorPromotion = true;
    }

    /// <summary>
    /// This coroutine tries to promote current anchor if conditions are met (a new surface was found or an existing one was updated).
    /// This is done every 0.5 seconds.
    /// </summary>
    private IEnumerator PromoteAnchorCoroutine()
    {
        bool done = false;

        while (!done)
        {
            yield return new WaitForSeconds(0.5f);

            if (_checkAnchorPromotion)
            {
                _checkAnchorPromotion = false;

                if (_arLibrary.RaycastFromPose(new Pose(transform.position, transform.rotation), out _, 0.25f))
                {
                    _arLibrary.DestroyAnchor(PlacedObjectAnchorId);
                    _arLibrary.CreateAnchorFromPose(PlacedObjectAnchorId, new Pose(transform.position, transform.rotation),
                        out Pose hitPose, 0.25f);
                    Dummy.position = hitPose.position;
                    _arLibrary.AttachGameObjectToAnchor(PlacedObjectAnchorId, gameObject, true);

                    done = true;
                    _arLibrary.SurfaceDetectedEvent -= SetAnchorPromotion;
                    _arLibrary.SurfaceUpdatedEvent -= SetAnchorPromotion;
                }
            }
        }
    }

    protected virtual void OnPlayerSynchronization()
    {
        if (DynamicObjectState == GameUtils.DynamicObjectState.Placed)
        {
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            this.transform.position = _worldToMarkerSpaceMatrix.MultiplyPoint3x4(transform.position);
            this.transform.rotation = _anchorPose.rotation * transform.rotation;

            if(AnchorOnSpawn)
                Anchor();
        }
    }

    protected virtual void OnDestroy()
    {
        _arLibrary.SurfaceDetectedEvent -= SetAnchorPromotion;
        _arLibrary.SurfaceUpdatedEvent -= SetAnchorPromotion;
    }

    public virtual void TapOnInteractiveObject(GameObject interactiveObject) { }
    public virtual void HoldInteractiveObject(GameObject interactiveObject, bool isPressed) { }
    public virtual void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement) { }

    public virtual bool IsUseful(GameObject gameObj)
    {
        return true;
    }
}
