﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SyncButton
{
    private class SyncButtonHook : MonoBehaviour
    {
        public Action<PhotonMessageInfo> OnPushRPC;
        public Action<bool,PhotonMessageInfo> OnReleaseRPC;

        [PunRPC]
        private void PushRPC(PhotonMessageInfo info)
        {
            OnPushRPC(info);
        }

        [PunRPC]
        private void ReleaseRPC(bool force, PhotonMessageInfo info)
        {
            OnReleaseRPC(force, info);
        }
    }

    public delegate void PushEventHandler(int actorNumber);
    public event PushEventHandler Pushed;

    public delegate void ReleaseEventHandler(int actorNumber);
    public event ReleaseEventHandler Released;

    public GameObject GameObject;
    private PlayerInfo _owner;
    public bool IsPushed { get { return _owner != null; } }

    private PhotonView _photonView;

    public SyncButton(GameObject gameObject)
    {
        GameObject = gameObject;
        _photonView = gameObject.GetComponent<PhotonView>();

        _owner = null;

        SyncButtonHook syncButtonHook = _photonView.gameObject.AddComponent<SyncButtonHook>();
        syncButtonHook.OnPushRPC = PushRPC;
        syncButtonHook.OnReleaseRPC = ReleaseRPC;

    }

    public void Push()
    {
        if (_owner != null)
        {
            //the button is already pushed by someone
            return;
        }

        //Call RPC to decide who will push this button
        //The target of this RPC is AllBufferedViaServer, so that, in case of concurrency, everyone will receive the rpcs
        //in the same order. The first call will set the owner, the subsequent ones will be discarded.
        _photonView.RPC("PushRPC", RpcTarget.AllBufferedViaServer);
    }

    public void Release(bool force = false)
    {
        if (!force == false && _owner != PhotonNetworkManager.Instance.LocalPlayer)
        {
            return;
        }

        _photonView.RPC("ReleaseRPC", RpcTarget.AllBufferedViaServer, force);
    }

    private void PushRPC(PhotonMessageInfo info)
    {
        if (_owner != null)
        {
            //the button is already pushed by someone
            return;
        }


        if (PhotonNetworkManager.Instance.LocalPlayer.ActorNumber == info.Sender.ActorNumber)
        {
            _owner = PhotonNetworkManager.Instance.LocalPlayer;
        }
        else
        {
            _owner = PhotonNetworkManager.Instance.OtherPlayers[info.Sender.ActorNumber];
        }

        if (Pushed != null) Pushed(info.Sender.ActorNumber);
    }

    private void ReleaseRPC(bool force, PhotonMessageInfo info)
    {
        if (!force && (_owner == null || _owner.ActorNumber != info.Sender.ActorNumber))
        {
            return;
        }

        _owner = null;
        if (Released != null) Released(info.Sender.ActorNumber);
    }


}
