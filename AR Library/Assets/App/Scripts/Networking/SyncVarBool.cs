﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SyncVarBool
{
    private class SyncVarBoolHook : MonoBehaviour
    {
        public Action<bool, string> OnRPC;

        [PunRPC]
        private void SyncVarBoolRPC(bool value, string identifier)
        {
            OnRPC(value, identifier);
        }
    }

    public delegate void ValueChangedHandler(bool value);
    public event ValueChangedHandler ValueChanged;

    public bool Value
    {
        get { return _value; }
        set
        {
            _value = value;
            _photonView.RPC("SyncVarBoolRPC", RpcTarget.AllBuffered, value, _identifier);
            //if (ValueChanged != null) ValueChanged(value);
        }
    }

    private bool _value;
    private string _identifier;
    private PhotonView _photonView;

    public SyncVarBool(bool initialValue, PhotonView photonView, string identifier)
    {
        _value = initialValue;
        _photonView = photonView;
        _identifier = identifier;

        SyncVarBoolHook syncVarHook = _photonView.gameObject.AddComponent<SyncVarBoolHook>();
        syncVarHook.OnRPC = SyncVarBoolRPC;
    }

    private void SyncVarBoolRPC(bool value, string identifier)
    {
        if (identifier == _identifier)
        {
            _value = value;
            if (ValueChanged != null) ValueChanged(value);
        }
    }
}
