﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public abstract class EquippableDynamicObject : DynamicObject
{
    /// <summary>
    ///     Local pose of the equippable dynamic object (relative to the camera), when it is equipped.
    ///     It must be initialized in Awake.
    /// </summary>
    protected Pose? _equippedPose;

    #region Unity

    protected override void Update()
    {
        if (this._photonView == null || PhotonNetwork.IsConnectedAndReady == false)
        {
            return;
        }

        if (DynamicObjectState == GameUtils.DynamicObjectState.Equipped)
        {
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            if (!_photonView.IsMine)
            {
                this.UpdatePosition(_worldToMarkerSpaceMatrix);
                this.UpdateRotation(_anchorPose.rotation);
                this.UpdateScale();
            }
        }
        else
        {
            base.Update();
        }
    }

    #endregion

    #region public methods

    public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(DynamicObjectState);
        }
        else
        {
            DynamicObjectState = (GameUtils.DynamicObjectState)stream.ReceiveNext();
        }

        if (DynamicObjectState == GameUtils.DynamicObjectState.Equipped)
        {
            m_PositionControl.OnPhotonSerializeView(transform.position, _worldToMarkerSpaceMatrix, stream, info);
            m_RotationControl.OnPhotonSerializeView(transform.rotation, _anchorPose.rotation, stream, info);
            m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

            ForceInitialData(stream, _worldToMarkerSpaceMatrix, _anchorPose.rotation);
        }
        else
        {
            base.OnPhotonSerializeView(stream, info);
        }
    }

    public override void Place(Vector3 restPosition, Quaternion restRotation)
    {
        Unequip();
        base.Place(restPosition, restRotation);
    }
    
    public override void ForcePlace()
    {
        ForceUnequip();
        base.ForcePlace();
    }

    public virtual void Equip()
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (!_equippedPose.HasValue)
        {
            Debug.LogError("EquippableDynamicObject/Equip: " + PlacedObjectAnchorId + ", _equippedPose was not initialized.");
            return;
        }

        //Change State
        DynamicObjectState = GameUtils.DynamicObjectState.Equipped;

        //Update pose relative to parent (camera)
        transform.localPosition = _equippedPose.Value.position;
        transform.localRotation = _equippedPose.Value.rotation;
        Dummy.localPosition = Vector3.zero;

        Debug.Log("EquippableDynamicObject/Equip: " + PlacedObjectAnchorId + ", calling EquipRPC.");
        _photonView.RPC("EquipRPC", RpcTarget.OthersBuffered);
    }

    [PunRPC]
    public virtual void EquipRPC()
    {
        if (!_equippedPose.HasValue)
        {
            Debug.LogError("EquippableDynamicObject/EquipRPC: " + PlacedObjectAnchorId + ", _equippedPose was not initialized.");
            return;
        }

        Debug.Log("EquippableDynamicObject/EquipRPC: " + PlacedObjectAnchorId + ",  called.");

        //Change State
        DynamicObjectState = GameUtils.DynamicObjectState.Equipped;

        //Update pose relative to owner
        if (Owner != null)
        {
            transform.position = Owner.Player.transform.position + _equippedPose.Value.position;
            transform.rotation = Owner.Player.transform.rotation * _equippedPose.Value.rotation;
        }
        Dummy.localPosition = Vector3.zero;
    }

    public virtual void Unequip()
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        if (!_equippedPose.HasValue)
        {
            Debug.LogError("EquippableDynamicObject/Unequip: " + PlacedObjectAnchorId + ", _equippedPose was not initialized.");
            return;
        }

        //Change State
        DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;

        //Update position relative to parent (camera), rotation remains the same
        this.transform.position = Camera.main.transform.position +
                                  Camera.main.transform.forward * _distanceFromCamera;
        transform.localRotation = Quaternion.Euler(GrabStartLocalRotation);
        //Rotate(GameUtils.RotAxis.Right, 0);
        Dummy.localPosition = -RotationCenter;

        Debug.Log("EquippableDynamicObject/Unequip: " + PlacedObjectAnchorId + ", calling UnequipRPC.");
        _photonView.RPC("UnequipRPC", RpcTarget.OthersBuffered, _distanceFromCamera);
    }

    public virtual void ForceUnequip()
    {
        if (!_equippedPose.HasValue)
        {
            Debug.LogError("EquippableDynamicObject/ForceUnequip: " + PlacedObjectAnchorId + ", _equippedPose was not initialized.");
            return;
        }

        //Change State
        DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;

        if (Owner != null)
        {
            this.transform.position = Owner.Player.transform.position + Owner.Player.transform.forward * _distanceFromCamera;
        }
        Dummy.localPosition = -RotationCenter;

        Debug.Log("EquippableDynamicObject/ForceUnequip: " + PlacedObjectAnchorId + ", calling UnequipRPC.");
        _photonView.RPC("UnequipRPC", RpcTarget.OthersBuffered, _distanceFromCamera);
    }

    [PunRPC]
    public virtual void UnequipRPC(float distanceFromPlayer)
    {
        if (!_equippedPose.HasValue)
        {
            Debug.LogError("EquippableDynamicObject/UnequipRPC: " + PlacedObjectAnchorId + ", _equippedPose was not initialized.");
            return;
        }

        Debug.Log("EquippableDynamicObject/UnequipRPC: " + PlacedObjectAnchorId + ",  called.");

        //Change State
        DynamicObjectState = GameUtils.DynamicObjectState.HeldInAir;

        //Update position relative to owner, rotation remains the same
        if (Owner != null)
        {
            this.transform.position = Owner.Player.transform.position + Owner.Player.transform.forward * distanceFromPlayer;
        }
        Dummy.localPosition = -RotationCenter;
    }

    #endregion

}
