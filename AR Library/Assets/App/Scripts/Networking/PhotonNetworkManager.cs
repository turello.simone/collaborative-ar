﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ExitGames.Client.Photon;
using JetBrains.Annotations;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Object = UnityEngine.Object;

public class PhotonNetworkManager : MonoBehaviourPunCallbacks, INetworkManager
{
    public event GameUtils.Connected ConnectedEvent;
    public event GameUtils.Disconnected DisconnectedEvent;
    public event GameUtils.LobbyJoined LobbyJoinedEvent;
    public event GameUtils.LobbyLeft LobbyLeftEvent;
    public event GameUtils.RoomCreated RoomCreatedEvent;
    public event GameUtils.RoomCreatedFailed RoomCreatedFailedEvent;
    public event GameUtils.RoomJoined RoomJoinedEvent;
    public event GameUtils.RoomJoinedFailed RoomJoinedFailedEvent;
    public event GameUtils.RoomLeft RoomLeftEvent;
    public event GameUtils.RoomListUpdated RoomListUpdatedEvent;
    public event GameUtils.PlayerEnteredRoom PlayerEnteredRoomEvent;
    public event GameUtils.PlayerLeftRoom PlayerLeftRoomEvent;
    public event GameUtils.PlayerColorSet PlayerColorSetEvent;

    public event GameUtils.StaticObjectSpawned StaticObjectSpawnedEvent;
    public event GameUtils.DynamicObjectSpawned DynamicObjectSpawnedEvent;
    public event GameUtils.MeshReceived MeshReceivedEvent;

    private enum EventCode : byte
    {
        SpawnStaticObject,
        SpawnDynamicObject,
        SendMesh,
        CreatePlayerFocalPoint,
        CreatePlayerFocalPointTarget,
        DestroyPlayerFocalPoint,
        ForceRemotePlayerVisualization,
        ForceRemoteGazeVisualization,
        ForceRemoteObjectManipulationVisualization,
        ForcePlayerFocalPointEnabled
    }

    #region Public attributes


    public int SendRate
    {
        get
        {
            return PhotonNetwork.SendRate;
        }
        set
        {
            PhotonNetwork.SendRate = value;
            PhotonNetwork.SerializationRate = value;
        }
    }
    public string AnchorId { get; set; }
    public bool IsMasterClient
    {
        get { return PhotonNetwork.IsMasterClient; }
    }
    public bool IsConnected
    {
        get { return PhotonNetwork.IsConnected; }
    }
    public bool InLobby
    {
        get { return PhotonNetwork.InLobby; }
    }
    public bool InRoom
    {
        get { return PhotonNetwork.InRoom; }
    }
    public string NetworkClientState
    {
        get { return PhotonNetwork.NetworkClientState.ToString(); }
    }
    public Dictionary<string, RoomInfo> RoomList;
    public CurrentRoomInfo CurrentRoom
    {
        get
        {
            if (!InRoom)
            {
                return null;
            }
            return _currentRoom;
        }
    }
    public LocalPlayerInfo LocalPlayer { get; set; }

    /// <summary>
    /// List of other players in the current room (Dictionary linking ActorNumber and PlayerInfo).
    /// </summary>
    public Dictionary<int, PlayerInfo> OtherPlayers
    {
        get
        {
            if (_otherPlayers != null && _currentRoom == null) 
            {
                //_currentRoom is null when LocalPlayer, OtherPlayers and CurrentRoom are not initialized yet
                //(OnJoinedRoom was not called yet)

                //In this case initialize OtherPlayers in order to let Photon process correctly cached events
                foreach (var player in PhotonNetwork.PlayerListOthers)
                {
                    PlayerInfo playerInfo;
                    if (!_otherPlayers.TryGetValue(player.ActorNumber, out playerInfo))
                    {
                        _otherPlayers.Add(player.ActorNumber, new PlayerInfo(player.NickName, player.ActorNumber));
                    }
                }
            }

            return _otherPlayers;
        }

        set
        {
            _otherPlayers = value;
        }
    }

    public int NumberOfPlayers
    {
        get
        {
            int nPlayers = 0;
            if (InRoom)
            {
                nPlayers = 1 + OtherPlayers.Count;
            }
            return nPlayers;
        }
    }

    public int NumberOfPlayersReady
    {
        get
        {
            int playersReady = 0;
            if (InRoom)
            {
                if (LocalPlayer != null && LocalPlayer.IsReady) playersReady++;
                foreach (var player in OtherPlayers.Values)
                {
                    if (player.IsReady) playersReady++;
                }
            }
            return playersReady;
        }
    }

    [Tooltip("The prefab to use for representing the player")]
    public GameObject playerPrefab;
    public GameUtils.PlayerVisualization LocalPlayerVisualizationSetting
    {
        get { return _localPlayerVisualizationSetting; }
        set
        {
            _localPlayerVisualizationSetting = value;
            if (LocalPlayer.PlayerManager != null)
                LocalPlayer.PlayerManager.PlayerVisualizationSetting = value;

            GameUtils.Mute(GameUIManager.Instance.LocalPlayerDropdown.onValueChanged);
            GameUIManager.Instance.LocalPlayerDropdown.value =
                GameUIManager.Instance.PlayerVisualizationToInt(value);
            GameUtils.Unmute(GameUIManager.Instance.LocalPlayerDropdown.onValueChanged);
        }
    }
    [SerializeField] private GameUtils.PlayerVisualization _localPlayerVisualizationSetting;
    public GameUtils.PlayerVisualization RemotePlayerVisualizationSetting
    {
        get { return _remotePlayerVisualizationSetting; }
        set
        {
            _remotePlayerVisualizationSetting = value;
            foreach (PlayerInfo player in OtherPlayers.Values)
            {
                if (player.PlayerManager != null)
                    player.PlayerManager.PlayerVisualizationSetting = value;
            }

            GameUtils.Mute(GameUIManager.Instance.RemotePlayerDropdown.onValueChanged);
            GameUIManager.Instance.RemotePlayerDropdown.value =
                GameUIManager.Instance.PlayerVisualizationToInt(value);
            GameUtils.Unmute(GameUIManager.Instance.RemotePlayerDropdown.onValueChanged);
        }
    }
    [SerializeField] private GameUtils.PlayerVisualization _remotePlayerVisualizationSetting;
    public GameUtils.AvatarType AvatarType
    {
        get { return _avatarType; }
        set
        {
            _avatarType = value;
            foreach (PlayerInfo player in OtherPlayers.Values)
            {
                if (player.PlayerManager != null)
                    player.PlayerManager.AvatarType = value;
            }
        }
    }
    [SerializeField] private GameUtils.AvatarType _avatarType;

    [Tooltip("The prefab to use for representing the player gaze")]
    public GameObject gazePrefab;
    public GameUtils.GazeVisualization LocalGazeVisualizationSetting
    {
        get { return _localGazeVisualizationSetting; }
        set
        {
            _localGazeVisualizationSetting = value;
            if (LocalPlayer.Gaze != null)
                LocalPlayer.GazeManager.GazeVisualizationSetting = value;

            GameUtils.Mute(GameUIManager.Instance.LocalGazeDropdown.onValueChanged);
            GameUIManager.Instance.LocalGazeDropdown.value =
                GameUIManager.Instance.GazeVisualizationToInt(value);
            GameUtils.Unmute(GameUIManager.Instance.LocalGazeDropdown.onValueChanged);
        }
    }
    [SerializeField] private GameUtils.GazeVisualization _localGazeVisualizationSetting;
    public GameUtils.GazeVisualization RemoteGazeVisualizationSetting
    {
        get { return _remoteGazeVisualizationSetting; }
        set
        {
            _remoteGazeVisualizationSetting = value;
            foreach (PlayerInfo player in OtherPlayers.Values)
            {
                if (player.Gaze != null)
                    player.GazeManager.GazeVisualizationSetting = value;
            }

            GameUtils.Mute(GameUIManager.Instance.RemoteGazeDropdown.onValueChanged);
            GameUIManager.Instance.RemoteGazeDropdown.value =
                GameUIManager.Instance.GazeVisualizationToInt(value);
            GameUtils.Unmute(GameUIManager.Instance.RemoteGazeDropdown.onValueChanged);
        }
    }
    [SerializeField] private GameUtils.GazeVisualization _remoteGazeVisualizationSetting;

    public bool PlayerFocalPointEnabled
    {
        get { return _playerFocalPointEnabled; }
        set
        {
            _playerFocalPointEnabled = value;

            GameUtils.Mute(GameUIManager.Instance.PlayerFocalPointEnabledToggle.onValueChanged);
            GameUIManager.Instance.PlayerFocalPointEnabledToggle.isOn = value;
            GameUtils.Unmute(GameUIManager.Instance.PlayerFocalPointEnabledToggle.onValueChanged);
        }
    }
    [SerializeField] private bool _playerFocalPointEnabled = true;

    public Color Player1Color;
    public Color Player2Color;
    public Color Player3Color;

    #endregion


    #region Private attributes

    private Dictionary<int, PlayerInfo> _otherPlayers;
    private CurrentRoomInfo _currentRoom;
    private readonly byte MeshRegistrationTypeCode = 255;

    #endregion


    #region Singleton

    private static PhotonNetworkManager _instance = null;
    private static readonly object Padlock = new object();
    public static PhotonNetworkManager Instance
    {
        get
        {
            lock (Padlock)
            {
                if (_instance == null)
                {
                    GameObject obj = Instantiate(Resources.Load("PhotonNetworkManager")) as GameObject;
                    _instance = obj.GetComponent<PhotonNetworkManager>();
                }

                return _instance;
            }
        }
    }

    #endregion


    #region Unity CallBacks

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        //SendRate = 20;
        LocalPlayer = new LocalPlayerInfo();

        LocalPlayer.ColorSetEvent += (player, value) =>
        {
            if (PlayerColorSetEvent != null) PlayerColorSetEvent(LocalPlayer);
        };


        DontDestroyOnLoad(this.gameObject);

        AnchorId = null;
        OtherPlayers = new Dictionary<int, PlayerInfo>();
        RoomList = new Dictionary<string, RoomInfo>();
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

        #if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
        }
        #endif

    }

    #endregion


    #region Public Methods

    /// <summary>
    /// Connect to server (async). Will raise: ConnectedEvent
    /// </summary>
    public void Connect(string playerName)
    {
        LocalPlayer.Name = playerName;
        PhotonNetwork.ConnectUsingSettings();
    }

    /// <summary>
    /// Try to create a room. Will raise: (RoomCreatedEvent and RoomJoinedEvent) or RoomCreatedFailedEvent
    /// </summary>
    public void CreateRoom(string roomName, string password)
    {
        RoomOptions options = new RoomOptions { MaxPlayers = 3, CleanupCacheOnLeave = false };
        PhotonNetwork.CreateRoom(roomName + GetHashString(password), options, null);
    }

    /// <summary>
    /// Join a room by name and password (must be in lobby first, will automatically leave lobby on join). Will raise: RoomJoinedEvent or RoomJoinedFailedEvent
    /// </summary>
    public void JoinRoom(string roomName, string password)
    {
        if (!InLobby)
        {
            string error = Lean.Localization.LeanLocalization.GetTranslationText("MainMenu/JoinedFailedNoLobby");
            if (RoomJoinedFailedEvent != null) RoomJoinedFailedEvent(error);
        }
        else if (!RoomList.ContainsKey(roomName))
        {
            string error = Lean.Localization.LeanLocalization.GetTranslationText("MainMenu/JoinedFailedNoRoom");
            if (RoomJoinedFailedEvent != null) RoomJoinedFailedEvent(error);
        }
        else if (!GetHashString(password).Equals(RoomList[roomName].PasswordHash))
        {
            string error = Lean.Localization.LeanLocalization.GetTranslationText("MainMenu/JoinedFailedWrongPassword");
            if (RoomJoinedFailedEvent != null) RoomJoinedFailedEvent(error);
        }
        else
        {
            Loader.Instance.StartLoadingBar();
            PhotonNetwork.JoinRoom(roomName + RoomList[roomName].PasswordHash);
        }
    }

    /// <summary>
    /// Leave the current room. Will raise: RoomLeftEvent
    /// </summary>
    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    /// <summary>
    /// Join Lobby in order to be able to receive room list (async). Will raise: LobbyJoinedEvent
    /// </summary>
    public void JoinLobby()
    {
        PhotonNetwork.JoinLobby();
    }

    /// <summary>
    /// Leave lobby to stop getting updates  about available rooms. Will raise: LobbyLeftEvent
    /// </summary>
    public void LeaveLobby()
    {
        PhotonNetwork.LeaveLobby();
    }

    public void StartMultiplayer()
    {
        Debug.Log("StartMultiplayer");

        GameUIManager.Instance.PlayerColorBorderEnabled = true;

        if (playerPrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in Prefab 'PhotonNetworkManager'", this);
        }
        else if (gazePrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> gazePrefab Reference. Please set it up in Prefab 'PhotonNetworkManager'", this);
        }
        else
        {
            Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            LocalPlayer.Player = PhotonNetwork.Instantiate(playerPrefab.name, Vector3.zero, Quaternion.identity, 0);
            LocalPlayer.Gaze = PhotonNetwork.Instantiate(gazePrefab.name, Vector3.zero, Quaternion.identity);
        }

        LocalPlayer.State = GameUtils.PlayerState.InGame;
    }

    /// <summary>
    /// Spawn a static object over the network relative to the anchor selected, synchronizing every PhotonView in its children.
    /// </summary>
    /// <param name="prefabName"></param>
    /// <param name="objectPose"></param>
    public void SpawnStaticObject(string prefabName, Pose objectPose)
    {
        Pose anchorPose = new Pose();
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

        //Debug.Log("SpawnStaticObject: \nworldToMarkerSpaceMatrix\n" + worldToMarkerSpaceMatrix.ToString() + "\n anchorPose " + anchorPose.ToString() + "\nobjectPose " + objectPose.ToString());
        //GameObject staticObject = Lean.Pool.LeanPool.Spawn(Resources.Load(prefabName) as GameObject, objectPose.position, objectPose.rotation);
        //staticObject.name = prefabName;
        //PhotonView photonView = gameObject.GetComponent<PhotonView>();
        //StaticObjects.Add(photonView.ViewID, gameObject);

        GameObject staticObject = Resources.Load(prefabName) as GameObject;

        PhotonView[] photonViews = staticObject.GetComponentsInChildren<PhotonView>();
        int[] viewIDs = new int[photonViews.Length];
        for (int i = 0; i < photonViews.Length; i++)
        {
            if (PhotonNetwork.AllocateViewID(photonViews[i]))
            {
                viewIDs[i] = photonViews[i].ViewID;
            }
            else
            {
                Debug.Log("SpawnStaticObject: failed to allocate viewID", staticObject);
                return;
            }
        }

        //staticObject.GetComponent<StaticObject>().OnNetworkSpawn();
        //if (StaticObjectSpawnedEvent != null) StaticObjectSpawnedEvent(staticObject);


        object[] data = new object[]
        {
            prefabName,
            worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(objectPose.position),
            Quaternion.LookRotation(worldToMarkerSpaceMatrix.inverse.MultiplyVector(objectPose.forward), worldToMarkerSpaceMatrix.inverse.MultiplyVector(objectPose.up)),
            viewIDs
        };

        Debug.Log("SpawnStaticObject sent data: " + (string)data[0] + ", " + (Vector3)data[1] + ", " + (Quaternion)data[2] + ", " + (int[])data[3]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte) EventCode.SpawnStaticObject, data, raiseEventOptions, sendOptions);
    }

    /// <summary>
    /// Spawn a dynamic object over the network relative to the anchor selected, synchronizing every PhotonView in its children.
    /// </summary>
    /// <param name="prefabName"></param>
    /// <param name="objectPose"></param>
    public void SpawnDynamicObject(string prefabName, Pose objectPose)
    {
        Pose anchorPose = new Pose();
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

        //GameObject dynamicObject = Lean.Pool.LeanPool.Spawn(Resources.Load(prefabName) as GameObject, objectPose.position, objectPose.rotation);
        //dynamicObject.name = prefabName;

        GameObject dynamicObject = Resources.Load(prefabName) as GameObject;

        PhotonView[] photonViews = dynamicObject.GetComponentsInChildren<PhotonView>();
        int[] viewIDs = new int[photonViews.Length];
        for (int i = 0; i < photonViews.Length; i++)
        {
            if (PhotonNetwork.AllocateViewID(photonViews[i]))
            {
                viewIDs[i] = photonViews[i].ViewID;
            }
            else
            {
                Debug.Log("SpawnDynamicObject: failed to allocate viewID", dynamicObject);
                return;
            }
        }

        //dynamicObject.GetComponent<DynamicObject>().OnNetworkSpawn();
        //if (DynamicObjectSpawnedEvent != null) DynamicObjectSpawnedEvent(dynamicObject);


        object[] data = new object[]
        {
            prefabName,
            worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(objectPose.position),
            Quaternion.LookRotation(worldToMarkerSpaceMatrix.inverse.MultiplyVector(objectPose.forward), worldToMarkerSpaceMatrix.inverse.MultiplyVector(objectPose.up)),
            viewIDs
        };

        Debug.Log("SpawnDynamicObject sent data: " + (string)data[0] + ", " + (Vector3)data[1] + ", " + (Quaternion)data[2] + ", " + (int[])data[3]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte) EventCode.SpawnDynamicObject, data, raiseEventOptions, sendOptions);
    }
    
    public void SendMesh(string meshObjectName, Mesh mesh, Transform meshTransform)
    {
        Pose anchorPose = new Pose();
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);
        object[] data = new object[]
        {
            meshObjectName,
            mesh,
            worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(meshTransform.position),
            Quaternion.LookRotation(worldToMarkerSpaceMatrix.inverse.MultiplyVector(meshTransform.forward), worldToMarkerSpaceMatrix.inverse.MultiplyVector(meshTransform.up)),
            meshTransform.localScale,

        };

        Debug.Log("SendMesh sent data: " + (string)data[0] + ", " + (Mesh)data[1] + ", " + (Vector3)data[2] + ", " + (Quaternion)data[3] + ", " + (Vector3)data[4]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.Others,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte)EventCode.SendMesh, data, raiseEventOptions, sendOptions);
    }

    public void GrabDynamicObject(GameObject dynamicObject)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("GrabDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }
        DynamicObject transformSync;
        if ((transformSync = dynamicObject.GetComponent<DynamicObject>()).Owner == null)
        {
            Debug.Log("PhotonNetworkManager/GrabDynamicObject: called.");

            //Grab the object
            transformSync.Grab();
        }

    }

    public void PlaceDynamicObject(GameObject dynamicObject, Vector3 restPosition, Quaternion restRotation)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("PlaceDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }

        DynamicObject transformSync;
        if ((transformSync = dynamicObject.GetComponent<DynamicObject>()).Owner == LocalPlayer)
        {
            Debug.Log("PhotonNetworkManager/PlaceDynamicObject: called.");
            //Place the object
            transformSync.Place(restPosition, restRotation);
        }
    }

    public void FreezeDynamicObject(GameObject dynamicObject)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("FreezeDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }

        DynamicObject transformSync = dynamicObject.GetComponent<DynamicObject>();
        if (transformSync == null)
        {
            Debug.LogError("FreezeDynamicObject failed: " + dynamicObject.name + " has no DynamicObject Component.");
            return;
        }

        if (transformSync.Owner == LocalPlayer)
        {
            Debug.Log("PhotonNetworkManager/FreezeDynamicObject: called.");
            //Freeze the object
            transformSync.Freeze();
            FocalPointManager.Instance.CreateFreezedObjectFocalPoint(dynamicObject);
        }
    }

    public void UnfreezeDynamicObject(GameObject dynamicObject)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("UnfreezeDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }

        DynamicObject transformSync = dynamicObject.GetComponent<DynamicObject>();
        if (transformSync == null)
        {
            Debug.LogError("UnfreezeDynamicObject failed: " + dynamicObject.name + " has no DynamicObject Component.");
            return;
        }

        if (transformSync.Owner == LocalPlayer)
        {
            Debug.Log("PhotonNetworkManager/UnfreezeDynamicObject: called.");
            //Unfreeze the object
            transformSync.Unfreeze();
            FocalPointManager.Instance.DestroyFreezedObjectFocalPoint();
        }
    }

    public void RotateDynamicObject(GameObject dynamicObject, GameUtils.RotAxis axis, float angle)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("RotateDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }

        DynamicObject transformSync;
        if ((transformSync = dynamicObject.GetComponent<DynamicObject>()).Owner == LocalPlayer)
        {
            //Rotate the object
            transformSync.Rotate(axis, angle);
        }

    }
    
    public void RotateDynamicObject(GameObject dynamicObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("RotateDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }

        DynamicObject transformSync;
        if ((transformSync = dynamicObject.GetComponent<DynamicObject>()).Owner == LocalPlayer)
        {
            //Rotate the object
            transformSync.Rotate(screenPos, deltaMovement);
        }

    }

    public void TranslateDynamicObjectTowardsCameraForward(GameObject dynamicObject, float translation)
    {
        //Check if dynamicObject has a PhotonView
        if (dynamicObject.GetComponent<PhotonView>() == null)
        {
            Debug.Log("RotateDynamicObject failed: " + dynamicObject.name + " has no PhotonView Component.");
            return;
        }

        DynamicObject transformSync;
        if ((transformSync = dynamicObject.GetComponent<DynamicObject>()).Owner == LocalPlayer)
        {
            //Rotate the object
            transformSync.TranslateTowardsCameraForward(translation);
        }

    }

    public bool TapOnInteractiveObject(GameObject interactiveObject)
    {
        Debug.Log("PhotonNetworkManager/TapOnInteractiveObject: " + interactiveObject.name + " tapped.");
        if (!interactiveObject.CompareTag("Interactive"))
        {
            return false;
        }

        if (interactiveObject.layer == GameUtils.Layer.StaticObjects)
        {
            GameObject obj;
            if ((obj = GameUtils.GetObjectRoot(interactiveObject)) == null)
            {
                return false;
            }
            obj.GetComponent<StaticObject>().TapOnInteractiveObject(interactiveObject);
            return true;
        }
        else if (interactiveObject.layer == GameUtils.Layer.DynamicObjects)
        {
            GameObject obj;
            if ((obj = GameUtils.GetObjectRoot(interactiveObject)) == null)
            {
                return false;
            }
            obj.GetComponent<DynamicObject>().TapOnInteractiveObject(interactiveObject);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (!interactiveObject.CompareTag("Interactive"))
        {
            return false;
        }

        if(isPressed) Vibration.VibrateAndroid(200);

        if (interactiveObject.layer == GameUtils.Layer.StaticObjects)
        {
            GameObject obj;
            if ((obj = GameUtils.GetObjectRoot(interactiveObject)) == null)
            {
                return false;
            }
            obj.GetComponent<StaticObject>().HoldInteractiveObject(interactiveObject, isPressed);
            return true;
        }
        else if (interactiveObject.layer == GameUtils.Layer.DynamicObjects)
        {
            GameObject obj;
            if ((obj = GameUtils.GetObjectRoot(interactiveObject)) == null)
            {
                return false;
            }
            obj.GetComponent<DynamicObject>().HoldInteractiveObject(interactiveObject, isPressed);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        if (!interactiveObject.CompareTag("Interactive"))
        {
            return false;
        }

        if (interactiveObject.layer == GameUtils.Layer.StaticObjects)
        {
            GameObject obj;
            if ((obj = GameUtils.GetObjectRoot(interactiveObject)) == null)
            {
                return false;
            }
            obj.GetComponent<StaticObject>().SwipeOnInteractiveObject(interactiveObject, screenPos, deltaMovement);
            return true;
        }
        else if (interactiveObject.layer == GameUtils.Layer.DynamicObjects)
        {
            GameObject obj;
            if ((obj = GameUtils.GetObjectRoot(interactiveObject)) == null)
            {
                return false;
            }
            obj.GetComponent<DynamicObject>().SwipeOnInteractiveObject(interactiveObject, screenPos, deltaMovement);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void CreatePlayerFocalPoint(Vector3 position, Vector3 normal, [CanBeNull] GameObject target = null)
    {
        if (!PlayerFocalPointEnabled) return;

        if (CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Translation &&
            (target == CurrentRoom.TranslationSceneContraption.Quadrant1.ButtonDown ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant1.ButtonUp ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant2.ButtonDown ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant2.ButtonUp ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant3.ButtonDown ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant3.ButtonUp ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant4.ButtonDown ||
            target == CurrentRoom.TranslationSceneContraption.Quadrant4.ButtonUp))
            return;

        Pose anchorPose = new Pose();
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

        Pose pose = new Pose();
        pose.position = position;
        pose.rotation = Quaternion.FromToRotation(Vector3.up, normal);

        if (target == null)
        {
            object[] data = new object[]
            {
                worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(pose.position),
                Quaternion.Inverse(anchorPose.rotation) * pose.rotation
            };

            Debug.Log("PhotonNetworkManager/CreatePlayerFocalPoint: sent data: " + (Vector3)data[0] + "\n" + position);

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.DoNotCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = true
            };

            PhotonNetwork.RaiseEvent((byte)EventCode.CreatePlayerFocalPoint, data, raiseEventOptions, sendOptions);

            OnlineLogger.Instance.SendGameEvent(CurrentRoom, LocalPlayer, OnlineLogger.EventType.PlayerFocalPointCreated);
        }
        else
        {
            //target is not null
            PhotonView targetPhotonView = target.GetComponent<PhotonView>();
            if (targetPhotonView == null)
            {
                Debug.LogError("PhotonNetworkManager/CreatePlayerFocalPoint: Target must have a PhotonView.");
                return;
            }

            Matrix4x4 worldToObjectSpaceMatrix = new Matrix4x4();
            worldToObjectSpaceMatrix.SetTRS(target.transform.position, target.transform.rotation, Vector3.one);


            object[] data = new object[]
            {
                worldToObjectSpaceMatrix.inverse.MultiplyPoint3x4(pose.position),
                targetPhotonView.ViewID,
                Quaternion.Inverse(target.transform.rotation) * pose.rotation
            };

            Debug.Log("PhotonNetworkManager/CreatePlayerFocalPoint: sent data: " + (Vector3)data[0] + ", " + (int)data[1]);

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.DoNotCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = true
            };

            PhotonNetwork.RaiseEvent((byte)EventCode.CreatePlayerFocalPointTarget, data, raiseEventOptions, sendOptions);

            OnlineLogger.Instance.SendGameEvent(CurrentRoom, LocalPlayer,
                OnlineLogger.EventType.PlayerFocalPointCreated, GameUtils.GetObjectRoot(target), target.IsUseful());
        }
    }

    public void DestroyPlayerFocalPoint()
    {
        object[] data = new object[] { };

        Debug.Log("PhotonNetworkManager/DestroyPlayerFocalPoint");

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte)EventCode.DestroyPlayerFocalPoint, data, raiseEventOptions, sendOptions);

        OnlineLogger.Instance.SendGameEvent(CurrentRoom, LocalPlayer, OnlineLogger.EventType.PlayerFocalPointDestroyed);
    }

    public Color GetPlayerColor(int playerIndex)
    {
        switch (playerIndex)
        {
            case 0: return Color.black;
            case 1: return Player1Color;
            case 2: return Player2Color;
            case 3: return Player3Color;
            default: return Color.black;
        }
    }

    public PlayerInfo GetPlayerInfoFromPlayerIndex(int playerIndex)
    {
        if (LocalPlayer.PlayerIndex == playerIndex) return LocalPlayer;
        foreach (var player in OtherPlayers.Values)
        {
            if (player.PlayerIndex == playerIndex) return player;
        }

        return null;
    }

    public PlayerInfo GetPlayerInfoFromActorNumber(int actorNumber)
    {
        PlayerInfo player = null;
        if (LocalPlayer.ActorNumber == actorNumber) player = LocalPlayer;
        else
            OtherPlayers.TryGetValue(actorNumber, out player);

        return player;
    }

    public void ForcePlaceAllObjects()
    {
        if(LocalPlayer.ObjectHeld != null) LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().ForcePlace();
        foreach (var player in OtherPlayers.Values)
        {
            if (player.ObjectHeld != null) player.ObjectHeld.GetComponent<DynamicObject>().ForcePlace();
        }
    }

    public PlayerInfo GetMasterClient()
    {
        if (IsMasterClient) return LocalPlayer;
        return OtherPlayers.Values.FirstOrDefault(player => player.IsMasterClient);
    }

    public void ResetPlayersReady()
    {
        if (CurrentRoom != null)
        {
            if(LocalPlayer != null) LocalPlayer.IsReady = false;

            foreach(PlayerInfo player in OtherPlayers.Values)
            {
                player.IsReady = false;
            }
        }
    }

    public void ForceRemotePlayerVisualization(GameUtils.PlayerVisualization visualization)
    {
        object[] data = new object[]
        {
            GameUIManager.Instance.PlayerVisualizationToInt(visualization)
        };

        Debug.Log("PhotonNetworkManager/ForceRemotePlayerVisualization: sent data: " + (int) data[0]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte)EventCode.ForceRemotePlayerVisualization, data, raiseEventOptions, sendOptions);
    }
    
    public void ForceRemoteGazeVisualization(GameUtils.GazeVisualization visualization)
    {
        object[] data = new object[]
        {
            GameUIManager.Instance.GazeVisualizationToInt(visualization)
        };

        Debug.Log("PhotonNetworkManager/ForceRemoteGazeVisualization: sent data: " + (int)data[0]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte)EventCode.ForceRemoteGazeVisualization, data, raiseEventOptions, sendOptions);
    }
    
    public void ForceRemoteObjectManipulationVisualization(GameUtils.ObjectManipulationVisualization visualization)
    {
        object[] data = new object[]
        {
            GameUIManager.Instance.ObjManipToInt(visualization)
        };

        Debug.Log("PhotonNetworkManager/ForceRemoteObjectManipulationVisualization: sent data: " + (int)data[0]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte)EventCode.ForceRemoteObjectManipulationVisualization, data, raiseEventOptions, sendOptions);
    }
    
    public void ForcePlayerFocalPointEnabled(bool focalPointEnabled)
    {
        object[] data = new object[]
        {
            focalPointEnabled
        };

        Debug.Log("PhotonNetworkManager/ForcePlayerFocalPointEnabled: sent data: " + (bool)data[0]);

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.DoNotCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = true
        };

        PhotonNetwork.RaiseEvent((byte)EventCode.ForcePlayerFocalPointEnabled, data, raiseEventOptions, sendOptions);
    }

    //public void 

    #endregion


    #region Private Methods

    private void OnEvent(EventData photonEvent)
    {
        //Debug.Log("Event received: " + photonEvent.ToStringFull());

        //Stop processing Rpcs until this onEvent is finished
        if (photonEvent.Code == (byte) EventCode.SpawnStaticObject ||
            photonEvent.Code == (byte) EventCode.SpawnDynamicObject ||
            photonEvent.Code == (byte) EventCode.SendMesh ||
            photonEvent.Code == (byte) EventCode.CreatePlayerFocalPoint ||
            photonEvent.Code == (byte) EventCode.CreatePlayerFocalPointTarget ||
            photonEvent.Code == (byte) EventCode.DestroyPlayerFocalPoint ||
            photonEvent.Code == (byte) EventCode.ForceRemotePlayerVisualization ||
            photonEvent.Code == (byte) EventCode.ForceRemoteGazeVisualization ||
            photonEvent.Code == (byte) EventCode.ForceRemoteObjectManipulationVisualization ||
            photonEvent.Code == (byte) EventCode.ForcePlayerFocalPointEnabled)
            PhotonNetwork.IsMessageQueueRunning = false;

        if (photonEvent.Code == (byte) EventCode.SpawnStaticObject)
        {
            Pose anchorPose = new Pose();
            Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
            ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
            worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

            object[] data = (object[])photonEvent.CustomData;
            Debug.Log("SpawnStaticObject received data: " + (string)data[0] + ", " + (Vector3)data[1] + ", " + (Quaternion)data[2] + ", " + (int[])data[3]);

            GameObject staticObject = Lean.Pool.LeanPool.Spawn(Resources.Load((string) data[0]) as GameObject,
                worldToMarkerSpaceMatrix.MultiplyPoint3x4((Vector3) data[1]),
                anchorPose.rotation * (Quaternion) data[2]);
            staticObject.name = (string)data[0];

            PhotonView[] photonViews = staticObject.GetComponentsInChildren<PhotonView>();
            int[] viewIDs = (int[])data[3];
            for (int i = 0; i < photonViews.Length; i++)
            {
                photonViews[i].ViewID = viewIDs[i];
            }
            //StaticObjects.Add(photonView.ViewID, gameObject);

            staticObject.GetComponent<StaticObject>().OnNetworkSpawn();
            if (StaticObjectSpawnedEvent != null) StaticObjectSpawnedEvent(staticObject);
        }
        else if (photonEvent.Code == (byte)EventCode.SpawnDynamicObject)
        {
            Pose anchorPose = new Pose();
            Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
            ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
            worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

            object[] data = (object[])photonEvent.CustomData;
            Debug.Log("SpawnDynamicObject received data: " + (string)data[0] + ", " + (Vector3)data[1] + ", " + (Quaternion)data[2] + ", " + (int[])data[3]);

            GameObject dynamicObject = Lean.Pool.LeanPool.Spawn(Resources.Load((string) data[0]) as GameObject,
                worldToMarkerSpaceMatrix.MultiplyPoint3x4((Vector3) data[1]),
                anchorPose.rotation * (Quaternion) data[2]);
            dynamicObject.name = (string)data[0];

            PhotonView[] photonViews = dynamicObject.GetComponentsInChildren<PhotonView>();
            int[] viewIDs = (int[])data[3];
            for (int i = 0; i < photonViews.Length; i++)
            {
                photonViews[i].ViewID = viewIDs[i];
            }

            dynamicObject.GetComponent<DynamicObject>().OnNetworkSpawn();
            if (DynamicObjectSpawnedEvent != null) DynamicObjectSpawnedEvent(dynamicObject);
        }
        else if (photonEvent.Code == (byte)EventCode.SendMesh)
        {
            Pose anchorPose = new Pose();
            Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
            ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
            worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

            object[] data = (object[])photonEvent.CustomData;

            Debug.Log("SendMesh received data: " + (string)data[0] + ", " + (Mesh)data[1] + ", " + (Vector3)data[2] + ", " + (Quaternion)data[3] + ", " + (Vector3)data[4]);

            if (MeshReceivedEvent != null) MeshReceivedEvent((string)data[0], (Mesh)data[1], worldToMarkerSpaceMatrix.MultiplyPoint3x4((Vector3)data[2]), anchorPose.rotation * (Quaternion)data[3], (Vector3)data[4]);
        }
        else if (photonEvent.Code == (byte) EventCode.CreatePlayerFocalPoint)
        {
            if (!PlayerFocalPointEnabled) return;

            Pose anchorPose = new Pose();
            Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
            ARManager.Instance.ARLibrary.GetAnchorPose(AnchorId, out anchorPose);
            worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);

            object[] data = (object[])photonEvent.CustomData;

            Debug.Log("PhotonNetworkManager/CreatePlayerFocalPoint: received data: " + (Vector3)data[0] + "\n" + worldToMarkerSpaceMatrix.MultiplyPoint3x4((Vector3)data[0]));

            PlayerInfo player;
            if (LocalPlayer.ActorNumber == photonEvent.Sender)
            {
                FocalPointManager.Instance.CreatePlayerFocalPoint(LocalPlayer,
                    worldToMarkerSpaceMatrix.MultiplyPoint3x4((Vector3) data[0]),
                    anchorPose.rotation * (Quaternion) data[1]);
            }
            else if(OtherPlayers.TryGetValue(photonEvent.Sender, out player))
            {
                FocalPointManager.Instance.CreatePlayerFocalPoint(player,
                    worldToMarkerSpaceMatrix.MultiplyPoint3x4((Vector3) data[0]),
                    anchorPose.rotation * (Quaternion) data[1]);
            }
            AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.BoopPlayerFocalPoint);
        }
        else if (photonEvent.Code == (byte) EventCode.CreatePlayerFocalPointTarget)
        {
            object[] data = (object[])photonEvent.CustomData;

            GameObject target = PhotonView.Find((int)data[1]).gameObject;

            Matrix4x4 worldToObjectSpaceMatrix = new Matrix4x4();
            worldToObjectSpaceMatrix.SetTRS(target.transform.position, target.transform.rotation, Vector3.one);

            Debug.Log("PhotonNetworkManager/CreatePlayerFocalPoint: received data: " + (Vector3) data[0] + ", " + (int) data[1]);

            PlayerInfo player;
            if (LocalPlayer.ActorNumber == photonEvent.Sender)
            {
                FocalPointManager.Instance.CreatePlayerFocalPoint(LocalPlayer,
                    worldToObjectSpaceMatrix.MultiplyPoint3x4((Vector3) data[0]),
                    target.transform.rotation * (Quaternion) data[2], target);
            }
            else if (OtherPlayers.TryGetValue(photonEvent.Sender, out player))
            {
                FocalPointManager.Instance.CreatePlayerFocalPoint(player,
                    worldToObjectSpaceMatrix.MultiplyPoint3x4((Vector3) data[0]),
                    target.transform.rotation * (Quaternion) data[2], target);
            }
            AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.BoopPlayerFocalPoint);
        }
        else if (photonEvent.Code == (byte) EventCode.DestroyPlayerFocalPoint)
        {
            Debug.Log("PhotonNetworkManager/DestroyPlayerFocalPoint");

            PlayerInfo player;
            if (LocalPlayer.ActorNumber == photonEvent.Sender)
                FocalPointManager.Instance.DestroyPlayerFocalPoint(LocalPlayer);
            else if(OtherPlayers.TryGetValue(photonEvent.Sender, out player))
                FocalPointManager.Instance.DestroyPlayerFocalPoint(player);

            AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.LowBoopPlayerFocalPoint);
        }
        else if (photonEvent.Code == (byte) EventCode.ForceRemotePlayerVisualization)
        {
            object[] data = (object[]) photonEvent.CustomData;
            GameUtils.PlayerVisualization
                visualization = GameUIManager.Instance.IntToPlayerVisualization((int) data[0]);
            RemotePlayerVisualizationSetting = visualization;
        }
        else if (photonEvent.Code == (byte) EventCode.ForceRemoteGazeVisualization)
        {
            object[] data = (object[]) photonEvent.CustomData;
            GameUtils.GazeVisualization visualization = GameUIManager.Instance.IntToGazeVisualization((int) data[0]);
            RemoteGazeVisualizationSetting = visualization;
        }
        else if (photonEvent.Code == (byte) EventCode.ForceRemoteObjectManipulationVisualization)
        {
            object[] data = (object[]) photonEvent.CustomData;
            GameUtils.ObjectManipulationVisualization visualization =
                GameUIManager.Instance.IntToObjManip((int) data[0]);
            ObjectManipulation.Instance.RemoteVisualization = visualization;
        }
        else if (photonEvent.Code == (byte) EventCode.ForcePlayerFocalPointEnabled)
        {
            object[] data = (object[]) photonEvent.CustomData;
            PlayerFocalPointEnabled = (bool) data[0];
        }

        if (photonEvent.Code == (byte) EventCode.SpawnStaticObject ||
            photonEvent.Code == (byte) EventCode.SpawnDynamicObject ||
            photonEvent.Code == (byte) EventCode.SendMesh ||
            photonEvent.Code == (byte) EventCode.CreatePlayerFocalPoint ||
            photonEvent.Code == (byte) EventCode.CreatePlayerFocalPointTarget ||
            photonEvent.Code == (byte) EventCode.DestroyPlayerFocalPoint ||
            photonEvent.Code == (byte) EventCode.ForceRemotePlayerVisualization ||
            photonEvent.Code == (byte) EventCode.ForceRemoteGazeVisualization ||
            photonEvent.Code == (byte) EventCode.ForceRemoteObjectManipulationVisualization ||
            photonEvent.Code == (byte) EventCode.ForcePlayerFocalPointEnabled)
            PhotonNetwork.IsMessageQueueRunning = true;
    }

    private string GetHashString(string inputString)
    {
        SHA256 algorithm = SHA256.Create();
        byte[] hashValue = algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        return BitConverter.ToString(hashValue).Replace("-", String.Empty);
    }

    #endregion


    #region Photon Callbacks


    public override void OnConnectedToMaster()
    {
        PhotonPeer.RegisterType(typeof(Mesh), MeshRegistrationTypeCode, MeshSerializer.WriteMesh, MeshSerializer.ReadMesh);
        PhotonPeer.RegisterType(typeof(SubSceneManager.RoomScene), SubSceneManager.RoomScene.RegistrationTypeCode,
            SubSceneManager.RoomScene.Serialize, SubSceneManager.RoomScene.Deserialize);

        if (ConnectedEvent != null) ConnectedEvent();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        if(RoomList != null) RoomList.Clear();

        if(LocalPlayer != null) LocalPlayer.Dispose();
        LocalPlayer = new LocalPlayerInfo();

        if (OtherPlayers != null)
        {
            foreach (var player in OtherPlayers.Values)
            {
                player.Dispose();
            }
            OtherPlayers.Clear();
        }

        if (_currentRoom != null)
        {
            _currentRoom.Dispose();
            _currentRoom = null;
        }

        Loader.Instance.LoadScene(Loader.Scene.MainMenu);

        if (DisconnectedEvent != null) DisconnectedEvent(cause.ToString());
    }


    public override void OnJoinedLobby()
    {
        if (LobbyJoinedEvent != null) LobbyJoinedEvent();
    }

    public override void OnLeftLobby()
    {
        RoomList.Clear();

        if (LobbyLeftEvent != null) LobbyLeftEvent();
    }

    public override void OnCreatedRoom()
    {
        Loader.Instance.LoadScene(Loader.Scene.ARScene);
        if (RoomCreatedEvent != null) RoomCreatedEvent();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnCreateRoomFailed " + returnCode + ": " + message);
        if (RoomCreatedFailedEvent != null) RoomCreatedFailedEvent(message);
    }

    public override void OnJoinedRoom()
    {
        //Update _currentRoom
        _currentRoom = new CurrentRoomInfo(
            PhotonNetwork.CurrentRoom.Name.Remove(PhotonNetwork.CurrentRoom.Name.Length - 64),
            PhotonNetwork.CurrentRoom.Name.Substring(PhotonNetwork.CurrentRoom.Name.Length - 64),
            PhotonNetwork.CurrentRoom.PlayerCount, PhotonNetwork.CurrentRoom.MaxPlayers);

        //Update LocalPlayer and OtherPlayers
        LocalPlayer.ActorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
        LocalPlayer.State = GameUtils.PlayerState.Synchronizing;
        LocalPlayer.AspectRatio = Camera.main.aspect;
        LocalPlayer.FieldOfView = Camera.main.fieldOfView;

        PlayerInfo p;
        foreach (var player in PhotonNetwork.PlayerListOthers)
        {
            if (!OtherPlayers.TryGetValue(player.ActorNumber, out p))
            {
                OtherPlayers.Add(player.ActorNumber, new PlayerInfo(player.NickName, player.ActorNumber));
            }
        }

        if (RoomJoinedEvent != null) RoomJoinedEvent();
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRoomFailed " + returnCode + ": " + message);
        if (RoomJoinedFailedEvent != null) RoomJoinedFailedEvent(message);
    }

    public override void OnLeftRoom()
    {
        //Update LocalPlayer and OtherPlayers
        string nickname = LocalPlayer.Name;
        LocalPlayer.Dispose();
        LocalPlayer = new LocalPlayerInfo(nickname);

        foreach (var player in OtherPlayers.Values)
        {
            player.Dispose();
        }
        OtherPlayers.Clear();

        //Update _currentRoom
        if (_currentRoom != null)
        {
            _currentRoom.Dispose();
            _currentRoom = null;
        }


        if (RoomLeftEvent != null) RoomLeftEvent();
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        PlayerInfo player;
        player = new PlayerInfo(other.NickName, other.ActorNumber);
        OtherPlayers.Add(other.ActorNumber, player);
        Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting
        _currentRoom.PlayerCount += 1;

        player.ColorSetEvent += (playerTmp, value) =>
        {
            if (PlayerColorSetEvent != null) PlayerColorSetEvent(player);
        };

        if (PlayerEnteredRoomEvent != null) PlayerEnteredRoomEvent(player);
    }

    public override void OnPlayerLeftRoom(Player other)
    {
        PlayerInfo player = OtherPlayers[other.ActorNumber];
        if (IsMasterClient)
        {
            if (player.Player != null) PhotonNetwork.Destroy(player.Player);
            if (player.Gaze != null) PhotonNetwork.Destroy(player.Gaze);
            if (player.ObjectHeld != null) player.ObjectHeld.GetComponent<DynamicObject>().ForcePlace();
        }
        player.Dispose();
        OtherPlayers.Remove(other.ActorNumber);
        Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects
        _currentRoom.PlayerCount -= 1;
        if (PlayerLeftRoomEvent != null) PlayerLeftRoomEvent(player);

    }

    public override void OnRoomListUpdate(List<Photon.Realtime.RoomInfo> roomInfos)
    {
        foreach (var info in roomInfos)
        {
            string infoName = info.Name.Remove(info.Name.Length - 64);
            string infoPasswordHash = info.Name.Substring(info.Name.Length - 64);

            // Remove room from room list if it got closed, became invisible or was marked as removed
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (RoomList.ContainsKey(infoName))
                {
                    RoomList.Remove(infoName);
                }
                continue;
            }

            // Update room info
            if (RoomList.ContainsKey(infoName))
            {
                RoomInfo roomInfo = RoomList[infoName];
                roomInfo.PlayerCount = info.PlayerCount;
                roomInfo.MaxPlayers = info.MaxPlayers;
            }
            // Add new room info to list
            else
            {
                RoomInfo roomInfo = new RoomInfo(infoName, infoPasswordHash, info.PlayerCount, info.MaxPlayers);
                RoomList.Add(roomInfo.Name, roomInfo);
            }
        }

        if (RoomListUpdatedEvent != null) RoomListUpdatedEvent(RoomList);
    }


    #endregion


}
