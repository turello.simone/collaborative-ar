﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using Photon.Pun;
using UnityEngine;

/// <summary>
/// Static Object class. It automatically anchors the static object and updates its position when necessary.
/// </summary>
public abstract class StaticObject : MonoBehaviour, IPoolable
{
    public string MarkerAnchorId { get; set; }
    /// <summary>
    /// Id of the anchor this object is attached to.
    /// </summary>
    public string AnchorId { get; private set; }

    public bool Enabled
    {
        get => _enabled;
        private set => _enabled = value;
    }

    protected ARLibrary _arLibrary;
    protected PhotonNetworkManager _networkManager;
    protected PhotonView _photonView;
    private bool _checkAnchorPromotion;
    private bool _enabled;

    protected virtual void Awake()
    {
        _networkManager = PhotonNetworkManager.Instance;
        _arLibrary = ARManager.Instance.ARLibrary;
        AnchorId = this.name + "_" + this.gameObject.GetInstanceID();
        _photonView = GetComponent<PhotonView>();

        Enabled = true;

        //CustomOutline outline = this.gameObject.AddComponent<CustomOutline>();
        //Destroy(outline);
    }

    private void OnPlayerStateChanged(PlayerInfo player, GameUtils.PlayerState newValue)
    {
        if (newValue >= GameUtils.PlayerState.Synchronized)
        {
            Pose anchorPose;
            _arLibrary.GetAnchorPose(MarkerAnchorId, out anchorPose);
            Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
            worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);
            transform.position = worldToMarkerSpaceMatrix.MultiplyPoint3x4(transform.position);
            transform.rotation = anchorPose.rotation * transform.rotation;

            Anchor();

            _networkManager.LocalPlayer.PlayerStateChangedEvent -= OnPlayerStateChanged;
        }
    }

    /// <summary>
    /// Try to anchor this object to a near surface, otherwise anchor to session.
    /// It destroys the anchor before creating a new one, if an anchor with the same name exists.
    /// </summary>
    public void Anchor()
    {
        if (_networkManager.LocalPlayer.State == GameUtils.PlayerState.Error ||
            _networkManager.LocalPlayer.State == GameUtils.PlayerState.Synchronizing)
            return;

        _arLibrary.DestroyAnchor(AnchorId);
        //Try to anchor this object to a surface, otherwise anchor to session
        Pose hitPose = new Pose();
        if (_arLibrary.CreateAnchorFromPose(AnchorId, new Pose(transform.position, transform.rotation), out hitPose, 0.25f))
        {
            transform.position = hitPose.position;
            transform.rotation = hitPose.rotation;
            _arLibrary.AttachGameObjectToAnchor(AnchorId, gameObject, true);
        }
        else
        {
            _arLibrary.CreateSessionAnchor(AnchorId, new Pose(transform.position, transform.rotation));
            _arLibrary.AttachGameObjectToAnchor(AnchorId, gameObject, true);

            //Promote this session anchor to a trackable one if necessary
            _arLibrary.SurfaceDetectedEvent += SetAnchorPromotion;
            _arLibrary.SurfaceUpdatedEvent += SetAnchorPromotion;
            StartCoroutine(PromoteAnchorCoroutine());
        }
    }

    private void SetAnchorPromotion(GameObject surfaceGameObject)
    {
        //A new surface was found or an existing one was updated
        _checkAnchorPromotion = true;
    }

    /// <summary>
    /// This coroutine tries to promote current anchor if conditions are met (a new surface was found or an existing one was updated).
    /// This is done every 0.5 seconds.
    /// </summary>
    private IEnumerator PromoteAnchorCoroutine()
    {
        bool done = false;

        while (!done)
        {
            yield return new WaitForSeconds(0.5f);

            if (_checkAnchorPromotion)
            {
                _checkAnchorPromotion = false;

                if (_arLibrary.RaycastFromPose(new Pose(transform.position, transform.rotation), out _, 0.25f))
                {

                    _arLibrary.DestroyAnchor(AnchorId);
                    _arLibrary.CreateAnchorFromPose(AnchorId, new Pose(transform.position, transform.rotation),
                        out Pose hitPose, 0.25f);
                    transform.position = hitPose.position;
                    transform.rotation = hitPose.rotation;
                    _arLibrary.AttachGameObjectToAnchor(AnchorId, gameObject, true);

                    done = true;
                    _arLibrary.SurfaceDetectedEvent -= SetAnchorPromotion;
                    _arLibrary.SurfaceUpdatedEvent -= SetAnchorPromotion;
                }
            }
        }
    }

    protected virtual void OnDestroy()
    {
        _arLibrary.SurfaceDetectedEvent -= SetAnchorPromotion;
        _arLibrary.SurfaceUpdatedEvent -= SetAnchorPromotion;
    }

    public virtual void TapOnInteractiveObject(GameObject interactiveObject) { }
    public virtual void HoldInteractiveObject(GameObject interactiveObject, bool isPressed) { }
    public virtual void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement) { }

    public void OnSpawn() { }
    public void OnDespawn() { }

    public virtual void OnNetworkSpawn()
    {
        MarkerAnchorId = _networkManager.AnchorId;

        //If this object was created before marker sync (when player State is Synchronizing or Error)
        //[that is i'm connecting after initial preparation done by master client],
        //its position must be updated when marker anchor is created (when player State becomes Synchronized)
        //and it must be anchored
        if ((_networkManager.LocalPlayer.State == GameUtils.PlayerState.Error ||
             _networkManager.LocalPlayer.State == GameUtils.PlayerState.Synchronizing))
        {
            _networkManager.LocalPlayer.PlayerStateChangedEvent += OnPlayerStateChanged;
        }
        else
        {
            //its position is correct, anchor immediately
            Anchor();
        }
    }

    public virtual void Reset()
    {
        _photonView.RPC("ResetRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public virtual void ResetRPC()
    {
        Enable(true);
    }

    public virtual bool IsUseful(GameObject gameObj)
    {
        return true;
    }

    public virtual void Enable(bool objEnabled)
    {
        if (Enabled != objEnabled)
        {
            Enabled = objEnabled;
            this.gameObject.RenderersEnabled(objEnabled);
            this.gameObject.CollidersEnabled(objEnabled);
        }
    }
}
