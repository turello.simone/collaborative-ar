﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomInfo
{
    public string Name;
    public string PasswordHash;
    public int PlayerCount;
    public int MaxPlayers;

    public RoomInfo(string name, string passwordHash, int playerCount, int maxPlayers)
    {
        Name = name;
        PasswordHash = passwordHash;
        PlayerCount = playerCount;
        MaxPlayers = maxPlayers;
    }

}
