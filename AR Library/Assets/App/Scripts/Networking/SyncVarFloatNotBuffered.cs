﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SyncVarFloatNotBuffered
{
    private class SyncVarFloatNotBufferedHook : MonoBehaviourPun, IPunObservable
    {
        public float Value;

        private float _networkValue;
        private float _difference;
        private PhotonView _photonView;

        bool _firstTake = false;

        void OnEnable()
        {
            _firstTake = true;
        }

        public void Update()
        {
            if (!photonView.IsMine)
            {
                Value = Mathf.MoveTowards(Value, _networkValue,
                    _difference * (1.0f / PhotonNetwork.SerializationRate));
            }
        }


        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                //Debug.Log("SyncVarFloat sending: " + Value);
                stream.SendNext(Value);
            }
            else
            {
                _networkValue = (float)stream.ReceiveNext();
                //Debug.Log("SyncVarFloat received: " + _networkValue);

                if (_firstTake)
                {
                    _difference = 0f;
                    Value = this._networkValue;
                }
                else
                {
                    _difference = Mathf.Abs(Value - _networkValue);
                }
            }
        }
    }

    public float Value
    {
        get => _syncVarHook.Value;
        set => _syncVarHook.Value = value;
    }

    private SyncVarFloatNotBufferedHook _syncVarHook;

    public SyncVarFloatNotBuffered(float initialValue, PhotonView photonView)
    {
        _syncVarHook = photonView.gameObject.AddComponent<SyncVarFloatNotBufferedHook>();
        _syncVarHook.Value = initialValue;
        if (photonView.ObservedComponents.Count == 1 && photonView.ObservedComponents[0] == null)
        {
            photonView.ObservedComponents.Clear();
            photonView.Synchronization = ViewSynchronization.UnreliableOnChange;
        }
        photonView.ObservedComponents.Add(_syncVarHook);
    }

}
