﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using Photon.Pun;
using UnityEditor;
using UnityEngine;

public class GazeManager : MarkerTransformSync
{
    [Header("Gaze properties")]
    public GameObject Cursor;
    public GameObject CursorPerpendicular;
    public GameObject CursorFar;
    public GameObject Ray;

    [MustBeAssigned] public Texture CursorTexture;
    [MustBeAssigned] public Texture CursorFarTexture;
    [MustBeAssigned] public Texture CursorLocalTexture;
    [MustBeAssigned] public Texture CursorLocalFarTexture;


    public bool Hidden
    {
        get { return _hidden; }
        set
        {
            _hidden = value;
            UpdateRenderers();
        }
    }
    public GameUtils.GazeVisualization GazeVisualizationSetting
    {
        get { return _gazeVisualizationSetting; }
        set
        {
            _gazeVisualizationSetting = value;
            UpdateRenderers();
        }
    }

    private PlayerInfo _playerInfo;
    private bool _hidden;
    private ObjectManipulation _objectManipulation;
    private RaycastHit _surfaceHitInfo; //last hit on a detected surface
    private RaycastHit _objHitInfo; //last hit on a static/dynamic object
    [SerializeField, ReadOnly] private GameUtils.GazeHitType _hitType;
    public GameUtils.GazeHitType HitType
    {
        get
        {
            return _hitType;
        }
        set
        {
            if (value != _hitType)
            {
                if (_photonView.IsMine)
                {
                    if (value == GameUtils.GazeHitType.None)
                    {
                        this.transform.parent = Camera.main.transform;
                        this.transform.position =
                            Camera.main.transform.position + Camera.main.transform.forward * _objectManipulation.MaxInteractionDistance;
                        _gazetargetPosition = transform.position;
                        this.transform.rotation = Quaternion.LookRotation(Camera.main.transform.up, -Camera.main.transform.forward);
                    }
                    else
                    {
                        this.transform.parent = null;
                        if(_hitType == GameUtils.GazeHitType.None) transform.position = _gazetargetPosition;
                    }
                }
                _hitType = value;
                UpdateCursorRenderers();
            }
        }
    }
    private GameObject _dummy;
    [SerializeField, ReadOnly] private GameObject _objectHit;

    private Matrix4x4 _worldToObjectSpaceMatrix; //Roto-translation matrix from world space to _objectHit space
    [SerializeField, ReadOnly] private GameUtils.GazeVisualization _gazeVisualizationSetting;
    private Color _color;

    private Vector3 _gazetargetPosition;
    private Vector3 _smoothingVelocity;
    [SerializeField] private float _smoothTime = 0.03f;

    protected override void Awake()
    {
        if (Cursor == null || CursorPerpendicular == null || CursorFar == null || Ray == null)
        {
            Debug.LogError("Gaze properties not correctly set.");
        }

        base.Awake();
        _hidden = false;
        _worldToObjectSpaceMatrix = new Matrix4x4();
        _dummy = transform.Find("Dummy").gameObject;

        if (_photonView.IsMine)
        {
            _playerInfo = _networkManager.LocalPlayer;

            GazeVisualizationSetting = _networkManager.LocalGazeVisualizationSetting;

            Cursor.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
            Cursor.GetComponent<Projector>().farClipPlane *= 1.5f;
            Cursor.GetComponent<Projector>().nearClipPlane *= 1.5f;
            Cursor.GetComponent<Projector>().orthographicSize *= 1.5f;
            CursorPerpendicular.GetComponent<Projector>().farClipPlane *= 1.5f;
            CursorPerpendicular.GetComponent<Projector>().nearClipPlane *= 1.5f;
            CursorPerpendicular.GetComponent<Projector>().orthographicSize *= 1.5f;

            Cursor.GetComponent<Projector>().material.SetTexture("_ShadowTex", CursorLocalTexture);
            CursorPerpendicular.GetComponent<Projector>().material.SetTexture("_ShadowTex", CursorLocalTexture);
            CursorFar.GetComponent<Renderer>().material.mainTexture = CursorLocalFarTexture;
        }
        else
        {
            //Set Gaze object to the corresponding PlayerInfo
            if (_networkManager.OtherPlayers.TryGetValue(_photonView.Owner.ActorNumber, out _playerInfo))
            {
                _playerInfo.Gaze = this.gameObject;
            }

            GazeVisualizationSetting = _networkManager.RemoteGazeVisualizationSetting;

            Cursor.GetComponent<Projector>().material.SetTexture("_ShadowTex", CursorTexture);
            CursorPerpendicular.GetComponent<Projector>().material.SetTexture("_ShadowTex", CursorTexture);
            CursorFar.GetComponent<Renderer>().material.mainTexture = CursorFarTexture;
        }

    }

    protected override void Start()
    {
        base.Start();

        _objectManipulation = ObjectManipulation.Instance;

        if (_playerInfo.ObjectHeld != null)
        {
            PlayerInfo_OnObjectHeldChanged(_playerInfo, _playerInfo.ObjectHeld);
        }
        _playerInfo.ObjectHeldChangedEvent += PlayerInfo_OnObjectHeldChanged;

        //Initialize color
        _color = _playerInfo.Color;

        if (_color != Color.black)
        {
            InitializeColor(_color);
        }
        else
        {
            _playerInfo.ColorSetEvent += OnColorSetEvent;
        }
    }



    protected override void Update()
    {
        if (this._photonView == null || PhotonNetwork.IsConnectedAndReady == false)
        {
            return;
        }

        if (_photonView.IsMine)
        {
            base.Update();

            //Debug.Log("GazeTransformSync " + m_PhotonView.Owner.NickName + ": updating local pose.");

            Ray screenRay = Camera.main.ScreenPointToRay(new Vector3((float)(Screen.width / 2.0), (float)(Screen.height / 2.0), 0));

            //Check collision with (static or dynamic)objects and detected surfaces
            bool objHit = Physics.Raycast(screenRay, out _objHitInfo, _objectManipulation.MaxInteractionDistance,
                GameUtils.Mask.StaticObjects | GameUtils.Mask.DynamicObjects | GameUtils.Mask.Special);
            bool surfaceHit = _arLibrary.Raycast(screenRay, out _surfaceHitInfo, _objectManipulation.MaxInteractionDistance);

            //if both an object and a surface were hit, decide which is closer
            if (objHit && surfaceHit)
            {
                if (_objHitInfo.distance < _surfaceHitInfo.distance)
                {
                    surfaceHit = false;
                }
                else
                {
                    objHit = false;
                }
            }

            if (objHit)
            {
                _objectHit = _objHitInfo.collider.gameObject;
                _gazetargetPosition = _objHitInfo.point;
                this.transform.up = _objHitInfo.normal;
                HitType = GameUtils.GazeHitType.Object;
                //this.transform.position = _objHitInfo.point;
            }
            else if (surfaceHit)
            {
                _gazetargetPosition = _surfaceHitInfo.point;
                //this.transform.position = _surfaceHitPose.position;
                this.transform.up = _surfaceHitInfo.normal;
                HitType = GameUtils.GazeHitType.Surface;
            }
            else
            {
                HitType = GameUtils.GazeHitType.None;
            }

            if (HitType != GameUtils.GazeHitType.None)
                transform.position = Vector3.SmoothDamp(transform.position, _gazetargetPosition, ref _smoothingVelocity,
                    _smoothTime);
        }
        else
        {
            //Debug.Log("GazeTransformSync " + m_PhotonView.Owner.NickName + ": updating pose.");

            switch (HitType)
            {
                case GameUtils.GazeHitType.None:
                    base.Update();
                    _dummy.transform.localPosition = Vector3.zero;
                    _dummy.transform.localRotation = Quaternion.identity;
                    break;
                case GameUtils.GazeHitType.Surface:
                    base.Update();
                    Ray ray = new Ray(transform.position, transform.up);
                    Ray inverseRay = new Ray(transform.position, -transform.up);
                    RaycastHit hitInfo;
                    if (_arLibrary.Raycast(inverseRay, out hitInfo, 0.25f))
                    {
                        _dummy.transform.position = hitInfo.point;
                        _dummy.transform.up = hitInfo.normal;
                    }
                    else if (_arLibrary.Raycast(ray, out hitInfo, 0.25f))
                    {
                        _dummy.transform.position = hitInfo.point;
                        _dummy.transform.up = hitInfo.normal;
                    }
                    else
                    {
                        _dummy.transform.localPosition = Vector3.zero;
                        _dummy.transform.localRotation = Quaternion.identity;
                    }

                    break;
                case GameUtils.GazeHitType.Object:
                    if (_objectHit == null) return;
                    _dummy.transform.localPosition = Vector3.zero;
                    _dummy.transform.localRotation = Quaternion.identity;

                    _worldToObjectSpaceMatrix.SetTRS(_objectHit.transform.position, _objectHit.transform.rotation, Vector3.one);
                    this.UpdatePosition(_worldToObjectSpaceMatrix);
                    this.UpdateRotation(_objectHit.transform.rotation);
                    this.UpdateScale();
                    break;
            }
        }

    }

    private void LateUpdate()
    {
        UpdateRay();
    }

    private void OnDestroy()
    {
        _playerInfo.ObjectHeldChangedEvent -= PlayerInfo_OnObjectHeldChanged;
    }

    public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(HitType);
            if (HitType == GameUtils.GazeHitType.None || HitType == GameUtils.GazeHitType.Surface)
            {
                base.OnPhotonSerializeView(stream, info);
            }
            else if(HitType == GameUtils.GazeHitType.Object)
            {
                _worldToObjectSpaceMatrix.SetTRS(_objectHit.transform.position, _objectHit.transform.rotation, Vector3.one);

                //Send _objectHit's viewID
                stream.SendNext(_objectHit.transform.GetComponent<PhotonView>().ViewID); 
                //Synchronize position and rotation of the hitPoint relative to _objectHit position
                m_PositionControl.OnPhotonSerializeView(transform.position, _worldToObjectSpaceMatrix, stream, info);
                m_RotationControl.OnPhotonSerializeView(transform.rotation, _objectHit.transform.rotation, stream, info);
                m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

                ForceInitialData(stream, _worldToObjectSpaceMatrix, _objectHit.transform.rotation);
            }
        }
        else
        {
            HitType = (GameUtils.GazeHitType) stream.ReceiveNext();
            if (HitType == GameUtils.GazeHitType.None || HitType == GameUtils.GazeHitType.Surface)
            {
                base.OnPhotonSerializeView(stream, info);
            }
            else if (HitType == GameUtils.GazeHitType.Object)
            {
                //Receive _objectHit's viewID
                int viewID = (int) stream.ReceiveNext();
                if (PhotonView.Find(viewID) == null)
                {
                    Debug.Log("GazeTransformSync: No objecthit found with specified viewID (" + viewID + ").");
                    return;
                }
                else
                {
                    _objectHit = PhotonView.Find(viewID).gameObject;
                }

                _worldToObjectSpaceMatrix.SetTRS(_objectHit.transform.position, _objectHit.transform.rotation,
                    Vector3.one);

                m_PositionControl.OnPhotonSerializeView(transform.position, _worldToObjectSpaceMatrix, stream, info);
                m_RotationControl.OnPhotonSerializeView(transform.rotation, _objectHit.transform.rotation, stream, info);
                m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

                ForceInitialData(stream, _worldToObjectSpaceMatrix, _objectHit.transform.rotation);
            }

        }
    }


    private void PlayerInfo_OnObjectHeldChanged(PlayerInfo player, GameObject newvalue)
    {
        if (newvalue != null)
        {
            //Player has grabbed an object, deactivate gaze visualization
            Hidden = true;
        }
        else
        {
            //Player has put down an object, activate gaze visualization
            Hidden = false;
        }
    }

    private void UpdateRenderers()
    {
        if (Hidden)
        {
            Cursor.GetComponent<Projector>().enabled = false;
            CursorPerpendicular.GetComponent<Projector>().enabled = false;
            CursorFar.GetComponent<MeshRenderer>().enabled = false;
            Ray.GetComponent<LineRenderer>().enabled = false;
        }
        else
        {
            switch (_gazeVisualizationSetting)
            {
                case GameUtils.GazeVisualization.None:
                    Cursor.GetComponent<Projector>().enabled = false;
                    CursorPerpendicular.GetComponent<Projector>().enabled = false;
                    CursorFar.GetComponent<MeshRenderer>().enabled = false;
                    Ray.GetComponent<LineRenderer>().enabled = false;
                    break;
                case GameUtils.GazeVisualization.Cursor:
                    Cursor.GetComponent<Projector>().enabled = true;
                    CursorPerpendicular.GetComponent<Projector>().enabled = true;
                    CursorFar.GetComponent<MeshRenderer>().enabled = true;
                    Ray.GetComponent<LineRenderer>().enabled = false;
                    UpdateCursorRenderers();
                    break;
                case GameUtils.GazeVisualization.CursorRay:
                    Cursor.GetComponent<Projector>().enabled = true;
                    CursorPerpendicular.GetComponent<Projector>().enabled = true;
                    CursorFar.GetComponent<MeshRenderer>().enabled = true;
                    Ray.GetComponent<LineRenderer>().enabled = true;
                    UpdateCursorRenderers();
                    UpdateRay();
                    break;
            }

        }
    }

    private void UpdateCursorRenderers()
    {
        if (!Hidden && (GazeVisualizationSetting == GameUtils.GazeVisualization.Cursor ||
                        GazeVisualizationSetting == GameUtils.GazeVisualization.CursorRay))
        {
            if (HitType == GameUtils.GazeHitType.None)
            {
                Cursor.GetComponent<Projector>().enabled = false;
                CursorPerpendicular.GetComponent<Projector>().enabled = false;
                CursorFar.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                Cursor.GetComponent<Projector>().enabled = true;
                CursorPerpendicular.GetComponent<Projector>().enabled = true;
                CursorFar.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }

    private void UpdateRay()
    {
        if (!Hidden && GazeVisualizationSetting == GameUtils.GazeVisualization.CursorRay && _playerInfo.Player != null)
        {
            Ray.GetComponent<LineRenderer>().SetPosition(1, Ray.transform.InverseTransformPoint(_playerInfo.Player.transform.position));
        }
    }

    private void InitializeColor(Color color)
    {

        // Projector material is like renderer.shaderMaterial, in order to change local material color make a copy of the shared one.
        Material cursorMaterial = new Material(Cursor.GetComponent<Projector>().material);
        cursorMaterial.color = color;
        Cursor.GetComponent<Projector>().material = cursorMaterial;

        Material cursorPerpendicularMaterial = new Material(CursorPerpendicular.GetComponent<Projector>().material);
        cursorPerpendicularMaterial.color = color;
        CursorPerpendicular.GetComponent<Projector>().material = cursorPerpendicularMaterial;

        Color cursorFarStartColor = CursorFar.GetComponent<MeshRenderer>().material.GetColor("_TintColor");
        CursorFar.GetComponent<MeshRenderer>().material.SetColor("_TintColor", color.WithAlphaSetTo(cursorFarStartColor.a));

        Ray.GetComponent<LineRenderer>().startColor = color;
        Ray.GetComponent<LineRenderer>().endColor = color;
    }

    private void OnColorSetEvent(PlayerInfo player, Color newvalue)
    {
        _color = newvalue;
        InitializeColor(newvalue);
        _playerInfo.ColorSetEvent -= OnColorSetEvent;
    }

}
