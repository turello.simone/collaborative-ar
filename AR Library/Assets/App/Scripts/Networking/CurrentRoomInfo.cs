﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class CurrentRoomInfo : RoomInfo, IInRoomCallbacks
{
    public delegate void RoomSceneChanged(SubSceneManager.RoomScene newValue);
    public event RoomSceneChanged RoomSceneChangedEvent;
    public delegate void MasterClientSwitched(PlayerInfo player);
    public event MasterClientSwitched MasterClientSwitchedEvent;
    public delegate void GazeConvergenceFocalPointCompleted(GazeConvergenceFocalPoint.Type type);
    public event GazeConvergenceFocalPointCompleted GazeConvergenceFocalPointCompletedEvent;

    public SubSceneManager.RoomScene RoomScene
    {
        get
        {
            if (_roomScene.Type == SubSceneManager.SubSceneType.Uninitialized &&
                PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.ROOM_SCENE,
                    out _temp))
            {
                _roomScene = (SubSceneManager.RoomScene)_temp;
            }
            return _roomScene;
        }

        set
        {
            if (PhotonNetwork.IsMasterClient)
            {
                //_roomScene = value;
                PhotonNetworkManager.Instance.ResetPlayersReady();
                _props = new Hashtable() { { GameUtils.ROOM_SCENE, value } };
                PhotonNetwork.CurrentRoom.SetCustomProperties(_props);
            }
        }
    }

    public GameUtils.SimpleGameMode SimpleGameMode
    {
        get
        {
            if (_simpleGameMode == GameUtils.SimpleGameMode.Uninitialized &&
                PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.SIMPLE_GAMEMODE,
                    out _temp))
            {
                _simpleGameMode = (GameUtils.SimpleGameMode) _temp;
            }
            return _simpleGameMode;
        }
        set
        {
            if (PhotonNetwork.IsMasterClient)
            {
                _simpleGameMode = value;

                _props = new Hashtable() { { GameUtils.SIMPLE_GAMEMODE, value } };
                PhotonNetwork.CurrentRoom.SetCustomProperties(_props);
            }
        }
    }
    private GameUtils.SimpleGameMode _simpleGameMode;

    #region Scene Objects 
    //Tutorial Scene
    public TutorialSceneContraption TutorialSceneContraption;
    private Pose _contraptionPose;
    public Pose ContraptionPose //Synchronized over network. The actual value saved is in marker space, the value returned is in world space, conversion is done automatically.
    {
        get => GetPose(ref _contraptionPose, GameUtils.CONTRAPTION_POSITION, GameUtils.CONTRAPTION_ROTATION);
        set => SetPose(value, ref _contraptionPose, GameUtils.CONTRAPTION_POSITION, GameUtils.CONTRAPTION_ROTATION);
    } 
    public TutorialSceneCylinder TutorialSceneCylinderAzathoth;
    public Pose AzathothCylinderPose
    {
        get
        {
            Pose p = Pose.identity;
            if (TutorialSceneContraption != null)
            {
                p.position = TutorialSceneContraption.GetComponent<TutorialSceneContraption>().AzathothCylinderBase.transform
                    .position;
                p.rotation = TutorialSceneContraption.GetComponent<TutorialSceneContraption>().AzathothCylinderBase.transform
                    .rotation;
            }
            return p;
        }
    }
    public TutorialSceneCylinder TutorialSceneCylinderCthugha;
    public Pose CthughaCylinderPose
    {
        get
        {
            Pose p = Pose.identity;
            if (TutorialSceneContraption != null)
            {
                p.position = TutorialSceneContraption.GetComponent<TutorialSceneContraption>().CthughaCylinderBase.transform
                    .position;
                p.rotation = TutorialSceneContraption.GetComponent<TutorialSceneContraption>().CthughaCylinderBase.transform
                    .rotation;
            }
            return p;
        }
    }
    public TutorialSceneCylinder TutorialSceneCylinderYogSothoth;
    public Pose YogSothothCylinderPose
    {
        get
        {
            Pose p = Pose.identity;
            if (TutorialSceneContraption != null)
            {
                p.position = TutorialSceneContraption.GetComponent<TutorialSceneContraption>().YogSothothCylinderBase.transform
                    .position;
                p.rotation = TutorialSceneContraption.GetComponent<TutorialSceneContraption>().YogSothothCylinderBase.transform
                    .rotation;
            }
            return p;
        }
    }

    //Pedestal Scene
    public PedestalScenePedestal PedestalScenePedestal;
    private Pose _pedestalPose;
    public Pose PedestalPose
    {
        get => GetPose(ref _pedestalPose, GameUtils.PEDESTAL_POSITION, GameUtils.PEDESTAL_ROTATION);
        set => SetPose(value, ref _pedestalPose, GameUtils.PEDESTAL_POSITION, GameUtils.PEDESTAL_ROTATION);
    }

    public PedestalScenePressurePlate PedestalScenePressurePlate1;
    private Pose _pressurePlate1Pose;
    public Pose PressurePlate1Pose
    {
        get => GetPose(ref _pressurePlate1Pose, GameUtils.PRESSUREPLATE1_POSITION, GameUtils.PRESSUREPLATE1_ROTATION);
        set => SetPose(value, ref _pressurePlate1Pose, GameUtils.PRESSUREPLATE1_POSITION, GameUtils.PRESSUREPLATE1_ROTATION);
    }

    public PedestalScenePressurePlate PedestalScenePressurePlate2;
    private Pose _pressurePlate2Pose;
    public Pose PressurePlate2Pose
    {
        get => GetPose(ref _pressurePlate2Pose, GameUtils.PRESSUREPLATE2_POSITION, GameUtils.PRESSUREPLATE2_ROTATION);
        set => SetPose(value, ref _pressurePlate2Pose, GameUtils.PRESSUREPLATE2_POSITION, GameUtils.PRESSUREPLATE2_ROTATION);
    }

    public PedestalScenePressurePlate PedestalScenePressurePlate3;
    private Pose _pressurePlate3Pose;
    public Pose PressurePlate3Pose
    {
        get => GetPose(ref _pressurePlate3Pose, GameUtils.PRESSUREPLATE3_POSITION, GameUtils.PRESSUREPLATE3_ROTATION);
        set => SetPose(value, ref _pressurePlate3Pose, GameUtils.PRESSUREPLATE3_POSITION, GameUtils.PRESSUREPLATE3_ROTATION);
    }

    //Painting Scene
    public PaintingSceneFlashlight PaintingSceneFlashlight1;
    private Pose _flashlight1Pose;
    public Pose Flashlight1Pose
    {
        get => GetPose(ref _flashlight1Pose, GameUtils.FLASHLIGHT1_POSITION, GameUtils.FLASHLIGHT1_ROTATION);
        set => SetPose(value, ref _flashlight1Pose, GameUtils.FLASHLIGHT1_POSITION, GameUtils.FLASHLIGHT1_ROTATION);
    }

    public PaintingSceneFlashlight PaintingSceneFlashlight2;
    private Pose _flashlight2Pose;
    public Pose Flashlight2Pose
    {
        get => GetPose(ref _flashlight2Pose, GameUtils.FLASHLIGHT2_POSITION, GameUtils.FLASHLIGHT2_ROTATION);
        set => SetPose(value, ref _flashlight2Pose, GameUtils.FLASHLIGHT2_POSITION, GameUtils.FLASHLIGHT2_ROTATION);
    }

    public PaintingSceneFlashlight PaintingSceneFlashlight3;
    private Pose _flashlight3Pose;
    public Pose Flashlight3Pose
    {
        get => GetPose(ref _flashlight3Pose, GameUtils.FLASHLIGHT3_POSITION, GameUtils.FLASHLIGHT3_ROTATION);
        set => SetPose(value, ref _flashlight3Pose, GameUtils.FLASHLIGHT3_POSITION, GameUtils.FLASHLIGHT3_ROTATION);
    }

    public PaintingSceneRiddle PaintingSceneRiddle;
    private Pose _riddlePose;
    public Pose RiddlePose
    {
        get => GetPose(ref _riddlePose, GameUtils.RIDDLE_POSITION, GameUtils.RIDDLE_ROTATION);
        set => SetPose(value, ref _riddlePose, GameUtils.RIDDLE_POSITION, GameUtils.RIDDLE_ROTATION);
    }

    public PaintingScenePainting PaintingScenePainting;
    private Pose _paintingPose;
    public Pose PaintingPose
    {
        get => GetPose(ref _paintingPose, GameUtils.PAINTING_POSITION, GameUtils.PAINTING_ROTATION);
        set => SetPose(value, ref _paintingPose, GameUtils.PAINTING_POSITION, GameUtils.PAINTING_ROTATION);
    }

    //Translation Scene
    public TranslationSceneContraption TranslationSceneContraption;

    public TranslationScenePainting TranslationScenePainting;

    public TranslationSceneCandleHolder TranslationSceneCandleHolder;
    private Pose _candleHolderPose;
    public Pose CandleHolderPose
    {
        get => GetPose(ref _candleHolderPose, GameUtils.CANDLEHOLDER_POSITION, GameUtils.CANDLEHOLDER_ROTATION);
        set => SetPose(value, ref _candleHolderPose, GameUtils.CANDLEHOLDER_POSITION, GameUtils.CANDLEHOLDER_ROTATION);
    }

    public TranslationSceneChalice TranslationSceneChalice;
    private Pose _chalicePose;
    public Pose ChalicePose
    {
        get => GetPose(ref _chalicePose, GameUtils.CHALICE_POSITION, GameUtils.CHALICE_ROTATION);
        set => SetPose(value, ref _chalicePose, GameUtils.CHALICE_POSITION, GameUtils.CHALICE_ROTATION);
    }

    public TranslationSceneDagger TranslationSceneDagger;
    private Pose _daggerPose;
    public Pose DaggerPose
    {
        get => GetPose(ref _daggerPose, GameUtils.DAGGER_POSITION, GameUtils.DAGGER_ROTATION);
        set => SetPose(value, ref _daggerPose, GameUtils.DAGGER_POSITION, GameUtils.DAGGER_ROTATION);
    }

    public TranslationSceneSkull TranslationSceneSkull;
    private Pose _skullPose;
    public Pose SkullPose
    {
        get => GetPose(ref _skullPose, GameUtils.SKULL_POSITION, GameUtils.SKULL_ROTATION);
        set => SetPose(value, ref _skullPose, GameUtils.SKULL_POSITION, GameUtils.SKULL_ROTATION);
    }

    //Training scenes
    public TrainingSceneStaticObject TrainingSceneStaticObject;
    public TrainingSceneDynamicObject TrainingSceneDynamicObject1;
    public TrainingSceneDynamicObject TrainingSceneDynamicObject2;
    public TrainingSceneDynamicObject TrainingSceneDynamicObject3;
    public TrainingSceneDynamicObject TrainingSceneDynamicObject4;
    public TrainingGazeSceneCubes TrainingGazeSceneCubes;
    public TrainingManipSceneHoles TrainingManipSceneHoles;
    public TrainingManipSceneShape TrainingManipSceneRectangle;
    public TrainingManipSceneShape TrainingManipSceneSquare;
    public TrainingManipSceneShape TrainingManipSceneTriangle;
    public TrainingPosScenePlates TrainingPosScenePlates;

    #endregion

    #region Task Objects

    public TaskGazeSameSysCubes TaskGazeSameSysCubes;
    public TaskGazeDiffSysCubes TaskGazeDiffSysCubes;
    public TaskGazeSameUsrCubes TaskGazeSameUsrCubes;
    public TaskGazeDiffUsrCubes TaskGazeDiffUsrCubes;
    public TaskManipDiffSysShape TaskManipDiffSysRectangle;
    public TaskManipDiffSysShape TaskManipDiffSysSquare;
    public TaskManipDiffSysShape TaskManipDiffSysTriangle;
    public TaskManipDiffSysHoles TaskManipDiffSysHoles;
    public TaskManipDiffUsrTriangle TaskManipDiffUsrTriangleFirst1;
    public TaskManipDiffUsrTriangle TaskManipDiffUsrTriangleFirst2;
    public TaskManipDiffUsrTriangle TaskManipDiffUsrTriangleSecond1;
    public TaskManipDiffUsrTriangle TaskManipDiffUsrTriangleSecond2;
    public TaskManipDiffUsrTriangle TaskManipDiffUsrTriangleThird1;
    public TaskManipDiffUsrTriangle TaskManipDiffUsrTriangleThird2;
    public TaskManipDiffUsrHoles TaskManipDiffUsrHoles;
    public TaskPosSameSysPlates TaskPosSameSysPlates;
    public TaskPosDiffSysPlates TaskPosDiffSysPlates;
    public TaskPosSameUsrPlates TaskPosSameUsrPlates;
    public TaskPosDiffUsrPlates TaskPosDiffUsrPlates;

    #endregion

    private SubSceneManager.RoomScene _roomScene;
    private Dictionary<GazeConvergenceFocalPoint.Type, bool> _gazeConvergenceStatus;
    private object _temp;
    private Hashtable _props;

    public CurrentRoomInfo(string name, string passwordHash, int playerCount, int maxPlayers) : base(name, passwordHash, playerCount, maxPlayers)
    {
        PhotonNetwork.AddCallbackTarget(this);

        if (PhotonNetwork.IsMasterClient)
        {
            _props = new Hashtable();
            _props.Add(GameUtils.PLAYER_COLOR1, -1);
            _props.Add(GameUtils.PLAYER_COLOR2, -1);
            _props.Add(GameUtils.PLAYER_COLOR3, -1);
            PhotonNetwork.CurrentRoom.SetCustomProperties(_props);

            SimpleGameMode = (GameUtils.SimpleGameMode) PlayerPrefs.GetInt(GameUtils.SIMPLE_GAMEMODE, (int) GameUtils.SimpleGameMode.Disabled);
        }

        _roomScene = new SubSceneManager.RoomScene(SubSceneManager.SubSceneType.Uninitialized, new SubSceneManager.TaskInfo());

        _gazeConvergenceStatus = new Dictionary<GazeConvergenceFocalPoint.Type, bool>();
        //Initialize _gazeConvergenceStatus from network
        foreach (GazeConvergenceFocalPoint.Type type in Enum.GetValues(typeof(GazeConvergenceFocalPoint.Type)))
        {
            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.FOCALPOINT + type, out _temp))
            {
                _gazeConvergenceStatus.Add(type, (bool)_temp);
            }
            else
            {
                _gazeConvergenceStatus.Add(type, false);
            }
        }
    }
    
    public void Dispose()
    {
        RoomSceneChangedEvent = null;
        MasterClientSwitchedEvent = null;
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public bool GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type type, out bool completed)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("CurrentRoomInfo/GetGazeConvergenceStatus: Type not defined.");
            completed = false;
            return false;
        }

        if (_gazeConvergenceStatus.TryGetValue(type, out completed))
        {
            return true;
        }
        completed = false;
        return false;
    }

    public bool SetGazeConvergenceStatusCompleted(GazeConvergenceFocalPoint.Type type)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("CurrentRoomInfo/SetGazeConvergenceStatus: Type not defined.");
            return false;
        }

        if (!_gazeConvergenceStatus[type])
        {
            _gazeConvergenceStatus[type] = true;
            _props = new Hashtable() { { GameUtils.FOCALPOINT + type, true } };
            PhotonNetwork.CurrentRoom.SetCustomProperties(_props);
        }
        return true;
    }

    public bool ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type type)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("CurrentRoomInfo/SetGazeConvergenceStatus: Type not defined.");
            return false;
        }


        if (_gazeConvergenceStatus[type])
        {
            _gazeConvergenceStatus[type] = false;
            _props = new Hashtable() { { GameUtils.FOCALPOINT + type, false } };
            PhotonNetwork.CurrentRoom.SetCustomProperties(_props);
        }
        return true;
    }

    public void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.TryGetValue(GameUtils.SIMPLE_GAMEMODE, out _temp))
        {
            _simpleGameMode = (GameUtils.SimpleGameMode) _temp;
            PlayerPrefs.SetInt(GameUtils.SIMPLE_GAMEMODE, (int) _temp);
            GameUIManager.Instance.SimpleGameModeToggle.isOn = (_simpleGameMode == GameUtils.SimpleGameMode.Enabled);
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.ROOM_SCENE, out _temp))
        {
            _roomScene = (SubSceneManager.RoomScene) _temp;
            if (RoomSceneChangedEvent != null) RoomSceneChangedEvent(_roomScene);
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.CONTRAPTION_POSITION, out _temp))
        {
            _contraptionPose.position = (Vector3) _temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.CONTRAPTION_ROTATION, out _temp))
        {
            _contraptionPose.rotation = (Quaternion) _temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PEDESTAL_POSITION, out _temp))
        {
            _pedestalPose.position = (Vector3)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PEDESTAL_ROTATION, out _temp))
        {
            _pedestalPose.rotation = (Quaternion)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PRESSUREPLATE1_POSITION, out _temp))
        {
            _pressurePlate1Pose.position = (Vector3)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PRESSUREPLATE1_ROTATION, out _temp))
        {
            _pressurePlate1Pose.rotation = (Quaternion)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PRESSUREPLATE2_POSITION, out _temp))
        {
            _pressurePlate2Pose.position = (Vector3)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PRESSUREPLATE2_ROTATION, out _temp))
        {
            _pressurePlate2Pose.rotation = (Quaternion)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PRESSUREPLATE3_POSITION, out _temp))
        {
            _pressurePlate3Pose.position = (Vector3)_temp;
        }
        if (propertiesThatChanged.TryGetValue(GameUtils.PRESSUREPLATE3_ROTATION, out _temp))
        {
            _pressurePlate3Pose.rotation = (Quaternion)_temp;
        }
        foreach (GazeConvergenceFocalPoint.Type type in Enum.GetValues(typeof(GazeConvergenceFocalPoint.Type)))
        {
            if (propertiesThatChanged.TryGetValue(GameUtils.FOCALPOINT + type, out _temp))
            {
                _gazeConvergenceStatus[type] = (bool) _temp;

                if ((bool) _temp)
                {
                    GazeConvergenceFocalPointCompletedEvent?.Invoke(type);
                    PhotonNetworkManager.Instance.LocalPlayer.IsGazeOnFocalPoint = false;
                    AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.PingGazeConvergence);

                    if (PhotonNetworkManager.Instance.IsMasterClient)
                        OnlineLogger.Instance.SendGameEvent(this, PhotonNetworkManager.Instance.LocalPlayer,
                            OnlineLogger.EventType.GazeCompleted);
                }
            }
        }
    }

    public void OnMasterClientSwitched(Player newMasterClient)
    {
        PlayerInfo masterClient;
        if (PhotonNetworkManager.Instance.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            masterClient = PhotonNetworkManager.Instance.LocalPlayer;
        }
        else
        {
            masterClient = PhotonNetworkManager.Instance.OtherPlayers[newMasterClient.ActorNumber];
        }

        if (MasterClientSwitchedEvent != null) MasterClientSwitchedEvent(masterClient);
    }

    public void OnPlayerEnteredRoom(Player newPlayer) { }
    public void OnPlayerLeftRoom(Player otherPlayer) { }
    public void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) { }

    private Pose GetPose(ref Pose pose, string positionString, string rotationString)
    {
        if (pose.position == Vector3.zero && PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(positionString, out _temp))
        {
            pose.position = (Vector3)_temp;
        }
        if (pose.rotation == Quaternion.identity && PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(rotationString, out _temp))
        {
            pose.rotation = (Quaternion)_temp;
        }

        return pose.ToWorldSpace(EscapeGameManager.Instance.MarkerAnchorId);
    }

    private void SetPose(Pose newValue, ref Pose pose, string positionString, string rotationString)
    {
        Pose valueRelativeToAnchor = newValue.ToAnchorSpace(EscapeGameManager.Instance.MarkerAnchorId);
        if (!pose.Equals(valueRelativeToAnchor))
        {
            pose = valueRelativeToAnchor;
            _props = new Hashtable() { { positionString, valueRelativeToAnchor.position } };
            PhotonNetwork.CurrentRoom.SetCustomProperties(_props);
            _props = new Hashtable() { { rotationString, valueRelativeToAnchor.rotation } };
            PhotonNetwork.CurrentRoom.SetCustomProperties(_props);
        }
    }

}
