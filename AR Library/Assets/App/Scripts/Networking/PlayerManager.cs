﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class PlayerManager : MarkerTransformSync
{

    [Header("Player properties")]
    [MustBeAssigned] public Canvas Canvas;
    [MustBeAssigned] public TextMeshProUGUI PlayerName;
    [MustBeAssigned] public GameObject AvatarOculus;
    [MustBeAssigned] public GameObject AvatarSimple;
    [MustBeAssigned] public GameObject ViewFrustum;
    public float ViewFrustumMaxDistance;
    public float ViewFrustumSizeMultiplier = 0.8f;

    public GameUtils.PlayerVisualization PlayerVisualizationSetting
    {
        get { return _playerVisualizationSetting; }

        set
        {
            _playerVisualizationSetting = value;

            Canvas.enabled = value.HasFlag(GameUtils.PlayerVisualization.Name);
            ViewFrustum.GetComponent<Renderer>().enabled = value.HasFlag(GameUtils.PlayerVisualization.Frustum);
            //Debug.LogErrorFormat("Fustum: visualization {0}, active {1}",
            //    value.HasFlag(GameUtils.PlayerVisualization.Frustum), ViewFrustum.GetComponent<Renderer>().enabled);
            AvatarSimple.GetComponentsInChildren<Renderer>().ToList().ForEach(r =>
                r.enabled = (value.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
                             AvatarType == GameUtils.AvatarType.Simple));
            AvatarOculus.GetComponent<Renderer>().enabled = (value.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
                                                             AvatarType == GameUtils.AvatarType.Oculus);
        }
    }
    [SerializeField, ReadOnly] private GameUtils.PlayerVisualization _playerVisualizationSetting;

    public GameUtils.AvatarType AvatarType
    {
        get { return _avatarType; }

        set
        {
            _avatarType = value;
            switch (value)
            {
                case GameUtils.AvatarType.Simple:
                    PlayerVisualizationSetting = PlayerVisualizationSetting;
                    break;
                case GameUtils.AvatarType.Oculus:
                    PlayerVisualizationSetting = PlayerVisualizationSetting;
                    break;
            }
        }
    }
    [SerializeField, ReadOnly] private GameUtils.AvatarType _avatarType;

    private PlayerInfo _playerInfo;
    private float _avatarCanvasDistance;
    private Camera _camera;

    protected override void Awake()
    {
        base.Awake();

        _avatarType = _networkManager.AvatarType;
        if (_photonView.IsMine)
        {
            PlayerVisualizationSetting = _networkManager.LocalPlayerVisualizationSetting;
        }
        else
        {
            PlayerVisualizationSetting = _networkManager.RemotePlayerVisualizationSetting;

            //Set Player object to the corresponding PlayerInfo
            if (_networkManager.OtherPlayers.TryGetValue(_photonView.Owner.ActorNumber, out _playerInfo))
            {
                _playerInfo.Player = this.gameObject;
            }
        }

        _avatarCanvasDistance = Vector3.Distance(AvatarOculus.transform.position, Canvas.transform.position);
        _camera = Camera.main;
    }

    protected override void Start()
    {
        base.Start();
        PlayerName.text = _photonView.Owner.NickName + " #" + _photonView.Owner.ActorNumber;

        if (_photonView.IsMine)
        {
            transform.position = _camera.transform.position;
            transform.rotation = _camera.transform.rotation;
            transform.parent = _camera.transform;

            _playerInfo = _networkManager.LocalPlayer;
        }

        if (_playerInfo != null)
        {
            //Set frustum dimensions
            float frustumHeight = 2.0f * ViewFrustumMaxDistance * Mathf.Tan(_playerInfo.FieldOfView * 0.5f * Mathf.Deg2Rad);
            float frustumWidth = frustumHeight * _playerInfo.AspectRatio;
            ViewFrustum.transform.localScale = new Vector3(frustumWidth * ViewFrustumSizeMultiplier,
                frustumHeight * ViewFrustumSizeMultiplier, ViewFrustumMaxDistance);

            //Set player color to name, avatar and view frustum
            Color playerColor = _playerInfo.Color;

            if (playerColor != Color.black)
            {
                InitializeColor(playerColor);
            }
            else
            {
                _playerInfo.ColorSetEvent += OnColorSet;
            }
        }
    }

    protected override void Update()
    {
        base.Update();

        Canvas.transform.position = AvatarOculus.transform.position + Vector3.up * _avatarCanvasDistance;
        Canvas.transform.LookAt(_camera.transform, _camera.transform.up);
        Canvas.transform.Rotate(0, 180f, 0);
    }

    private void InitializeColor(Color color)
    {
        PlayerName.color = new Color(color.r, color.g, color.b, PlayerName.color.a);
        AvatarOculus.GetComponent<MeshRenderer>().materials[1].color = color;
        AvatarOculus.GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", color);
        AvatarSimple.GetComponent<MeshRenderer>().material.color = color;

        
        ViewFrustum.GetComponent<MeshRenderer>().materials[0].color =
            color.WithAlphaSetTo(ViewFrustum.GetComponent<MeshRenderer>().materials[0].color.a);

        //ViewFrustum.GetComponent<MeshRenderer>().materials[1].SetColor("_TintColor",
        //    color.WithAlphaSetTo(ViewFrustum.GetComponent<MeshRenderer>().materials[1].GetColor("_TintColor").a));

        Color startColor = ViewFrustum.GetComponent<MeshRenderer>().materials[1].GetColor("_TintColor");
        ViewFrustum.GetComponent<MeshRenderer>().materials[1].SetColor("_TintColor", color.WithAlphaSetTo(startColor.a));
    }

    private void OnColorSet(PlayerInfo player, Color newValue)
    {
        InitializeColor(newValue);
        _playerInfo.ColorSetEvent -= OnColorSet;
    }
}
