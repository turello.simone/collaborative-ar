﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SyncVarInt
{
    private class SyncVarIntHook : MonoBehaviour
    {
        public Action<int, string> OnRPC;

        [PunRPC]
        private void SyncVarIntRPC(int value, string identifier)
        {
            OnRPC(value, identifier);
        }
    }

    public delegate void ValueChangedHandler(int value);
    public event ValueChangedHandler ValueChanged;

    public int Value
    {
        get { return _value; }
        set
        {
            _value = value;
            _photonView.RPC("SyncVarIntRPC", RpcTarget.AllBuffered, value, _identifier);
            //if (ValueChanged != null) ValueChanged(value);
        }
    }

    private int _value;
    private string _identifier;
    private PhotonView _photonView;

    public SyncVarInt(int initialValue, PhotonView photonView, string identifier)
    {
        _value = initialValue;
        _photonView = photonView;
        _identifier = identifier;

        SyncVarIntHook syncVarHook = _photonView.gameObject.AddComponent<SyncVarIntHook>();
        syncVarHook.OnRPC = SyncVarIntRPC;
    }

    private void SyncVarIntRPC(int value, string identifier)
    {
        if (identifier == _identifier)
        {
            _value = value;
            if (ValueChanged != null) ValueChanged(value);
        }
    }
}
