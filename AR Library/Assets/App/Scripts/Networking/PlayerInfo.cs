﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using Photon.Voice.PUN;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerInfo : IInRoomCallbacks
{
    public delegate void PlayerStateChanged(PlayerInfo player, GameUtils.PlayerState newValue);
    public delegate void ObjectHeldChanged(PlayerInfo player, GameObject newValue);
    public delegate void ColorSet(PlayerInfo player, Color newValue);

    public event PlayerStateChanged PlayerStateChangedEvent;
    public event ObjectHeldChanged ObjectHeldChangedEvent;
    public event ColorSet ColorSetEvent;

    public virtual string Name { get; set; }
    public virtual int ActorNumber
    {
        get { return _actorNumber; }
        set
        {
            _actorNumber = value;
            //Find PhotonPlayer with this actorNumber
            _photonPlayer = PhotonNetwork.CurrentRoom.GetPlayer(_actorNumber);

            if (_color == 0 && _actorNumber != -1)
            {
                if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.PLAYER_COLOR1, out _temp) &&
                    (int)_temp == _actorNumber)
                {
                    _color = 1;
                }
                else if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.PLAYER_COLOR2, out _temp) &&
                         (int)_temp == _actorNumber)
                {
                    _color = 2;
                }
                else if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.PLAYER_COLOR3, out _temp) &&
                         (int)_temp == _actorNumber)
                {
                    _color = 3;
                }
            }
        }
    }

    public GameObject Player
    {
        get { return _player; }
        set
        {
            _player = value;
            if (value != null)
            {
                MuteVoice(!IsVoiceEnabled);
            }
        }
    }
    private GameObject _player;

    public PlayerManager PlayerManager {
        get
        {
            if (Player != null) return Player.GetComponent<PlayerManager>();
            else return null;
        }
    }
    public GameObject Gaze;
    public GazeManager GazeManager
    {
        get
        {
            if (Gaze != null) return Gaze.GetComponent<GazeManager>();
            else return null;
        }
    }
    public virtual bool IsMasterClient
    {
        get { return _actorNumber != -1 &&_photonPlayer != null && _photonPlayer.IsMasterClient; }
    }
    public virtual GameUtils.PlayerState State
    {
        get
        {
            _photonPlayer.CustomProperties.TryGetValue(GameUtils.PLAYER_STATE, out _temp);
            if (_temp != null)
            {
                return (GameUtils.PlayerState) _temp;
            }
            else
            {
                return GameUtils.PlayerState.Error;
            }
        }
    }
    public Color Color
    {
        get
        {
            return PhotonNetworkManager.Instance.GetPlayerColor(_color);
        }
    }
    public int PlayerIndex { get { return _color; } }
    public GameObject ObjectHeld //automatically synchronized over network via DynamicObject
    {
        get { return _objectHeld; }
        set
        {
            if (value != _objectHeld)
            {
                _objectHeld = value;
                if (ObjectHeldChangedEvent != null) ObjectHeldChangedEvent(this, value);
            }
        }
    }
    public bool IsGazeOnFocalPoint
    {
        get
        {
            _photonPlayer.CustomProperties.TryGetValue(GameUtils.GAZE_ON_FOCALPOINT, out _temp);
            if (_temp != null)
            {
                return (bool) _temp;
            }
            else
            {
                return false;
            }
        }
    }
    public bool IsReady
    {
        get
        {
            _photonPlayer.CustomProperties.TryGetValue(GameUtils.READY, out _temp);
            if (_temp != null)
            {
                return (bool)_temp;
            }
            else
            {
                return false;
            }
        }
        set
        {
            Hashtable temp = new Hashtable() { { GameUtils.READY, value } };
            _photonPlayer.SetCustomProperties(temp);
        }
    }
    /*public bool IsHelper
    {
        get
        {
            _photonPlayer.CustomProperties.TryGetValue(GameUtils.HELPER, out _temp);
            if (_temp != null)
            {
                return (bool)_temp;
            }
            else
            {
                return false;
            }
        }
    }*/
    public float AspectRatio
    {
        get
        {
            _photonPlayer.CustomProperties.TryGetValue(GameUtils.ASPECT_RATIO, out _temp);
            if (_temp != null)
            {
                return (float) _temp;
            }
            else
            {
                return 0;
            }
        }
    }
    public float FieldOfView
    {
        get
        {
            _photonPlayer.CustomProperties.TryGetValue(GameUtils.FIELD_OF_VIEW, out _temp);
            if (_temp != null)
            {
                return (float) _temp;
            }
            else
            {
                return 0;
            }
        }
    }

    public virtual bool IsVoiceEnabled
    {
        get { return _isVoiceEnabled; }
        set
        {
            _isVoiceEnabled = value;
            MuteVoice(!value);
        }
    }
    private bool _isVoiceEnabled;
    private AudioSource _voiceAudioSource;

    protected Player _photonPlayer;
    protected int _actorNumber;
    protected int _color;
    protected object _temp;
    private GameObject _objectHeld;


    public PlayerInfo(string name, int actorNumber, GameObject player = null, GameObject gaze = null, bool isVoiceEnabled = false)
    {
        Name = name;
        ActorNumber = actorNumber;
        Player = player;
        Gaze = gaze;

        IsVoiceEnabled = isVoiceEnabled;

        PhotonNetwork.AddCallbackTarget(this);
    }

    public virtual void Dispose()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            switch (_color)
            {
                case 1:
                    PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { GameUtils.PLAYER_COLOR1, -1 } });
                    break;
                case 2:
                    PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { GameUtils.PLAYER_COLOR2, -1 } });
                    break;
                case 3:
                    PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { GameUtils.PLAYER_COLOR3, -1 } });
                    break;
            }
        }
        PlayerStateChangedEvent = null;
        ObjectHeldChangedEvent = null;
        ColorSetEvent = null;
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public virtual void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        if (target.Equals(_photonPlayer))
        {
            if (changedProps.TryGetValue(GameUtils.PLAYER_STATE, out _temp))
            {
                FirePlayerStateChangedEvent(this, (GameUtils.PlayerState)_temp);
            }
        }
    }

    public void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (_actorNumber != -1)
        {

            if (propertiesThatChanged.TryGetValue(GameUtils.PLAYER_COLOR1, out _temp) && (int)_temp == _actorNumber)
            {
                _color = 1;
                if (ColorSetEvent != null) ColorSetEvent(this, Color);
            }
            else if (propertiesThatChanged.TryGetValue(GameUtils.PLAYER_COLOR2, out _temp) && (int)_temp == _actorNumber)
            {
                _color = 2;
                if (ColorSetEvent != null) ColorSetEvent(this, Color);
            }
            else if (propertiesThatChanged.TryGetValue(GameUtils.PLAYER_COLOR3, out _temp) && (int)_temp == _actorNumber)
            {
                _color = 3;
                if (ColorSetEvent != null) ColorSetEvent(this, Color);
            }
        }
    }
    public void OnPlayerEnteredRoom(Player newPlayer) { }
    public void OnPlayerLeftRoom(Player otherPlayer) { }
    public void OnMasterClientSwitched(Player newMasterClient) { }

    protected void FirePlayerStateChangedEvent(PlayerInfo player, GameUtils.PlayerState newValue)
    {
        if (PlayerStateChangedEvent != null) PlayerStateChangedEvent(player, newValue);
    }

    private void MuteVoice(bool value)
    {
        if (Player != null)
        {
            if (_voiceAudioSource == null) _voiceAudioSource = Player.GetComponent<AudioSource>();
            if (_voiceAudioSource == null) return;

            _voiceAudioSource.mute = value;
        }

    }
}
