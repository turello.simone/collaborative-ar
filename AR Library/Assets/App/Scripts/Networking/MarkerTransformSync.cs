﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace Photon.Pun
{
    /// <summary>
    /// This class helps you to synchronize position, rotation and scale
    /// of a GameObject, relative to an anchor. It also gives you many different options to make
    /// the synchronized values appear smooth, even when the data is only
    /// send a couple of times per second.
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
    public class MarkerTransformSync : MonoBehaviour, IPunObservable
    {
        //The PositionModel, RotationModel and ScaleMode store the data you are able to
        //configure in the inspector while the "control" objects below are actually moving
        //the object and calculating all the inter- and extrapolation
        [Header("TransformView Models")]

        [SerializeField]
        public PhotonTransformViewPositionModel m_PositionModel = new PhotonTransformViewPositionModel();

        [SerializeField]
        public PhotonTransformViewRotationModel m_RotationModel = new PhotonTransformViewRotationModel();

        [SerializeField]
        public PhotonTransformViewScaleModel m_ScaleModel = new PhotonTransformViewScaleModel();

        protected MarkerTransformViewPositionControl m_PositionControl;
        protected MarkerTransformViewRotationControl m_RotationControl;
        protected PhotonTransformViewScaleControl m_ScaleControl;
        protected PhotonView _photonView;

        protected Pose _anchorPose;
        protected Matrix4x4 _worldToMarkerSpaceMatrix;
        protected ARLibrary _arLibrary;
        protected PhotonNetworkManager _networkManager;

        /// <summary>
        /// String identifying the anchor which we want to sync relative to. Initialized from the network manager.
        /// </summary>
        public string AnchorId { get; set; }

        protected bool m_ReceivedNetworkUpdate = false;
        /// <summary>
        /// Flag to skip initial data when Object is instantiated and rely on the first deserialized data instead.
        /// </summary>
        private bool m_firstTake = false;

        protected virtual void Awake()
        {
            _networkManager = PhotonNetworkManager.Instance;
            _arLibrary = ARManager.Instance.ARLibrary;
            AnchorId = _networkManager.AnchorId;
            _anchorPose = new Pose(Vector3.zero, Quaternion.identity);
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix = new Matrix4x4();
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            this._photonView = GetComponent<PhotonView>();
            this.m_PositionControl = new MarkerTransformViewPositionControl(this.m_PositionModel);
            this.m_RotationControl = new MarkerTransformViewRotationControl(this.m_RotationModel);
            this.m_ScaleControl = new PhotonTransformViewScaleControl(this.m_ScaleModel);

        }

        protected virtual void Start()
        {

        }

        protected virtual void OnEnable()
        {
            m_firstTake = true;
        }

        protected virtual void Update()
        {
            _arLibrary.GetAnchorPose(AnchorId, out _anchorPose);
            _worldToMarkerSpaceMatrix.SetTRS(_anchorPose.position, _anchorPose.rotation, Vector3.one);

            if (AnchorId == null || _photonView == null || _photonView.IsMine ||
                PhotonNetwork.IsConnectedAndReady == false)
            {
                return;
            }

            this.UpdatePosition(_worldToMarkerSpaceMatrix);
            this.UpdateRotation(_anchorPose.rotation);
            this.UpdateScale();
            //Debug.Log("MarkerTransformSync: position and rotation updated.");
        }


        protected void UpdatePosition(Matrix4x4 transformationMatrix)
        {
            if (this.m_PositionModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
            {
                return;
            }

            transform.position = this.m_PositionControl.UpdatePosition(transform.position, transformationMatrix);
        }

        protected void UpdateRotation(Quaternion referenceRotation)
        {
            if (this.m_RotationModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
            {
                return;
            }

            transform.rotation = this.m_RotationControl.GetRotation(transform.rotation, referenceRotation);
        }

        protected void UpdateScale()
        {
            if (this.m_ScaleModel.SynchronizeEnabled == false || this.m_ReceivedNetworkUpdate == false)
            {
                return;
            }

            transform.localScale = this.m_ScaleControl.GetScale(transform.localScale);
        }

        /// <summary>
        /// These values are synchronized to the remote objects if the interpolation mode
        /// or the extrapolation mode SynchronizeValues is used. Your movement script should pass on
        /// the current speed (in units/second) and turning speed (in angles/second) so the remote
        /// object can use them to predict the objects movement.
        /// </summary>
        /// <param name="speed">The current movement vector of the object in units/second.</param>
        /// <param name="turnSpeed">The current turn speed of the object in angles/second.</param>
        public void SetSynchronizedValues(Vector3 speed, float turnSpeed)
        {
            this.m_PositionControl.SetSynchronizedValues(speed, turnSpeed);
        }


        public virtual void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            this.m_PositionControl.OnPhotonSerializeView(transform.position, _worldToMarkerSpaceMatrix, stream, info);
            this.m_RotationControl.OnPhotonSerializeView(transform.rotation, _anchorPose.rotation, stream, info);
            this.m_ScaleControl.OnPhotonSerializeView(transform.localScale, stream, info);

            ForceInitialData(stream, _worldToMarkerSpaceMatrix, _anchorPose.rotation);
        }

        public virtual void ForceInitialData(PhotonStream stream, Matrix4x4 transformationMatrix, Quaternion referenceRotation)
        {
            if (stream.IsReading)
            {
                this.m_ReceivedNetworkUpdate = true;

                // force latest data to avoid initial drifts when player is instantiated.
                if (m_firstTake)
                {
                    m_firstTake = false;

                    if (this.m_PositionModel.SynchronizeEnabled)
                    {
                        this.transform.position = transformationMatrix.inverse.MultiplyPoint3x4(this.m_PositionControl.GetNetworkPosition());
                    }

                    if (this.m_RotationModel.SynchronizeEnabled)
                    {
                        this.transform.rotation = referenceRotation * this.m_RotationControl.GetNetworkRotation();
                    }

                    if (this.m_ScaleModel.SynchronizeEnabled)
                    {
                        this.transform.localScale = this.m_ScaleControl.GetNetworkScale();
                    }
                }
            }
        }

    }

    [System.Serializable]
    public class MarkerTransformViewPositionControl
    {
        PhotonTransformViewPositionModel m_Model;
        float m_CurrentSpeed;
        double m_LastSerializeTime;
        Vector3 m_SynchronizedSpeed = Vector3.zero;
        float m_SynchronizedTurnSpeed = 0;
        Matrix4x4 m_TransformationMatrix;
        /// <summary>
        /// Last Position sent/received from network (it is relative to a specific transform)
        /// </summary>
        Vector3 m_NetworkPosition;
        /// <summary>
        /// Last Position sent/received, converted to world space;
        ///it is updated every updatePosition even if no new network positions are received
        ///(based on the new transformation matrix) [the marker anchor estimated position
        ///(and its transformation matrix) may change in time]
        /// </summary>
        Vector3 m_Position; 
        Queue<Vector3> m_OldPositions = new Queue<Vector3>(); //Old positions (already converted to unity space)

        bool m_UpdatedPositionAfterOnSerialize = true;

        public MarkerTransformViewPositionControl(PhotonTransformViewPositionModel model)
        {
            m_Model = model;
        }

        Vector3 GetOldestStoredPosition()
        {
            Vector3 oldPosition = m_Position;

            if (m_OldPositions.Count > 0)
            {
                oldPosition = m_OldPositions.Peek();
            }

            return oldPosition;
        }

        /// <summary>
        /// These values are synchronized to the remote objects if the interpolation mode
        /// or the extrapolation mode SynchronizeValues is used. Your movement script should pass on
        /// the current speed (in units/second) and turning speed (in angles/second) so the remote
        /// object can use them to predict the objects movement.
        /// </summary>
        /// <param name="speed">The current movement vector of the object in units/second.</param>
        /// <param name="turnSpeed">The current turn speed of the object in angles/second.</param>
        public void SetSynchronizedValues(Vector3 speed, float turnSpeed)
        {
            m_SynchronizedSpeed = speed;
            m_SynchronizedTurnSpeed = turnSpeed;
        }

        /// <summary>
        /// Calculates the new position based on the values setup in the inspector
        /// </summary>
        /// <param name="currentPosition">The current position.</param>
        /// <param name="transformationMatrix">Transformation matrix which is used to calculate the new position.</param>
        /// <returns>The new position.</returns>
        public Vector3 UpdatePosition(Vector3 currentPosition, Matrix4x4 transformationMatrix)
        {
            Vector3 oldPos = m_Position;
            m_Position = transformationMatrix.MultiplyPoint3x4(GetNetworkPosition());
            Vector3 targetPosition = m_Position + GetExtrapolatedPositionOffset();

            switch (m_Model.InterpolateOption)
            {
                case PhotonTransformViewPositionModel.InterpolateOptions.Disabled:
                    if (m_UpdatedPositionAfterOnSerialize == false)
                    {
                        currentPosition = targetPosition;
                        m_UpdatedPositionAfterOnSerialize = true;
                    }
                    break;

                case PhotonTransformViewPositionModel.InterpolateOptions.FixedSpeed:
                    currentPosition = Vector3.MoveTowards(currentPosition, targetPosition, Time.deltaTime * m_Model.InterpolateMoveTowardsSpeed);
                    break;

                case PhotonTransformViewPositionModel.InterpolateOptions.EstimatedSpeed:
                    if (m_OldPositions.Count == 0)
                    {
                        // special case: we have no previous updates in memory, so we can't guess a speed!
                        break;
                    }

                    if (m_TransformationMatrix != transformationMatrix)
                    {
                        currentPosition = Vector3.MoveTowards(currentPosition, targetPosition, Vector3.Distance(oldPos, m_Position));
                    }
                    else
                    {
                        // knowing the last (incoming) position and the one before, we can guess a speed.
                        // note that the speed is times sendRateOnSerialize! we send X updates/sec, so our estimate has to factor that in.
                        var estimatedSpeed = Vector3.Distance(m_Position, GetOldestStoredPosition()) /
                                             m_OldPositions.Count * PhotonNetwork.SerializationRate;

                        // move towards the targetPosition (including estimates, if that's active) with the speed calculated from the last updates.
                        currentPosition = Vector3.MoveTowards(currentPosition, targetPosition, Time.deltaTime * estimatedSpeed);
                    }
                    break;

                case PhotonTransformViewPositionModel.InterpolateOptions.SynchronizeValues:
                    if (m_SynchronizedSpeed.magnitude == 0)
                    {
                        currentPosition = targetPosition;
                    }
                    else
                    {
                        currentPosition = Vector3.MoveTowards(currentPosition, targetPosition, Time.deltaTime * m_SynchronizedSpeed.magnitude);
                    }

                    break;

                case PhotonTransformViewPositionModel.InterpolateOptions.Lerp:
                    currentPosition = Vector3.Lerp(currentPosition, targetPosition, Time.deltaTime * m_Model.InterpolateLerpSpeed);
                    break;
            }

            if (m_Model.TeleportEnabled == true)
            {
                if (Vector3.Distance(currentPosition, m_Position) > m_Model.TeleportIfDistanceGreaterThan)
                {
                    currentPosition = m_Position;
                    //Debug.Log("Teleport");
                }
            }

            m_TransformationMatrix = transformationMatrix;
            return currentPosition;
        }

        /// <summary>
        /// Gets the last position that was received through the network
        /// </summary>
        /// <returns></returns>
        public Vector3 GetNetworkPosition()
        {
            return m_NetworkPosition;
        }

        /// <summary>
        /// Calculates an estimated position based on the last synchronized position,
        /// the time when the last position was received and the movement speed of the object
        /// </summary>
        /// <returns>Estimated position of the remote object</returns>
        public Vector3 GetExtrapolatedPositionOffset()
        {
            float timePassed = (float)(PhotonNetwork.Time - m_LastSerializeTime);

            if (m_Model.ExtrapolateIncludingRoundTripTime == true)
            {
                timePassed += (float)PhotonNetwork.GetPing() / 1000f;
            }

            Vector3 extrapolatePosition = Vector3.zero;

            switch (m_Model.ExtrapolateOption)
            {
                case PhotonTransformViewPositionModel.ExtrapolateOptions.SynchronizeValues:
                    Quaternion turnRotation = Quaternion.Euler(0, m_SynchronizedTurnSpeed * timePassed, 0);
                    extrapolatePosition = turnRotation * (m_SynchronizedSpeed * timePassed);
                    break;
                case PhotonTransformViewPositionModel.ExtrapolateOptions.FixedSpeed:
                    Vector3 moveDirection = (m_Position - GetOldestStoredPosition()).normalized;

                    extrapolatePosition = moveDirection * m_Model.ExtrapolateSpeed * timePassed;
                    break;
                case PhotonTransformViewPositionModel.ExtrapolateOptions.EstimateSpeedAndTurn:
                    Vector3 moveDelta = (m_Position - GetOldestStoredPosition()) * PhotonNetwork.SerializationRate;
                    extrapolatePosition = moveDelta * timePassed;
                    break;
            }

            return extrapolatePosition;
        }

        public void OnPhotonSerializeView(Vector3 currentPosition, Matrix4x4 transformationMatrix, PhotonStream stream, PhotonMessageInfo info)
        {
            if (m_Model.SynchronizeEnabled == false)
            {
                return;
            }

            if (stream.IsWriting == true)
            {
                SerializeData(currentPosition, transformationMatrix, stream, info);
            }
            else
            {
                DeserializeData(transformationMatrix, stream, info);
            }

            m_LastSerializeTime = PhotonNetwork.Time;
            m_UpdatedPositionAfterOnSerialize = false;
        }

        void SerializeData(Vector3 currentPosition, Matrix4x4 transformationMatrix, PhotonStream stream, PhotonMessageInfo info)
        {
            stream.SendNext(transformationMatrix.inverse.MultiplyPoint3x4(currentPosition));
            m_NetworkPosition = transformationMatrix.inverse.MultiplyPoint3x4(currentPosition);
            m_Position = currentPosition;
            //Debug.Log("MarkerTransformSync: position sent.");

            if (m_Model.ExtrapolateOption == PhotonTransformViewPositionModel.ExtrapolateOptions.SynchronizeValues ||
                m_Model.InterpolateOption == PhotonTransformViewPositionModel.InterpolateOptions.SynchronizeValues)
            {
                stream.SendNext(m_SynchronizedSpeed);
                stream.SendNext(m_SynchronizedTurnSpeed);
            }
        }

        void DeserializeData(Matrix4x4 transformationMatrix, PhotonStream stream, PhotonMessageInfo info)
        {
            Vector3 readPosition = (Vector3)stream.ReceiveNext();
            //Debug.Log("MarkerTransformSync: position received." + readPosition);

            if (m_Model.ExtrapolateOption == PhotonTransformViewPositionModel.ExtrapolateOptions.SynchronizeValues ||
                m_Model.InterpolateOption == PhotonTransformViewPositionModel.InterpolateOptions.SynchronizeValues)
            {
                m_SynchronizedSpeed = (Vector3)stream.ReceiveNext();
                m_SynchronizedTurnSpeed = (float)stream.ReceiveNext();
            }

            if (m_OldPositions.Count == 0)
            {
                // if we don't have old positions yet, this is the very first update this client reads. let's use this as current AND old position.
                m_Position = transformationMatrix.MultiplyPoint3x4(readPosition);
                m_NetworkPosition = readPosition;
            }

            // the previously received position becomes the old(er) one and queued. the new one is the m_NetworkPosition
            m_OldPositions.Enqueue(m_Position);
            m_Position = transformationMatrix.MultiplyPoint3x4(readPosition);
            m_NetworkPosition = readPosition;

            // reduce items in queue to defined number of stored positions.
            while (m_OldPositions.Count > m_Model.ExtrapolateNumberOfStoredPositions)
            {
                m_OldPositions.Dequeue();
            }
        }

    }


    [System.Serializable]
    public class MarkerTransformViewRotationControl
    {
        PhotonTransformViewRotationModel m_Model;
        Quaternion m_NetworkRotation;

        public MarkerTransformViewRotationControl(PhotonTransformViewRotationModel model)
        {
            m_Model = model;
        }

        /// <summary>
        /// Gets the last rotation that was received through the network
        /// </summary>
        /// <returns></returns>
        public Quaternion GetNetworkRotation()
        {
            return m_NetworkRotation;
        }

        public Quaternion GetRotation(Quaternion currentRotation, Quaternion referenceRotation)
        {
            Quaternion rotation = referenceRotation * m_NetworkRotation;
            switch (m_Model.InterpolateOption)
            {
                default:
                case PhotonTransformViewRotationModel.InterpolateOptions.Disabled:
                    return rotation;
                case PhotonTransformViewRotationModel.InterpolateOptions.RotateTowards:
                    return Quaternion.RotateTowards(currentRotation, rotation, m_Model.InterpolateRotateTowardsSpeed * Time.deltaTime);
                case PhotonTransformViewRotationModel.InterpolateOptions.Lerp:
                    return Quaternion.Lerp(currentRotation, rotation, m_Model.InterpolateLerpSpeed * Time.deltaTime);
            }
        }

        public void OnPhotonSerializeView(Quaternion currentRotation, Quaternion referenceRotation, PhotonStream stream, PhotonMessageInfo info)
        {
            if (m_Model.SynchronizeEnabled == false)
            {
                return;
            }

            if (stream.IsWriting == true)
            {
                Quaternion localRotation = Quaternion.Inverse(referenceRotation) * currentRotation;
                stream.SendNext(localRotation);
                m_NetworkRotation = localRotation;
            }
            else
            {
                m_NetworkRotation = (Quaternion)stream.ReceiveNext();
            }
        }
    }

}