﻿using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using Photon.Voice.PUN;
using Photon.Voice.Unity;
using UnityEngine;

public class LocalPlayerInfo : PlayerInfo
{
    public override string Name
    {
        get { return _name;}
        set
        {
            if (value != null)
            {
                PhotonNetwork.LocalPlayer.NickName = value;
            }
            _name = value;
        }
    }

    public override int ActorNumber
    {
        get
        {
            if (_actorNumber == -1)
            {
                _actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            }
            return _actorNumber;
        }
        set
        {
            _actorNumber = value;
            _photonPlayer = PhotonNetwork.LocalPlayer;

            if (value != -1)
            {
                TrySetColor();
            }
        }
    }
    public override bool IsMasterClient
    {
        get { return PhotonNetwork.IsMasterClient; }
    }
    public new GameUtils.PlayerState State
    {
        get
        {
            if (!PhotonNetwork.InRoom)
            {
                return GameUtils.PlayerState.Error;
            }
            return _playerState;
        }
        set
        {
            _playerState = value;
            Debug.Log("LocalPlayer: State changed to " + value.ToString());
            _props = new Hashtable() { { GameUtils.PLAYER_STATE, value } };
            _photonPlayer.SetCustomProperties(_props);
            FirePlayerStateChangedEvent(this, value);
        }
    }
    public new bool IsGazeOnFocalPoint
    {
        get
        {
            return (bool)_isGazeOnFocalPoint;
        }
        set
        {
            _isGazeOnFocalPoint = value;
            _props = new Hashtable() { { GameUtils.GAZE_ON_FOCALPOINT, value } };
            _photonPlayer.SetCustomProperties(_props);
        }
    }
    public new bool IsReady
    {
        get
        {
            return (bool)_isReady;
        }
        set
        {
            if (_isReady == value) return;
            
            _isReady = value;
            _props = new Hashtable() { { GameUtils.READY, value } };
            _photonPlayer.SetCustomProperties(_props);
        }
    }
    /*public new bool IsHelper
    {
        get
        {
            return (bool)_isHelper;
        }
        set
        {
            if (_isHelper == value) return;

            _isHelper = value;
            _props = new Hashtable() { { GameUtils.HELPER, value } };
            _photonPlayer.SetCustomProperties(_props);
        }
    }*/
    public new float AspectRatio
    {
        get
        {
            return _aspectRatio;
        }
        set
        {
            _aspectRatio = value;
            _props = new Hashtable() { { GameUtils.ASPECT_RATIO, value } };
            _photonPlayer.SetCustomProperties(_props);
        }
    }
    public new float FieldOfView
    {
        get
        {
            return _fieldOfView;
        }
        set
        {
            _fieldOfView = value;
            _props = new Hashtable() { { GameUtils.FIELD_OF_VIEW, value } };
            _photonPlayer.SetCustomProperties(_props);
        }
    }

    public override bool IsVoiceEnabled {
        get { return base.IsVoiceEnabled; }
        set
        {
            if(PhotonVoiceNetwork.Instance != null && PhotonVoiceNetwork.Instance.PrimaryRecorder != null)
                PhotonVoiceNetwork.Instance.PrimaryRecorder.DebugEchoMode = value;
            base.IsVoiceEnabled = value;
        }
    }

    public bool IsVoiceTransmissionEnabled
    {
        get { return _isVoiceTransmissionEnabled; }
        set
        {
            if (PhotonVoiceNetwork.Instance == null || PhotonVoiceNetwork.Instance.PrimaryRecorder == null) return;

            PhotonVoiceNetwork.Instance.PrimaryRecorder.TransmitEnabled = true;
            PhotonVoiceNetwork.Instance.PrimaryRecorder.AutoStart = value;
            if (PhotonVoiceNetwork.Instance.PrimaryRecorder.IsInitialized)
                PhotonVoiceNetwork.Instance.PrimaryRecorder.IsRecording = value;
            _isVoiceTransmissionEnabled = value;
        }
    }
    private bool _isVoiceTransmissionEnabled;

    private string _name;
    private GameUtils.PlayerState _playerState;
    private Hashtable _props;
    private bool _isGazeOnFocalPoint;
    private bool _isReady;
    //private bool _isHelper;
    private float _aspectRatio;
    private float _fieldOfView;

    public LocalPlayerInfo(string name = null, int actorNumber = -1, GameObject player = null, GameObject gaze = null,
        bool isVoiceEnabled = false, bool isVoiceTransmissionEnabled = false) : base(name, actorNumber, player, gaze, isVoiceEnabled)
    {
        IsVoiceTransmissionEnabled = isVoiceTransmissionEnabled;
        PhotonNetwork.NetworkingClient.OpResponseReceived += NetworkingClientOnOpResponseReceived;
    }

    public override void Dispose()
    {
        base.Dispose();
        PhotonNetwork.NetworkingClient.OpResponseReceived -= NetworkingClientOnOpResponseReceived;
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        if (target.Equals(_photonPlayer))
        {
            if (changedProps.TryGetValue(GameUtils.READY, out _temp))
            {
                _isReady = (bool) _temp;
            }
        }
    }

    private void TrySetColor()
    {
        if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.PLAYER_COLOR1, out _temp) &&
            (int)_temp == -1)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { GameUtils.PLAYER_COLOR1, _actorNumber } },
                new Hashtable() { { GameUtils.PLAYER_COLOR1, -1 } });
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.PLAYER_COLOR2, out _temp) &&
                 (int)_temp == -1)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { GameUtils.PLAYER_COLOR2, _actorNumber } },
                new Hashtable() { { GameUtils.PLAYER_COLOR2, -1 } });
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue(GameUtils.PLAYER_COLOR3, out _temp) &&
                 (int)_temp == -1)
        {
            PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() { { GameUtils.PLAYER_COLOR3, _actorNumber } },
                new Hashtable() { { GameUtils.PLAYER_COLOR3, -1 } });
        }
    }

    private void NetworkingClientOnOpResponseReceived(OperationResponse opResponse)
    {
        if (opResponse.OperationCode == OperationCode.SetProperties &&
            opResponse.ReturnCode == ErrorCode.InvalidOperation && _color == 0)
        {
            TrySetColor();
        }
    }

    public void BecomeMasterClient()
    {
        PhotonNetwork.SetMasterClient(PhotonNetwork.LocalPlayer);
    }

}
