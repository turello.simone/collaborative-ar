﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INetworkManager {

    event GameUtils.StaticObjectSpawned StaticObjectSpawnedEvent;
    event GameUtils.MeshReceived MeshReceivedEvent;

    string AnchorId { get; set; }
    bool IsMasterClient { get; }
    LocalPlayerInfo LocalPlayer { get; set; }
    Dictionary<int, PlayerInfo> OtherPlayers { get; set; }
    //Dictionary<int, GameObject> StaticObjects { get; set; }



    /// <summary>
    /// Create a static object synchronized between players (relative to selected anchor). 
    /// </summary>
    /// <param name="prefabName">Name of the object.</param>
    /// <param name="objectPose">Position and rotation of the object.</param>
    void SpawnStaticObject(string prefabName, Pose objectPose);

    /// <summary>
    /// Send a mesh through network to all the other players and its transform relative to selected anchor. 
    /// </summary>
    /// <param name="meshObjectName">Name of the mesh.</param>
    /// <param name="mesh">The mesh object.</param>
    /// <param name="meshTransform">Transform of the mesh.</param>
    void SendMesh(string meshObjectName, Mesh mesh, Transform meshTransform);

}
