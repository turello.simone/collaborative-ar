﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MyUtils.Singleton<Loader>
{
    [MustBeAssigned] public GameObject Canvas;
    [MustBeAssigned] public TextMeshProUGUI Text;
    [MustBeAssigned] public Slider Slider;

    public enum Scene
    {
        MainMenu,
        ARScene
    }

    public override void Awake()
    {
        base.Awake();
        Canvas.SetActive(false);
    }

    public void LoadScene(Scene scene)
    {
        Slider.value = 0;
        Canvas.SetActive(true);
        StartCoroutine(LoadSceneAsync(scene));
    }

    public void StartLoadingBar()
    {
        Slider.value = 0;
        Canvas.SetActive(true);
        StartCoroutine(StartLoadingBarAsync());
    }

    private IEnumerator LoadSceneAsync(Scene scene)
    {
        PhotonNetwork.LoadLevel(scene.ToString());

        while (PhotonNetwork.LevelLoadingProgress < 1f)
        {
            Slider.value = PhotonNetwork.LevelLoadingProgress/0.9f;
            yield return null;
        }
        Canvas.SetActive(false);
    }

    private IEnumerator StartLoadingBarAsync()
    {
        while (PhotonNetwork.LevelLoadingProgress < 1f)
        {
            Slider.value = PhotonNetwork.LevelLoadingProgress/0.9f;
            yield return null;
        }
        Canvas.SetActive(false);
    }

}
