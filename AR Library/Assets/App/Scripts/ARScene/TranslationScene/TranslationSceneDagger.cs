﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationSceneDagger : DynamicObject
{
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TranslationSceneDagger = this;
    }
}
