﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationScene : IGameScene
{
    public TranslationScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TranslationSceneContraption == null)
                _networkManager.SpawnStaticObject(_gameManager.TranslationSceneContraptionPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TranslationSceneContraption.Reset();

            if (_networkManager.CurrentRoom.TranslationScenePainting == null)
                _networkManager.SpawnStaticObject(_gameManager.TranslationScenePaintingPrefab.name, _networkManager.CurrentRoom.PaintingPose);
            else
                _networkManager.CurrentRoom.TranslationScenePainting.Reset();

            if (_networkManager.CurrentRoom.TranslationSceneCandleHolder == null)
                _networkManager.SpawnDynamicObject(_gameManager.TranslationSceneCandleHolderPrefab.name, _networkManager.CurrentRoom.CandleHolderPose);
            else
                _networkManager.CurrentRoom.TranslationSceneCandleHolder.Reset();

            if (_networkManager.CurrentRoom.TranslationSceneChalice == null)
                _networkManager.SpawnDynamicObject(_gameManager.TranslationSceneChalicePrefab.name, _networkManager.CurrentRoom.ChalicePose);
            else
                _networkManager.CurrentRoom.TranslationSceneChalice.Reset();

            if (_networkManager.CurrentRoom.TranslationSceneDagger == null)
                _networkManager.SpawnDynamicObject(_gameManager.TranslationSceneDaggerPrefab.name, _networkManager.CurrentRoom.DaggerPose);
            else
                _networkManager.CurrentRoom.TranslationSceneDagger.Reset();

            if (_networkManager.CurrentRoom.TranslationSceneSkull == null)
                _networkManager.SpawnDynamicObject(_gameManager.TranslationSceneSkullPrefab.name, _networkManager.CurrentRoom.SkullPose);
            else
                _networkManager.CurrentRoom.TranslationSceneSkull.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TranslationScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        ARManager.Instance.EnableHoleInOcclusionWithShadows(true, _networkManager.CurrentRoom.TranslationScenePainting.OcclusionHoleRadius);

        bool completed;
        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PaintingOpening,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingOpening);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PaintingOpening,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingOpening);
        }

        if (_networkManager.IsMasterClient &&
            _networkManager.CurrentRoom.TranslationScenePainting.State == TranslationScenePainting.PaintingState.Tridimensional)
        {
            _networkManager.CurrentRoom.TranslationScenePainting.State = TranslationScenePainting.PaintingState.Open;
        }

        ObjectManipulation.Instance.InteractionEnabled = true;

        yield return new WaitUntil(() =>
            _networkManager.CurrentRoom.TranslationSceneContraption.State >=
            TranslationSceneContraption.ContraptionState.RightPassword);

        ObjectManipulation.Instance.InteractionEnabled = false;

        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening);
            yield return new WaitUntil(() => _networkManager.CurrentRoom.GetGazeConvergenceStatus(
                                                 GazeConvergenceFocalPoint.Type.ContraptionFinalOpening,
                                                 out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening);
        }

        if (_networkManager.IsMasterClient &&
            _networkManager.CurrentRoom.TranslationSceneContraption.State == TranslationSceneContraption.ContraptionState.RightPassword)
        {
            _networkManager.CurrentRoom.TranslationSceneContraption.State = TranslationSceneContraption.ContraptionState.Open;
        }

        yield return new WaitForSeconds(5f);
        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/SceneCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public override void Destroy()
    {
        base.Destroy();
        if (FocalPointManager.Instance != null) FocalPointManager.Instance.DisableCurrentGazeConvergenceFocalPoint();
        if (ARManager.Instance != null) ARManager.Instance.EnableHoleInOcclusionWithShadows(false);
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneContraption != null)
            PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneContraption.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TranslationScenePainting != null)
            PhotonNetworkManager.Instance.CurrentRoom.TranslationScenePainting.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneCandleHolder != null)
            PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneCandleHolder.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneChalice != null)
            PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneChalice.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneDagger != null)
            PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneDagger.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneSkull != null)
            PhotonNetworkManager.Instance.CurrentRoom.TranslationSceneSkull.Enable(enabled);
    }

}
