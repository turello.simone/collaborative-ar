﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationSceneSkull : DynamicObject
{
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TranslationSceneSkull = this;
    }
}
