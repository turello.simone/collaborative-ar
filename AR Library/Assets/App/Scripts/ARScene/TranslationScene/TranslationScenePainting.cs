﻿using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TranslationScenePainting : StaticObject
{
    public enum PaintingState
    {
        Default,
        Tridimensional,
        Open
    }

    #region Attributes

    [Header("Painting properties")]
    [MustBeAssigned] public GameObject LeftEye;
    [MustBeAssigned] public GameObject RightEye;
    [MustBeAssigned] public GameObject Disk;
    [MustBeAssigned] public GameObject DiskHandle;
    [MustBeAssigned] public MeshRenderer CthulhuHoleRenderer;

    [MustBeAssigned] public GameObject FocalPoint;
    [MustBeAssigned] public Animator Animator;

    public float OcclusionHoleRadius = 0.75f;

    public PaintingState State
    {
        get { return (PaintingState)_state.Value; }
        set { _state.Value = (int)value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;

    public float DiskAdjustmentSpeed = 2f;

    private PhotonView _diskPhotonView;
    private bool _isDiskInteracted;
    private const float _diskAngleStep = 360f / 26f;
    private const float _uvStep = 1f / 26f;
    private readonly int _rotationHash = Animator.StringToHash("Rotation");
    private Vector3 _leftEyeStartRelativePosition;
    private Vector3 _rightEyeStartRelativePosition;


    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();

        _diskPhotonView = Disk.GetComponent<PhotonView>();

        _state = new SyncVarInt((int)PaintingState.Tridimensional, _photonView, STATEID);
        OnPaintingStateChange((int)PaintingState.Tridimensional);
        _state.ValueChanged += OnPaintingStateChange;

        _isDiskInteracted = false;

        _leftEyeStartRelativePosition = LeftEye.transform.localPosition;
        _rightEyeStartRelativePosition = RightEye.transform.localPosition;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TranslationScenePainting = this;

        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingOpening, FocalPoint);
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();

        Disk.transform.localRotation = Quaternion.identity;
        CthulhuHoleRenderer.material.SetTextureOffset("_MainTex", Vector2.zero);

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingOpening);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingOpening, FocalPoint);

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PaintingOpening);
            State = PaintingState.Tridimensional;
        }
    }

    private void Update()
    {
        if (_networkManager.LocalPlayer == null || _networkManager.LocalPlayer.Player == null) return;

        if (State != PaintingState.Default)
        {
            Vector3 leftEyePos = LeftEye.transform.parent.position +
                                 (_leftEyeStartRelativePosition.x * LeftEye.transform.parent.right +
                                  _leftEyeStartRelativePosition.y * LeftEye.transform.parent.up +
                                  _leftEyeStartRelativePosition.z * LeftEye.transform.parent.forward);
            Vector3 rightEyePos = RightEye.transform.parent.position +
                                  (_rightEyeStartRelativePosition.x * RightEye.transform.parent.right +
                                   _rightEyeStartRelativePosition.y * RightEye.transform.parent.up +
                                   _rightEyeStartRelativePosition.z * RightEye.transform.parent.forward);
            Debug.DrawLine(leftEyePos, Camera.main.transform.position);

            LeftEye.transform.rotation = Quaternion.LookRotation(
                Camera.main.transform.position - leftEyePos, transform.forward);
            RightEye.transform.rotation = Quaternion.LookRotation(
                Camera.main.transform.position - rightEyePos, transform.forward);

            ARManager.Instance.SetOcclusionWithShadowsHolePosition(transform.position);
        }

        if (State == PaintingState.Open)
        {
            float diskXRot = GameUtils.GetSignedAngle(Quaternion.identity, Disk.transform.localRotation, Vector3.right); //angle in range (-180;180)
            if (diskXRot < 0) diskXRot += 360; //angle in range (0, 360)

            Animator.SetFloat(_rotationHash, diskXRot / 360f);

            float offset;
            if (diskXRot < 360 - _diskAngleStep)
            {
                offset = Mathf.Lerp(0, 0.893f, diskXRot / (360f - _diskAngleStep));
            }
            else
            {
                offset = Mathf.Lerp(0.893f, 1, (diskXRot - 360f + _diskAngleStep) / _diskAngleStep);
            }
            CthulhuHoleRenderer.material.SetTextureOffset("_MainTex", new Vector2(0, offset));

            if (!_isDiskInteracted && _diskPhotonView.IsMine)
            {
                int div = Mathf.RoundToInt(diskXRot / _diskAngleStep);
                
                Disk.transform.localRotation = Quaternion.RotateTowards(Disk.transform.localRotation,
                    Quaternion.Euler(_diskAngleStep * div, 0, 0), DiskAdjustmentSpeed * Time.deltaTime);
            }
        }
    }

    #endregion


    #region Public methods

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (isPressed)
        {
            if (interactiveObject == DiskHandle)
            {
                _isDiskInteracted = true;
                if (!_diskPhotonView.IsMine) _diskPhotonView.RequestOwnership();
            }
        }
        else
        {
            _isDiskInteracted = false;
        }
    }

    public override void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        if (State == PaintingState.Open && interactiveObject == DiskHandle && _isDiskInteracted)
        {
            Vector2 diskCenterScreenPos = Camera.main.WorldToScreenPoint(Disk.transform.position);
            Vector2 startScreenPos = screenPos - deltaMovement;
            Vector2 destScreenPos = screenPos;

            Vector2 startVector = startScreenPos - diskCenterScreenPos;
            Vector2 destVector = destScreenPos - diskCenterScreenPos;

            float angle = Vector2.SignedAngle(startVector, destVector);

            Disk.transform.localRotation *= Quaternion.Euler(-2*angle, 0, 0);
        }
    }

    #endregion


    #region Private methods

    private void OnPaintingStateChange(int value)
    {
        switch ((PaintingState)value)
        {
            case PaintingState.Tridimensional:
                break;
            case PaintingState.Open:
                Animator.SetTrigger("Open");
                break;
        }
    }

    #endregion
}
