﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Analytics;

public class TranslationSceneContraption : StaticObject
{
    public enum ContraptionState
    {
        Default,
        Closed,
        RightPassword,
        Open
    }


    [Serializable] public struct QuadrantStruct
    {
        [MustBeAssigned] public MeshRenderer Renderer;
        [MustBeAssigned] public GameObject ButtonDown;
        [MustBeAssigned] public GameObject ButtonUp;
        [ReadOnly] public Letter Letter;
    }

    public enum Letter
    {
        A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
    }

    private class Quadrant
    {
        public string Id;
        public SyncButton ButtonDown;
        public SyncButton ButtonUp;

        public Letter Letter;
        public Material Material;
        public float Timer;
        public Coroutine Coroutine;

        public Quadrant(string id, GameObject buttonDown, GameObject buttonUp, Material material)
        {
            Id = id;
            ButtonDown = new SyncButton(buttonDown);
            ButtonUp = new SyncButton(buttonUp);
            Material = material;
            Timer = 0f;

            Letter = Letter.A;
            Material.SetTextureOffset("_MainTex", Vector2.zero);
        }
    }



    #region Attributes

    [Header("Contraption properties")]

    public QuadrantStruct Quadrant1;
    public QuadrantStruct Quadrant2;
    public QuadrantStruct Quadrant3;
    public QuadrantStruct Quadrant4;

    [MustBeAssigned] public GameObject FocalPoint;
    [MustBeAssigned] public Animator Animator;

    public float TimeNeededToChangeLetter = 0.5f;
    public float TimeBeforeDoubleSpeed = 1f;
    public float TimeNeededToConfirmPassword = 2f;

    public ContraptionState State
    {
        get { return (ContraptionState)_state.Value; }
        set { _state.Value = (int)value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;

    private float _normalOffset = 0.038086f;
    private float _AZOffset = 0.047607f;
    private float _passwordTimer;

    private Quadrant _quadrant1;
    private const string QUADRANT1 = "Quadrant1";
    private Quadrant _quadrant2;
    private const string QUADRANT2 = "Quadrant2";
    private Quadrant _quadrant3;
    private const string QUADRANT3 = "Quadrant3";
    private Quadrant _quadrant4;
    private const string QUADRANT4 = "Quadrant4";

    private Quadrant[] _quadrants; 

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();

        _quadrant1 = new Quadrant(QUADRANT1, Quadrant1.ButtonDown, Quadrant1.ButtonUp, Quadrant1.Renderer.material);
        _quadrant2 = new Quadrant(QUADRANT2, Quadrant2.ButtonDown, Quadrant2.ButtonUp, Quadrant2.Renderer.material);
        _quadrant3 = new Quadrant(QUADRANT3, Quadrant3.ButtonDown, Quadrant3.ButtonUp, Quadrant3.Renderer.material);
        _quadrant4 = new Quadrant(QUADRANT4, Quadrant4.ButtonDown, Quadrant4.ButtonUp, Quadrant4.Renderer.material);

        _quadrants = new Quadrant[4];
        _quadrants[0] = _quadrant1;
        _quadrants[1] = _quadrant2;
        _quadrants[2] = _quadrant3;
        _quadrants[3] = _quadrant4;

        _state = new SyncVarInt((int)ContraptionState.Closed, _photonView, STATEID);
        OnContraptionStateChange((int)ContraptionState.Closed);
        _state.ValueChanged += OnContraptionStateChange;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TranslationSceneContraption = this;

        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening, FocalPoint);
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening, FocalPoint);
        
        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.ContraptionFinalOpening);

            _quadrant1.ButtonDown.Release(true);
            _quadrant1.ButtonUp.Release(true);

            _quadrant2.ButtonDown.Release(true);
            _quadrant2.ButtonUp.Release(true);

            _quadrant3.ButtonDown.Release(true);
            _quadrant3.ButtonUp.Release(true);

            _quadrant4.ButtonDown.Release(true);
            _quadrant4.ButtonUp.Release(true);

            _photonView.RPC("ChangeLetterRPC", RpcTarget.AllBuffered, _quadrant1.Id, Letter.A, 0f, true);
            _photonView.RPC("ChangeLetterRPC", RpcTarget.AllBuffered, _quadrant2.Id, Letter.A, 0f, true);
            _photonView.RPC("ChangeLetterRPC", RpcTarget.AllBuffered, _quadrant3.Id, Letter.A, 0f, true);
            _photonView.RPC("ChangeLetterRPC", RpcTarget.AllBuffered, _quadrant4.Id, Letter.A, 0f, true);

            State = ContraptionState.Closed;
        }
    }

    private void Update()
    {
        if (_networkManager.IsMasterClient)
        {
            foreach (var quadrant in _quadrants)
            {
                if (quadrant.ButtonDown.IsPushed && !quadrant.ButtonUp.IsPushed)
                {
                    quadrant.Timer += Time.deltaTime;

                    if (quadrant.Coroutine == null)
                        ChangeLetterToPrevious(quadrant, (quadrant.Timer > TimeBeforeDoubleSpeed));

                }
                else if (!quadrant.ButtonDown.IsPushed && quadrant.ButtonUp.IsPushed)
                {
                    quadrant.Timer += Time.deltaTime;

                    if (quadrant.Coroutine == null)
                        ChangeLetterToNext(quadrant, (quadrant.Timer > TimeBeforeDoubleSpeed));
                }
                else
                {
                    quadrant.Timer = 0f;
                }
            }

            if (_quadrant1.Letter == Letter.R && _quadrant2.Letter == Letter.L && _quadrant3.Letter == Letter.U &&
                _quadrant4.Letter == Letter.H)
            {
                _passwordTimer += Time.deltaTime;
                if (_passwordTimer > TimeNeededToConfirmPassword)
                {
                    State = ContraptionState.RightPassword;
                }
            }
            else
            {
                _passwordTimer = 0f;
            }
        }
    }

    #endregion


    #region Public methods

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        SyncButton button = null;

        if (interactiveObject == _quadrant1.ButtonDown.GameObject)
        {
            button = _quadrant1.ButtonDown;
        }
        else if (interactiveObject == _quadrant1.ButtonUp.GameObject)
        {
            button = _quadrant1.ButtonUp;
        }


        else if (interactiveObject == _quadrant2.ButtonDown.GameObject)
        {
            button = _quadrant2.ButtonDown;
        }
        else if (interactiveObject == _quadrant2.ButtonUp.GameObject)
        {
            button = _quadrant2.ButtonUp;
        }


        else if (interactiveObject == _quadrant3.ButtonDown.GameObject)
        {
            button = _quadrant3.ButtonDown;
        }
        else if (interactiveObject == _quadrant3.ButtonUp.GameObject)
        {
            button = _quadrant3.ButtonUp;
        }


        else if (interactiveObject == _quadrant4.ButtonDown.GameObject)
        {
            button = _quadrant4.ButtonDown;
        }
        else if (interactiveObject == _quadrant4.ButtonUp.GameObject)
        {
            button = _quadrant4.ButtonUp;
        }

        if (button != null)
        {
            if(isPressed) button.Push();
            else button.Release();
        }

    }

    #endregion


    #region Private methods

    private void OnContraptionStateChange(int value)
    {
        switch ((ContraptionState) value)
        {
            case ContraptionState.Closed:
                break;
            case ContraptionState.RightPassword:
                break;
            case ContraptionState.Open:
                Animator.SetTrigger("Open");
                break;
        }
    }

    private void ChangeLetterToNext(Quadrant quadrant, bool isFast)
    {
        float offset = quadrant.Material.GetTextureOffset("_MainTex").y;
        if (quadrant.Letter == Letter.Z)
        {
            offset -= _AZOffset;
        }
        else
        {
            offset -= _normalOffset;
        }
        Letter letter = (Letter)(((int)quadrant.Letter + 1) % 26);

        _photonView.RPC("ChangeLetterRPC", RpcTarget.AllBuffered, quadrant.Id, letter, offset, isFast);
    }

    private void ChangeLetterToPrevious(Quadrant quadrant, bool isFast)
    {
        float offset = quadrant.Material.GetTextureOffset("_MainTex").y;
        if (quadrant.Letter == Letter.A)
        {
            offset += _AZOffset;
        }
        else
        {
            offset += _normalOffset;
        }
        Letter letter = (Letter)(((int)quadrant.Letter + 26 - 1) % 26);

        _photonView.RPC("ChangeLetterRPC", RpcTarget.AllBuffered, quadrant.Id, letter, offset, isFast);
    }

    private IEnumerator ChangeLetterCoroutine(Quadrant quadrant, Letter letter, float targetUvOffset, bool isFast)
    {
        float startOffset = quadrant.Material.GetTextureOffset("_MainTex").y;
        float completionPercentage = 0f;
        float speed = 1f / TimeNeededToChangeLetter;

        if (isFast)
        {
            speed *= 2;
        }
        
        while (completionPercentage < 1f)
        {
            yield return 0;
            completionPercentage += speed * Time.deltaTime;
            quadrant.Material.SetTextureOffset("_MainTex", new Vector2(0, Mathf.Lerp(startOffset, targetUvOffset, completionPercentage)));
        }

        quadrant.Letter = letter;

        //For debug
        switch (quadrant.Id)
        {
            case QUADRANT1:
                Quadrant1.Letter = letter;
                break;
            case QUADRANT2:
                Quadrant2.Letter = letter;
                break;
            case QUADRANT3:
                Quadrant3.Letter = letter;
                break;
            case QUADRANT4:
                Quadrant4.Letter = letter;
                break;
        }

        quadrant.Coroutine = null;
    }


    #endregion


    #region Pun RPCs

    [PunRPC]
    private void ChangeLetterRPC(string quadrantId, Letter letter, float targetUvOffset, bool isFast)
    {
        Quadrant quadrant = null;
        switch (quadrantId)
        {
            case QUADRANT1:
                quadrant = _quadrant1;
                break;
            case QUADRANT2:
                quadrant = _quadrant2;
                break;
            case QUADRANT3:
                quadrant = _quadrant3;
                break;
            case QUADRANT4:
                quadrant = _quadrant4;
                break;
        }

        if (quadrant == null) return;

        if (!_networkManager.IsMasterClient && quadrant.Coroutine != null)
        {
            StopCoroutine(quadrant.Coroutine);
        }

        quadrant.Coroutine = StartCoroutine(ChangeLetterCoroutine(quadrant, letter, targetUvOffset, isFast));
    }

    #endregion

}
