﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationSceneChalice : DynamicObject
{
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TranslationSceneChalice = this;
    }
}
