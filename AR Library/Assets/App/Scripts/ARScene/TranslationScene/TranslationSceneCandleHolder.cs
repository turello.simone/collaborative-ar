﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationSceneCandleHolder : DynamicObject
{
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TranslationSceneCandleHolder = this;
    }
}
