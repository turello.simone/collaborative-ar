﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DigitalRubyShared;
using MyBox;
using MyUtils;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Assertions.Comparers;

public class ObjectManipulation : SingletonInScene<ObjectManipulation>
{
    public float MaxInteractionDistance = 3.0f;

    public GameUtils.ObjectManipulationVisualization LocalVisualization
    {
        get { return _localVisualization;}
        set
        {
            _localVisualization = value;

            ObjectManipulationView view;
            if (Views.TryGetValue(_networkManager.LocalPlayer.ActorNumber, out view))
            {
                view.Visualization = value;
            }

            GameUtils.Mute(GameUIManager.Instance.LocalObjectManipDropdown.onValueChanged);
            GameUIManager.Instance.LocalObjectManipDropdown.value =
                GameUIManager.Instance.ObjManipToInt(value);
            GameUtils.Unmute(GameUIManager.Instance.LocalObjectManipDropdown.onValueChanged);
        }
    }
    [SerializeField, EnumFlag] private GameUtils.ObjectManipulationVisualization _localVisualization;

    public GameUtils.ObjectManipulationVisualization RemoteVisualization
    {
        get { return _remoteVisualization; }
        set
        {
            _remoteVisualization = value;
            if (Views != null)
            {
                foreach (var view in Views)
                {
                    if(view.Key != _networkManager.LocalPlayer.ActorNumber)
                        view.Value.Visualization = value;
                }
            }

            GameUtils.Mute(GameUIManager.Instance.RemoteObjectManipDropdown.onValueChanged);
            GameUIManager.Instance.RemoteObjectManipDropdown.value =
                GameUIManager.Instance.ObjManipToInt(value);
            GameUtils.Unmute(GameUIManager.Instance.RemoteObjectManipDropdown.onValueChanged);

            Debug.Log("ObjectManip Visualization changed");
        }
    }
    [SerializeField, EnumFlag] private GameUtils.ObjectManipulationVisualization _remoteVisualization;

    [Tooltip("Outline Width used for object manipulation visualization")]
    public float OutlineWidth = 2f;
    [Tooltip("The prefab used for visualize object manipulation.")]
    public GameObject ObjectManipulationViewPrefab;
    [Tooltip("Translation speed of dynamic objects during manipulation (zoom/pinch).")]
    public float DynamicObjectTranslationSpeed = 1;
    [Tooltip("Rotation speed of dynamic objects during manipulation (swipe).")]
    public float DynamicObjectRotationSpeed = 1;
    [Tooltip("Radius of the grid shown on detected surfaces when an object is close to them.")]
    public float ProximityRadius = 0.2f;
    [Tooltip("Added Radius of the circle shown on detected surfaces when an object is close to them.")]
    public float CircleAddedRadius = 0.04f;

    public bool OutlineObjectsWithGazeCursor = true;
    [ConditionalField("OutlineObjectsWithGazeCursor")] public float GazeCursorRadius = 0.04f;

    public bool InteractionEnabled
    {
        get
        {
            return _interactionEnabled;
        }

        set
        {
            if (value != _interactionEnabled)
            {
                _interactionEnabled = value;
                if (value)
                {
                    _tapGestureRecognizer.StateUpdated += TapGestureCallback;
                    FingersScript.Instance.AddGesture(_tapGestureRecognizer);
                    _doubleTapGestureRecognizer.StateUpdated += DoubleTapGestureCallback;
                    FingersScript.Instance.AddGesture(_doubleTapGestureRecognizer);
                    _holdTapGestureRecognizer.StateUpdated += HoldTapGestureCallback;
                    FingersScript.Instance.AddGesture(_holdTapGestureRecognizer);
                    _pinchGestureRecognizer.StateUpdated += PinchGestureCallback;
                    FingersScript.Instance.AddGesture(_pinchGestureRecognizer);
                    _swipeGestureRecognizer.StateUpdated += SwipeGestureCallback;
                    FingersScript.Instance.AddGesture(_swipeGestureRecognizer);
                    _rotateGestureRecognizer.StateUpdated += RotateGestureCallback;
                    FingersScript.Instance.AddGesture(_rotateGestureRecognizer);
                }
                else
                {
                    FingersScript.Instance.RemoveGesture(_tapGestureRecognizer);
                    _tapGestureRecognizer.StateUpdated -= TapGestureCallback;
                    FingersScript.Instance.RemoveGesture(_doubleTapGestureRecognizer);
                    _doubleTapGestureRecognizer.StateUpdated -= DoubleTapGestureCallback;
                    FingersScript.Instance.RemoveGesture(_holdTapGestureRecognizer);
                    _tapGestureRecognizer.StateUpdated -= HoldTapGestureCallback;
                    FingersScript.Instance.RemoveGesture(_pinchGestureRecognizer);
                    _pinchGestureRecognizer.StateUpdated -= PinchGestureCallback;
                    FingersScript.Instance.RemoveGesture(_swipeGestureRecognizer);
                    _swipeGestureRecognizer.StateUpdated -= SwipeGestureCallback;
                    FingersScript.Instance.RemoveGesture(_rotateGestureRecognizer);
                    _swipeGestureRecognizer.StateUpdated -= RotateGestureCallback;
                }
            }
        }
    }

    public Dictionary<int, ObjectManipulationView> Views;

    private PhotonNetworkManager _networkManager;
    private ARManager _arManager;
    private ARLibrary _arLibrary;
    private TapGestureRecognizer _tapGestureRecognizer;
    private TapGestureRecognizer _doubleTapGestureRecognizer;
    private TapGestureRecognizer _holdTapGestureRecognizer;
    private ScaleGestureRecognizer _pinchGestureRecognizer;
    private OneTouchScaleGestureRecognizer _swipeGestureRecognizer;
    private RotateGestureRecognizer _rotateGestureRecognizer;
    private bool _isHoldingObject;
    private bool _interactionEnabled;
    private GameObject _interactedObject;
    private GameObject _interactedHeldObject;
    //private Coroutine _startHoldCoroutine;
    private CustomOutline _holdTapOutline;
    private Coroutine _enableGazeOutlineCoroutine;
    private int _counterXSwipe;
    private int _counterYSwipe;
    private int _swipeSamples = 10;
    private float _minswipeX = 5f;
    private float _minswipeY = 5f;
    

    #region Unity

    public override void Awake ()
    {
        if (ObjectManipulationViewPrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> ObjectManipulationViewPrefab Reference.", this);
        }

        _networkManager = PhotonNetworkManager.Instance;
        _arManager = ARManager.Instance;
        _arLibrary = _arManager.ARLibrary;
        _interactionEnabled = false;
        _isHoldingObject = false;
        _arLibrary.EnableSurfaceCollider = _isHoldingObject;

        //Create tap recognizer
        _tapGestureRecognizer = new TapGestureRecognizer();

        //Create double tap recognizer
        _doubleTapGestureRecognizer = new TapGestureRecognizer();
        _doubleTapGestureRecognizer.NumberOfTapsRequired = 2;
        _doubleTapGestureRecognizer.ThresholdSeconds = 0.2f;
        //_doubleTapGestureRecognizer.AllowSimultaneousExecution(_tapGestureRecognizer);
        _tapGestureRecognizer.RequireGestureRecognizerToFail = _doubleTapGestureRecognizer;


        //Create hold tap recognizer
        _holdTapGestureRecognizer = new TapGestureRecognizer();
        _holdTapGestureRecognizer.SendBeginState = true;
        _holdTapGestureRecognizer.ThresholdSeconds = float.MaxValue;
        _holdTapGestureRecognizer.AllowSimultaneousExecutionWithAllGestures();

        //Create zoom/pinch recognizer
        _pinchGestureRecognizer = new ScaleGestureRecognizer();

        //Create swipe recognizer
        _swipeGestureRecognizer = new OneTouchScaleGestureRecognizer();

        //Create rotate recognizer
        _rotateGestureRecognizer = new RotateGestureRecognizer();

        Views = new Dictionary<int, ObjectManipulationView>();

    }

    private void Start()
    {
        //Initialize dictionary of views, creating one for each player in the room (including the local player)
        GameObject localPlayerViewObj = Instantiate(ObjectManipulationViewPrefab);
        ObjectManipulationView localPlayerView = localPlayerViewObj.GetComponent<ObjectManipulationView>();
        localPlayerView.Initialize(_networkManager.LocalPlayer, LocalVisualization, true, OutlineWidth, CircleAddedRadius,
            OutlineObjectsWithGazeCursor, GazeCursorRadius);
        Views.Add(_networkManager.LocalPlayer.ActorNumber, localPlayerView);

        foreach (PlayerInfo player in _networkManager.OtherPlayers.Values)
        {
            GameObject viewObj = Instantiate(ObjectManipulationViewPrefab);
            ObjectManipulationView view = viewObj.GetComponent<ObjectManipulationView>();
            view.Initialize(player, RemoteVisualization, false, OutlineWidth, CircleAddedRadius,
                OutlineObjectsWithGazeCursor, GazeCursorRadius);
            Views.Add(player.ActorNumber, view);
        }

        _networkManager.LocalPlayer.ObjectHeldChangedEvent += LocalPlayer_OnObjectHeldChanged;
        _networkManager.PlayerEnteredRoomEvent += NetworkManager_OnPlayerEnteredRoom;
        _networkManager.PlayerLeftRoomEvent += NetworkManager_OnPlayerLeftRoom;
    }

    private void OnDisable()
    {
        InteractionEnabled = false;
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        _networkManager.LocalPlayer.ObjectHeldChangedEvent -= LocalPlayer_OnObjectHeldChanged;
        _networkManager.PlayerEnteredRoomEvent -= NetworkManager_OnPlayerEnteredRoom;
        _networkManager.PlayerLeftRoomEvent -= NetworkManager_OnPlayerLeftRoom;
    }

    private void Update()
    {
        if (!InteractionEnabled) return;

        if (_isHoldingObject && _networkManager.LocalPlayer != null && _networkManager.LocalPlayer.ObjectHeld != null)
        {
            _arManager.SetVisualizationProximityPosition(_networkManager.LocalPlayer.ObjectHeld.transform.position);
        }

    }


    #endregion


    #region Input Callbacks

    private void TapGestureCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Ended)
        {
            //Debug.Log("Tapped at " + gesture.FocusX + ", " + gesture.FocusY);
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
            if (!_isHoldingObject)
            {
                //I'm not holding an object

                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit dynamicObjHitInfo;
                RaycastHit staticObjHitInfo;
                RaycastHit surfaceHitInfo;

                //Find the first object hit between Dynamic Objects, Static Objects and Surfaces
                float minDistance = float.MaxValue;
                bool dynamicObjHit = Physics.Raycast(screenRay, out dynamicObjHitInfo, MaxInteractionDistance,
                    GameUtils.Mask.DynamicObjects);
                if (dynamicObjHit)
                {
                    minDistance = dynamicObjHitInfo.distance;
                }
                bool staticObjHit = Physics.Raycast(screenRay, out staticObjHitInfo, MaxInteractionDistance,
                    GameUtils.Mask.StaticObjects);
                if (staticObjHit)
                {
                    if (staticObjHitInfo.distance < minDistance)
                    {
                        minDistance = staticObjHitInfo.distance;
                        if (dynamicObjHit) dynamicObjHit = false;
                    }
                }

                if (dynamicObjHit || staticObjHit)
                {
                    if (_arLibrary.Raycast(screenRay, out surfaceHitInfo, MaxInteractionDistance) &&
                        surfaceHitInfo.distance < minDistance)
                    {
                        if (dynamicObjHit) dynamicObjHit = false;
                        if (staticObjHit) staticObjHit = false;
                    }
                }

                if (dynamicObjHit)
                {
                    //Check collision with dynamic objects to pick one up 
                    GameObject dynObj = GameUtils.GetObjectRoot(dynamicObjHitInfo.transform.gameObject);
                    _networkManager.GrabDynamicObject(dynObj);
                }
                else if (staticObjHit && staticObjHitInfo.transform.CompareTag("Interactive"))
                {
                    //Check collision with static Objects to tap on a interactive object
                    _networkManager.TapOnInteractiveObject(staticObjHitInfo.transform.gameObject);
                }
            }
            else
            {
                //I'm holding a dynamic object

                if (_networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().DynamicObjectState !=
                    GameUtils.DynamicObjectState.Equipped)
                {
                    //The dynamic object i'm holding is not equipped

                    /*Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                    RaycastHit dynamicObjHitInfo;
                    RaycastHit staticObjHitInfo;
                    RaycastHit interactiveObjHitInfo = new RaycastHit();
                    RaycastHit tempHit;
                    Pose surfaceHitPose;

                    //check hit with ar surfaces
                    bool surfaceHit = _arLibrary.Raycast(screenRay, out surfaceHitPose, MaxInteractionDistance);
                    float surfaceHitDistance = Vector3.Distance(screenRay.origin, surfaceHitPose.position);

                    //check hit with an interactive object (which is part of the dynamic object held) 
                    bool interactiveObjHit = false;
                    float interactiveObjHitDistance = float.MaxValue;
                    foreach (Collider coll in _networkManager.LocalPlayer.ObjectHeld.transform
                        .GetComponentsInChildren<Collider>())
                    {
                        if (coll.gameObject.CompareTag("Interactive"))
                        {
                            if (coll.Raycast(screenRay, out tempHit, MaxInteractionDistance) &&
                                tempHit.distance < interactiveObjHitDistance)
                            {
                                interactiveObjHit = true;
                                interactiveObjHitInfo = tempHit;
                                interactiveObjHitDistance = interactiveObjHitInfo.distance;
                            }
                        }
                    }

                    Debug.Log("ObjectInteraction/TapGestureCallback: Holding a dynamic object; surfaceHit " + surfaceHit + ", interactiveObjHit " + interactiveObjHit + ".");

                    //Determine which hit is closer
                    if (surfaceHit && interactiveObjHit)
                    {
                        if (surfaceHitDistance < interactiveObjHitDistance)
                        {
                            surfaceHit = true;
                            interactiveObjHit = false;
                        }
                        else
                        {
                            surfaceHit = false;
                            interactiveObjHit = true;
                        }
                    }

                    if (surfaceHit)
                    {
                        //Try to put down the object held

                        bool dynamicObjHit = Physics.Raycast(screenRay, out dynamicObjHitInfo, MaxInteractionDistance,
                            GameUtils.Mask.DynamicObjects);

                        //Check if there is a dynamic object in front of detected surface hit.
                        if (!dynamicObjHit || surfaceHitDistance < dynamicObjHitInfo.distance)
                        {
                            //There are no dynamic objects in front of detected surface hit.

                            bool staticObjHit = Physics.Raycast(screenRay, out staticObjHitInfo, MaxInteractionDistance,
                                GameUtils.Mask.StaticObjects);

                            //Check if there is a static object in front of detected surface hit.
                            if (!staticObjHit || surfaceHitDistance < staticObjHitInfo.distance)
                            {
                                //There are no static objects in front of detected surface hit.
                                _networkManager.PlaceDynamicObject(_networkManager.LocalPlayer.ObjectHeld, surfaceHitPose.position, surfaceHitPose.rotation);
                            }
                        }
                    }
                    else if (interactiveObjHit)
                    {
                        //Try to tap on the interactive object hit
                        bool dynamicObjHit = Physics.Raycast(screenRay, out dynamicObjHitInfo, MaxInteractionDistance,
                            GameUtils.Mask.DynamicObjects);

                        //Check if there is a dynamic object in front of interactive object hit.
                        if (!dynamicObjHit || interactiveObjHitInfo.distance <= dynamicObjHitInfo.distance)
                        {
                            _networkManager.TapOnInteractiveObject(interactiveObjHitInfo.transform.gameObject);
                        }
                    }*/

                    Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                    RaycastHit interactiveObjHitInfo = new RaycastHit();
                    RaycastHit dynamicObjHitInfo;
                    RaycastHit tempHit;

                    //check hit with an interactive object (which is part of the dynamic object held) 
                    bool interactiveObjHit = false;
                    bool objectHeldHit = false;
                    float interactiveObjHitDistance = float.MaxValue;
                    foreach (Collider coll in _networkManager.LocalPlayer.ObjectHeld.transform
                        .GetComponentsInChildren<Collider>())
                    {
                        if (coll.Raycast(screenRay, out tempHit, MaxInteractionDistance))
                        {
                            objectHeldHit = true;
                            if (coll.gameObject.CompareTag("Interactive") && tempHit.distance < interactiveObjHitDistance)
                            {
                                interactiveObjHit = true;
                                interactiveObjHitInfo = tempHit;
                                interactiveObjHitDistance = interactiveObjHitInfo.distance;
                            }
                        }
                    }

                    if (interactiveObjHit)
                    {
                        //Try to tap on the interactive object hit
                        bool dynamicObjHit = Physics.Raycast(screenRay, out dynamicObjHitInfo, MaxInteractionDistance,
                            GameUtils.Mask.DynamicObjects);

                        //Check if there is a dynamic object in front of interactive object hit.
                        if (!dynamicObjHit || interactiveObjHitInfo.distance <= dynamicObjHitInfo.distance)
                        {
                            _networkManager.TapOnInteractiveObject(interactiveObjHitInfo.transform.gameObject);
                        }
                    }
                    else if (!objectHeldHit)
                    {
                        //Try to put down the object held
                        if (_networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().DynamicObjectState ==
                            GameUtils.DynamicObjectState.HeldOnSurface)
                        {
                            GameObject obj = _networkManager.LocalPlayer.ObjectHeld;
                            DynamicObject dynObj = obj.GetComponent<DynamicObject>();
                            Quaternion objRotation = Quaternion.LookRotation(
                                Vector3.ProjectOnPlane(obj.transform.forward, dynObj.SurfaceHitInfo.normal),
                                dynObj.SurfaceHitInfo.normal);

                            _networkManager.PlaceDynamicObject(obj, dynObj.SurfaceHitInfo.point, objRotation);
                        }
                    }

                }
                else
                {
                    //The dynamic object i'm holding is equipped

                    //check hit with an interactive object, part of the dynamic object held
                    Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                    RaycastHit interactiveObjHitInfo = new RaycastHit();
                    RaycastHit tempHit;

                    bool interactiveObjHit = false;
                    float interactiveObjHitDistance = float.MaxValue;
                    foreach (Collider coll in _networkManager.LocalPlayer.ObjectHeld.transform
                        .GetComponentsInChildren<Collider>())
                    {
                        if (coll.gameObject.CompareTag("Interactive"))
                        {
                            if (coll.Raycast(screenRay, out tempHit, MaxInteractionDistance) && tempHit.distance < interactiveObjHitDistance)
                            {
                                interactiveObjHit = true;
                                interactiveObjHitInfo = tempHit;
                                interactiveObjHitDistance = interactiveObjHitInfo.distance;
                            }
                        }
                    }

                    if (interactiveObjHit)
                    {
                        /*RaycastHit staticObjHitInfo;
                        RaycastHit dynamicObjHitInfo;
                        Pose surfaceHitPose;*/

                        //Try to tap on the interactive object hit

                        //NOT REALLY NEEDED
                        /*//Check if there is a detected surface or a static object or a dynamic object in front of interactive object hit.
                        if ((!_arLibrary.Raycast(screenRay, out surfaceHitPose, _maxInteractionDistance) ||
                             interactiveObjHitInfo.distance <= Vector3.Distance(screenRay.origin, surfaceHitPose.position)) &&
                            (!Physics.Raycast(screenRay, out staticObjHitInfo, _maxInteractionDistance, GameUtils.Mask.StaticObjects) ||
                             interactiveObjHitInfo.distance <= staticObjHitInfo.distance) &&
                            (!Physics.Raycast(screenRay, out dynamicObjHitInfo, _maxInteractionDistance, GameUtils.Mask.DynamicObjects) ||
                            interactiveObjHitInfo.distance <= dynamicObjHitInfo.distance) )
                        {
                            //There are none
                            _networkManager.TapOnInteractiveObject(interactiveObjHitInfo.transform.gameObject);
                        }*/

                        _networkManager.TapOnInteractiveObject(interactiveObjHitInfo.transform.gameObject);

                    }

                }
            }

        }


    }

    private void DoubleTapGestureCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Ended)
        {
            Debug.Log("Double Tapped at " + gesture.FocusX + ", " + gesture.FocusY);
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);

            Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
            RaycastHit objectHitInfo;
            RaycastHit surfaceHitInfo;
            bool objectHit = Physics.Raycast(screenRay, out objectHitInfo, MaxInteractionDistance,
                GameUtils.Mask.DynamicObjects | GameUtils.Mask.StaticObjects);
            bool surfaceHit = _arLibrary.Raycast(screenRay, out surfaceHitInfo, MaxInteractionDistance);

            if (objectHit && surfaceHit)
            {
                //check which hit is closer
                if (surfaceHitInfo.distance < objectHitInfo.distance)
                {
                    _networkManager.CreatePlayerFocalPoint(surfaceHitInfo.point, surfaceHitInfo.normal);
                }
                else
                {
                    if(objectHitInfo.transform.gameObject.layer == GameUtils.Layer.StaticObjects)
                        _networkManager.CreatePlayerFocalPoint(objectHitInfo.point, objectHitInfo.normal, objectHitInfo.transform.gameObject);
                    else if(objectHitInfo.transform.gameObject.layer == GameUtils.Layer.DynamicObjects)
                        _networkManager.CreatePlayerFocalPoint(objectHitInfo.point, objectHitInfo.normal, GameUtils.GetObjectRoot(objectHitInfo.transform.gameObject));
                }
            }
            else if (objectHit)
            {
                if (objectHitInfo.transform.gameObject.layer == GameUtils.Layer.StaticObjects)
                    _networkManager.CreatePlayerFocalPoint(objectHitInfo.point, objectHitInfo.normal, objectHitInfo.transform.gameObject);
                else if (objectHitInfo.transform.gameObject.layer == GameUtils.Layer.DynamicObjects)
                    _networkManager.CreatePlayerFocalPoint(objectHitInfo.point, objectHitInfo.normal, GameUtils.GetObjectRoot(objectHitInfo.transform.gameObject));
            }
            else if (surfaceHit)
            {
                _networkManager.CreatePlayerFocalPoint(surfaceHitInfo.point, objectHitInfo.normal);
            }

        }

    }

    private void HoldTapGestureCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Began)
        {
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
            if (!_isHoldingObject)
            {
                //I'm not holding an object, try to hold tap on a (static) interactive object

                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit interactiveObjHitInfo;

                //Check collision with static Objects and check if first hit is on a interactive object
                if (Physics.Raycast(screenRay, out interactiveObjHitInfo, MaxInteractionDistance, GameUtils.Mask.StaticObjects) &&
                    interactiveObjHitInfo.transform.CompareTag("Interactive"))
                {
                    RaycastHit dynamicObjHitInfo;
                    RaycastHit surfaceHitInfo;

                    //Check if there is a dynamic object or a detected surface in front of the (static) interactive object hit.
                    if ((!Physics.Raycast(screenRay, out dynamicObjHitInfo, MaxInteractionDistance, GameUtils.Mask.DynamicObjects) ||
                         interactiveObjHitInfo.distance <= dynamicObjHitInfo.distance) &&
                        (!_arLibrary.Raycast(screenRay, out surfaceHitInfo, MaxInteractionDistance) ||
                        interactiveObjHitInfo.distance <= surfaceHitInfo.distance) )
                    {
                        //There is none
                        _interactedHeldObject = interactiveObjHitInfo.transform.gameObject;
                        //_startHoldCoroutine = StartCoroutine(StartHoldOnInteractiveObject(_interactedHeldObject));
                        _networkManager.HoldInteractiveObject(_interactedHeldObject, true);
                        AddOutlineToObject(_interactedHeldObject);
                    }
                }
                /*else
                {
                    //Check if a dynamicObject is hit; if so highlight it with an outline
                    if (Physics.Raycast(screenRay, out var dynamicObjHitInfo, MaxInteractionDistance,
                            GameUtils.Mask.DynamicObjects) &&
                        (!_arLibrary.Raycast(screenRay, out var surfaceHitPose, MaxInteractionDistance) ||
                         dynamicObjHitInfo.distance <= Vector3.Distance(screenRay.origin, surfaceHitPose.position)))
                    {
                        AddOutlineToObject(GameUtils.GetObjectRoot(dynamicObjHitInfo.transform.gameObject));
                    }
                }*/
            }
            else
            {
                //I'm holding a dynamic object, try to hold tap on a (dynamic) interactive object
                //(which is part of the dynamic object i'm holding),
                //regardless if the dynamic object held is equipped or not

                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit interactiveObjHitInfo;

                bool dynamicObjHit = Physics.Raycast(screenRay, out interactiveObjHitInfo, MaxInteractionDistance,
                    GameUtils.Mask.DynamicObjects);

                //check if a dynamic object was hit, if the dynamic object hit is the one the player is holding and
                //if it is an interactive object
                if (dynamicObjHit && GameUtils.GetObjectRoot(interactiveObjHitInfo.transform.gameObject) ==
                    _networkManager.LocalPlayer.ObjectHeld && interactiveObjHitInfo.transform.CompareTag("Interactive"))
                {
                    //NOT REALLY NEEDED
                    /*RaycastHit staticObjHitInfo;
                    Pose surfaceHitPose;

                    //Check if there is a detected surface or a static object in front of the interactive object hit.
                    if ((!_arLibrary.Raycast(screenRay, out surfaceHitPose, _maxInteractionDistance) ||
                         interactiveObjHitInfo.distance <= Vector3.Distance(screenRay.origin, surfaceHitPose.position)) &&
                        (!Physics.Raycast(screenRay, out staticObjHitInfo, _maxInteractionDistance,
                             GameUtils.Mask.StaticObjects) ||
                         interactiveObjHitInfo.distance <= staticObjHitInfo.distance) )
                    {
                        _startHoldCoroutine = StartCoroutine(StartHoldOnInteractiveObject(interactiveObjHitInfo.transform.gameObject));
                    }*/

                    _interactedHeldObject = interactiveObjHitInfo.transform.gameObject;
                    //_startHoldCoroutine = StartCoroutine(StartHoldOnInteractiveObject(_interactedHeldObject));
                    _networkManager.HoldInteractiveObject(_interactedHeldObject, true);
                    AddOutlineToObject(_interactedHeldObject);
                }
            }

        }
        else if (gesture.State == GestureRecognizerState.Failed || gesture.State == GestureRecognizerState.Ended)
        {
            /*if (_interactedHeldObject == null)
            {
                if (_startHoldCoroutine != null)
                    StopCoroutine(_startHoldCoroutine);
            }
            else
            {
                _networkManager.HoldInteractiveObject(_interactedHeldObject, false);
            }*/

            if(_interactedHeldObject != null)
                _networkManager.HoldInteractiveObject(_interactedHeldObject, false);

            _interactedHeldObject = null;
            RemoveOutlineFromPreviousObject();
        }
    }

    private void PinchGestureCallback(GestureRecognizer gesture)
    {
        if (_isHoldingObject && gesture.State == GestureRecognizerState.Executing)
        {
            //ScaleMultiplier assumes values between 0.25 and 1.75
            //f(x) = -(4/3) * x + (4/3)
            //f(x) rescales values between -1 and 1
            float movementOffset = (-(4.0f / 3.0f) * _pinchGestureRecognizer.ScaleMultiplier + (4.0f / 3.0f)) * DynamicObjectTranslationSpeed;

            //Debug.Log("ScaleMultiplier: " + _pinchGestureRecognizer.ScaleMultiplier + ", movementOffset: " + movementOffset + ", Focus: " + _pinchGestureRecognizer.FocusX + ", " + _pinchGestureRecognizer.FocusY);

            _networkManager.TranslateDynamicObjectTowardsCameraForward(_networkManager.LocalPlayer.ObjectHeld, movementOffset);
        }
    }

    private void SwipeGestureCallback(GestureRecognizer gesture)
    {
        if (!_isHoldingObject)
        {
            //I'm not holding an object, try to swipe a (static) interactive object

            if (gesture.State == GestureRecognizerState.Began)
            {
                if (_interactedObject != null)
                {
                    return;
                }
                Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit interactiveObjHitInfo;

                //Check collision with static Objects and check if first hit is on a interactive object
                if (Physics.Raycast(screenRay, out interactiveObjHitInfo, MaxInteractionDistance, GameUtils.Mask.StaticObjects) &&
                    interactiveObjHitInfo.transform.CompareTag("Interactive"))
                {
                    RaycastHit dynamicObjHitInfo;
                    RaycastHit surfaceHitInfo;

                    //Check if there is a dynamic object or a detected surface in front of the (static) interactive object hit.
                    if ((!Physics.Raycast(screenRay, out dynamicObjHitInfo, MaxInteractionDistance, GameUtils.Mask.DynamicObjects) ||
                         interactiveObjHitInfo.distance < dynamicObjHitInfo.distance) &&
                        (!_arLibrary.Raycast(screenRay, out surfaceHitInfo, MaxInteractionDistance) ||
                         interactiveObjHitInfo.distance < surfaceHitInfo.distance))
                    {
                        //There is none
                        //Save in _interactedObject the object on which it will be performed the swipe
                        _interactedObject = interactiveObjHitInfo.transform.gameObject;
                    }
                }
            }
            else if (gesture.State == GestureRecognizerState.Executing)
            {
                if (_interactedObject != null)
                {
                    //I'm interacting with an Object Tagged with "Interactive"
                    _networkManager.SwipeOnInteractiveObject(_interactedObject, new Vector2(_swipeGestureRecognizer.FocusX, _swipeGestureRecognizer.FocusY),
                        new Vector2(_swipeGestureRecognizer.DeltaX, _swipeGestureRecognizer.DeltaY));
                }
            }
            else if (gesture.State == GestureRecognizerState.Ended)
            {
                _interactedObject = null;
            }
        }
        else
        {
            //I'm holding a dynamic object, try to swipe on a (dynamic) interactive object
            //(which is part of the dynamic object i'm holding)

            if (gesture.State == GestureRecognizerState.Began)
            {
                if (_interactedObject != null)
                {
                    return;
                }
                Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);

                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit interactiveObjHitInfo;

                bool dynamicObjHit = Physics.Raycast(screenRay, out interactiveObjHitInfo, MaxInteractionDistance,
                    GameUtils.Mask.DynamicObjects);

                //check if a dynamic object was hit, if the dynamic object hit is the one the player is holding and
                //if it is an interactive object
                if (dynamicObjHit && GameUtils.GetObjectRoot(interactiveObjHitInfo.transform.gameObject) ==
                    _networkManager.LocalPlayer.ObjectHeld && interactiveObjHitInfo.transform.CompareTag("Interactive"))
                {
                    //No need to check if there is a detected surface or a static object in front of the interactive object hit.

                    _interactedObject = interactiveObjHitInfo.transform.gameObject;
                }

                _counterXSwipe = 0;
                _counterYSwipe = 0;
            }
            else if (gesture.State == GestureRecognizerState.Executing)
            {
                if (_interactedObject != null)
                {
                    //I'm interacting with an Object Tagged with "Interactive"
                    _networkManager.SwipeOnInteractiveObject(_interactedObject, new Vector2(_swipeGestureRecognizer.FocusX, _swipeGestureRecognizer.FocusY),
                        new Vector2(_swipeGestureRecognizer.DeltaX, _swipeGestureRecognizer.DeltaY));
                }
                else
                {
                    if (_networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().DynamicObjectState !=
                        GameUtils.DynamicObjectState.Equipped)
                    {
                        //I'm swiping not on a interactive object, so I rotate the DynamicObject I'm holding, if it is not equipped

                        //float rotationX = -_swipeGestureRecognizer.DeltaX /** DynamicObjectRotationSpeed*/;
                        //float rotationY = _swipeGestureRecognizer.DeltaY /** DynamicObjectRotationSpeed*/;
                        //Debug.Log("RotationX: " + rotationX + ", RotationY: " + rotationY +
                        //          ", DeltaX: " + _swipeGestureRecognizer.DeltaX + ", DeltaY: " + _swipeGestureRecognizer.DeltaY);

                        //Debug.LogFormat("XRotation: {0}, YRotation: {1}", _swipeGestureRecognizer.DeltaX,
                        //    _swipeGestureRecognizer.DeltaY);

                        /*if (Mathf.Abs(rotationX) > _minswipeX) _counterXSwipe++;
                        else _counterXSwipe = 0;

                        if (Mathf.Abs(rotationY) > _minswipeY) _counterYSwipe++;
                        else _counterYSwipe = 0;

                        if(_counterXSwipe >= _swipeSamples)
                            _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Up, rotationX);

                        if(_counterYSwipe >= _swipeSamples)
                            _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Right, rotationY);

                        if (_counterXSwipe < _swipeSamples && _counterYSwipe < _swipeSamples)
                        {
                            if (Mathf.Abs(rotationX) >= Mathf.Abs(rotationY))
                                _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Up, rotationX);
                            else
                                _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Right, rotationY);

                        }*/
                        /*if (Mathf.Abs(rotationX) >= Mathf.Abs(rotationY))
                            _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Up, rotationX);
                        else
                            _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Right, rotationY);*/

                        /*if (Math.Abs(rotationX) > 0.1f)
                            _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld,
                                GameUtils.RotAxis.Up, rotationX);
                        if (Math.Abs(rotationY) > 0.1f)
                            _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld,
                                GameUtils.RotAxis.Right, rotationY);*/

                        _networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld,
                            new Vector2(_swipeGestureRecognizer.FocusX, _swipeGestureRecognizer.FocusY),
                            new Vector2(_swipeGestureRecognizer.DeltaX, _swipeGestureRecognizer.DeltaY));
                    }
                }
            }
            else if (gesture.State == GestureRecognizerState.Ended)
            {
                _interactedObject = null;
            }
        }
    }

    private void RotateGestureCallback(GestureRecognizer gesture)
    {
        if (_isHoldingObject)
        {
            if (gesture.State == GestureRecognizerState.Executing)
            {
                //_networkManager.RotateDynamicObject(_networkManager.LocalPlayer.ObjectHeld, GameUtils.RotAxis.Forward, -_rotateGestureRecognizer.RotationDegreesDelta);
            }
        }
    }

    #endregion


    #region Network Callbacks

    private void LocalPlayer_OnObjectHeldChanged(PlayerInfo player, GameObject newValue)
    {
        _isHoldingObject = (newValue != null);

        //if holding an object enable surface colliders in order to be able to spherecast con surfaces
        _arLibrary.EnableSurfaceCollider = _isHoldingObject;

        if (_isHoldingObject)
        {
            _arManager.SetOverlaySurfaceMaterials(_arManager.SurfaceType1, ARManager.SurfaceRenderType.VisualizationProximity);
            _arManager.SetVisualizationProximityRadius(newValue.GetComponent<DynamicObject>().EncumbranceRadius + ProximityRadius);
            GameUIManager.Instance.SetActiveInGamePanel(GameUIManager.InGamePanel.FreezeObject);
        }
        else
        {
            _arManager.SetOverlaySurfaceMaterials(_arManager.SurfaceType1);
            GameUIManager.Instance.SetActiveInGamePanel(GameUIManager.InGamePanel.Default);
        }
    }

    private void NetworkManager_OnPlayerEnteredRoom(PlayerInfo player)
    {
        GameObject viewObj = Instantiate(ObjectManipulationViewPrefab);
        ObjectManipulationView view = viewObj.GetComponent<ObjectManipulationView>();
        view.Initialize(player, RemoteVisualization, false, OutlineWidth, CircleAddedRadius,
            OutlineObjectsWithGazeCursor, GazeCursorRadius);
        Views.Add(player.ActorNumber, view);
    }

    private void NetworkManager_OnPlayerLeftRoom(PlayerInfo player)
    {
        if (!Views.TryGetValue(player.ActorNumber, out var view)) return;
        view.Destroy();
        Destroy(view);
    }

    #endregion


    #region Private methods

    /*private IEnumerator StartHoldOnInteractiveObject(GameObject interactedObject)
    {
        //yield return new WaitForSeconds(0.4f);
        if (_interactedObject == null)
        {
            _interactedObject = interactedObject;
            _networkManager.HoldInteractiveObject(_interactedObject, true);
            AddOutlineToObject(_interactedObject);
        }

        yield return 0;
    }*/

    private void AddOutlineToObject(GameObject obj)
    {
        if (_holdTapOutline != null) return;

        Views[_networkManager.LocalPlayer.ActorNumber].OutlineObjectsWithGazeCursor = false;

        CustomOutline oldOutline = obj.GetComponent<CustomOutline>();
        if (oldOutline == null)
        {
            _holdTapOutline = obj.AddComponent<CustomOutline>();
            _holdTapOutline.OutlineMode = CustomOutline.Mode.OutlineVisible;
            _holdTapOutline.OutlineColor = _networkManager.LocalPlayer.Color;
            _holdTapOutline.OutlineWidth = OutlineWidth;
            _holdTapOutline.enabled = true;
        }
        else
        {
            Destroy(oldOutline);
            StartCoroutine(AddObjectOutlineCoroutine(obj));
        }
    }

    private IEnumerator AddObjectOutlineCoroutine(GameObject objectInteracted)
    {
        yield return 0;
        if (objectInteracted.GetComponent<CustomOutline>() == null)
        {
            _holdTapOutline = objectInteracted.AddComponent<CustomOutline>();
            _holdTapOutline.OutlineMode = CustomOutline.Mode.OutlineVisible;
            _holdTapOutline.OutlineColor = _networkManager.LocalPlayer.Color;
            _holdTapOutline.OutlineWidth = OutlineWidth;
            _holdTapOutline.FadeIn = false;
            _holdTapOutline.enabled = true;
        }
    }

    private void RemoveOutlineFromPreviousObject()
    {
        if(_enableGazeOutlineCoroutine != null) StopCoroutine(_enableGazeOutlineCoroutine);
        _enableGazeOutlineCoroutine = StartCoroutine(EnableGazeOutlineAfterFadeCoroutine());

        if (_holdTapOutline != null)
        {
            _holdTapOutline.FadeOutAndDestroy();
        }
    }

    private IEnumerator EnableGazeOutlineAfterFadeCoroutine()
    {
        yield return new WaitForSeconds(0.26f);
        Views[_networkManager.LocalPlayer.ActorNumber].OutlineObjectsWithGazeCursor = true;
    }


    #endregion

}
