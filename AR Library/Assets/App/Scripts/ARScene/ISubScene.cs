﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ISubScene
{
    protected PhotonNetworkManager _networkManager;
    protected EscapeGameManager _gameManager;

    private Coroutine _progressionCoroutine;
    private Action _nextSubTaskButtonAction;
    private float _startTime;
    private int _lastMasterPlayerIndex = 0;

    public ISubScene(string sceneName, bool initialized)
    {
        _networkManager = PhotonNetworkManager.Instance;
        _gameManager = EscapeGameManager.Instance;

        HintManager.Instance.HintEnabled = false;
        GameUIManager.Instance.VisualizationPresetDropdown.value = 0;

        GameUIManager.Instance.SetInstructionsText("");
        GameUIManager.Instance.SetActiveInstructionsPanel(true);

        //EnableObjects(true);
        SpawnObjects();

        _progressionCoroutine = SubSceneManager.Instance.StartCoroutine(Progression());
    }

    public virtual void Destroy()
    {
        if (_progressionCoroutine != null) SubSceneManager.Instance.StopCoroutine(_progressionCoroutine);
        _networkManager.ForcePlaceAllObjects();
        HintManager.Instance.HintEnabled = false;
        ARManager.Instance.EnableHoleInOcclusionWithShadows(false);
        GameUIManager.Instance.SetActiveInGamePanel(GameUIManager.InGamePanel.Default);
        GameUIManager.Instance.SetActiveNextScenePanel(false);
        GameUIManager.Instance.SetActiveNextSubTaskPanel(false);
        _networkManager.CurrentRoom.MasterClientSwitchedEvent -= ShowNextSceneButton;
        _networkManager.CurrentRoom.MasterClientSwitchedEvent -= ShowNextSubTaskButton;
        _networkManager.DestroyPlayerFocalPoint();
        //EnableObjects(false);
    }

    protected abstract void SpawnObjects();

    protected virtual IEnumerator Progression()
    {
        yield return 0;
        yield return new WaitUntil(() => _networkManager.InRoom);
    }
    //protected abstract void EnableObjects(bool enabled);

    protected void ShowCompletionTextAndNextSceneButton(string text)
    {
        //Play success sound
        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

        //Show completion text
        HintManager.Instance.AddHint(text);

        //Show next scene button
        if (_networkManager.IsMasterClient)
        {
            float endTime = Time.time;

            if (_lastMasterPlayerIndex != 0 && _networkManager.GetPlayerInfoFromPlayerIndex(_lastMasterPlayerIndex) != null)
            {
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom,
                    _networkManager.GetPlayerInfoFromPlayerIndex(_lastMasterPlayerIndex), OnlineLogger.EventType.SceneCompleted);
                OnlineLogger.Instance.SendTaskCompletion(_networkManager.CurrentRoom.Name,
                    _networkManager.CurrentRoom.RoomScene, endTime - _startTime, 
                    _networkManager.GetPlayerInfoFromPlayerIndex(_lastMasterPlayerIndex));
            }
            else
            {
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom,
                    _networkManager.LocalPlayer, OnlineLogger.EventType.SceneCompleted);
                OnlineLogger.Instance.SendTaskCompletion(_networkManager.CurrentRoom.Name,
                    _networkManager.CurrentRoom.RoomScene, endTime - _startTime);
            }


            ShowNextSceneButton(_networkManager.LocalPlayer);
        }
        else
        {
            _networkManager.CurrentRoom.MasterClientSwitchedEvent += ShowNextSceneButton;
        }
    }

    private void ShowNextSceneButton(PlayerInfo player)
    {
        if (_networkManager.IsMasterClient &&
            SubSceneManager.Instance.GetNextSceneInSequence(_networkManager.CurrentRoom.RoomScene).Type !=
            SubSceneManager.SubSceneType.Uninitialized)
        {
            GameUIManager.Instance.SetActiveNextScenePanel(true);
        }
    }

    protected IEnumerator ShowCompletionTextAndWaitNextSubTask(string text, Action nextSubTaskButtonAction, Func<bool> waitPredicate)
    {
        _nextSubTaskButtonAction = nextSubTaskButtonAction;

        //Play success sound
        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

        //Show completion text
        HintManager.Instance.AddHint(text);

        //Show next scene button
        if (_networkManager.IsMasterClient)
        {
            float endTime = Time.time;

            if (_lastMasterPlayerIndex != 0 && _networkManager.GetPlayerInfoFromPlayerIndex(_lastMasterPlayerIndex) != null)
            {
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom,
                    _networkManager.GetPlayerInfoFromPlayerIndex(_lastMasterPlayerIndex), OnlineLogger.EventType.SceneCompleted);
                OnlineLogger.Instance.SendTaskCompletion(_networkManager.CurrentRoom.Name,
                    _networkManager.CurrentRoom.RoomScene, endTime - _startTime,
                    _networkManager.GetPlayerInfoFromPlayerIndex(_lastMasterPlayerIndex));
            }
            else
            {
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom,
                    _networkManager.LocalPlayer, OnlineLogger.EventType.SceneCompleted);
                OnlineLogger.Instance.SendTaskCompletion(_networkManager.CurrentRoom.Name,
                    _networkManager.CurrentRoom.RoomScene, endTime - _startTime);
            }

            ShowNextSubTaskButton(_networkManager.LocalPlayer);
        }
        else
        {
            _networkManager.CurrentRoom.MasterClientSwitchedEvent += ShowNextSubTaskButton;
        }

        yield return new WaitUntil(waitPredicate);
    }

    private void ShowNextSubTaskButton(PlayerInfo player)
    {
        if (_networkManager.IsMasterClient)
        {
            GameUIManager.Instance.SetNextSubTaskButtonAction(_nextSubTaskButtonAction);
            GameUIManager.Instance.SetActiveNextSubTaskPanel(true);
        }
    }

    protected IEnumerator ShowInstructionsAndCheckReady(string instructions)
    {
        _networkManager.DestroyPlayerFocalPoint();

        GameUIManager.Instance.SetInstructionsText(instructions);
        //GameUIManager.Instance.SetActiveInGamePanel(GameUIManager.InGamePanel.Default);
        GameUIManager.Instance.SetActiveInstructionsPanel(true);

        //"CHEATING" version, requires all current players in room to be ready
        //if(_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
        //    yield return new WaitUntil(() => _networkManager.NumberOfPlayersReady == _networkManager.NumberOfPlayers);

        //correct version, requires 3 players to be ready
        //else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
        //    yield return new WaitUntil(() => _networkManager.NumberOfPlayersReady == 3);

        yield return new WaitUntil(() => (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled &&
                                          _networkManager.NumberOfPlayersReady == _networkManager.NumberOfPlayers) ||
                                         (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled &&
                                          _networkManager.NumberOfPlayersReady == 3) );


        GameUIManager.Instance.SetActiveInstructionsPanel(false);

        yield return 0;

        _startTime = Time.time;

        if (_networkManager.IsMasterClient)
            OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom, 
                _networkManager.LocalPlayer, OnlineLogger.EventType.SceneStarted);
    }

    protected IEnumerator ShowInstructionsAndCheckReady(int masterPlayerIndex, string instructionsMaster, string instructionsSlave)
    {
        _networkManager.DestroyPlayerFocalPoint();

        _lastMasterPlayerIndex = masterPlayerIndex;

        string instructions = "";

        if (_networkManager.LocalPlayer.PlayerIndex == masterPlayerIndex)
            instructions = instructionsMaster;
        else
            instructions = instructionsSlave;

        GameUIManager.Instance.SetInstructionsText(instructions);
        //GameUIManager.Instance.SetActiveInGamePanel(GameUIManager.InGamePanel.Default);
        GameUIManager.Instance.SetActiveInstructionsPanel(true);

        yield return new WaitUntil(() => (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled &&
                                          _networkManager.NumberOfPlayersReady == _networkManager.NumberOfPlayers) ||
                                         (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled &&
                                          _networkManager.NumberOfPlayersReady == 3));

        GameUIManager.Instance.SetActiveInstructionsPanel(false);

        yield return 0;

        _startTime = Time.time;

        if (_networkManager.IsMasterClient)
        {
            if (masterPlayerIndex != 0 && _networkManager.GetPlayerInfoFromPlayerIndex(masterPlayerIndex) != null)
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom,
                    _networkManager.GetPlayerInfoFromPlayerIndex(masterPlayerIndex), OnlineLogger.EventType.SceneStarted);
            else
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom,
                    _networkManager.LocalPlayer, OnlineLogger.EventType.SceneStarted);
        }
    }

    protected void ShowHint(string instructions)
    {
        HintManager.Instance.AddHint(instructions);
    }

    protected void ShowHint(int masterPlayerIndex, string instructionsMaster, string instructionsSlave)
    {
        string instructions = "";

        if (_networkManager.LocalPlayer.PlayerIndex == masterPlayerIndex)
            instructions = instructionsMaster;
        else
            instructions = instructionsSlave;

        HintManager.Instance.AddHint(instructions);
    }

}
