﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class PaintingScenePainting : StaticObject
{
    public enum PaintingState
    {
        Default,
        Tridimensional,
        Open
    }

    #region Attributes

    [Header("Painting properties")]
    [MustBeAssigned] public GameObject LeftEye;
    [MustBeAssigned] public GameObject RightEye;
    [MustBeAssigned] public GameObject FocalPointMoon;
    [MustBeAssigned] public Animator Animator;

    public float OcclusionHoleRadius = 0.75f;

    public PaintingState State
    {
        get { return (PaintingState) _state.Value; }
        set { _state.Value = (int) value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;

    private Vector3 _leftEyeStartRelativePosition;
    private Vector3 _rightEyeStartRelativePosition;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();

        _state = new SyncVarInt((int)PaintingState.Tridimensional, _photonView, STATEID);
        OnPaintingStateChange((int)PaintingState.Tridimensional); 
        _state.ValueChanged += OnPaintingStateChange;

        _leftEyeStartRelativePosition = LeftEye.transform.localPosition;
        _rightEyeStartRelativePosition = RightEye.transform.localPosition;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.PaintingScenePainting = this;

        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingMoon,
            FocalPointMoon, 2f, false, false, false);
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingMoon);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingMoon,
            FocalPointMoon, 2f, false, false, false);

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PaintingMoon);
            State = PaintingState.Tridimensional;
        }
    }

    private void Update()
    {
        if (_networkManager.LocalPlayer == null || _networkManager.LocalPlayer.Player == null) return;

        if (State != PaintingState.Default)
        {
            Vector3 leftEyePos = LeftEye.transform.parent.position +
                (_leftEyeStartRelativePosition.x * LeftEye.transform.parent.right +
                 _leftEyeStartRelativePosition.y * LeftEye.transform.parent.up +
                 _leftEyeStartRelativePosition.z * LeftEye.transform.parent.forward);
            Vector3 rightEyePos = RightEye.transform.parent.position +
                (_rightEyeStartRelativePosition.x * RightEye.transform.parent.right +
                 _rightEyeStartRelativePosition.y * RightEye.transform.parent.up +
                 _rightEyeStartRelativePosition.z * RightEye.transform.parent.forward);
            Debug.DrawLine(leftEyePos, Camera.main.transform.position);

            LeftEye.transform.rotation = Quaternion.LookRotation(
                Camera.main.transform.position - leftEyePos, transform.forward);
            RightEye.transform.rotation = Quaternion.LookRotation(
                Camera.main.transform.position - rightEyePos, transform.forward);

            ARManager.Instance.SetOcclusionWithShadowsHolePosition(transform.position);
        }
    }

    #endregion

    #region Private methods

    private void OnPaintingStateChange(int value)
    {
        switch ((PaintingState)value)
        {
            case PaintingState.Tridimensional:
                break;
            case PaintingState.Open:
                Animator.SetTrigger("Flatten");
                break;
        }
    }

    #endregion

}
