﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintingScene : IGameScene
{
    public PaintingScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.PaintingSceneRiddle == null)
                _networkManager.SpawnStaticObject(_gameManager.PaintingSceneRiddlePrefab.name,
                    _networkManager.CurrentRoom.RiddlePose);
            else
                _networkManager.CurrentRoom.PaintingSceneRiddle.Reset();

            if (_networkManager.CurrentRoom.PaintingScenePainting == null)
                _networkManager.SpawnStaticObject(_gameManager.PaintingScenePaintingPrefab.name,
                    _networkManager.CurrentRoom.PaintingPose);
            else
                _networkManager.CurrentRoom.PaintingScenePainting.Reset();

            if (_networkManager.CurrentRoom.PaintingSceneFlashlight1 == null)
                _networkManager.SpawnDynamicObject(_gameManager.PaintingSceneFlashlight1Prefab.name,
                    _networkManager.CurrentRoom.Flashlight1Pose);
            else
                _networkManager.CurrentRoom.PaintingSceneFlashlight1.Reset();

            if (_networkManager.CurrentRoom.PaintingSceneFlashlight2 == null)
                _networkManager.SpawnDynamicObject(_gameManager.PaintingSceneFlashlight2Prefab.name,
                    _networkManager.CurrentRoom.Flashlight2Pose);
            else
                _networkManager.CurrentRoom.PaintingSceneFlashlight2.Reset();

            if (_networkManager.CurrentRoom.PaintingSceneFlashlight3 == null)
                _networkManager.SpawnDynamicObject(_gameManager.PaintingSceneFlashlight3Prefab.name,
                    _networkManager.CurrentRoom.Flashlight3Pose);
            else
                _networkManager.CurrentRoom.PaintingSceneFlashlight3.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PaintingScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        ObjectManipulation.Instance.InteractionEnabled = true;

        ARManager.Instance.EnableHoleInOcclusionWithShadows(true, _networkManager.CurrentRoom.PaintingScenePainting.OcclusionHoleRadius);

        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PaintingMoon,
                out var completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingMoon);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PaintingMoon,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PaintingMoon);
        }

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.PaintingScenePainting.State = PaintingScenePainting.PaintingState.Open;
        }

        yield return new WaitForSeconds(2f);
        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/SceneCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public override void Destroy()
    {
        base.Destroy();
        if (FocalPointManager.Instance != null) FocalPointManager.Instance.DisableCurrentGazeConvergenceFocalPoint();
        if (ARManager.Instance != null) ARManager.Instance.EnableHoleInOcclusionWithShadows(false);
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneRiddle != null)
            PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneRiddle.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PaintingScenePainting != null)
            PhotonNetworkManager.Instance.CurrentRoom.PaintingScenePainting.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneFlashlight1 != null)
            PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneFlashlight1.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneFlashlight2 != null)
            PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneFlashlight2.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneFlashlight3 != null)
            PhotonNetworkManager.Instance.CurrentRoom.PaintingSceneFlashlight3.Enable(enabled);
    }

}
