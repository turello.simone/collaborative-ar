﻿using System;
using System.CodeDom;
using Lean.Pool;
using System.Collections;
using System.Collections.Generic;
using MyBox;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class PaintingSceneRiddle : StaticObject
{
    #region Attributes

    [Header("Riddle properties")]
    [MustBeAssigned] public MeshRenderer TextRenderer;

    private Material _textMaterial;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        /*_textMaterial = TextRenderer.material;
        SetFlashlightsProperties(0, 0);
        SetFlashlightEnabled(PaintingSceneFlashlight.FlashlightType.One, false);
        SetFlashlightEnabled(PaintingSceneFlashlight.FlashlightType.Two, false);
        SetFlashlightEnabled(PaintingSceneFlashlight.FlashlightType.Three, false);*/
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.PaintingSceneRiddle = this;
    }

    #endregion


    #region Public methods

    public void SetFlashlightsProperties(float angle, float range)
    {
        TextRenderer.material.SetFloat("_LightAngle", angle);
        TextRenderer.material.SetFloat("_LightRange", range * 0.8f);
    }

    public void SetFlashlightEnabled(PaintingSceneFlashlight.FlashlightType type, bool enabled)
    {
        switch (type)
        {
            case PaintingSceneFlashlight.FlashlightType.One:
                TextRenderer.material.SetInt("_Light1Enabled", enabled ? 1 : 0);
                break;
            case PaintingSceneFlashlight.FlashlightType.Two:
                TextRenderer.material.SetInt("_Light2Enabled", enabled ? 1 : 0);
                break;
            case PaintingSceneFlashlight.FlashlightType.Three:
                TextRenderer.material.SetInt("_Light3Enabled", enabled ? 1 : 0);
                break;
        }
    }

    public void SetFlashlightPose(PaintingSceneFlashlight.FlashlightType type, Pose pose)
    {
        switch (type)
        {
            case PaintingSceneFlashlight.FlashlightType.One:
                TextRenderer.material.SetVector("_Light1Position", pose.position);
                TextRenderer.material.SetVector("_Light1Direction", pose.forward);
                break;
            case PaintingSceneFlashlight.FlashlightType.Two:
                TextRenderer.material.SetVector("_Light2Position", pose.position);
                TextRenderer.material.SetVector("_Light2Direction", pose.forward);
                break;
            case PaintingSceneFlashlight.FlashlightType.Three:
                TextRenderer.material.SetVector("_Light3Position", pose.position);
                TextRenderer.material.SetVector("_Light3Direction", pose.forward);
                break;
        }
    }

    #endregion

}
