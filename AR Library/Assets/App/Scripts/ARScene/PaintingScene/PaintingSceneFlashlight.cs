﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class PaintingSceneFlashlight : EquippableDynamicObject
{

    public enum FlashlightState
    {
        Default,
        Off,
        On
    }

    public enum FlashlightType
    {
        One,
        Two,
        Three
    }

    #region Properties

    [Header("Flashlight properties")]
    public FlashlightType Type;

    [MustBeAssigned] public GameObject Button;
    [MustBeAssigned] public Projector LightProjector;
    //public Light SpotLight;
    [MustBeAssigned] public GameObject LightBeam;
    [MustBeAssigned] public Animator Animator;
    [MustBeAssigned] public MeshRenderer FlashlightRenderer;

    public FlashlightState State //AutoSync with equip / unequip RPCs
    {
        get { return _state; }
        set
        {
            _state = value;
            OnFlashlightStateChange(value);
        }
    }
    [SerializeField, ReadOnly] private FlashlightState _state;
    private readonly int _stateHash = Animator.StringToHash("FlashlightState");
    
    public float LightRange = 1.5f;
    public float LightAngle = 30f;
    //[SerializeField] private float _smoothing = 1.0f;

    private float _hitDistance;
    private GameUIManager _gameUIManager;

    #endregion


    #region Unity

    protected override void Awake()
    {
        _equippedPose = new Pose(new Vector3(0, -0.1f, 0.11f), Quaternion.identity);

        base.Awake();

        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();
        _gameUIManager = GameUIManager.Instance;

        //Initialize LightProjector
        LightProjector.farClipPlane = 10f;
        LightProjector.fieldOfView = LightAngle;
        //Initialize SpotLight
        //SpotLight.range = LightRange;
        //SpotLight.spotAngle = LightAngle;
        //Initialize LightBeam
        float radius = LightRange * Mathf.Tan(LightAngle / 2.0f * Mathf.Deg2Rad);
        LightBeam.transform.localScale = new Vector3(radius, LightRange, radius);
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        switch (Type)
        {
            case FlashlightType.One:
                _networkManager.CurrentRoom.PaintingSceneFlashlight1 = this;
                break;
            case FlashlightType.Two:
                _networkManager.CurrentRoom.PaintingSceneFlashlight2 = this;
                break;
            case FlashlightType.Three:
                _networkManager.CurrentRoom.PaintingSceneFlashlight3 = this;
                break;
        }

        _networkManager.CurrentRoom.PaintingSceneRiddle.SetFlashlightsProperties(LightAngle, LightRange);

        State = FlashlightState.Off;
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();

        State = FlashlightState.Off;
    }

    protected override void Update()
    {
        base.Update();
        if (State == FlashlightState.On)
        {
            _networkManager.CurrentRoom.PaintingSceneRiddle.SetFlashlightPose(Type, new Pose(transform.position, transform.rotation));

            //We need to adjust length of Lightbeam (modifying its scale),
            //based on distance from the center of the screen to closest surface or static object

            Ray ray = new Ray(LightProjector.transform.position, LightProjector.transform.forward);
            RaycastHit staticObjHitInfo; //last hit on a static object
            RaycastHit surfaceHitInfo; //last hit pose on a detected surface

            _hitDistance = LightRange;

            //Check collision with static objects and detected surfaces and set the minimum hit distance
            bool staticObjHit = Physics.Raycast(ray, out staticObjHitInfo, LightRange,
                        GameUtils.Mask.StaticObjects);
            if (staticObjHit)
            {
                _hitDistance = staticObjHitInfo.distance;
            }

            bool surfaceHit = _arLibrary.Raycast(ray, out surfaceHitInfo, LightRange);

            if (surfaceHit)
            {
                _hitDistance = surfaceHitInfo.distance;
            }

            if (staticObjHit && surfaceHit)
            {
                if (staticObjHitInfo.distance < surfaceHitInfo.distance)
                {
                    _hitDistance = staticObjHitInfo.distance;
                }
                else
                {
                    _hitDistance = surfaceHitInfo.distance;
                }
            }

            //The lightbeam must be hitDistance long
            //if there was no hit, hitDistance is LightRange
            Vector3 scale;
            scale.y = _hitDistance;
            float radius = _hitDistance * Mathf.Tan(LightAngle / 2.0f * Mathf.Deg2Rad);
            scale.x = radius;
            scale.z = radius;
            LightBeam.transform.localScale = scale;

            //Also the LightProjector must be hitDistance long
            //LightProjector.farClipPlane = Mathf.Lerp(LightProjector.farClipPlane, Mathf.Clamp(_hitDistance * 1.1f, 0, LightRange), Time.deltaTime * _smoothing);
        }
    }

    #endregion

    #region public methods 

    public override void TapOnInteractiveObject(GameObject interactiveObject)
    {
        /*if (Owner == _networkManager.LocalPlayer && interactiveObject == Button)
        {
            if (State == FlashlightState.Off)
            {
                Equip();
            }
            else if (State == FlashlightState.On)
            {
                Unequip();
            }
        }*/
    }

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (isPressed && Owner == _networkManager.LocalPlayer && interactiveObject == Button)
        {
            if (State == FlashlightState.Off)
            {
                Equip();
            }
            else if (State == FlashlightState.On)
            {
                Unequip();
            }
        }
    }

    public override void Equip()
    {
        State = FlashlightState.On;
        base.Equip();
    }

    public override void Unequip()
    {
        State = FlashlightState.Off;
        base.Unequip();
    }
    
    public override void ForceUnequip()
    {
        State = FlashlightState.Off;
        base.ForceUnequip();
    }

    #endregion

    #region private methods 

    private void OnFlashlightStateChange(FlashlightState value)
    {
        Animator.SetInteger(_stateHash, (int)value);

        switch (value)
        {
            case FlashlightState.Off:
                LightProjector.enabled = false;
                //SpotLight.enabled = false;
                LightBeam.GetComponent<MeshRenderer>().enabled = false;
                FlashlightRenderer.material.SetColor("_EmissionColor", Color.black);

                if (Owner == _networkManager.LocalPlayer)
                {
                    _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.FreezeObject);
                }

                _networkManager.CurrentRoom.PaintingSceneRiddle.SetFlashlightEnabled(Type, false);
                break;
            case FlashlightState.On:
                LightProjector.enabled = true;
                //SpotLight.enabled = true;
                LightBeam.GetComponent<MeshRenderer>().enabled = true;
                FlashlightRenderer.material.SetColor("_EmissionColor", new Color(0.8f, 0.8f, 0.8f));

                if (Owner == _networkManager.LocalPlayer)
                {
                    _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.UnequipObject);
                }

                _networkManager.CurrentRoom.PaintingSceneRiddle.SetFlashlightEnabled(Type, true);
                break;
        }
    }

    #endregion

    #region RPCs

    [PunRPC]
    public override void EquipRPC()
    {
        State = FlashlightState.On;
        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.ButtonPressed);

        base.EquipRPC();
    }

    [PunRPC]
    public override void UnequipRPC(float distanceFromPlayer)
    {
        State = FlashlightState.Off;
        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.ButtonPressedUp);

        base.UnequipRPC(distanceFromPlayer);
    }

    #endregion
}
