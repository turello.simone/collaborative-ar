﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IGameScene : ISubScene
{
    public IGameScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, initialized)
    {
        GameUIManager.Instance.SetSceneNameVisualizationAndLocation(sceneName, visualization, location);

        ObjectManipulation.Instance.InteractionEnabled = false;

        _networkManager.AvatarType = GameUtils.AvatarType.Oculus;

        _networkManager.LocalPlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        _networkManager.LocalGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
        ObjectManipulation.Instance.LocalVisualization = GameUtils.ObjectManipulationVisualization.Hand;

        switch (visualization)
        {
            case SubSceneManager.Visualization.Base:
                _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                   GameUtils.PlayerVisualization.Avatar;
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.Ray |
                                                                  GameUtils.ObjectManipulationVisualization.Outline |
                                                                  GameUtils.ObjectManipulationVisualization.Hand;
                _networkManager.PlayerFocalPointEnabled = false;
                break;
            case SubSceneManager.Visualization.Complete:
                _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                   GameUtils.PlayerVisualization.Avatar;
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.Ray |
                                                                  GameUtils.ObjectManipulationVisualization.Outline |
                                                                  GameUtils.ObjectManipulationVisualization.Hand;
                _networkManager.PlayerFocalPointEnabled = true;
                break;
        }
    }

}
