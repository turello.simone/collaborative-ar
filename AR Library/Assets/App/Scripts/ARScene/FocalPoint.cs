﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using UnityEngine;
using UnityEngine.UI;

public class FocalPoint
{
    public virtual bool Enabled
    {
        get { return _enabled; }
        set
        {
            if (value != _enabled)
            {
                _enabled = value;
                _focalPointArrowUI.SetActive(value && ArrowEnabled);
                _focalPointIconUI.SetActive(value && IconEnabled);
                if (Outline != null)
                {
                    Outline.enabled = (value && OutlineEnabled);
                }
            }
        }
    }

    public bool OutlineEnabled
    {
        get { return _outlineEnabled; }
        set
        {
            _outlineEnabled = value;
            if (Outline != null)
            {
                Outline.enabled = (value && Enabled);
            }
        }
    }

    public bool IconEnabled
    {
        get { return _iconEnabled; }
        set
        {
            _iconEnabled = value;
            _focalPointIconUI.SetActive(value && Enabled);
        }
    }

    public bool ArrowEnabled
    {
        get { return _arrowEnabled; }
        set
        {
            _arrowEnabled = value;
            _focalPointArrowUI.SetActive(value && Enabled);
        }
    }

    public bool IsDestroyed { get; private set; }

    public GameObject TargetGameObject;

    private GameObject _focalPointArrowUI;
    private GameObject _arrowUI;
    private GameObject _focalPointIconUI;
    private Image[] _ArrowUIImages;
    private Image[] _IconUIImages;
    private bool _targetGameObjectWasNull;
    private float _targetGameObjectBBDiameter;
    public CustomOutline Outline;
    private Camera _camera;
    private bool _enabled;
    private int _borderSize = 100;
    private bool _outlineEnabled;
    private bool _iconEnabled;
    private bool _arrowEnabled;
    private ARLibrary _arLibrary;
    private string _anchorId;
    private Vector3? _position;
    private Vector2 _referenceResolution;
    protected Vector3 _relativePosition;

    protected struct Temp
    {
        public GameObject TargetPlayer1;
        public GameObject TargetPlayer2;
        public GameObject TargetPlayer3;
        public Color Color;
        public float OutlineWidth;
        public bool OutlineEnabled;
        public bool ReplaceOutline;

        public Temp(GameObject targetPlayer1, GameObject targetPlayer2, GameObject targetPlayer3, Color color,
            float outlineWidth, bool outlineEnabled, bool replaceOutline)
        {
            TargetPlayer1 = targetPlayer1;
            TargetPlayer2 = targetPlayer2;
            TargetPlayer3 = targetPlayer3;
            Color = color;
            OutlineWidth = outlineWidth;
            OutlineEnabled = outlineEnabled;
            ReplaceOutline = replaceOutline;
        }
    }
    protected Temp _temp;


    public FocalPoint(GameObject target, GameObject focalPointArrowUI, GameObject focalPointIconUI, Color color,
        float outlineWidth, Vector3? position = null, bool enabled = true, bool outlineEnabled = true,
        bool replaceOutline = true, bool iconEnabled = true, bool arrowEnabled = true)
    {
        _arLibrary = ARManager.Instance.ARLibrary;
        _focalPointArrowUI = focalPointArrowUI;
        _focalPointIconUI = focalPointIconUI;
        _position = position;
        _arrowUI = _focalPointArrowUI.transform.Find("Arrow").gameObject;
        _camera = Camera.main;

        TargetGameObject = target;
        _targetGameObjectWasNull = false;
        //if target is null, create an empty gameObject as target
        if (target == null && position.HasValue)
        {
            _targetGameObjectWasNull = true;
            TargetGameObject = new GameObject();
            TargetGameObject.transform.position = position.Value;
            _anchorId = "PlayerFocalPoint" + TargetGameObject.GetInstanceID();
            TargetGameObject.name = _anchorId;
            //this new gameObject must be anchored (creating a new anchor)
            Anchor(TargetGameObject, _anchorId);
        }

        Enabled = enabled;
        IconEnabled = iconEnabled;
        ArrowEnabled = arrowEnabled;

        _ArrowUIImages = _focalPointArrowUI.GetComponentsInChildren<Image>();
        for (var i = 0; i < _ArrowUIImages.Length; i++)
        {
            _ArrowUIImages[i].enabled = false;
            _ArrowUIImages[i].color = color;
        }

        _IconUIImages = _focalPointIconUI.GetComponentsInChildren<Image>();
        for (var i = 0; i < _IconUIImages.Length; i++)
        {
            _IconUIImages[i].enabled = true;
            _IconUIImages[i].color = color;
        }


        //Position identifies a specific point over the targetGameObject to point to.
        //If position is not specified (is null) the focal point is the targetObject pivot
        //and the distance is calculated from its bounding box
        //(if there is no collider [hence no bounding box] the distance is 0).
        //If position is specified, the focal point is the position and the distance is 0.

        if (_position.HasValue)
        {
            _targetGameObjectBBDiameter = 0;
        }
        else
        {
            Collider collider;
            if ((collider = TargetGameObject.GetComponent<Collider>()) != null)
            {
                _targetGameObjectBBDiameter = collider.bounds.extents.magnitude;
            }
            else
            {
                _targetGameObjectBBDiameter = 0;
            }
        }

        CustomOutline oldOutline = TargetGameObject.GetComponent<CustomOutline>();
        if (oldOutline == null)
        {
            Outline = TargetGameObject.AddComponent<CustomOutline>();
            Outline.OutlineMode = CustomOutline.Mode.OutlineVisible;
            Outline.OutlineColor = color;
            Outline.OutlineWidth = outlineWidth;
            OutlineEnabled = outlineEnabled;
        }
        else if (outlineEnabled && replaceOutline)
        {
            //An outline is already present on the object, destroy it and replace it
            Object.Destroy(oldOutline);
            FocalPointManager.Instance.AddOutlineNextFrame(this, color, outlineWidth);
        }
        
        IsDestroyed = false;

        _referenceResolution = _focalPointArrowUI.GetComponentInParent<CanvasScaler>().referenceResolution;

        if (_position.HasValue && !_targetGameObjectWasNull)
        {
            _relativePosition = TargetGameObject.transform.InverseTransformPoint(_position.Value);
        }

    }

    public FocalPoint(GameObject targetPlayer1, GameObject targetPlayer2, GameObject targetPlayer3,
        GameObject focalPointArrowUI, GameObject focalPointIconUI, Color color, float outlineWidth,
        Vector3? position = null, bool enabled = true, bool outlineEnabled = true, bool replaceOutline = true,
        bool iconEnabled = true, bool arrowEnabled = true)
    {
        _temp = new Temp(targetPlayer1, targetPlayer2, targetPlayer3, color, outlineWidth, outlineEnabled,
            replaceOutline);

        _arLibrary = ARManager.Instance.ARLibrary;
        _focalPointArrowUI = focalPointArrowUI;
        _focalPointIconUI = focalPointIconUI;
        _position = position;
        _arrowUI = _focalPointArrowUI.transform.Find("Arrow").gameObject;
        _camera = Camera.main;

        Enabled = enabled;
        IconEnabled = iconEnabled;
        ArrowEnabled = arrowEnabled;

        _ArrowUIImages = _focalPointArrowUI.GetComponentsInChildren<Image>();
        for (var i = 0; i < _ArrowUIImages.Length; i++)
        {
            _ArrowUIImages[i].enabled = false;
            _ArrowUIImages[i].color = color;
        }

        _IconUIImages = _focalPointIconUI.GetComponentsInChildren<Image>();
        for (var i = 0; i < _IconUIImages.Length; i++)
        {
            _IconUIImages[i].enabled = true;
            _IconUIImages[i].color = color;
        }

        IsDestroyed = false;

        _referenceResolution = _focalPointArrowUI.GetComponentInParent<CanvasScaler>().referenceResolution;

        if (PhotonNetworkManager.Instance.LocalPlayer.PlayerIndex != 0)
        {
            Initialize(PhotonNetworkManager.Instance.LocalPlayer, new Color());
        }
        else
        {
            PhotonNetworkManager.Instance.LocalPlayer.ColorSetEvent += Initialize;
        }
    }

    protected virtual void Initialize(PlayerInfo player, Color newValue)
    {
        GameObject target = null;
        //Debug.Log("TEST: " + PhotonNetworkManager.Instance.LocalPlayer.PlayerIndex);
        switch (PhotonNetworkManager.Instance.LocalPlayer.PlayerIndex)
        {
            case 1:
                if (_temp.TargetPlayer1 != null)
                    target = _temp.TargetPlayer1;
                break;
            case 2:
                if (_temp.TargetPlayer2 != null)
                    target = _temp.TargetPlayer2;
                break;
            case 3:
                if (_temp.TargetPlayer3 != null)
                    target = _temp.TargetPlayer3;
                break;
        }

        if (target != null)
        {
            TargetGameObject = target;
            _targetGameObjectWasNull = false;

            //Position identifies a specific point over the targetGameObject to point to.
            //If position is not specified (is null) the focal point is the targetObject pivot
            //and the distance is calculated from its bounding box
            //(if there is no collider [hence no bounding box] the distance is 0).
            //If position is specified, the focal point is the position and the distance is 0.

            if (_position.HasValue)
            {
                _targetGameObjectBBDiameter = 0;
            }
            else
            {
                Collider collider;
                if ((collider = TargetGameObject.GetComponent<Collider>()) != null)
                {
                    _targetGameObjectBBDiameter = collider.bounds.extents.magnitude;
                }
                else
                {
                    _targetGameObjectBBDiameter = 0;
                }
            }

            CustomOutline oldOutline = TargetGameObject.GetComponent<CustomOutline>();
            if (oldOutline == null)
            {
                Outline = TargetGameObject.AddComponent<CustomOutline>();
                Outline.OutlineMode = CustomOutline.Mode.OutlineVisible;
                Outline.OutlineColor = _temp.Color;
                Outline.OutlineWidth = _temp.OutlineWidth;
                OutlineEnabled = _temp.OutlineEnabled;
            }
            else if (_temp.OutlineEnabled && _temp.ReplaceOutline)
            {
                //An outline is already present on the object, destroy it and replace it
                Object.Destroy(oldOutline);
                FocalPointManager.Instance.AddOutlineNextFrame(this, _temp.Color, _temp.OutlineWidth);
            }

            if (_position.HasValue && !_targetGameObjectWasNull)
            {
                _relativePosition = TargetGameObject.transform.InverseTransformPoint(_position.Value);
            }
        }
        else
        {
            ArrowEnabled = false;
            IconEnabled = false;
            OutlineEnabled = false;
        }

        PhotonNetworkManager.Instance.LocalPlayer.ColorSetEvent -= Initialize;
    }

    public virtual void Update ()
	{
	    if (!Enabled || IsDestroyed || TargetGameObject == null)
	    {
	        return;
	    }

        Vector3 targetScreenPosition;

        if (_position.HasValue)
	    {
            if(_targetGameObjectWasNull)
	            targetScreenPosition = _camera.WorldToScreenPoint(_position.Value);
            else
                targetScreenPosition = _camera.WorldToScreenPoint(TargetGameObject.transform.TransformPoint(_relativePosition));
        }
        else
        {
            targetScreenPosition = _camera.WorldToScreenPoint(TargetGameObject.transform.position);
        }

        if(IconEnabled)
	        _focalPointIconUI.transform.position = new Vector3(targetScreenPosition.x, targetScreenPosition.y, 0);


        Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;
	    Vector3 screenBounds = new Vector3(screenCenter.x - _borderSize, screenCenter.y - _borderSize, 0);

        //coordinate translation ( make (0,0) the center of the screen instead of (Screen.width/2, Screen.height/2) )
        targetScreenPosition -= screenCenter;

        bool isOffScreen = targetScreenPosition.z < 0 ||
                           targetScreenPosition.x < -screenCenter.x ||
	                       targetScreenPosition.x > screenCenter.x ||
	                       targetScreenPosition.y < -screenCenter.y ||
	                       targetScreenPosition.y > screenCenter.y;

        if (isOffScreen)
	    {
	        for (var i = 0; i < _ArrowUIImages.Length; i++)
	        {
	            _ArrowUIImages[i].enabled = true;
	        }

            if (targetScreenPosition.z < 0)
	        {
	            targetScreenPosition *= -1;
	        }
	        Vector3 positionOnScreenBorder;

            // y = m * x
            float m = targetScreenPosition.y / targetScreenPosition.x;

            if (targetScreenPosition.y > 0)
	        {
	            targetScreenPosition = new Vector3(screenCenter.y / m, screenCenter.y, 0);
	            positionOnScreenBorder = new Vector3(screenBounds.y / m, screenBounds.y, 0);
	        }
            else
	        {
	            targetScreenPosition = new Vector3(-screenCenter.y / m, -screenCenter.y, 0);
	            positionOnScreenBorder = new Vector3(-screenBounds.y / m, -screenBounds.y, 0);
	        }

            if (targetScreenPosition.x > screenCenter.x)
	        {
	            targetScreenPosition = new Vector3(screenCenter.x, screenCenter.x * m, 0);

	        }
	        else if (targetScreenPosition.x < -screenCenter.x)
	        {
	            targetScreenPosition = new Vector3(-screenCenter.x, -screenCenter.x * m, 0);
	        }

	        if (positionOnScreenBorder.x > screenBounds.x)
	        {
	            positionOnScreenBorder = new Vector3(screenBounds.x, screenBounds.x * m, 0);

	        }
	        else if (positionOnScreenBorder.x < -screenBounds.x)
	        {
	            positionOnScreenBorder = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
	        }

	        targetScreenPosition.z = 0;
	        _arrowUI.transform.localPosition = new Vector3(-Vector3.Distance(targetScreenPosition, positionOnScreenBorder), 0);
        }
        else
        {
            for (var i = 0; i < _ArrowUIImages.Length; i++)
            {
                _ArrowUIImages[i].enabled = false;
            }


            //Check if icon is obstructed by a dynamic or a static object; if so, change alpha
            Ray ray;
            if (_position.HasValue)
            {
                if (_targetGameObjectWasNull)
                    ray = new Ray(_position.Value, Camera.main.transform.position - _position.Value);
                else
                    ray = new Ray(TargetGameObject.transform.TransformPoint(_relativePosition),
                        Camera.main.transform.position - TargetGameObject.transform.TransformPoint(_relativePosition));
            }
            else ray = new Ray(TargetGameObject.transform.position, Camera.main.transform.position - TargetGameObject.transform.position);

            if (Physics.Raycast(ray, out _, ObjectManipulation.Instance.MaxInteractionDistance,
                GameUtils.Mask.DynamicObjects | GameUtils.Mask.StaticObjects))
            {
                for (var i = 0; i < _IconUIImages.Length; i++)
                {
                    _IconUIImages[i].SetAlpha(0.3f);
                }

            }
            else
            {
                for (var i = 0; i < _IconUIImages.Length; i++)
                {
                    _IconUIImages[i].SetAlpha(1f);
                }
            }

            /*float distanceFromObject = DistanceAndDiameterToPixelSize(
                                            Vector3.Distance(TargetGameObject.transform.position, _camera.transform.position),
                                            _targetGameObjectBBDiameter) / 2f;
            _arrowUI.transform.localPosition = new Vector3(-distanceFromObject, 0);


            if ((_arrowUI.transform.position - screenCenter).x < -screenBounds.x ||
                (_arrowUI.transform.position - screenCenter).x > screenBounds.x ||
                (_arrowUI.transform.position - screenCenter).y < -screenBounds.y ||
                (_arrowUI.transform.position - screenCenter).y > screenBounds.y)
            {
                Vector3 positionOnScreenBorder;
                float m = targetScreenPosition.y / targetScreenPosition.x;

                if (targetScreenPosition.y > 0)
                {
                    positionOnScreenBorder = new Vector3(screenBounds.y / m, screenBounds.y, 0);
                }
                else
                {
                    positionOnScreenBorder = new Vector3(-screenBounds.y / m, -screenBounds.y, 0);
                }

                if (positionOnScreenBorder.x > screenBounds.x)
                {
                    positionOnScreenBorder = new Vector3(screenBounds.x, screenBounds.x * m, 0);

                }
                else if (positionOnScreenBorder.x < -screenBounds.x)
                {
                    positionOnScreenBorder = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
                }

                targetScreenPosition.z = 0;
                _arrowUI.transform.localPosition = new Vector3(-Vector3.Distance(targetScreenPosition, positionOnScreenBorder), 0);
            }*/

        }

        float angle = Mathf.Atan2(targetScreenPosition.y, targetScreenPosition.x);
	    _focalPointArrowUI.transform.localEulerAngles = new Vector3(0, 0, angle * Mathf.Rad2Deg);

	    _focalPointArrowUI.transform.localPosition = new Vector3(targetScreenPosition.x,
	        targetScreenPosition.y, 0);

	    _arrowUI.transform.localPosition =
	        new Vector3(_arrowUI.transform.localPosition.x * _referenceResolution.x / Screen.width,
	            _arrowUI.transform.localPosition.y * _referenceResolution.y / Screen.height);
	    _focalPointArrowUI.transform.localPosition =
	        new Vector3(_focalPointArrowUI.transform.localPosition.x * _referenceResolution.x / Screen.width,
	            _focalPointArrowUI.transform.localPosition.y * _referenceResolution.y / Screen.height);

    }

    public virtual void Destroy()
    {
        if (Outline != null)
        {
            Outline.FadeOutAndDestroy();
            Outline = null;
        }

        Enabled = false;

        Object.Destroy(_focalPointArrowUI);
        Object.Destroy(_focalPointIconUI);

        //destroy the target if was null at the creation of FocalPoint, destroying also the anchor
        if (_targetGameObjectWasNull)
        {
            _arLibrary.DestroyAnchorAndGameObjects(_anchorId);
        }

        IsDestroyed = true;

        PhotonNetworkManager.Instance.LocalPlayer.ColorSetEvent -= Initialize;
    }

    private float DistanceAndDiameterToPixelSize(float distance, float diameter)
    {
        float pixelSize = (diameter * Mathf.Rad2Deg * Screen.height) / (distance * Camera.main.fieldOfView);
        return pixelSize;
    }

    private void Anchor(GameObject gameObj, string anchorId)
    {
        //Try to anchor this object to a surface, otherwise anchor to session

        Pose hitPose = new Pose();
        if (_arLibrary.CreateAnchorFromPose(anchorId, new Pose(gameObj.transform.position, gameObj.transform.rotation), out hitPose, 0.25f))
        {
            gameObj.transform.position = hitPose.position;
            gameObj.transform.rotation = hitPose.rotation;
            _arLibrary.AttachGameObjectToAnchor(anchorId, gameObj, true);
        }
        else
        {
            _arLibrary.CreateSessionAnchor(anchorId, new Pose(gameObj.transform.position, gameObj.transform.rotation));
            _arLibrary.AttachGameObjectToAnchor(anchorId, gameObj, true);
        }
    }
}
