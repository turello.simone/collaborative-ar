﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using TMPro;
using UnityEngine;

public class HintManager : MyUtils.SingletonInScene<HintManager>
{
    [MustBeAssigned] public GameObject HintPanel;
    [MustBeAssigned] public GameObject ContinuePanel;
    public float AnimationDuration = 0.2f;

    /// <summary>
    /// Whether the HintsSystem is enabled. When it is false, no hints are shown. Default value is true.
    /// </summary>
    public bool HintsSystemEnabled
    {
        get { return _hintsSystemEnabled; }
        set
        {
            _hintsSystemEnabled = value;
            HintEnabled = _hintEnabled;
        }
    }

    public string Text
    {
        get { return _text; }
        private set
        {
            _text = value;
            _hintTextMesh.text = value;
        }
    }

    /// <summary>
    /// Whether current hint is enabled.
    /// </summary>
    public bool HintEnabled
    {
        get { return _hintEnabled; }
        set
        {
            if (_hintsSystemEnabled)
            {
                if (!_hintEnabled && value)
                {
                    _animationManager.ShowMenu();
                    ContinuePanel.SetActive(_continueEnabled);
                }
                else if (_hintEnabled && !value)
                {
                    _animationManager.HideMenu();
                    ContinuePanel.SetActive(false);
                }
            }
            else
            {
                HintPanel.SetActive(false);
                ContinuePanel.SetActive(false);
            }
            _hintEnabled = value;
        }
    }

    private TextMeshProUGUI _hintTextMesh;
    private AiryUIAnimationManager _animationManager;
    private string _text;
    private bool _hintEnabled;
    private bool _hintsSystemEnabled;
    private bool _continueEnabled;
    private string _text2;


    public override void Awake()
    {
        base.Awake();

        _hintTextMesh = HintPanel.GetComponentInChildren<TextMeshProUGUI>();
        if(_hintTextMesh == null) Debug.LogError("HintManager: Missing hint text in the assigned HintPanel.");
        _animationManager = HintPanel.GetComponentInChildren<AiryUIAnimationManager>();
        if (_animationManager == null) Debug.LogError("HintManager: Missing AiryUIAnimationManager in the assigned HintPanel.");

        HintPanel.SetActive(false);
        _hintsSystemEnabled = true;
    }

    /// <summary>
    /// Adds and enable a new hint / change current hint to another one
    /// </summary>
    /// <param name="text">Hint text</param>
    public void AddHint(string text)
    {
        if (!_hintEnabled && !HintPanel.activeSelf)
        {
            //there wasn't a hint enabled
            if (text != null) Text = text;
            _continueEnabled = false;
            HintEnabled = true;
        }
        else
        {
            //there was already a hint enabled
            //disable current hint
            HintEnabled = false;

            //Wait for animation and then add hint
            this.DelayedAction(AnimationDuration + 0.1f, () => AddHint(text));
        }
    }

    public void AddHint(string text1, string text2)
    {
        if (!_hintEnabled && !HintPanel.activeSelf)
        {
            //there wasn't a hint enabled
            if (text1 != null) Text = text1;
            if (text2 != null) _text2 = text2;
            _continueEnabled = true;
            HintEnabled = true;
        }
        else
        {
            //there was already a hint enabled
            //disable current hint
            HintEnabled = false;

            //Wait for animation and then add hint
            this.DelayedAction(AnimationDuration + 0.1f, () => AddHint(text1, text2));
        }
    }

    public void OnContinueButtonClicked()
    {
        AddHint(_text2);
    }

}