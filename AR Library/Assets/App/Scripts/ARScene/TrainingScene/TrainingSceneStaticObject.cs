﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TrainingSceneStaticObject : StaticObject
{
    [MustBeAssigned] public GameObject[] Buttons;
    [MustBeAssigned] public GameObject Cube;
    [MustBeAssigned] public Collider FocalPointCollider;
    [MustBeAssigned] public Collider[] PlatformColliders;
    [MustBeAssigned] public Renderer[] Beacons;

    public bool PlatformsEnabled
    {
        get { return _platformsEnabled; }
        set
        {
            _platformsEnabled = value;
            for (var i = 0; i < PlatformColliders.Length; i++)
            {
                PlatformColliders[i].enabled = value;
                Beacons[i].enabled = false;
            }
        }
    }
    private bool _platformsEnabled = false;
    public bool FocalPointEnabled
    {
        get { return _focalPointEnabled; }
        set
        {
            _focalPointEnabled = value;
            FocalPointCollider.enabled = value;
        }
    }
    private bool _focalPointEnabled = false;
    public int NumberOfButtonPressed = 0;
    public float WaitTimeOnFocalPoint = 1f;

    public int CurrentCubeColor
    {
        get { return _currentCubeColor; }
        set
        {
            _currentCubeColor = value;
            switch (value)
            {
                case 0:
                    Cube.GetComponent<Renderer>().material.color = _startCubeColor;
                    break;
                case 1:
                    Cube.GetComponent<Renderer>().material.color = Color.red;
                    break;
                case 2:
                    Cube.GetComponent<Renderer>().material.color = Color.green;
                    break;
                case 3:
                    Cube.GetComponent<Renderer>().material.color = Color.blue;
                    break;
            }
        }
    }
    private int _currentCubeColor;

    public bool GetButtonPressed(int i) { return _ButtonsPressed[i].Value; }
    public void SetButtonPressed(int i, bool value) { _ButtonsPressed[i].Value = value; }
    private const string BUTTON_PRESSED = "ButtonPressed";
    private SyncVarBool[] _ButtonsPressed;
    private float _progressTime = 0;
    private Color _startCubeColor;

    private Animator _animator;

    #region Unity

    protected override void Awake()
    {
        base.Awake();

        _animator = GetComponentInChildren<Animator>();

        _ButtonsPressed = new SyncVarBool[Buttons.Length];
        for (var i = 0; i < Buttons.Length; i++)
        {
            string buttonName = BUTTON_PRESSED + i;
            _ButtonsPressed[i] = new SyncVarBool(false, _photonView, buttonName);
            _ButtonsPressed[i].ValueChanged += value => _animator.SetBool(buttonName, value);
        }

        FocalPointEnabled = false;
        PlatformsEnabled = false;
        _startCubeColor = Cube.GetComponent<Renderer>().material.color;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TrainingSceneStaticObject = this;
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        NumberOfButtonPressed = 0;

        if (_networkManager.IsMasterClient)
        {
            for (var i = 0; i < Buttons.Length; i++)
            {
                _ButtonsPressed[i].Value = false;
            }
        }

        FocalPointEnabled = false;
        PlatformsEnabled = false;
        _progressTime = 0;
        CurrentCubeColor = 0;
    }

    public override void Enable(bool objEnabled)
    {
        PlatformsEnabled = false;
        base.Enable(objEnabled);
    }

    private void Update()
    {
        if (_networkManager.LocalPlayer.State != GameUtils.PlayerState.InGame)
            return;

        if (_networkManager.IsMasterClient && FocalPointEnabled)
        {
            int count = 0;

            if (CheckFocalPoint(_networkManager.LocalPlayer)) count++;
            else
            {
                foreach (PlayerInfo player in _networkManager.OtherPlayers.Values)
                {
                    if (player.State == GameUtils.PlayerState.InGame && CheckFocalPoint(player))
                    {
                        count++;
                        break;
                    }
                }
            }

            if (count > 0)
            {
                _progressTime += Time.deltaTime;
                if (_progressTime >= WaitTimeOnFocalPoint)
                {
                    _progressTime = 0.0f;
                    _photonView.RPC("ChangeCubeColorRPC", RpcTarget.AllBuffered, (CurrentCubeColor+1)%4);
                }
            }
            else
            {
                _progressTime = 0.0f;
            }
        }

        if (PlatformsEnabled)
        {
            for (var i = 0; i < PlatformColliders.Length; i++)
            {
                int count = 0;

                if (CheckPlatforms(_networkManager.LocalPlayer, PlatformColliders[i])) count++;
                else
                {
                    foreach (var player in _networkManager.OtherPlayers.Values)
                    {
                        if (player.State == GameUtils.PlayerState.InGame && CheckPlatforms(player, PlatformColliders[i]))
                        {
                            count++;
                            break;
                        }
                    }
                }

                Beacons[i].enabled = count > 0;
            }
        }
    }

    #endregion

    #region public methods

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        for (var i = 0; i < Buttons.Length; i++)
        {
            if (interactiveObject == Buttons[i])
            {
                SetButtonPressed(i, isPressed);
                NumberOfButtonPressed++;
                break;
            }
        }
    }

    public override bool IsUseful(GameObject gameObj)
    {
        for (var i = 0; i < Buttons.Length; i++)
        {
            if (gameObj == Buttons[i]) return true;
        }
        return false;
    }

    [PunRPC]
    public void ChangeCubeColorRPC(int color)
    {
        Debug.Log("ChangeCubeColorRPC received.");
        CurrentCubeColor = color;
    }

    #endregion

    #region private methods

    private bool CheckFocalPoint(PlayerInfo player)
    {
        Ray ray = new Ray(player.Player.transform.position, player.Player.transform.forward);
        return FocalPointCollider.Raycast(ray, out var hitInfoFocalPoint, ObjectManipulation.Instance.MaxInteractionDistance) &&
               (!Physics.Raycast(ray, out var hitInfoObjects, ObjectManipulation.Instance.MaxInteractionDistance,
                    GameUtils.Mask.StaticObjects | GameUtils.Mask.DynamicObjects) ||
                hitInfoObjects.distance >= hitInfoFocalPoint.distance);
    }
    
    private bool CheckPlatforms(PlayerInfo player, Collider collider)
    {
        return GameUtils.IsPointWithinCollider(collider, player.Player.transform.position);
    }

    #endregion
}
