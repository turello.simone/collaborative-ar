﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TrainingGazeSceneCubes : StaticObject
{
    [MustBeAssigned] public GameObject[] Cubes;
    [MustBeAssigned] public Collider[] FocalPointColliders;

    public float WaitTimeOnFocalPoint = 2f;
    public float CooldownFocalPoint = 3f;

    private float[] _progressTime;
    private Sequence[] _sequences;
    private Color _startColor;
    private Color _cubeHighlightCompleteColor = Color.green.BrightnessOffset(0.8f);

    #region Unity

    protected override void Awake()
    {
        base.Awake();

        _startColor = Cubes[0].GetComponent<Renderer>().material.color;
        _progressTime = new float[Cubes.Length];
        _sequences = new Sequence[Cubes.Length];

        for (var i = 0; i < _progressTime.Length; i++)
        {
            _progressTime[i] = 0;
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TrainingGazeSceneCubes = this;
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < Cubes.Length; i++)
        {
            Cubes[i].GetComponent<Renderer>().material.DOKill();
            Cubes[i].GetComponent<Renderer>().material.color = _startColor;
        }

        for (var i = 0; i < _sequences.Length; i++)
        {
            _sequences[i].Kill();
        }

        for (var i = 0; i < _progressTime.Length; i++)
        {
            _progressTime[i] = 0;
        }
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if ((currentScene.Type != SubSceneManager.SubSceneType.TrainingGaze &&
            _networkManager.LocalPlayer.State != GameUtils.PlayerState.InGame) ||
            GameUIManager.Instance.InstructionsPanelEnabled)
            return;

        for (var i = 0; i < Cubes.Length; i++)
        {
            if (CheckFocalPoint(i, _networkManager.LocalPlayer))
            {
                _progressTime[i] += Time.deltaTime;
                if (_progressTime[i] >= WaitTimeOnFocalPoint)
                {
                    _progressTime[i] = - CooldownFocalPoint;
                    CompleteCube(i);
                }
            }
            else
            {
                if(_progressTime[i] < 0)
                    _progressTime[i] += Time.deltaTime;
                else
                    _progressTime[i] = 0;
            }
        }
    }

    #endregion

    #region private methods

    private bool CheckFocalPoint(int index, PlayerInfo player)
    {
        Ray ray = new Ray(player.Player.transform.position, player.Player.transform.forward);
        return FocalPointColliders[index].Raycast(ray, out var hitInfoFocalPoint, ObjectManipulation.Instance.MaxInteractionDistance) &&
               (!Physics.Raycast(ray, out var hitInfoObjects, ObjectManipulation.Instance.MaxInteractionDistance,
                    GameUtils.Mask.StaticObjects | GameUtils.Mask.DynamicObjects) ||
                hitInfoObjects.distance >= hitInfoFocalPoint.distance);
    }

    private void CompleteCube(int index)
    {
        Cubes[index].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

        _sequences[index] = DOTween.Sequence();
        _sequences[index].AppendInterval(CooldownFocalPoint - 0.5f)
            .Append(Cubes[index].GetComponent<Renderer>().material.DOColor(_startColor, 0.5f));
    }

    #endregion
}
