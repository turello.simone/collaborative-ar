﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TrainingScene : IGameScene
{
    public TrainingScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TrainingSceneStaticObject == null)
                _networkManager.SpawnStaticObject(_gameManager.TrainingSceneStaticObjectPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TrainingSceneStaticObject.Reset();

            Pose pose = _networkManager.CurrentRoom.ContraptionPose;
            pose.position += pose.forward * 0.7f;
            pose.rotation = Quaternion.LookRotation(-pose.forward, pose.up);
            if (_networkManager.CurrentRoom.TrainingSceneDynamicObject1 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingSceneDynamicObject1Prefab.name, pose);
            else
                _networkManager.CurrentRoom.TrainingSceneDynamicObject1.Reset();

            pose = _networkManager.CurrentRoom.ContraptionPose;
            pose.position += -pose.forward * 0.7f;
            pose.rotation = Quaternion.LookRotation(pose.forward, pose.up);
            if (_networkManager.CurrentRoom.TrainingSceneDynamicObject2 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingSceneDynamicObject2Prefab.name, pose);
            else
                _networkManager.CurrentRoom.TrainingSceneDynamicObject2.Reset();

            pose = _networkManager.CurrentRoom.ContraptionPose;
            pose.position += pose.right * 0.7f;
            pose.rotation = Quaternion.LookRotation(-pose.right, pose.up);
            if (_networkManager.CurrentRoom.TrainingSceneDynamicObject3 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingSceneDynamicObject3Prefab.name, pose);
            else
                _networkManager.CurrentRoom.TrainingSceneDynamicObject3.Reset();

            pose = _networkManager.CurrentRoom.ContraptionPose;
            pose.position += -pose.right * 0.7f;
            pose.rotation = Quaternion.LookRotation(pose.right, pose.up);
            if (_networkManager.CurrentRoom.TrainingSceneDynamicObject4 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingSceneDynamicObject4Prefab.name, pose);
            else
                _networkManager.CurrentRoom.TrainingSceneDynamicObject4.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        _networkManager.AvatarType = GameUtils.AvatarType.Simple;
        _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.None;
        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.None;
        _networkManager.PlayerFocalPointEnabled = false;

        /*string text1 = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_PlayerColor");
        text1 = text1.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());
        string text2 = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_PlayerFocalPoint");
        
        HintManager.Instance.AddHint(text1, text2);*/

        ObjectManipulation.Instance.InteractionEnabled = true;
        _networkManager.CurrentRoom.TrainingSceneStaticObject.PlatformsEnabled = true;

        /*ObjectManipulation.Instance.InteractionEnabled = true;
        _networkManager.CurrentRoom.TrainingSceneStaticObject.FocalPointEnabled = true;

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_InteractiveObjects"));
        yield return new WaitUntil(
            () => _networkManager.CurrentRoom.TrainingSceneStaticObject.NumberOfButtonPressed > 0);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectGrab"));
        yield return new WaitUntil(() => _networkManager.LocalPlayer.ObjectHeld != null);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectManip"));
        yield return new WaitUntil(() =>
            _networkManager.LocalPlayer.ObjectHeld != null &&
            _networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().IsFreezed);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectUnfreeze"));
        yield return new WaitUntil(() =>
            _networkManager.LocalPlayer.ObjectHeld != null &&
            !_networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().IsFreezed);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectPlace"));
        yield return new WaitUntil(() => _networkManager.LocalPlayer.ObjectHeld == null);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_PlayerFocalPoint"));*/
    }

    public override void Destroy()
    {
        base.Destroy();
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneStaticObject != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneStaticObject.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject1 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject1.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject2 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject2.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject3 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject3.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject4 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingSceneDynamicObject4.Enable(enabled);
    }
}
