﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingPosScene : IGameScene
{
    public TrainingPosScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TrainingPosScenePlates == null)
                _networkManager.SpawnStaticObject(_gameManager.TrainingPosScenePlatesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TrainingPosScenePlates.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        _networkManager.AvatarType = GameUtils.AvatarType.Simple;
        _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.None;
        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        _networkManager.PlayerFocalPointEnabled = false;

        ObjectManipulation.Instance.InteractionEnabled = true;

        _networkManager.CurrentRoom.TrainingPosScenePlates.PlatformsEnabled = true;

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_PositionPlatform"));
    }

    public override void Destroy()
    {
        base.Destroy();
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingPosScenePlates != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingPosScenePlates.Enable(enabled);
    }
}
