﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TrainingSceneDynamicObject : DynamicObject
{
    public enum NumberType
    {
        One,
        Two,
        Three,
        Four
    }

    [MustBeAssigned] public NumberType Number;
    [MustBeAssigned] public Renderer BodyRenderer;
    [MustBeAssigned] public GameObject Ring;
    public float RingRotationSpeed = 200f;
    public Gradient Gradient;
    private PhotonView _ringPhotonView;
    private Color _startColor;

    protected override void Awake()
    {
        base.Awake();

        _ringPhotonView = Ring.GetComponent<PhotonView>();
        _startColor = BodyRenderer.material.color;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        switch (Number)
        {
            case NumberType.One:
                _networkManager.CurrentRoom.TrainingSceneDynamicObject1 = this;
                break;
            case NumberType.Two:
                _networkManager.CurrentRoom.TrainingSceneDynamicObject2 = this;
                break;
            case NumberType.Three:
                _networkManager.CurrentRoom.TrainingSceneDynamicObject3 = this;
                break;
            case NumberType.Four:
                _networkManager.CurrentRoom.TrainingSceneDynamicObject4 = this;
                break;
        }

    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        BodyRenderer.material.color = _startColor;
        Ring.transform.localRotation = Quaternion.identity;
    }

    protected override void Update()
    {
        base.Update();

        //Update Number
        /*float offset = (Ring.transform.localRotation.eulerAngles.y % 720f) / 720f / 0.5f;
        if (offset > 1) offset = 2 - offset;
        BodyRenderer.material.color = Color.Lerp(_startColor, new Color(0.4f, 0.8f, 0), offset);*/

        float offset = (Mathf.Abs(Ring.transform.localRotation.eulerAngles.y) % 90f) / 90f;
        BodyRenderer.material.color = Gradient.Evaluate(offset);
        //Debug.LogFormat("Test {0}", offset);
    }


    public override void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        if (Owner == _networkManager.LocalPlayer && interactiveObject == Ring)
        {
            Vector3 gearCenterPos = Ring.transform.position;
            Vector2 startScreenPos = screenPos - deltaMovement;
            Vector2 destScreenPos = screenPos;

            Vector3 startPos = Camera.main.ScreenToWorldPoint(new Vector3(startScreenPos.x, startScreenPos.y, Camera.main.nearClipPlane));
            Vector3 destPos = Camera.main.ScreenToWorldPoint(new Vector3(destScreenPos.x, destScreenPos.y, Camera.main.nearClipPlane));

            Vector3 startVector = startPos - gearCenterPos;
            Vector3 destVector = destPos - gearCenterPos;

            float angle = Vector3.SignedAngle(startVector, destVector, Ring.transform.up);

            /*Vector2 gearCenterScreenPos = Camera.main.WorldToScreenPoint(Ring.transform.position);
            Vector2 startScreenPos = screenPos - deltaMovement;
            Vector2 destScreenPos = screenPos;

            Vector2 startVector = startScreenPos - gearCenterScreenPos;
            Vector2 destVector = destScreenPos - gearCenterScreenPos;

            float angle = Vector2.SignedAngle(startVector, destVector);*/

            Ring.transform.localRotation *= Quaternion.Euler(0, RingRotationSpeed * angle, 0);
        }
    }

    [PunRPC]
    public override void GrabRPC(Vector3 startPosition, Quaternion startRotation, PhotonMessageInfo info)
    {
        base.GrabRPC(startPosition, startRotation, info);

        if (Owner == _networkManager.LocalPlayer)
        {
            _ringPhotonView.RequestOwnership();
        }
    }
}
