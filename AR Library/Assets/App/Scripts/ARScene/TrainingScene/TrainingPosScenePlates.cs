﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TrainingPosScenePlates : StaticObject
{
    [MustBeAssigned] public Collider[] PlatformColliders;
    [MustBeAssigned] public Renderer[] Beacons;

    public bool PlatformsEnabled
    {
        get { return _platformsEnabled; }
        set
        {
            _platformsEnabled = value;
            for (var i = 0; i < PlatformColliders.Length; i++)
            {
                PlatformColliders[i].enabled = value;
                Beacons[i].enabled = false;
            }
        }
    }
    private bool _platformsEnabled = false;

    #region Unity

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.TrainingPosScenePlates = this;

        PlatformsEnabled = true;
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        PlatformsEnabled = false;
    }

    public override void Enable(bool objEnabled)
    {
        PlatformsEnabled = false;
        base.Enable(objEnabled);
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (currentScene.Type != SubSceneManager.SubSceneType.TrainingPos &&
            _networkManager.LocalPlayer.State < GameUtils.PlayerState.InGame &&
            GameUIManager.Instance.CurrentSettingsPanel != GameUIManager.SettingsPanel.None)
            return;

        if (PlatformsEnabled)
        {
            for (var i = 0; i < PlatformColliders.Length; i++)
            {
                int count = 0;

                if (CheckPlatforms(_networkManager.LocalPlayer, PlatformColliders[i])) count++;
                else
                {
                    foreach (var player in _networkManager.OtherPlayers.Values)
                    {
                        if (player.State == GameUtils.PlayerState.InGame && CheckPlatforms(player, PlatformColliders[i]))
                        {
                            count++;
                            break;
                        }
                    }
                }

                Beacons[i].enabled = count > 0;
            }
        }
    }

    #endregion

    #region private methods

    private bool CheckPlatforms(PlayerInfo player, Collider collider)
    {
        return GameUtils.IsPointWithinCollider(collider, player.Player.transform.position);
    }

    #endregion
}
