﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingManipScene : IGameScene
{
    public TrainingManipScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TrainingManipSceneRectangle == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingManipSceneRectanglePrefab.name, _networkManager.CurrentRoom.CandleHolderPose);
            else
                _networkManager.CurrentRoom.TrainingManipSceneRectangle.Reset();

            if (_networkManager.CurrentRoom.TrainingManipSceneSquare == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingManipSceneSquarePrefab.name, _networkManager.CurrentRoom.ChalicePose);
            else
                _networkManager.CurrentRoom.TrainingManipSceneSquare.Reset();

            if (_networkManager.CurrentRoom.TrainingManipSceneTriangle == null)
                _networkManager.SpawnDynamicObject(_gameManager.TrainingManipSceneTrianglePrefab.name, _networkManager.CurrentRoom.SkullPose);
            else
                _networkManager.CurrentRoom.TrainingManipSceneTriangle.Reset();

            if (_networkManager.CurrentRoom.TrainingManipSceneHoles == null)
                _networkManager.SpawnStaticObject(_gameManager.TrainingManipSceneHolesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TrainingManipSceneHoles.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        _networkManager.AvatarType = GameUtils.AvatarType.Simple;
        _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.None;
        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.None;
        _networkManager.PlayerFocalPointEnabled = false;

        ObjectManipulation.Instance.InteractionEnabled = true;
        ARManager.Instance.EnableHoleInOcclusionWithShadows(true, _networkManager.CurrentRoom.TrainingManipSceneHoles.OcclusionHoleRadius);

        ShowNextSubTaskButton(_networkManager.LocalPlayer);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectGrab"));
        yield return new WaitUntil(() => _networkManager.LocalPlayer.ObjectHeld != null);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectManip"));
        yield return new WaitUntil(() =>
            _networkManager.LocalPlayer.ObjectHeld != null &&
            _networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().IsFreezed);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectUnfreeze"));
        yield return new WaitUntil(() =>
            _networkManager.LocalPlayer.ObjectHeld != null &&
            !_networkManager.LocalPlayer.ObjectHeld.GetComponent<DynamicObject>().IsFreezed);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectPlace"));
        yield return new WaitUntil(() => _networkManager.LocalPlayer.ObjectHeld == null);

        ShowHint(Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_DynamicObjectEmbed"));
    }

    public override void Destroy()
    {
        base.Destroy();
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneRectangle != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneRectangle.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneSquare != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneSquare.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneTriangle != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneTriangle.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneHoles != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingManipSceneHoles.Enable(enabled);
    }

    private void ShowNextSubTaskButton(PlayerInfo player)
    {
        //Show next scene button
        if (_networkManager.IsMasterClient)
        {
            GameUIManager.Instance.SetNextSubTaskButtonAction(SpawnObjects);
            GameUIManager.Instance.SetActiveNextSubTaskPanel(true);
        }
        else
        {
            _networkManager.CurrentRoom.MasterClientSwitchedEvent += ShowNextSubTaskButton;
        }
    }

}
