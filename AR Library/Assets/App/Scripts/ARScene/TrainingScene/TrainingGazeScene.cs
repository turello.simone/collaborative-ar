﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TrainingGazeScene : IGameScene
{
    public TrainingGazeScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }


    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TrainingGazeSceneCubes == null)
                _networkManager.SpawnStaticObject(_gameManager.TrainingGazeSceneCubesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TrainingGazeSceneCubes.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        _networkManager.AvatarType = GameUtils.AvatarType.Simple;
        _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.None;
        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        _networkManager.PlayerFocalPointEnabled = false;

        string text1 = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_PlayerColor");
        text1 = text1.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());
        string text2 = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/Instructions_GazeConvergence");

        HintManager.Instance.AddHint(text1, text2);

        ObjectManipulation.Instance.InteractionEnabled = true;
    }

    public override void Destroy()
    {
        base.Destroy();
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TrainingGazeSceneCubes != null)
            PhotonNetworkManager.Instance.CurrentRoom.TrainingGazeSceneCubes.Enable(enabled);
    }
}
