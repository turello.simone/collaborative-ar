﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalRubyShared;
using MyBox;
using MyUtils;
using UnityEngine;
using UnityEngine.Events;

public class EscapeGameManager : SingletonInScene<EscapeGameManager>
{
    public string MarkerName;
    public string MarkerAnchorId = "MarkerAnchor";

    private ARManager _arManager;
    private ARLibrary _arLibrary;
    private GameUIManager _gameUIManager;
    private PhotonNetworkManager _networkManager;
    private ObjectManipulation _objectManipulation;
    //private GameObject _light;

    private TapGestureRecognizer _tapGestureRecognizer;
    private OneTouchScaleGestureRecognizer _swipeGestureRecognizer;
    private RotateGestureRecognizer _rotateGestureRecognizer;

    [Header("Prescan Prefabs")]
    [MustBeAssigned] public GameObject PrescanFloorPrefab;
    [MustBeAssigned] public GameObject PrescanTableBigPrefab;
    [MustBeAssigned] public GameObject PrescanTableSmallPrefab;
    [MustBeAssigned] public GameObject PrescanWallPrefab;

    [Header("Training Scene Prefabs")]
    [MustBeAssigned] public GameObject TrainingSceneStaticObjectPrefab;
    [MustBeAssigned] public GameObject TrainingSceneDynamicObject1Prefab;
    [MustBeAssigned] public GameObject TrainingSceneDynamicObject2Prefab;
    [MustBeAssigned] public GameObject TrainingSceneDynamicObject3Prefab;
    [MustBeAssigned] public GameObject TrainingSceneDynamicObject4Prefab;
    [MustBeAssigned] public GameObject TrainingGazeSceneCubesPrefab;
    [MustBeAssigned] public GameObject TrainingManipSceneRectanglePrefab;
    [MustBeAssigned] public GameObject TrainingManipSceneSquarePrefab;
    [MustBeAssigned] public GameObject TrainingManipSceneTrianglePrefab;
    [MustBeAssigned] public GameObject TrainingManipSceneHolesPrefab;
    [MustBeAssigned] public GameObject TrainingPosScenePlatesPrefab;

    [Header("Tutorial Scene Prefabs")]
    [MustBeAssigned] public GameObject TutorialSceneContraptionPrefab;
    [MustBeAssigned] public GameObject TutorialSceneCylinderAzathothPrefab;
    [MustBeAssigned] public GameObject TutorialSceneCylinderCthughaPrefab;
    [MustBeAssigned] public GameObject TutorialSceneCylinderYogSothothPrefab;

    [Header("Pedestal Scene Prefabs")]
    [MustBeAssigned] public GameObject PedestalScenePedestalPrefab;
    [MustBeAssigned] public GameObject PedestalScenePressurePlate1Prefab;
    [MustBeAssigned] public GameObject PedestalScenePressurePlate2Prefab;
    [MustBeAssigned] public GameObject PedestalScenePressurePlate3Prefab;

    [Header("Painting Scene Prefabs")]
    [MustBeAssigned] public GameObject PaintingScenePaintingPrefab;
    [MustBeAssigned] public GameObject PaintingSceneRiddlePrefab;
    [MustBeAssigned] public GameObject PaintingSceneFlashlight1Prefab;
    [MustBeAssigned] public GameObject PaintingSceneFlashlight2Prefab;
    [MustBeAssigned] public GameObject PaintingSceneFlashlight3Prefab;

    [Header("Translation Scene Prefabs")]
    [MustBeAssigned] public GameObject TranslationSceneContraptionPrefab;
    [MustBeAssigned] public GameObject TranslationScenePaintingPrefab;
    [MustBeAssigned] public GameObject TranslationSceneCandleHolderPrefab;
    [MustBeAssigned] public GameObject TranslationSceneChalicePrefab;
    [MustBeAssigned] public GameObject TranslationSceneDaggerPrefab;
    [MustBeAssigned] public GameObject TranslationSceneSkullPrefab;

    [Header("Tasks Prefabs")]
    [MustBeAssigned] public GameObject TaskGazeSameSysCubesPrefab;
    [MustBeAssigned] public GameObject TaskGazeDiffSysCubesPrefab;
    [MustBeAssigned] public GameObject TaskGazeSameUsrCubesPrefab;
    [MustBeAssigned] public GameObject TaskGazeDiffUsrCubesPrefab;
    [MustBeAssigned] public GameObject TaskManipDiffSysRectanglePrefab;
    [MustBeAssigned] public GameObject TaskManipDiffSysSquarePrefab;
    [MustBeAssigned] public GameObject TaskManipDiffSysTrianglePrefab;
    [MustBeAssigned] public GameObject TaskManipDiffSysHolesPrefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrTriangleFirst2Prefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrTriangleFirst3Prefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrTriangleSecond1Prefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrTriangleSecond3Prefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrTriangleThird1Prefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrTriangleThird2Prefab;
    [MustBeAssigned] public GameObject TaskManipDiffUsrHolesPrefab;
    [MustBeAssigned] public GameObject TaskPosSameSysPlatesPrefab;
    [MustBeAssigned] public GameObject TaskPosDiffSysPlatesPrefab;
    [MustBeAssigned] public GameObject TaskPosSameUsrPlatesPrefab;
    [MustBeAssigned] public GameObject TaskPosDiffUsrPlatesPrefab;

    [Header("Initial Prep")]
    [ReadOnly] public bool InsideRoomGameStarted;
    [ReadOnly] public SubSceneManager.SubSceneType SubSceneTypeInitialSelection;
    [ReadOnly] public SubSceneManager.Interaction TaskInteractionInitialSelection;
    [ReadOnly] public SubSceneManager.Location TaskLocationInitialSelection;
    [ReadOnly] public SubSceneManager.VisualizationSequence TaskVisualizationSequenceInitialSelection;
    [ReadOnly] public SubSceneManager.Visualization SubSceneVisualizationInitialSelection;
    [ReadOnly] public int TaskInSequenceInitialSelection;
    [ReadOnly] public bool ObjectsPositionConfirmed;

    private string _imageAnchorId;
    private bool _objectsPositioningHasTapped;
    private GameObject _objectEdited;
    private Vector3 _oldRaycastHitPos;
    private bool _firstRaycastHit;

    #region UNITY

    public override void Awake()
    {
        base.Awake();
        _arManager = ARManager.Instance;
        _arLibrary = _arManager.ARLibrary;
        _networkManager = PhotonNetworkManager.Instance;

        _networkManager.AnchorId = MarkerAnchorId;
        _arLibrary.LightEstimationMode = ARUtils.LightEstimationMode.EnvironmentalHDRWithReflections;

        _arManager.SetImageTrackingDatabase();

        //Initialize gesture recognizers
        //Create tap recognizer
        _tapGestureRecognizer = new TapGestureRecognizer();

        //Create swipe recognizer
        _swipeGestureRecognizer = new OneTouchScaleGestureRecognizer();

        //Create rotate recognizer
        _rotateGestureRecognizer = new RotateGestureRecognizer();
    }

    private void Start()
    {
        _objectManipulation = ObjectManipulation.Instance;
        _gameUIManager = GameUIManager.Instance;

        StartCoroutine(InitialPrep());
    }

    public override void OnDestroy()
    {
        Lean.Pool.LeanPool.DespawnAll();
        _arLibrary.ImageAnchorCreatedEvent -= ArLibraryOnImageAnchorCreatedEvent;
        _tapGestureRecognizer.StateUpdated -= ObjectsPositioningTapCallback;
        _swipeGestureRecognizer.StateUpdated -= ObjectsEditingSwipeCallback;
        _rotateGestureRecognizer.StateUpdated -= ObjectsEditingRotateCallback;

        base.OnDestroy();
    }

    #endregion


    #region Coroutines

    private IEnumerator InitialPrep()
    {
        yield return new WaitUntil(() => _networkManager.CurrentRoom != null);

        if (_networkManager.IsMasterClient && _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized)
        {
            _networkManager.CurrentRoom.RoomScene = new SubSceneManager.RoomScene(SubSceneManager.SubSceneType.InitialPrep);

            //Inside Room
            Camera.main.cullingMask = ~(GameUtils.Mask.Players | GameUtils.Mask.DynamicObjects |
                                        GameUtils.Mask.StaticObjects | GameUtils.Mask.FocalPoints |
                                        GameUtils.Mask.PositionColliders | GameUtils.Mask.Special);
            InsideRoomGameStarted = false;
            _gameUIManager.SetActiveSettingsPanel(GameUIManager.SettingsPanel.InsideRoom);
            yield return new WaitUntil(() => InsideRoomGameStarted);

            yield return StartCoroutine(MasterClientInitialPrep());
        }
        else
        {
            //I'm not the master client or the room has been already initialized

            //Inside Room
            Camera.main.cullingMask = ~(GameUtils.Mask.Players | GameUtils.Mask.DynamicObjects |
                                        GameUtils.Mask.StaticObjects | GameUtils.Mask.FocalPoints |
                                        GameUtils.Mask.PositionColliders | GameUtils.Mask.Special);
            InsideRoomGameStarted = false;
            _gameUIManager.SetActiveSettingsPanel(GameUIManager.SettingsPanel.InsideRoom);
            yield return new WaitUntil(() => InsideRoomGameStarted);

            yield return StartCoroutine(MarkerSync());

            string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/WaitForMaster");
            HintManager.Instance.AddHint(instructions);
        }

        _networkManager.StartMultiplayer();
        OnRoomSceneChanged(_networkManager.CurrentRoom.RoomScene);

        _arManager.SetOverlaySurfaceMaterials(ARManager.SurfaceRenderType.OcclusionWithShadows);
        Camera.main.cullingMask = GameUtils.Mask.Everything;

        _networkManager.CurrentRoom.RoomSceneChangedEvent += OnRoomSceneChanged;

    }

    private void OnRoomSceneChanged(SubSceneManager.RoomScene roomScene)
    {
        SubSceneManager.Instance.LoadSubScene(roomScene);
    }

    private IEnumerator MarkerSync()
    {
        GameObject discovery = null;

        if (_arManager.Platform == ARUtils.ARPlatform.ARCore)
        {
            discovery = Instantiate(Resources.Load("ARCore/PlaneDiscovery") as GameObject);
        }

        _arLibrary.EnableImageTracking = true;
        _arManager.SetOverlaySurfaceMaterials(ARManager.SurfaceRenderType.Visualization);
        _imageAnchorId = null;
        _arLibrary.ImageAnchorCreatedEvent += ArLibraryOnImageAnchorCreatedEvent;
        _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.DetectSurface);
        yield return new WaitUntil(() => _imageAnchorId != null);
        _arLibrary.ImageAnchorCreatedEvent -= ArLibraryOnImageAnchorCreatedEvent;
        yield return StartCoroutine(CheckMarkerOnSurface(_imageAnchorId));

        //DEBUG
        GameObject pointer = Instantiate(Resources.Load("Pointer"), Vector3.zero, Quaternion.identity) as GameObject;
        pointer.GetComponentInChildren<MeshRenderer>().material.color = Color.yellow;
        _arLibrary.AttachGameObjectToAnchor(_imageAnchorId, pointer, false);

        yield return new WaitForSeconds(3.0f);
        _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.DetectMarker);
        yield return StartCoroutine(CreateMarkerAnchor(_imageAnchorId));
        _networkManager.LocalPlayer.State = GameUtils.PlayerState.Synchronized;

        //DEBUG
        _arLibrary.DestroyAnchorAndGameObjects(_imageAnchorId);
        Debug.Log("Marker Anchor \"" + MarkerAnchorId + "\" Created");
        GameObject pointer2 = Instantiate(Resources.Load("Pointer"), Vector3.zero, Quaternion.identity) as GameObject;
        pointer2.GetComponentInChildren<MeshRenderer>().material.color = Color.green;
        _arLibrary.AttachGameObjectToAnchor(MarkerAnchorId, pointer2, false);

        yield return new WaitForSeconds(1.0f);
        _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.Default);
        _arLibrary.EnableImageTracking = false;

        if (_arManager.Platform == ARUtils.ARPlatform.ARCore)
        {
            if (discovery != null) Destroy(discovery);
        }
    }

    private IEnumerator MasterClientInitialPrep()
    {
        //Select subscene
        SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Uninitialized;
        TaskInteractionInitialSelection = SubSceneManager.Interaction.Uninitialized;
        TaskLocationInitialSelection = SubSceneManager.Location.Uninitialized;
        _gameUIManager.SetActiveSettingsPanel(GameUIManager.SettingsPanel.SceneSelection);
        yield return new WaitUntil(() => SubSceneTypeInitialSelection != SubSceneManager.SubSceneType.Uninitialized);

        yield return StartCoroutine(MarkerSync());

        //Sequential positioning of Prefabs
        _tapGestureRecognizer.StateUpdated += ObjectsPositioningTapCallback;
        FingersScript.Instance.AddGesture(_tapGestureRecognizer);

        string anchorName = "temp";
        int anchorIndex = 0;

        GameObject prescanTableBig = null;
        Pose prescanTableBigPose = new Pose();
        yield return StartCoroutine(PositionPrefab(PrescanTableBigPrefab, anchorName + anchorIndex, (result) =>
        {
            prescanTableBig = result;
            prescanTableBigPose = new Pose(result.transform.position, result.transform.rotation);
        }));
        anchorIndex++;

        GameObject prescanTableSmall = null;
        Pose flashlight1Pose = new Pose();
        Pose flashlight2Pose = new Pose();
        Pose flashlight3Pose = new Pose();
        Pose candleHolderPose = new Pose();
        Pose chalicePose = new Pose();
        Pose daggerPose = new Pose();
        Pose skullPose = new Pose();
        yield return StartCoroutine(PositionPrefab(PrescanTableSmallPrefab, anchorName + anchorIndex, (result) =>
        {
            prescanTableSmall = result;
            flashlight1Pose = new Pose(result.transform.Find("Flashlight1").position, result.transform.Find("Flashlight1").rotation);
            flashlight2Pose = new Pose(result.transform.Find("Flashlight2").position, result.transform.Find("Flashlight2").rotation);
            flashlight3Pose = new Pose(result.transform.Find("Flashlight3").position, result.transform.Find("Flashlight3").rotation);
            candleHolderPose = new Pose(result.transform.Find("CandleHolder").position, result.transform.Find("CandleHolder").rotation);
            chalicePose = new Pose(result.transform.Find("Chalice").position, result.transform.Find("Chalice").rotation);
            daggerPose = new Pose(result.transform.Find("Dagger").position, result.transform.Find("Dagger").rotation);
            skullPose = new Pose(result.transform.Find("Skull").position, result.transform.Find("Skull").rotation);
        }));
        anchorIndex++;

        GameObject prescanFloor = null;
        Pose prescanFloorPose = new Pose();
        Pose pressurePlate1Pose = new Pose();
        Pose pressurePlate2Pose = new Pose();
        Pose pressurePlate3Pose = new Pose();
        yield return StartCoroutine(PositionPrefab(PrescanFloorPrefab, anchorName + anchorIndex, (result) =>
        {
            prescanFloor = result;
            prescanFloorPose = new Pose(result.transform.position, result.transform.rotation);
            pressurePlate1Pose = new Pose(result.transform.Find("PressurePlate1").position, result.transform.Find("PressurePlate1").rotation);
            pressurePlate2Pose = new Pose(result.transform.Find("PressurePlate2").position, result.transform.Find("PressurePlate2").rotation);
            pressurePlate3Pose = new Pose(result.transform.Find("PressurePlate3").position, result.transform.Find("PressurePlate3").rotation);
        }));
        anchorIndex++;

        GameObject prescanWall = null;
        Pose prescanWallPose = new Pose();
        yield return StartCoroutine(PositionPrefab(PrescanWallPrefab, anchorName + anchorIndex, (result) =>
        {
            prescanWall = result;
            prescanWallPose = new Pose(result.transform.position, result.transform.rotation);
        }));
        anchorIndex++;

        FingersScript.Instance.RemoveGesture(_tapGestureRecognizer);
        _tapGestureRecognizer.StateUpdated -= ObjectsPositioningTapCallback;

        //Edit object position and confirm
        ObjectsPositionConfirmed = false;
        _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.ConfirmObjectsPosition);
        _swipeGestureRecognizer.StateUpdated += ObjectsEditingSwipeCallback;
        FingersScript.Instance.AddGesture(_swipeGestureRecognizer);
        _rotateGestureRecognizer.StateUpdated += ObjectsEditingRotateCallback;
        FingersScript.Instance.AddGesture(_rotateGestureRecognizer);
        yield return new WaitUntil(() => ObjectsPositionConfirmed);
        FingersScript.Instance.RemoveGesture(_swipeGestureRecognizer);
        _swipeGestureRecognizer.StateUpdated -= ObjectsEditingSwipeCallback;
        FingersScript.Instance.RemoveGesture(_rotateGestureRecognizer);
        _rotateGestureRecognizer.StateUpdated -= ObjectsEditingRotateCallback;

        _gameUIManager.SetActiveInGamePanel(GameUIManager.InGamePanel.Default);

        //Save Objects Position into room info

        prescanTableBigPose = new Pose(prescanTableBig.transform.position, prescanTableBig.transform.rotation);
        flashlight1Pose = new Pose(prescanTableSmall.transform.Find("Flashlight1").position, prescanTableSmall.transform.Find("Flashlight1").rotation);
        flashlight2Pose = new Pose(prescanTableSmall.transform.Find("Flashlight2").position, prescanTableSmall.transform.Find("Flashlight2").rotation);
        flashlight3Pose = new Pose(prescanTableSmall.transform.Find("Flashlight3").position, prescanTableSmall.transform.Find("Flashlight3").rotation);
        candleHolderPose = new Pose(prescanTableSmall.transform.Find("CandleHolder").position, prescanTableSmall.transform.Find("CandleHolder").rotation);
        chalicePose = new Pose(prescanTableSmall.transform.Find("Chalice").position, prescanTableSmall.transform.Find("Chalice").rotation);
        daggerPose = new Pose(prescanTableSmall.transform.Find("Dagger").position, prescanTableSmall.transform.Find("Dagger").rotation);
        skullPose = new Pose(prescanTableSmall.transform.Find("Skull").position, prescanTableSmall.transform.Find("Skull").rotation);
        prescanFloorPose = new Pose(prescanFloor.transform.position, prescanFloor.transform.rotation);
        pressurePlate1Pose = new Pose(prescanFloor.transform.Find("PressurePlate1").position, prescanFloor.transform.Find("PressurePlate1").rotation);
        pressurePlate2Pose = new Pose(prescanFloor.transform.Find("PressurePlate2").position, prescanFloor.transform.Find("PressurePlate2").rotation);
        pressurePlate3Pose = new Pose(prescanFloor.transform.Find("PressurePlate3").position, prescanFloor.transform.Find("PressurePlate3").rotation);
        prescanWallPose = new Pose(prescanWall.transform.position, prescanWall.transform.rotation);

        _networkManager.CurrentRoom.ContraptionPose = prescanTableBigPose;
        _networkManager.CurrentRoom.PedestalPose = prescanFloorPose;
        _networkManager.CurrentRoom.PressurePlate1Pose = pressurePlate1Pose;
        _networkManager.CurrentRoom.PressurePlate2Pose = pressurePlate2Pose;
        _networkManager.CurrentRoom.PressurePlate3Pose = pressurePlate3Pose;
        _networkManager.CurrentRoom.PaintingPose = prescanWallPose;
        _networkManager.CurrentRoom.RiddlePose = prescanFloorPose;
        _networkManager.CurrentRoom.Flashlight1Pose = flashlight1Pose;
        _networkManager.CurrentRoom.Flashlight2Pose = flashlight2Pose;
        _networkManager.CurrentRoom.Flashlight3Pose = flashlight3Pose;
        _networkManager.CurrentRoom.CandleHolderPose = candleHolderPose;
        _networkManager.CurrentRoom.ChalicePose = chalicePose;
        _networkManager.CurrentRoom.DaggerPose = daggerPose;
        _networkManager.CurrentRoom.SkullPose = skullPose;

        //Destroy anchors and gameobjects attached
        for (int i = 0; i < anchorIndex; i++)
        {
            _arLibrary.DestroyAnchorAndGameObjects(anchorName + i);
        }

        //Initialize chosen scene
        if (SubSceneTypeInitialSelection == SubSceneManager.SubSceneType.Task)
            StartSceneTask(SubSceneTypeInitialSelection, TaskInteractionInitialSelection, TaskLocationInitialSelection,
                TaskVisualizationSequenceInitialSelection, TaskInSequenceInitialSelection);
        else if (SubSceneTypeInitialSelection != SubSceneManager.SubSceneType.Uninitialized)
            StartSceneGame(SubSceneTypeInitialSelection, SubSceneVisualizationInitialSelection);
    }

    private IEnumerator CheckMarkerOnSurface(string imageAnchorId)
    {
        Pose imageAnchorPose;
        Ray ray;
        Ray inverseRay;
        RaycastHit hitInfo;
        bool done = false;
        while (!done)
        {
            _arLibrary.GetAnchorPose(imageAnchorId, out imageAnchorPose);
            ray = new Ray(imageAnchorPose.position, imageAnchorPose.up);
            inverseRay = new Ray(imageAnchorPose.position, -imageAnchorPose.up);

            if (_arLibrary.Raycast(inverseRay, out hitInfo, 0.25f) || _arLibrary.Raycast(ray, out hitInfo, 0.25f))
            {
                done = true;
            }
            yield return null;
        }
    }

    /// <summary>
    /// Create a session anchor in the position of the marker
    /// (It creates the anchor only when the imageanchor is close to the detected surface the marker is on
    /// and its up vector is in the same direction of the vector normal to the plane)
    /// </summary>
    private IEnumerator CreateMarkerAnchor(string imageAnchorId)
    {
        Pose imageAnchorPose;
        RaycastHit hitInfo;
        float raycastLength = 0.02f;
        float targetTime = 1.0f;
        Camera cam = Camera.main;

        bool done = false;
        bool first = true;
        float timer = 0.0f;
        float currentTime;
        float deltaTime;
        float lastTime = 0;
        while (!done)
        {
            currentTime = Time.time;
            if (first) { lastTime = currentTime; first = false; }
            deltaTime = currentTime - lastTime;
            lastTime = currentTime;

            _arLibrary.GetAnchorPose(imageAnchorId, out imageAnchorPose);

            Vector3 imageViewportPosition = cam.WorldToViewportPoint(imageAnchorPose.position);
            //check if the imageanchor is on screen (in viewport space x and y between 0 and 1 and z > 0)
            //AND check if imageanchor position is correct (very close to a detected surface)
            //AND check if imageanchor rotation is correct (up vector parallel to the normal to the surface)
            if (imageViewportPosition.x >= 0 && imageViewportPosition.x <= 1 &&
                imageViewportPosition.y >= 0 && imageViewportPosition.y <= 1 &&
                imageViewportPosition.z >= 0 &&
                _arLibrary.RaycastFromPose(imageAnchorPose, out hitInfo, raycastLength) &&
                Math.Abs(Vector3.Dot(imageAnchorPose.up.normalized, hitInfo.normal.normalized) - 1.0f) < 0.01f)
            {
                //check if imageanchor is stable (its pose is correct since targetTime seconds)
                timer += deltaTime;
                _gameUIManager.ProgressBar.value = timer / targetTime;
                if (timer > targetTime)
                {
                    //create anchor

                    //anchor must be on the surface hit (hitInfo.point)
                    //with the same rotation of the imageAnchor (imageAnchorPose.rotation)

                    //Session anchor version
                    //_arLibrary.CreateSessionAnchor(MarkerAnchorId, new Pose(hitInfo.point, imageAnchorPose.rotation));

                    //Trackable anchor version
                    Pose hitPose;
                    _arLibrary.CreateAnchorFromPose(MarkerAnchorId, imageAnchorPose, out hitPose, raycastLength);

                    done = true;
                }
            }
            else
            {
                timer = 0.0f;
                _gameUIManager.ProgressBar.value = 0.0f;
            }

            yield return null;
        }
    }

    private IEnumerator PositionPrefab(GameObject prefab, string anchorString, Action<GameObject> gameObjectResult)
    {
        RaycastHit hitInfo;
        Ray ray;
        do
        {
            yield return null;
            ray = Camera.main.ScreenPointToRay(new Vector3((Screen.width / 2.0f), (Screen.height / 2.0f), 0));
        }
        while (!_arLibrary.Raycast(ray, out hitInfo, _objectManipulation.MaxInteractionDistance));

        GameObject obj = Instantiate(prefab, hitInfo.point, Quaternion.LookRotation(Vector3.forward, hitInfo.normal));

        _objectsPositioningHasTapped = false;
        while (!_objectsPositioningHasTapped)
        {
            ray = Camera.main.ScreenPointToRay(new Vector3((Screen.width / 2.0f), (Screen.height / 2.0f), 0));

            if (_arLibrary.Raycast(ray, out hitInfo, _objectManipulation.MaxInteractionDistance))
            {
                obj.transform.position = hitInfo.point;

                //obj.transform.up = hitInfo.normal;
                if (Mathf.Abs(Vector3.Dot(hitInfo.normal, Vector3.up)) > 0.5f)
                {
                    //Horizontal surface
                    //obj.transform.forward =
                    //    Vector3.ProjectOnPlane(obj.transform.position - Camera.main.transform.position, hitInfo.normal);
                    obj.transform.rotation = Quaternion.LookRotation(
                        Vector3.ProjectOnPlane(obj.transform.position - Camera.main.transform.position, hitInfo.normal),
                        hitInfo.normal);
                }
                else
                {
                    //Vertical surface
                    //obj.transform.forward = Vector3.ProjectOnPlane(Vector3.up, hitInfo.normal);
                    obj.transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(Vector3.up, hitInfo.normal),
                        hitInfo.normal);
                }
            }
            yield return null;
        }

        _arLibrary.CreateSessionAnchor(anchorString, new Pose(obj.transform.position, obj.transform.rotation));
        _arLibrary.AttachGameObjectToAnchor(anchorString, obj, true);

        gameObjectResult(obj);
    }


    #endregion


    #region public methods

    public void StartSceneTask(SubSceneManager.SubSceneType sceneType, SubSceneManager.Interaction interaction,
        SubSceneManager.Location location, SubSceneManager.VisualizationSequence visualizationSequence, int taskInSequence)
    {
        if (sceneType == SubSceneManager.SubSceneType.Task)
        {
            if (interaction == SubSceneManager.Interaction.Uninitialized ||
                location == SubSceneManager.Location.Uninitialized) return;

            SubSceneManager.TaskInfo task =
                SubSceneManager.Instance.GetTaskInSequence(interaction, location, visualizationSequence,
                    taskInSequence);
            _networkManager.CurrentRoom.RoomScene = new SubSceneManager.RoomScene(sceneType, task);
        }
    }

    public void StartSceneGame(SubSceneManager.SubSceneType sceneType, SubSceneManager.Visualization visualization)
    {
        if (sceneType != SubSceneManager.SubSceneType.Uninitialized && sceneType != SubSceneManager.SubSceneType.Task &&
            visualization != SubSceneManager.Visualization.Uninitialized)
        {
            SubSceneManager.RoomScene scene = new SubSceneManager.RoomScene(sceneType);
            scene.TaskInfo.Visualization = visualization;
            scene.TaskInfo.Location = SubSceneManager.Location.Local;
            _networkManager.CurrentRoom.RoomScene = scene;
        }
    }

    public void StartNextScene()
    {
        SubSceneManager.RoomScene nextScene = SubSceneManager.Instance.GetNextSceneInSequence(_networkManager.CurrentRoom.RoomScene);

        if (nextScene.Type != SubSceneManager.SubSceneType.Uninitialized)
        {
            _networkManager.CurrentRoom.RoomScene = nextScene;
        }
    }

    #endregion  


    #region private methods

    private void ArLibraryOnImageAnchorCreatedEvent(string anchorId, string imageName)
    {
        if (imageName == MarkerName)
        {
            _imageAnchorId = anchorId;
        }
    }

    #endregion


    #region Input Callbacks

    private void ObjectsPositioningTapCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Ended)
        {
            _objectsPositioningHasTapped = true;
        }
    }

    private void ObjectsEditingSwipeCallback(GestureRecognizer gesture)
    {

        if (gesture.State == GestureRecognizerState.Possible)
        {
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
            Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
            RaycastHit hitInfo;
            RaycastHit surfaceHitInfo;
            if (Physics.Raycast(screenRay, out hitInfo, _objectManipulation.MaxInteractionDistance,
                GameUtils.Mask.Default))
            {
                if (_arLibrary.Raycast(screenRay, out surfaceHitInfo, _objectManipulation.MaxInteractionDistance) &&
                    surfaceHitInfo.distance < hitInfo.distance)
                {
                    //There is a detected surface in front the hit.
                    return;
                }

                _objectEdited = hitInfo.transform.gameObject;
                _firstRaycastHit = true;
            }
        }
        else if (gesture.State == GestureRecognizerState.Executing)
        {
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
            Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
            RaycastHit surfaceHitInfo;
            if (_objectEdited!= null && _arLibrary.Raycast(screenRay, out surfaceHitInfo, _objectManipulation.MaxInteractionDistance))
            {
                if (_firstRaycastHit)
                {
                    _oldRaycastHitPos = surfaceHitInfo.point;
                    _firstRaycastHit = false;
                }
                Vector3 deltaPos = surfaceHitInfo.point - _oldRaycastHitPos;
                _oldRaycastHitPos = surfaceHitInfo.point;
                _objectEdited.transform.position += deltaPos;

                if (Mathf.Abs(Vector3.Dot(surfaceHitInfo.normal, _objectEdited.transform.up)) > 0.5f)
                {

                }
                else
                {
                    //_objectEdited.transform.up = surfaceHitInfo.normal;

                    if (Mathf.Abs(Vector3.Dot(surfaceHitInfo.normal, Vector3.up)) > 0.5f)
                    {
                        //new surface is Horizontal
                        //_objectEdited.transform.forward = Vector3.ProjectOnPlane(
                        //    _objectEdited.transform.position - Camera.main.transform.position, surfaceHitInfo.normal);

                        _objectEdited.transform.rotation = Quaternion.LookRotation(
                            Vector3.ProjectOnPlane(_objectEdited.transform.position - Camera.main.transform.position, surfaceHitInfo.normal),
                            surfaceHitInfo.normal);

                    }
                    else
                    {
                        //new surface is Vertical
                        //_objectEdited.transform.forward = Vector3.ProjectOnPlane(Vector3.up, surfaceHitInfo.normal);

                        _objectEdited.transform.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(Vector3.up, surfaceHitInfo.normal),
                            surfaceHitInfo.normal);
                    }
                }
            }

        }
        else if (gesture.State == GestureRecognizerState.Ended)
        {
            _objectEdited = null;
        }
    }

    private void ObjectsEditingRotateCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Began)
        {
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
            Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(screenRay, out hitInfo, _objectManipulation.MaxInteractionDistance,
                GameUtils.Mask.Default))
            {
                _objectEdited = hitInfo.transform.gameObject;
            }
        }
        else if (gesture.State == GestureRecognizerState.Executing)
        {
            _objectEdited.transform.Rotate(Vector3.up, -_rotateGestureRecognizer.RotationDegreesDelta, Space.Self);
            /*_objectEdited.transform.rotation *=
                Quaternion.AngleAxis(-_rotateGestureRecognizer.RotationDegreesDelta, _objectEdited.transform.up);*/
        }

    }

    #endregion
}
