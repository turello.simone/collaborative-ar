﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ITaskManipulation : ISubScene
{
    protected SubSceneManager.TaskInfo Task;

    public ITaskManipulation(string sceneName,SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, initialized)
    {
        GameUIManager.Instance.SetSceneNameVisualizationAndLocation(sceneName, task.Visualization, task.Location);

        Task = task;

        ObjectManipulation.Instance.InteractionEnabled = true;

        _networkManager.AvatarType = GameUtils.AvatarType.Simple;

        _networkManager.LocalPlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        _networkManager.LocalGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
        ObjectManipulation.Instance.LocalVisualization = GameUtils.ObjectManipulationVisualization.Hand;

        switch (task.Visualization)
        {
            case SubSceneManager.Visualization.Minimal:
                switch (task.Location)
                {
                    case SubSceneManager.Location.Local:
                        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Frustum;
                        break;
                    case SubSceneManager.Location.Remote:
                        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Frustum;
                        break;
                }
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
                ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.None;
                _networkManager.PlayerFocalPointEnabled = false;
                break;
            case SubSceneManager.Visualization.Base:
                switch (task.Location)
                {
                    case SubSceneManager.Location.Local:
                        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                           GameUtils.PlayerVisualization.Avatar;
                        break;
                    case SubSceneManager.Location.Remote:
                        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                           GameUtils.PlayerVisualization.Avatar;
                        break;
                }
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.Ray |
                                                                  GameUtils.ObjectManipulationVisualization.Outline |
                                                                  GameUtils.ObjectManipulationVisualization.Hand;
                _networkManager.PlayerFocalPointEnabled = false;
                break;
            case SubSceneManager.Visualization.Complete:
                switch (task.Location)
                {
                    case SubSceneManager.Location.Local:
                        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                           GameUtils.PlayerVisualization.Avatar;
                        break;
                    case SubSceneManager.Location.Remote:
                        _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                           GameUtils.PlayerVisualization.Avatar;
                        break;
                }
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.Ray |
                                                                  GameUtils.ObjectManipulationVisualization.Outline |
                                                                  GameUtils.ObjectManipulationVisualization.Hand;
                _networkManager.PlayerFocalPointEnabled = true;
                break;
        }

    }

    public override void Destroy()
    {
        base.Destroy();
        _networkManager.PlayerFocalPointEnabled = true;
    }

}
