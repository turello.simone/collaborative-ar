﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TaskPosDiffSysPlates : StaticObject
{
    #region Attributes

    [Header("TaskPosDiffSysPlates properties")]
    [MustBeAssigned] public GameObject[] Cylinders;
    [MustBeAssigned] public Renderer[] BasesRenderers;
    [ReadOnly] public bool[] PositionConvergenceCompleted;
    public float CompletionRequiredTime = 1f;

    public bool IsInitialized { get; private set; } = false;
    public int Repetitions = 3;
    public int CurrentRepetition
    {
        get => _currentRepetition.Value;
        private set => _currentRepetition.Value = value;
    }
    private const string REPID = "CurrentRepetition";
    private SyncVarInt _currentRepetition;

    private Color _cylinderHighlightCompleteColor = Color.green.BrightnessOffset(1f);
    [SerializeField, ReadOnly] private int[,] _selectedCylinders;
    private CustomOutline[] _basesOutlines;
    private Color _startBaseColor;
    private Color _startCylinderColor;
    private float _timer = 0;
    private Sequence _sequence;

    private SyncVarBool _player1OnBase;
    private const string PLAYER1BOOL = "PLAYER1BOOL";
    private SyncVarBool _player2OnBase;
    private const string PLAYER2BOOL = "PLAYER2BOOL";
    private SyncVarBool _player3OnBase;
    private const string PLAYER3BOOL = "PLAYER3BOOL";

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _timer = 0;
        _startBaseColor = BasesRenderers[0].material.color;
        _startCylinderColor = Cylinders[0].GetComponent<Renderer>().material.color;

        _basesOutlines = new CustomOutline[BasesRenderers.Length];

        for (int i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;
            if ((_basesOutlines[i] = BasesRenderers[i].GetComponent<CustomOutline>()) == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;
        }

        _player1OnBase = new SyncVarBool(false, _photonView, PLAYER1BOOL);
        _player2OnBase = new SyncVarBool(false, _photonView, PLAYER2BOOL);
        _player3OnBase = new SyncVarBool(false, _photonView, PLAYER3BOOL);
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskPosDiffSysPlates = this;

        _currentRepetition = new SyncVarInt(0, _photonView, REPID);
        _currentRepetition.ValueChanged += _currentRepetition_ValueChanged;

        if (_networkManager.IsMasterClient)
            SelectCylinders();
    }

    private void _currentRepetition_ValueChanged(int value)
    {
        if (value > 0)
        {
            _timer = 0;
            PositionConvergenceCompleted[value - 1] = true;
            AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

            if (_networkManager.IsMasterClient)
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer,
                    OnlineLogger.EventType.PosCompleted);

            EnableCylinderHighlight(value);
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _basesOutlines.Length; i++)
        {
            if (_basesOutlines[i] == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;
        }

        for (var i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;
            Cylinders[i].GetComponent<Renderer>().material.DOKill();
            Cylinders[i].GetComponent<Renderer>().material.color = _startCylinderColor;
            BasesRenderers[i].material.DOKill();
            BasesRenderers[i].material.color = _startBaseColor;
        }

        _selectedCylinders = null;
        PositionConvergenceCompleted = null;
        _timer = 0;
        IsInitialized = false;
        _sequence.Kill();

        if (_networkManager.IsMasterClient)
        {
            CurrentRepetition = 0;
            _player1OnBase.Value = false;
            _player2OnBase.Value = false;
            _player3OnBase.Value = false;
            SelectCylinders();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (_networkManager.NumberOfPlayersReady != _networkManager.NumberOfPlayers ||
            currentScene.Type != SubSceneManager.SubSceneType.Task ||
            currentScene.TaskInfo.Interaction != SubSceneManager.Interaction.Position ||
            currentScene.TaskInfo.InteractionFocus != SubSceneManager.InteractionFocus.DifferentObjects ||
            currentScene.TaskInfo.Input != SubSceneManager.Input.SystemCommanded)
            return;

        if (CurrentRepetition >= Repetitions) return;

        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;
        SetSyncVarBool(playerIndex,
            !GameUIManager.Instance.InstructionsPanelEnabled && IsInitialized &&
            !PositionConvergenceCompleted[CurrentRepetition] && GameUtils.IsPointWithinCollider(
                Cylinders[_selectedCylinders[CurrentRepetition, playerIndex - 1]].GetComponent<Collider>(),
                Camera.main.transform.position));

        if (_networkManager.IsMasterClient && IsInitialized && !PositionConvergenceCompleted[CurrentRepetition])
        {
            if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
            {
                if (_networkManager.GetPlayerInfoFromPlayerIndex(1) != null && _player1OnBase.Value &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(2) != null && _player2OnBase.Value &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(3) != null && _player3OnBase.Value)
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompletePositionConvergence();
                }
                else _timer = 0;
            }
            else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
            {
                if ((_networkManager.GetPlayerInfoFromPlayerIndex(1) == null || _player1OnBase.Value) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(2) == null || _player2OnBase.Value) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(3) == null || _player3OnBase.Value))
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompletePositionConvergence();
                }
                else _timer = 0;
            }
        }
    }

    #endregion

    #region Public methods

    public void EnableCylinderHighlight(int repetition)
    {
        if (_sequence != null)
        {
            _sequence.Kill();
            _sequence = null;
        }

        if (repetition == 0)
        {
            //enable current rep

            for (var i = 0; i < 3; i++)
            {
                if (_basesOutlines[_selectedCylinders[repetition, i]] != null)
                {
                    _basesOutlines[_selectedCylinders[repetition, i]].OutlineColor = _networkManager.GetPlayerColor(i + 1);
                    _basesOutlines[_selectedCylinders[repetition, i]].enabled = true;
                }

                Cylinders[_selectedCylinders[repetition, i]].GetComponent<Renderer>().enabled = true;
                Cylinders[_selectedCylinders[repetition, i]].GetComponent<Renderer>().material.color = _networkManager
                    .GetPlayerColor(i + 1).BrightnessOffset(0.4f).WithAlphaSetTo(_startCylinderColor.a);
                BasesRenderers[_selectedCylinders[repetition, i]].material.color =
                    _networkManager.GetPlayerColor(i + 1).BrightnessOffset(0.2f);
            }
        }
        else if (repetition == Repetitions)
        {
            //complete old rep

            for (var i = 0; i < 3; i++)
            {
                if (_basesOutlines[_selectedCylinders[repetition - 1, i]] != null)
                    _basesOutlines[_selectedCylinders[repetition - 1, i]].FadeOutAndDisable();

                Cylinders[_selectedCylinders[repetition - 1, i]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f);
                BasesRenderers[_selectedCylinders[repetition - 1, i]].material
                    .DOColor(_cylinderHighlightCompleteColor, 0.5f);
            }
        }
        else
        {
            for (var i = 0; i < 3; i++)
            {
                if (_basesOutlines[_selectedCylinders[repetition - 1, i]] != null)
                    _basesOutlines[_selectedCylinders[repetition - 1, i]].FadeOutAndDisable();
            }

            _sequence = DOTween.Sequence();
            _sequence
                //complete old rep
                .Append(Cylinders[_selectedCylinders[repetition - 1, 0]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1, 0]].material
                    .DOColor(_cylinderHighlightCompleteColor, 0.5f))
                .Join(Cylinders[_selectedCylinders[repetition - 1, 1]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1, 1]].material
                    .DOColor(_cylinderHighlightCompleteColor, 0.5f))
                .Join(Cylinders[_selectedCylinders[repetition - 1, 2]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1, 2]].material
                    .DOColor(_cylinderHighlightCompleteColor, 0.5f))

                .AppendInterval(0.5f)

                .Append(Cylinders[_selectedCylinders[repetition - 1, 0]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(0), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1, 0]].material
                    .DOColor(_startBaseColor, 0.5f))
                .Join(Cylinders[_selectedCylinders[repetition - 1, 1]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(0), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1, 1]].material
                    .DOColor(_startBaseColor, 0.5f))
                .Join(Cylinders[_selectedCylinders[repetition - 1, 2]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(0), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1, 2]].material
                    .DOColor(_startBaseColor, 0.5f))

                //enable current rep
                .OnKill(() =>
                {
                    for (var i = 0; i < 3; i++)
                    {
                        Cylinders[_selectedCylinders[repetition - 1, i]].GetComponent<Renderer>().enabled = false;
                        Cylinders[_selectedCylinders[repetition - 1, i]].GetComponent<Renderer>().material.color = _startCylinderColor;
                        BasesRenderers[_selectedCylinders[repetition - 1, i]].material.color = _startBaseColor;

                        if (_basesOutlines[_selectedCylinders[repetition, i]] != null)
                        {
                            _basesOutlines[_selectedCylinders[repetition, i]].OutlineColor = _networkManager.GetPlayerColor(i + 1);
                            _basesOutlines[_selectedCylinders[repetition, i]].enabled = true;
                        }

                        Cylinders[_selectedCylinders[repetition, i]].GetComponent<Renderer>().enabled = true;
                        Cylinders[_selectedCylinders[repetition, i]].GetComponent<Renderer>().material.color = _networkManager
                            .GetPlayerColor(i + 1).BrightnessOffset(0.4f).WithAlphaSetTo(_startCylinderColor.a);
                        BasesRenderers[_selectedCylinders[repetition, i]].material.color =
                            _networkManager.GetPlayerColor(i + 1).BrightnessOffset(0.2f);
                    }
                });
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCylinders()
    {
        int[,] selectedCylinders = new int[Repetitions, 3];

        for (int i = 0; i < Repetitions; i++)
        {
            int[] numbers = GameUtils.GenerateUniqueRandomNumbers(0, Cylinders.Length, 3);
            selectedCylinders[i, 0] = numbers[0];
            selectedCylinders[i, 1] = numbers[1];
            selectedCylinders[i, 2] = numbers[2];
        }

        _photonView.RPC("SelectCylindersRPC", RpcTarget.AllBuffered, MatrixSerializer.Serialize(selectedCylinders));
    }


    [PunRPC]
    public void SelectCylindersRPC(byte[] selectedCylinder)
    {
        _selectedCylinders = MatrixSerializer.Deserialize<int>(selectedCylinder);
        Repetitions = _selectedCylinders.GetLength(0);

        PositionConvergenceCompleted = new bool[Repetitions];
        for (var i = 0; i < PositionConvergenceCompleted.Length; i++)
        {
            PositionConvergenceCompleted[i] = false;
        }

        IsInitialized = true;

        EnableCylinderHighlight(0);
    }

    private void CompletePositionConvergence()
    {
        PositionConvergenceCompleted[CurrentRepetition] = true;
        CurrentRepetition++;
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        if (gameObj == Cylinders[_selectedCylinders[CurrentRepetition, 0]] ||
            gameObj == Cylinders[_selectedCylinders[CurrentRepetition, 1]] ||
            gameObj == Cylinders[_selectedCylinders[CurrentRepetition, 2]])
            return true;
        return false;
    }

    private void SetSyncVarBool(int playerIndex, bool value)
    {
        switch (playerIndex)
        {
            case 1:
                if (_player1OnBase.Value != value) _player1OnBase.Value = value;
                break;
            case 2:
                if (_player2OnBase.Value != value) _player2OnBase.Value = value;
                break;
            case 3:
                if (_player3OnBase.Value != value) _player3OnBase.Value = value;
                break;
        }
    }
}
