﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

public class TaskGazeDiffSysCubes : StaticObject
{
    #region Attributes

    [Header("TaskGazeDiffSysCubes properties")]
    [MustBeAssigned] public GameObject[] Cubes;

    public bool IsInitialized { get; private set; } = false;
    public int Repetitions = 3;
    public int CurrentRepetition
    {
        get => _currentRepetition.Value;
        private set => _currentRepetition.Value = value;
    }
    private const string REPID = "CurrentRepetition";
    private SyncVarInt _currentRepetition;

    private Color _cubeHighlightCompleteColor = Color.green.BrightnessOffset(1f);
    [SerializeField, ReadOnly] private int[,] _selectedCubes;
    private CustomOutline[] _outlines;
    private GameObject[] _gazeConvergenceFocalPoints = new GameObject[3];
    private Color _startColor;
    private Sequence _sequence;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _startColor = Cubes[0].GetComponent<Renderer>().material.color;

        _outlines = new CustomOutline[Cubes.Length];
        for (int i = 0; i < Cubes.Length; i++)
        {
            if ((_outlines[i] = Cubes[i].GetComponent<CustomOutline>()) == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskGazeDiffSysCubes = this;

        _currentRepetition = new SyncVarInt(0, _photonView, REPID);
        _currentRepetition.ValueChanged += _currentRepetition_ValueChanged;

        _networkManager.CurrentRoom.GazeConvergenceFocalPointCompletedEvent += CurrentRoom_GazeConvergenceFocalPointCompletedEvent;

        if (_networkManager.IsMasterClient)
            SelectCubes();
    }

    private void CurrentRoom_GazeConvergenceFocalPointCompletedEvent(GazeConvergenceFocalPoint.Type type)
    {
        if (_networkManager.IsMasterClient && type == GazeConvergenceFocalPoint.Type.TaskGazeDiffSys)
        {
            CurrentRepetition++;
        }
    }

    private void _currentRepetition_ValueChanged(int value)
    {
        if (value > 0)
        {
            EnableCubeHighlight(value);
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _outlines.Length; i++)
        {
            if (_outlines[i] == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;
        }

        for (var i = 0; i < Cubes.Length; i++)
        {
            Cubes[i].GetComponent<Renderer>().material.DOKill();
            Cubes[i].GetComponent<Renderer>().material.color = _startColor;
        }

        for (var i = 0; i < _gazeConvergenceFocalPoints.Length; i++)
        {
            Destroy(_gazeConvergenceFocalPoints[i]);
            _gazeConvergenceFocalPoints[i] = null;
        }

        _selectedCubes = null;

        IsInitialized = false;

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffSys);

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffSys);
            CurrentRepetition = 0;
            SelectCubes();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }


    #endregion

    #region Public methods

    public void EnableCubeHighlight(int repetition)
    {
        if (_sequence != null)
        {
            _sequence.Kill();
            _sequence = null;
        }

        if (repetition == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                if (_outlines[_selectedCubes[repetition, i]] != null)
                {
                    _outlines[_selectedCubes[repetition, i]].OutlineColor = _networkManager.GetPlayerColor(i + 1);
                    _outlines[_selectedCubes[repetition, i]].enabled = true;
                }

                Cubes[_selectedCubes[repetition, i]].GetComponent<Renderer>().material.color =
                    _networkManager.GetPlayerColor(i + 1).Lighter().Lighter();
            }

            _gazeConvergenceFocalPoints[0] =
                FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                    Cubes[_selectedCubes[repetition, 0]].transform.position, 0.1f, this.transform);
            _gazeConvergenceFocalPoints[1] =
                FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                    Cubes[_selectedCubes[repetition, 1]].transform.position, 0.1f, this.transform);
            _gazeConvergenceFocalPoints[2] =
                FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                    Cubes[_selectedCubes[repetition, 2]].transform.position, 0.1f, this.transform);

            FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                GazeConvergenceFocalPoint.Type.TaskGazeDiffSys, _gazeConvergenceFocalPoints[0], _gazeConvergenceFocalPoints[1],
                _gazeConvergenceFocalPoints[2], outlineEnabled: false, iconEnabled: false, arrowEnabled: false);
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffSys);
        }
        else if (CurrentRepetition == Repetitions)
        {
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                .TaskGazeDiffSys);

            for (int i = 0; i < 3; i++)
            {
                if (_outlines[_selectedCubes[repetition - 1, i]] != null) _outlines[_selectedCubes[repetition - 1, i]].FadeOutAndDisable();
                if (_gazeConvergenceFocalPoints[i] != null)
                {
                    Destroy(_gazeConvergenceFocalPoints[i]);
                    _gazeConvergenceFocalPoints[i] = null;
                }
                Cubes[_selectedCubes[repetition - 1, i]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
            }
        }
        else
        {
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                .TaskGazeDiffSys);
            if (_networkManager.IsMasterClient)
                _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type
                    .TaskGazeDiffSys);

            for (int i = 0; i < 3; i++)
            {
                if (_outlines[_selectedCubes[repetition - 1, i]] != null) _outlines[_selectedCubes[repetition - 1, i]].FadeOutAndDisable();
                if (_gazeConvergenceFocalPoints[i] != null)
                {
                    Destroy(_gazeConvergenceFocalPoints[i]);
                    _gazeConvergenceFocalPoints[i] = null;
                }
                Cubes[_selectedCubes[repetition - 1, i]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
            }

            _sequence = DOTween.Sequence();
            _sequence
                .Append(Cubes[_selectedCubes[repetition - 1, 0]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f))
                .Join(Cubes[_selectedCubes[repetition - 1, 1]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f))
                .Join(Cubes[_selectedCubes[repetition - 1, 2]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f))
                .AppendInterval(0.5f)
                .Append(Cubes[_selectedCubes[repetition - 1, 0]].GetComponent<Renderer>().material.DOColor(_startColor, 0.5f))
                .Join(Cubes[_selectedCubes[repetition - 1, 1]].GetComponent<Renderer>().material.DOColor(_startColor, 0.5f))
                .Join(Cubes[_selectedCubes[repetition - 1, 2]].GetComponent<Renderer>().material.DOColor(_startColor, 0.5f))
                .OnKill(() =>
                {
                    for (int i = 0; i < 3; i++)
                    {
                        Cubes[_selectedCubes[repetition - 1, i]].GetComponent<Renderer>().material.color = _startColor;
                    }

                    for (int i = 0; i < 3; i++)
                    {
                        if (_outlines[_selectedCubes[repetition, i]] != null)
                        {
                            _outlines[_selectedCubes[repetition, i]].OutlineColor = _networkManager.GetPlayerColor(i + 1);
                            _outlines[_selectedCubes[repetition, i]].enabled = true;
                        }

                        Cubes[_selectedCubes[repetition, i]].GetComponent<Renderer>().material.color =
                            _networkManager.GetPlayerColor(i + 1).Lighter().Lighter();
                    }

                    _gazeConvergenceFocalPoints[0] =
                        FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                            Cubes[_selectedCubes[repetition, 0]].transform.position, 0.1f, this.transform);
                    _gazeConvergenceFocalPoints[1] =
                        FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                            Cubes[_selectedCubes[repetition, 1]].transform.position, 0.1f, this.transform);
                    _gazeConvergenceFocalPoints[2] =
                        FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                            Cubes[_selectedCubes[repetition, 2]].transform.position, 0.1f, this.transform);

                    FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                        GazeConvergenceFocalPoint.Type.TaskGazeDiffSys, _gazeConvergenceFocalPoints[0], _gazeConvergenceFocalPoints[1],
                        _gazeConvergenceFocalPoints[2], outlineEnabled: false, iconEnabled: false, arrowEnabled: false);
                    FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffSys);
                });
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCubes()
    {
        int[,] selectedCubes = new int[Repetitions, 3];

        for (int i = 0; i < Repetitions; i++)
        {
            int[] numbers = GameUtils.GenerateUniqueRandomNumbers(0, Cubes.Length, 3);
            selectedCubes[i, 0] = numbers[0];
            selectedCubes[i, 1] = numbers[1];
            selectedCubes[i, 2] = numbers[2];
        }

        _photonView.RPC("SelectCubesRPC", RpcTarget.AllBuffered, MatrixSerializer.Serialize(selectedCubes));
    }

    [PunRPC]
    public void SelectCubesRPC(byte[] selectedCubes)
    {
        _selectedCubes = MatrixSerializer.Deserialize<int>(selectedCubes);
        Repetitions = _selectedCubes.GetLength(0);

        IsInitialized = true;

        EnableCubeHighlight(0);
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        if (gameObj == Cubes[_selectedCubes[CurrentRepetition, 0]] ||
            gameObj == Cubes[_selectedCubes[CurrentRepetition, 1]] ||
            gameObj == Cubes[_selectedCubes[CurrentRepetition, 2]] )
            return true;
        return false;
    }
}
