﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

public class TaskGazeSameSysCubes : StaticObject
{
    #region Attributes

    [Header("TaskGazeSameSysCubes properties")]
    [MustBeAssigned] public GameObject[] Cubes;
    [MustBeAssigned] public Color CubeHighlightColor = Color.red;

    public bool IsInitialized { get; private set; } = false;

    public int Repetitions = 3;
    public int CurrentRepetition
    {
        get => _currentRepetition.Value;
        private set => _currentRepetition.Value = value;
    }
    private const string REPID = "CurrentRepetition";
    private SyncVarInt _currentRepetition;

    private Color _cubeHighlightCompleteColor = Color.green.BrightnessOffset(1f);
    [SerializeField, ReadOnly] private int[] _selectedCubes;
    private CustomOutline[] _outlines;
    private GameObject _gazeConvergenceFocalPoint;
    private Color _startColor;
    private Sequence _sequence;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _startColor = Cubes[0].GetComponent<Renderer>().material.color;

        _outlines = new CustomOutline[Cubes.Length];
        for (int i = 0; i < Cubes.Length; i++)
        {
            if ((_outlines[i] = Cubes[i].GetComponent<CustomOutline>()) == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskGazeSameSysCubes = this;

        _currentRepetition = new SyncVarInt(0, _photonView, REPID);
        _currentRepetition.ValueChanged += _currentRepetition_ValueChanged;

        _networkManager.CurrentRoom.GazeConvergenceFocalPointCompletedEvent += CurrentRoom_GazeConvergenceFocalPointCompletedEvent;

        if (_networkManager.IsMasterClient) 
            SelectCubes();
    }

    private void CurrentRoom_GazeConvergenceFocalPointCompletedEvent(GazeConvergenceFocalPoint.Type type)
    {
        if (_networkManager.IsMasterClient && type == GazeConvergenceFocalPoint.Type.TaskGazeSameSys)
        {
            CurrentRepetition++;
        }
    }

    private void _currentRepetition_ValueChanged(int value)
    {
        if (value > 0)
        {
            EnableCubeHighlight(value);
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _outlines.Length; i++)
        {
            if (_outlines[i] == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;
        }

        for (var i = 0; i < Cubes.Length; i++)
        {
            Cubes[i].GetComponent<Renderer>().material.DOKill();
            Cubes[i].GetComponent<Renderer>().material.color = _startColor;
        }

        if (_gazeConvergenceFocalPoint != null)
        {
            Destroy(_gazeConvergenceFocalPoint);
            _gazeConvergenceFocalPoint = null;
        }
        
        _sequence.Kill();

        IsInitialized = false;

        _selectedCubes = null;

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameSys);

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameSys);
            CurrentRepetition = 0;
            SelectCubes();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    #endregion

    #region Public methods

    public void EnableCubeHighlight(int repetition)
    {
        if (_sequence != null)
        {
            _sequence.Kill();
            _sequence = null;
        }

        if (repetition == 0)
        {
            if (_outlines[_selectedCubes[repetition]] != null)
            {
                _outlines[_selectedCubes[repetition]].OutlineColor = CubeHighlightColor;
                _outlines[_selectedCubes[repetition]].enabled = true;
            }

            Cubes[_selectedCubes[repetition]].GetComponent<Renderer>().material.color = CubeHighlightColor.Lighter().Lighter();

            _gazeConvergenceFocalPoint =
                FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                    Cubes[_selectedCubes[repetition]].transform.position, 0.1f, this.transform);
            FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                GazeConvergenceFocalPoint.Type.TaskGazeSameSys, _gazeConvergenceFocalPoint, outlineEnabled: false,
                iconEnabled: false, arrowEnabled: false);
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameSys);
        }
        else if (CurrentRepetition == Repetitions)
        {
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                .TaskGazeSameSys);
            /*if (_networkManager.IsMasterClient)
                _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type
                    .TaskGazeSameSys);*/
            if (_outlines[_selectedCubes[repetition - 1]] != null)
                _outlines[_selectedCubes[repetition - 1]].FadeOutAndDisable();
            if (_gazeConvergenceFocalPoint != null)
            {
                Destroy(_gazeConvergenceFocalPoint);
                _gazeConvergenceFocalPoint = null;
            }

            Cubes[_selectedCubes[repetition - 1]].GetComponent<Renderer>().material
                .DOColor(_cubeHighlightCompleteColor, 0.5f);
        }
        else
        {
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                .TaskGazeSameSys);
            if (_networkManager.IsMasterClient)
                _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type
                    .TaskGazeSameSys);
            if (_outlines[_selectedCubes[repetition - 1]] != null)
                _outlines[_selectedCubes[repetition - 1]].FadeOutAndDisable();
            if (_gazeConvergenceFocalPoint != null)
            {
                Destroy(_gazeConvergenceFocalPoint);
                _gazeConvergenceFocalPoint = null;
            }

            _sequence = DOTween.Sequence();
            _sequence
                .Append(Cubes[_selectedCubes[repetition - 1]].GetComponent<Renderer>().material
                    .DOColor(_cubeHighlightCompleteColor, 0.5f))
                .AppendInterval(0.5f)
                .Append(Cubes[_selectedCubes[repetition - 1]].GetComponent<Renderer>().material
                    .DOColor(_startColor, 0.5f))
                /*.AppendCallback(() =>
                {
                    if (_outlines[_selectedCubes[repetition]] != null)
                    {
                        _outlines[_selectedCubes[repetition]].OutlineColor = CubeHighlightColor;
                        _outlines[_selectedCubes[repetition]].enabled = true;
                    }

                    Cubes[_selectedCubes[repetition]].GetComponent<Renderer>().material.color =
                        CubeHighlightColor.Lighter().Lighter();

                    _gazeConvergenceFocalPoint =
                        FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                            Cubes[_selectedCubes[repetition]].transform.position, 0.1f, this.transform);
                    FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                        GazeConvergenceFocalPoint.Type.TaskGazeSameSys, _gazeConvergenceFocalPoint,
                        outlineEnabled: false,
                        iconEnabled: false, arrowEnabled: false);
                    FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                        .TaskGazeSameSys);
                })*/
                /*.OnComplete(() =>
                {
                    Cubes[_selectedCubes[repetition]].GetComponent<Renderer>().material.color =
                        CubeHighlightColor.Lighter().Lighter();

                    _gazeConvergenceFocalPoint =
                        FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                            Cubes[_selectedCubes[repetition]].transform.position, 0.1f, this.transform);
                    FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                        GazeConvergenceFocalPoint.Type.TaskGazeSameSys, _gazeConvergenceFocalPoint,
                        outlineEnabled: false,
                        iconEnabled: false, arrowEnabled: false);
                    FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                        .TaskGazeSameSys);
                })*/
                .OnKill(() =>
                {
                    Cubes[_selectedCubes[repetition - 1]].GetComponent<Renderer>().material.color = _startColor;

                    if (_outlines[_selectedCubes[repetition]] != null)
                    {
                        _outlines[_selectedCubes[repetition]].OutlineColor = CubeHighlightColor;
                        _outlines[_selectedCubes[repetition]].enabled = true;
                    }

                    Cubes[_selectedCubes[repetition]].GetComponent<Renderer>().material.color =
                        CubeHighlightColor.Lighter().Lighter();

                    _gazeConvergenceFocalPoint =
                        FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                            Cubes[_selectedCubes[repetition]].transform.position, 0.1f, this.transform);
                    FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                        GazeConvergenceFocalPoint.Type.TaskGazeSameSys, _gazeConvergenceFocalPoint,
                        outlineEnabled: false,
                        iconEnabled: false, arrowEnabled: false);
                    FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type
                        .TaskGazeSameSys);
                });
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCubes()
    {
        var selectedCubes = GameUtils.GenerateUniqueRandomNumbers(0, Cubes.Length, Repetitions);

        _photonView.RPC("SelectCubesRPC", RpcTarget.AllBuffered, selectedCubes);
    }


    [PunRPC]
    public void SelectCubesRPC(int[] selectedCube)
    {
        _selectedCubes = selectedCube;
        Repetitions = selectedCube.Length;

        IsInitialized = true;

        EnableCubeHighlight(0);
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        if (gameObj == Cubes[_selectedCubes[CurrentRepetition]])
            return true;
        return false;
    }
}
