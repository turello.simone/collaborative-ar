﻿using System.Collections;
using MyBox;
using UnityEngine;

public class TaskPosDiffSys : ITaskPosition
{
    public TaskPosDiffSys(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task, 
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskPosDiffSysPlates == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskPosDiffSysPlatesPrefab.name, _networkManager.CurrentRoom.PedestalPose);
            else
                _networkManager.CurrentRoom.TaskPosDiffSysPlates.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskPosDiffSysPlates != null &&
                                         _networkManager.CurrentRoom.TaskPosDiffSysPlates.IsInitialized);

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffSys/Instructions_Start");
        instructions = instructions.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());

        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskPosDiffSysPlates.CurrentRepetition ==
                                         _networkManager.CurrentRoom.TaskPosDiffSysPlates.Repetitions);

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskPosDiffSysPlates != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskPosDiffSysPlates.Enable(enabled);
    }
}
