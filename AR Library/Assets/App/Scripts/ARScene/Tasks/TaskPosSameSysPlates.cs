﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TaskPosSameSysPlates : StaticObject
{
    #region Attributes

    [Header("TaskPosSameSysPlates properties")]
    [MustBeAssigned] public GameObject[] Cylinders;
    [MustBeAssigned] public Renderer[] BasesRenderers;
    [MustBeAssigned] public Color CylinderHighlightColor = Color.red;
    [ReadOnly] public bool[] PositionConvergenceCompleted;
    public float CompletionRequiredTime = 1f;

    public bool IsInitialized { get; private set; } = false;
    public int Repetitions = 3;
    public int CurrentRepetition
    {
        get => _currentRepetition.Value;
        private set => _currentRepetition.Value = value;
    }
    private const string REPID = "CurrentRepetition";
    private SyncVarInt _currentRepetition;

    private Color _cylinderHighlightCompleteColor = Color.green.BrightnessOffset(1f);
    [SerializeField, ReadOnly] private int[] _selectedCylinders;
    private CustomOutline[] _basesOutlines;
    private Color _startBaseColor;
    private Color _startCylinderColor;
    private float _timer = 0;
    private Sequence _sequence;

    private SyncVarBool _player1OnBase;
    private const string PLAYER1BOOL = "PLAYER1BOOL";
    private SyncVarBool _player2OnBase;
    private const string PLAYER2BOOL = "PLAYER2BOOL";
    private SyncVarBool _player3OnBase;
    private const string PLAYER3BOOL = "PLAYER3BOOL";

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _timer = 0;
        _startBaseColor = BasesRenderers[0].material.color;
        _startCylinderColor = Cylinders[0].GetComponent<Renderer>().material.color;

        _basesOutlines = new CustomOutline[BasesRenderers.Length];

        for (int i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;
            if ((_basesOutlines[i] = BasesRenderers[i].GetComponent<CustomOutline>()) == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineColor = CylinderHighlightColor;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;
        }

        _player1OnBase = new SyncVarBool(false, _photonView, PLAYER1BOOL);
        _player2OnBase = new SyncVarBool(false, _photonView, PLAYER2BOOL);
        _player3OnBase = new SyncVarBool(false, _photonView, PLAYER3BOOL);
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskPosSameSysPlates = this;

        _currentRepetition = new SyncVarInt(0, _photonView, REPID);
        _currentRepetition.ValueChanged += _currentRepetition_ValueChanged;

        if (_networkManager.IsMasterClient) SelectCylinders();
    }

    private void _currentRepetition_ValueChanged(int value)
    {
        if (value > 0)
        {
            _timer = 0;
            PositionConvergenceCompleted[value - 1] = true;
            AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

            if (_networkManager.IsMasterClient)
                OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer,
                    OnlineLogger.EventType.PosCompleted);

            EnableCylinderHighlight(value);
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _basesOutlines.Length; i++)
        {
            if (_basesOutlines[i] == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineColor = CylinderHighlightColor;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;
        }

        for (var i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;
            Cylinders[i].GetComponent<Renderer>().material.DOKill();
            Cylinders[i].GetComponent<Renderer>().material.color = _startCylinderColor;
            BasesRenderers[i].material.DOKill();
            BasesRenderers[i].material.color = _startBaseColor;
        }

        _selectedCylinders = null;
        PositionConvergenceCompleted = null;
        _timer = 0;
        IsInitialized = false;
        _sequence.Kill();

        if (_networkManager.IsMasterClient)
        {
            CurrentRepetition = 0;
            _player1OnBase.Value = false;
            _player2OnBase.Value = false;
            _player3OnBase.Value = false;
            SelectCylinders();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (_networkManager.NumberOfPlayersReady != _networkManager.NumberOfPlayers ||
            currentScene.Type != SubSceneManager.SubSceneType.Task ||
            currentScene.TaskInfo.Interaction != SubSceneManager.Interaction.Position ||
            currentScene.TaskInfo.InteractionFocus != SubSceneManager.InteractionFocus.SameObject ||
            currentScene.TaskInfo.Input != SubSceneManager.Input.SystemCommanded)
            return;

        if (CurrentRepetition >= Repetitions) return;

        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;
        SetSyncVarBool(playerIndex,
            !GameUIManager.Instance.InstructionsPanelEnabled && IsInitialized &&
            !PositionConvergenceCompleted[CurrentRepetition] && GameUtils.IsPointWithinCollider(
                Cylinders[_selectedCylinders[CurrentRepetition]].GetComponent<Collider>(),
                Camera.main.transform.position));

        //Debug.LogFormat("Test: {0} {1} {2}", _player1OnBase.Value, _player2OnBase.Value, _player3OnBase.Value);

        if (_networkManager.IsMasterClient && IsInitialized && !PositionConvergenceCompleted[CurrentRepetition])
        {
            if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
            {
                if (_networkManager.GetPlayerInfoFromPlayerIndex(1) != null && _player1OnBase.Value &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(2) != null && _player2OnBase.Value &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(3) != null && _player3OnBase.Value)
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompletePositionConvergence();
                }
                else _timer = 0;
            }
            else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
            {
                if ((_networkManager.GetPlayerInfoFromPlayerIndex(1) == null || _player1OnBase.Value) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(2) == null || _player2OnBase.Value) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(3) == null || _player3OnBase.Value))
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompletePositionConvergence();
                }
                else _timer = 0;
            }
        }
    }

    #endregion

    #region Public methods

    public void EnableCylinderHighlight(int repetition)
    {
        if (_sequence != null)
        {
            _sequence.Kill();
            _sequence = null;
        }

        if (repetition == 0)
        {
            //enable current rep
            if (_basesOutlines[_selectedCylinders[repetition]] != null)
                _basesOutlines[_selectedCylinders[repetition]].enabled = true;

            Cylinders[_selectedCylinders[repetition]].GetComponent<Renderer>().enabled = true;
            Cylinders[_selectedCylinders[repetition]].GetComponent<Renderer>().material.color =
                CylinderHighlightColor.BrightnessOffset(0.4f).WithAlphaSetTo(_startCylinderColor.a);
            BasesRenderers[_selectedCylinders[repetition]].material.color = CylinderHighlightColor.BrightnessOffset(0.2f);
        }
        else if (repetition == Repetitions)
        {
            //complete old rep
            if (_basesOutlines[_selectedCylinders[repetition - 1]] != null)
                _basesOutlines[_selectedCylinders[repetition - 1]].FadeOutAndDisable();

            Cylinders[_selectedCylinders[repetition - 1]].GetComponent<Renderer>().material
                .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f);
            BasesRenderers[_selectedCylinders[repetition - 1]].material.DOColor(_cylinderHighlightCompleteColor, 0.5f);
        }
        else
        {
            if (_basesOutlines[_selectedCylinders[repetition - 1]] != null)
                _basesOutlines[_selectedCylinders[repetition - 1]].FadeOutAndDisable();

            _sequence = DOTween.Sequence();
            _sequence
                //complete old rep
                .Append(Cylinders[_selectedCylinders[repetition - 1]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1]].material.DOColor(_cylinderHighlightCompleteColor, 0.5f))
                .AppendInterval(0.5f)
                .Append(Cylinders[_selectedCylinders[repetition - 1]].GetComponent<Renderer>().material
                    .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(0f), 0.5f))
                .Join(BasesRenderers[_selectedCylinders[repetition - 1]].material.DOColor(_startBaseColor, 0.5f))
                //enable current rep
                .OnKill(() =>
                {
                    Cylinders[_selectedCylinders[repetition - 1]].GetComponent<Renderer>().enabled = false;
                    Cylinders[_selectedCylinders[repetition - 1]].GetComponent<Renderer>().material.color =
                        _startCylinderColor;
                    BasesRenderers[_selectedCylinders[repetition - 1]].material.color = _startBaseColor;

                    if (_basesOutlines[_selectedCylinders[repetition]] != null)
                        _basesOutlines[_selectedCylinders[repetition]].enabled = true;

                    Cylinders[_selectedCylinders[repetition]].GetComponent<Renderer>().enabled = true;
                    Cylinders[_selectedCylinders[repetition]].GetComponent<Renderer>().material.color =
                        CylinderHighlightColor.BrightnessOffset(0.4f).WithAlphaSetTo(_startCylinderColor.a);
                    BasesRenderers[_selectedCylinders[repetition]].material.color = CylinderHighlightColor.BrightnessOffset(0.2f);
                });
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCylinders()
    {
        int[] selectedCylinders = GameUtils.GenerateUniqueRandomNumbers(0, Cylinders.Length, Repetitions);

        _photonView.RPC("SelectCylindersRPC", RpcTarget.AllBuffered, selectedCylinders);
    }


    [PunRPC]
    public void SelectCylindersRPC(int[] selectedCylinders)
    {
        _selectedCylinders = selectedCylinders;
        Repetitions = selectedCylinders.Length;
        PositionConvergenceCompleted = new bool[selectedCylinders.Length];
        for (var i = 0; i < PositionConvergenceCompleted.Length; i++)
        {
            PositionConvergenceCompleted[i] = false;
        }

        IsInitialized = true;

        EnableCylinderHighlight(0);
    }

    private void CompletePositionConvergence()
    {
        PositionConvergenceCompleted[CurrentRepetition] = true;
        CurrentRepetition++;
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        if (gameObj == Cylinders[_selectedCylinders[CurrentRepetition]])
            return true;
        return false;
    }

    private void SetSyncVarBool(int playerIndex, bool value)
    {
        switch (playerIndex)
        {
            case 1:
                if(_player1OnBase.Value != value) _player1OnBase.Value = value;
                break;
            case 2:
                if (_player2OnBase.Value != value) _player2OnBase.Value = value;
                break;
            case 3:
                if (_player3OnBase.Value != value) _player3OnBase.Value = value;
                break;
        }
    }
}
