﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class TaskManipDiffSysHoles : StaticObject
{
    #region Attributes

    [Header("TaskManipDiffSysHoles properties")]
    [MustBeAssigned] public GameObject HoleRectangle;
    [MustBeAssigned] public TextMeshPro HoleRectangleText;
    [MustBeAssigned] public TextMeshPro HoleRectangleUnderlineText;
    [MustBeAssigned] public GameObject HoleSquare;
    [MustBeAssigned] public TextMeshPro HoleSquareText;
    [MustBeAssigned] public TextMeshPro HoleSquareUnderlineText;
    [MustBeAssigned] public GameObject HoleTriangle;
    [MustBeAssigned] public TextMeshPro HoleTriangleText;
    [MustBeAssigned] public TextMeshPro HoleTriangleUnderlineText;

    public float OcclusionHoleRadius = 0.57f;

    [ReadOnly] public int RectangleNumber = -1;
    [ReadOnly] public int SquareNumber = -1;
    [ReadOnly] public int TriangleNumber = -1;

    public bool IsInitialized { get; private set; } = false;

    [SerializeField, ReadOnly] private int _rectangleRotation = -1;
    [SerializeField, ReadOnly] private int _squareRotation = -1;
    [SerializeField, ReadOnly] private int _triangleRotation = -1;
    [SerializeField, ReadOnly] private int[] _playerIndices = { 1, 2, 3 };
    private Color _startColor;


    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _startColor = HoleRectangle.GetComponent<Renderer>().material.color;

        HoleRectangleUnderlineText.enabled = false;
        HoleSquareUnderlineText.enabled = false;
        HoleTriangleUnderlineText.enabled = false;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskManipDiffSysHoles = this;

        if (_networkManager.IsMasterClient && (RectangleNumber == -1 || _rectangleRotation == -1 ||
                                               SquareNumber == -1 || _squareRotation == -1 || TriangleNumber == -1 ||
                                               _triangleRotation == -1))
            InitializeHoles();
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        RectangleNumber = -1;
        _rectangleRotation = -1;
        SquareNumber = -1;
        _squareRotation = -1;
        TriangleNumber = -1;
        _triangleRotation = -1;

        HoleRectangleText.text = "";
        HoleSquareText.text = "";
        HoleTriangleText.text = "";

        HoleRectangle.transform.localRotation = Quaternion.identity;
        HoleSquare.transform.localRotation = Quaternion.identity;
        HoleTriangle.transform.localRotation = Quaternion.identity;

        HoleRectangleUnderlineText.enabled = false;
        HoleSquareUnderlineText.enabled = false;
        HoleTriangleUnderlineText.enabled = false;

        IsInitialized = false;

        if (_networkManager.IsMasterClient)
        {
            InitializeHoles();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (currentScene.Type != SubSceneManager.SubSceneType.Task ||
            currentScene.TaskInfo.Interaction != SubSceneManager.Interaction.Manipulation ||
            currentScene.TaskInfo.InteractionFocus != SubSceneManager.InteractionFocus.DifferentObjects ||
            currentScene.TaskInfo.Input != SubSceneManager.Input.SystemCommanded)
            return;

        ARManager.Instance.SetOcclusionWithShadowsHolePosition(transform.position);
    }

    #endregion

    #region Public methods


    #endregion

    #region Private methods / Photon RPCs

    private void InitializeHoles()
    {
        RectangleNumber = Random.Range(1, 10);
        SquareNumber = Random.Range(1, 10);
        TriangleNumber = Random.Range(1, 10);

        _rectangleRotation = Random.Range(0, 8);
        _squareRotation = Random.Range(0, 8);
        _triangleRotation = Random.Range(0, 8);

        _playerIndices = _playerIndices.Shuffled();

        _photonView.RPC("InitializeHolesRPC", RpcTarget.AllBuffered, RectangleNumber, _rectangleRotation,
            SquareNumber, _squareRotation, TriangleNumber, _triangleRotation, _playerIndices);
    }


    [PunRPC]
    public void InitializeHolesRPC(int rectangleNumber, int rectangleRotation,
        int squareNumber, int squareRotation, int triangleNumber, int triangleRotation, int [] playerIndices)
    {
        RectangleNumber = rectangleNumber;
        SquareNumber = squareNumber;
        TriangleNumber = triangleNumber;

        if (RectangleNumber == 6 || RectangleNumber == 9) HoleRectangleUnderlineText.enabled = true;
        else HoleRectangleUnderlineText.enabled = false;

        if (SquareNumber == 6 || SquareNumber == 9) HoleSquareUnderlineText.enabled = true;
        else HoleSquareUnderlineText.enabled = false;

        if (TriangleNumber == 6 || TriangleNumber == 9) HoleTriangleUnderlineText.enabled = true;
        else HoleTriangleUnderlineText.enabled = false;

        _rectangleRotation = rectangleRotation;
        _squareRotation = squareRotation;
        _triangleRotation = triangleRotation;

        _playerIndices = playerIndices;

        _networkManager.CurrentRoom.TaskManipDiffSysRectangle.OwnerPlayerIndex = _playerIndices[0];
        _networkManager.CurrentRoom.TaskManipDiffSysSquare.OwnerPlayerIndex = _playerIndices[1];
        _networkManager.CurrentRoom.TaskManipDiffSysTriangle.OwnerPlayerIndex = _playerIndices[2];

        HoleRectangleText.text = rectangleNumber.ToString();
        HoleSquareText.text = squareNumber.ToString();
        HoleTriangleText.text = triangleNumber.ToString();

        HoleRectangle.transform.localRotation = Quaternion.Euler(0, _rectangleRotation * 45f, 0);
        HoleSquare.transform.localRotation = Quaternion.Euler(0, _squareRotation * 45f, 0);
        HoleTriangle.transform.localRotation = Quaternion.Euler(0, _triangleRotation * 45f, 0);

        IsInitialized = true;
    }

    #endregion
}
