﻿using System.Collections;
using UnityEngine;

public class TaskPosSameSys : ITaskPosition
{
    public TaskPosSameSys(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task, 
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskPosSameSysPlates == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskPosSameSysPlatesPrefab.name, _networkManager.CurrentRoom.PedestalPose);
            else
                _networkManager.CurrentRoom.TaskPosSameSysPlates.Reset();
        }
        else
            EnableObjects(true); 
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskPosSameSysPlates != null &&
                                         _networkManager.CurrentRoom.TaskPosSameSysPlates.IsInitialized);

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosSameSys/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskPosSameSysPlates.CurrentRepetition ==
                                         _networkManager.CurrentRoom.TaskPosSameSysPlates.Repetitions);

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskPosSameSysPlates != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskPosSameSysPlates.Enable(enabled);
    }
}
