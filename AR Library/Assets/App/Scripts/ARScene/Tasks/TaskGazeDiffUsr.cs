﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TaskGazeDiffUsr : ITaskGaze
{
    public TaskGazeDiffUsr(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task,
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskGazeDiffUsrCubes == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskGazeDiffUsrCubesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskGazeDiffUsrCubes != null &&
                                         _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.IsInitialized);

        string textMasterPlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffUsr/Instructions_Start_Master");
        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");

        //First gaze convergence
        // in first subtask the master is always the helper / master client, so skip it
        /*if (_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask == TaskGazeDiffUsrCubes.SubTask.First)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[0])
                    .ToHex());

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[0], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[0], textMasterPlayer,
                textSlavePlayer);

            if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1,
                    out var completed) && !completed)
            {
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.EnableCubesHighlight(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);
                FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);

                yield return new WaitUntil(() =>
                    _networkManager.CurrentRoom.GetGazeConvergenceStatus(
                        GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1, out completed) && completed);
                FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);
            }
            _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.DisableCubesHighlight(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);
            yield return ShowCompletionTextAndWaitNextSubTask(completionText,
                () => _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask = TaskGazeDiffUsrCubes.SubTask.Second,
                () => _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask == TaskGazeDiffUsrCubes.SubTask.Second);
        }*/

        //Second gaze convergence
        if (_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask == TaskGazeDiffUsrCubes.SubTask.Second)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[1])
                    .ToHex());

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[1], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[1], textMasterPlayer,
                textSlavePlayer);

            if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2,
                    out var completed) && !completed)
            {
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.EnableCubesHighlight(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);
                FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);

                yield return new WaitUntil(() =>
                    _networkManager.CurrentRoom.GetGazeConvergenceStatus(
                        GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2, out completed) && completed);
                FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);
            }
            _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.DisableCubesHighlight(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);
            yield return ShowCompletionTextAndWaitNextSubTask(completionText,
                () => _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask = TaskGazeDiffUsrCubes.SubTask.Third,
                () => _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask == TaskGazeDiffUsrCubes.SubTask.Third);
        }

        //Third gaze convergence
        if (_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.CurrentSubTask == TaskGazeDiffUsrCubes.SubTask.Third)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[2])
                    .ToHex());

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[2], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskGazeDiffUsrCubes.PlayerIndices[2], textMasterPlayer,
                textSlavePlayer);

            if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3,
                    out var completed) && !completed)
            {
                _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.EnableCubesHighlight(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);
                FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);

                yield return new WaitUntil(() =>
                    _networkManager.CurrentRoom.GetGazeConvergenceStatus(
                        GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3, out completed) && completed);
                FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);
            }
            _networkManager.CurrentRoom.TaskGazeDiffUsrCubes.DisableCubesHighlight(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);

            ShowCompletionTextAndNextSceneButton(completionText);
        }
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskGazeDiffUsrCubes != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskGazeDiffUsrCubes.Enable(enabled);
    }
}
