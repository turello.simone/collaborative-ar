﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TaskGazeDiffSys : ITaskGaze
{
    public TaskGazeDiffSys(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task,
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskGazeDiffSysCubes == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskGazeDiffSysCubesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TaskGazeDiffSysCubes.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskGazeDiffSysCubes != null &&
                                         _networkManager.CurrentRoom.TaskGazeDiffSysCubes.IsInitialized);

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffSys/Instructions_Start");
        instructions = instructions.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());

        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskGazeDiffSysCubes.CurrentRepetition ==
                                         _networkManager.CurrentRoom.TaskGazeDiffSysCubes.Repetitions);

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }


    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskGazeDiffSysCubes != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskGazeDiffSysCubes.Enable(enabled);
    }
}
