﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class TaskPosSameUsrPlates : StaticObject
{
    #region Attributes

    [Header("TaskPosSameUsrPlates properties")]
    [MustBeAssigned] public GameObject[] Cylinders;
    [MustBeAssigned] public Renderer[] BasesRenderers;
    [MustBeAssigned] public TextMeshPro[] Texts;
    [MustBeAssigned] public Color CylinderHighlightColor = Color.red;
    [ReadOnly] public bool[] PositionConvergencesCompleted = { false, false, false };
    public float CompletionRequiredTime = 1f;

    private Color _cylinderHighlightCompleteColor = Color.green.BrightnessOffset(0.8f);

    //_selectedCylinders and _playerIndices: index in these arrays is the order to follow when checking cylinders.
    //example: index = 0, the first cylinder to be activated is _selectedCylinders[0], only player _playerIndices[0] can see the text "index+1" on the cylinder
    [SerializeField, ReadOnly] private int[] _selectedCylinders = { -1, -1, -1 };
    [SerializeField, ReadOnly] private int[] _playerIndices = { 1, 2, 3 };
    private CustomOutline[] _basesOutlines;
    private Color _startBaseColor;
    private Color _startCylinderColor;
    private float _timer = 0;
    private int _currentPositionConvergence = 0;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _timer = 0;
        _currentPositionConvergence = 0;
        _startBaseColor = BasesRenderers[0].material.color;
        _startCylinderColor = Cylinders[0].GetComponent<Renderer>().material.color;

        _basesOutlines = new CustomOutline[BasesRenderers.Length];

        for (int i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;

            if ((_basesOutlines[i] = BasesRenderers[i].GetComponent<CustomOutline>()) == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineColor = CylinderHighlightColor;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;

            Texts[i].text = "";
            Texts[i].enabled = false;
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskPosSameUsrPlates = this;

        if (_networkManager.IsMasterClient &&
            (_selectedCylinders[0] == -1 || _selectedCylinders[1] == -1 || _selectedCylinders[2] == -1))
            SelectCylinders();
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _basesOutlines.Length; i++)
        {
            _basesOutlines[i].enabled = false;
        }

        for (var i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;
            Cylinders[i].GetComponent<Renderer>().material.DOKill();
            Cylinders[i].GetComponent<Renderer>().material.color = _startCylinderColor;

            BasesRenderers[i].material.DOKill();
            BasesRenderers[i].material.color = _startBaseColor;

            Texts[i].text = "";
            Texts[i].enabled = false;
        }

        for (var i = 0; i < _selectedCylinders.Length; i++)
        {
            _selectedCylinders[i] = -1;
        }

        for (var i = 0; i < PositionConvergencesCompleted.Length; i++)
        {
            PositionConvergencesCompleted[i] = false;
        }

        _timer = 0;
        _currentPositionConvergence = 0;

        if (_networkManager.IsMasterClient)
        {
            SelectCylinders();
        }
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (!_networkManager.IsMasterClient ||
            _networkManager.NumberOfPlayersReady != _networkManager.NumberOfPlayers ||
            currentScene.Type != SubSceneManager.SubSceneType.Task ||
            currentScene.TaskInfo.Interaction != SubSceneManager.Interaction.Position ||
            currentScene.TaskInfo.InteractionFocus != SubSceneManager.InteractionFocus.SameObject ||
            currentScene.TaskInfo.Input != SubSceneManager.Input.UserCommanded)
            return;

        if (_networkManager.IsMasterClient && _currentPositionConvergence <= 2 &&
            !PositionConvergencesCompleted[_currentPositionConvergence])
        {
            if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
            {
                if (_networkManager.GetPlayerInfoFromPlayerIndex(1) != null &&
                    GameUtils.IsPointWithinCollider(Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Collider>(),
                        _networkManager.GetPlayerInfoFromPlayerIndex(1).Player.transform.position) &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(2) != null &&
                    GameUtils.IsPointWithinCollider(Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Collider>(),
                        _networkManager.GetPlayerInfoFromPlayerIndex(2).Player.transform.position) &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(3) != null &&
                    GameUtils.IsPointWithinCollider(Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Collider>(),
                        _networkManager.GetPlayerInfoFromPlayerIndex(3).Player.transform.position))
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompleteCurrentPositionConvergence();
                }
                else _timer = 0;
            }
            else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
            {
                if ((_networkManager.GetPlayerInfoFromPlayerIndex(1) == null ||
                    GameUtils.IsPointWithinCollider(Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Collider>(),
                        _networkManager.GetPlayerInfoFromPlayerIndex(1).Player.transform.position)) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(2) == null ||
                    GameUtils.IsPointWithinCollider(Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Collider>(),
                        _networkManager.GetPlayerInfoFromPlayerIndex(2).Player.transform.position)) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(3) == null ||
                    GameUtils.IsPointWithinCollider(Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Collider>(),
                        _networkManager.GetPlayerInfoFromPlayerIndex(3).Player.transform.position)))
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompleteCurrentPositionConvergence();
                }
                else _timer = 0;
            }
        }
    }

    #endregion

    #region Public methods

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCylinders()
    {
        GameUtils.GenerateUniqueRandomNumbers(0, Cylinders.Length, out _selectedCylinders[0], out _selectedCylinders[1],
            out _selectedCylinders[2]);
        _playerIndices = _playerIndices.Shuffled();
        _photonView.RPC("SelectCylindersRPC", RpcTarget.AllBuffered, _selectedCylinders, _playerIndices);
    }


    [PunRPC]
    public void SelectCylindersRPC(int[] selectedCylinder, int[] playerIndices)
    {
        _selectedCylinders = selectedCylinder;
        _playerIndices = playerIndices;

        Texts[_selectedCylinders[0]].text = 1.ToString();
        Texts[_selectedCylinders[1]].text = 2.ToString();
        Texts[_selectedCylinders[2]].text = 3.ToString();

        if (_networkManager.LocalPlayer.PlayerIndex == 0)
            _networkManager.LocalPlayer.ColorSetEvent += EnableCylinderHighlight;
        else
            EnableCylinderHighlight(_networkManager.LocalPlayer, _networkManager.LocalPlayer.Color);
    }

    private void EnableCylinderHighlight(PlayerInfo player, Color localPlayerColor)
    {
        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;

        int arrayIndex = 0;
        for (int i = 0; i < _playerIndices.Length; i++)
        {
            if (_playerIndices[i] == playerIndex)
            {
                arrayIndex = i;
                break;
            }
        }

        if(_currentPositionConvergence > arrayIndex) return;

        //enable outline of _basesOutlines[_selectedCylinders[arrayIndex]]
        _basesOutlines[_selectedCylinders[arrayIndex]].enabled = true;

        //enable Cylinders[_selectedCylinders[arrayIndex]] and change its color
        Cylinders[_selectedCylinders[arrayIndex]].GetComponent<Renderer>().enabled = true;
        Cylinders[_selectedCylinders[arrayIndex]].GetComponent<Renderer>().material.color =
            CylinderHighlightColor.BrightnessOffset(0.4f).WithAlphaSetTo(_startCylinderColor.a);

        //change color of BasesRenderers[_selectedCylinders[arrayIndex]]
        BasesRenderers[_selectedCylinders[arrayIndex]].material.color = CylinderHighlightColor.BrightnessOffset(0.2f);

        //show text on Cylinders[_selectedCylinders[arrayIndex]], enabling Texts[_selectedCylinders[arrayIndex]]
        Texts[_selectedCylinders[arrayIndex]].enabled = true;

        _networkManager.LocalPlayer.ColorSetEvent -= EnableCylinderHighlight;
    }

    private void CompleteCurrentPositionConvergence()
    {
        _photonView.RPC("CompleteCurrentPositionConvergenceRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void CompleteCurrentPositionConvergenceRPC()
    {
        PositionConvergencesCompleted[_currentPositionConvergence] = true;

        if (_basesOutlines[_selectedCylinders[_currentPositionConvergence]] != null)
            _basesOutlines[_selectedCylinders[_currentPositionConvergence]].FadeOutAndDisable();

        Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Renderer>().enabled = true;
        Cylinders[_selectedCylinders[_currentPositionConvergence]].GetComponent<Renderer>().material
            .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f);

        BasesRenderers[_selectedCylinders[_currentPositionConvergence]].material.DOColor(_cylinderHighlightCompleteColor, 0.5f);

        Texts[_selectedCylinders[_currentPositionConvergence]].enabled = true;

        _currentPositionConvergence++;
    }

    #endregion
}
