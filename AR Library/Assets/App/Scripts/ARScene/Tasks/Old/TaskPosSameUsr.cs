﻿using System.Collections;
using UnityEngine;

public class TaskPosSameUsr : ITaskPosition
{
    public TaskPosSameUsr(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task,
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskPosSameUsrPlates == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskPosSameUsrPlatesPrefab.name, _networkManager.CurrentRoom.PedestalPose);
            else
                _networkManager.CurrentRoom.TaskPosSameUsrPlates.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = "";
        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        for (var i = 0; i < _networkManager.CurrentRoom.TaskPosSameUsrPlates.PositionConvergencesCompleted.Length; i++)
        {
            yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskPosSameUsrPlates.PositionConvergencesCompleted[i]);
        }

        ShowCompletionTextAndNextSceneButton("Task Completed");
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskPosSameUsrPlates != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskPosSameUsrPlates.Enable(enabled);
    }
}
