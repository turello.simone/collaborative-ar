﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class TaskGazeSameUsrCubes : StaticObject
{
    #region Attributes

    [Header("TaskGazeSameUsrCubes properties")]
    [MustBeAssigned] public GameObject[] Cubes;
    [MustBeAssigned] public TextMeshPro[] Texts;
    [MustBeAssigned] public Color CubeHighlightColor = Color.red;

    private Color _cubeHighlightCompleteColor = Color.green.BrightnessOffset(0.8f);

    //_selectedCubes and _playerIndices: index in these arrays is the order to follow when activating gaze convergence focal points.
    //example: index = 0, the first cube to be activated is _selectedCube[0], only player _playerIndices[0] can see the text "index+1" on the cube
    [SerializeField, ReadOnly] private int[] _selectedCubes = { -1, -1, -1 };
    [SerializeField, ReadOnly] private int[] _playerIndices = { 1, 2, 3 };
    private CustomOutline[] _outlines;
    private GameObject[] _gazeConvergenceFocalPoints = new GameObject[3];
    private Color _startColor;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _startColor = Cubes[0].GetComponent<Renderer>().material.color;

        _outlines = new CustomOutline[Cubes.Length];
        for (int i = 0; i < Cubes.Length; i++)
        {
            if ((_outlines[i] = Cubes[i].GetComponent<CustomOutline>()) == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineColor = CubeHighlightColor;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;

            Texts[i].text = "";
            Texts[i].enabled = false;
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskGazeSameUsrCubes = this;

        if (_networkManager.IsMasterClient &&
            (_selectedCubes[0] == -1 || _selectedCubes[1] == -1 || _selectedCubes[2] == -1)) SelectCubes();
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _outlines.Length; i++)
        {
            _outlines[i].enabled = false;
        }

        for (var i = 0; i < Cubes.Length; i++)
        {
            Cubes[i].GetComponent<Renderer>().material.DOKill();
            Cubes[i].GetComponent<Renderer>().material.color = _startColor;
        }

        for (var i = 0; i < Texts.Length; i++)
        {
            Texts[i].text = "";
            Texts[i].enabled = false;
        }

        for (var i = 0; i < _gazeConvergenceFocalPoints.Length; i++)
        {
            Destroy(_gazeConvergenceFocalPoints[i]);
            _gazeConvergenceFocalPoints[i] = null;
        }

        for (var i = 0; i < _selectedCubes.Length; i++)
        {
            _selectedCubes[i] = -1;
        }

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1);
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2);
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3);
            SelectCubes();
        }
    }

    #endregion

    #region Public methods

    public void DisableCubeHighlight(GazeConvergenceFocalPoint.Type type)
    {
        int index = -1;
        switch (type)
        {
            case GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1:
                index = 0;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2:
                index = 1;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3:
                index = 2;
                break;
        }

        if (index != -1)
        {
            Cubes[_selectedCubes[index]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
            Texts[_selectedCubes[index]].enabled = true;

            if (_playerIndices[index] == _networkManager.LocalPlayer.PlayerIndex && _outlines[_selectedCubes[index]] != null)
            {
                _outlines[_selectedCubes[index]].FadeOutAndDisable();
            }

            if (_gazeConvergenceFocalPoints[index] != null)
            {
                Destroy(_gazeConvergenceFocalPoints[index]);
                _gazeConvergenceFocalPoints[index] = null;
            }
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCubes()
    {
        GameUtils.GenerateUniqueRandomNumbers(0, Cubes.Length, out _selectedCubes[0], out _selectedCubes[1], out _selectedCubes[2]);
        _playerIndices = _playerIndices.Shuffled();
        _photonView.RPC("SelectCubesRPC", RpcTarget.AllBuffered, _selectedCubes, _playerIndices);
    }

    [PunRPC]
    public void SelectCubesRPC(int[] selectedCubes, int[] playerIndices)
    {
        _selectedCubes = selectedCubes;
        _playerIndices = playerIndices;

        Texts[selectedCubes[0]].text = 1.ToString();
        Texts[selectedCubes[1]].text = 2.ToString();
        Texts[selectedCubes[2]].text = 3.ToString();

        if (_networkManager.LocalPlayer.PlayerIndex == 0)
            _networkManager.LocalPlayer.ColorSetEvent += EnableCubeHighlight;
        else
            EnableCubeHighlight(_networkManager.LocalPlayer, _networkManager.LocalPlayer.Color);


        _gazeConvergenceFocalPoints[0] =
            FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                Cubes[selectedCubes[0]].transform.position, 0.1f, this.transform);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
            GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1, _gazeConvergenceFocalPoints[0], outlineEnabled: false,
            iconEnabled: false, arrowEnabled: false);
        

        _gazeConvergenceFocalPoints[1] =
                FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                    Cubes[selectedCubes[1]].transform.position, 0.1f, this.transform);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
                GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2, _gazeConvergenceFocalPoints[1], outlineEnabled: false,
                iconEnabled: false, arrowEnabled: false);
        

        _gazeConvergenceFocalPoints[2] =
            FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                Cubes[selectedCubes[2]].transform.position, 0.1f, this.transform);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
            GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3, _gazeConvergenceFocalPoints[2], outlineEnabled: false,
            iconEnabled: false, arrowEnabled: false);
    }

    private void EnableCubeHighlight(PlayerInfo player, Color localPlayerColor)
    {
        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;

        int arrayIndex = 0;
        for (int i = 0; i < _playerIndices.Length; i++)
        {
            if (_playerIndices[i] == playerIndex)
            {
                arrayIndex = i;
                break;
            }
        }

        //enable outline of Cubes[selectedCubes[arrayIndex]]
        _outlines[_selectedCubes[arrayIndex]].enabled = true;

        Cubes[_selectedCubes[arrayIndex]].GetComponent<Renderer>().material.color = CubeHighlightColor.Lighter().Lighter();

        //show text on Cubes[selectedCubes[arrayIndex]], the number shown is (arrayIndex + 1)
        Texts[_selectedCubes[arrayIndex]].enabled = true;

        _networkManager.LocalPlayer.ColorSetEvent -= EnableCubeHighlight;
    }

    #endregion
}
