﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskGazeSameUsr : ITaskGaze
{
    public TaskGazeSameUsr(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task,
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskGazeSameUsrCubes == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskGazeSameUsrCubesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TaskGazeSameUsrCubes.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions =
            "Look along with the other users at every cube marked with a number in ascending order, placing your gaze cursor on it. " +
            "Consider that every one of you can see only one marked cube.";
        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        bool completed;
        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1);
        }
        _networkManager.CurrentRoom.TaskGazeSameUsrCubes.DisableCubeHighlight(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr1);

        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2);
        }
        _networkManager.CurrentRoom.TaskGazeSameUsrCubes.DisableCubeHighlight(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr2);

        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3);
        }
        _networkManager.CurrentRoom.TaskGazeSameUsrCubes.DisableCubeHighlight(GazeConvergenceFocalPoint.Type.TaskGazeSameUsr3);

        ShowCompletionTextAndNextSceneButton("Task Completed");
    }


    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskGazeSameUsrCubes != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskGazeSameUsrCubes.Enable(enabled);
    }
}
