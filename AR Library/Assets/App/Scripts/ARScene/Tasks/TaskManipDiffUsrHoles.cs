﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class TaskManipDiffUsrHoles : StaticObject
{
    public enum SubTask
    {
        Default,
        First,
        Second,
        Third
    }

    #region Attributes

    [Header("TaskManipDiffSysHoles properties")]
    [MustBeAssigned] public GameObject[] Holes;
    [MustBeAssigned] public TextMeshPro[] HolesText;
    [MustBeAssigned] public TextMeshPro[] HolesUnderlineText;

    public float OcclusionHoleRadius = 1.07f;

    public SubTask CurrentSubTask
    {
        get => (SubTask)_currentSubTask.Value;
        set => _currentSubTask.Value = (int)value;
    }
    private const string SUBTASKID = "SubTask";
    private SyncVarInt _currentSubTask;

    public bool IsInitialized { get; private set; } = false;

    [ReadOnly] public int[] PlayerIndices = { 1, 2, 3 };
    public int[,] SelectedHoles = {{-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}};
    public int[,] Numbers = {{-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}};
    private int[,] _holesRotation;

    private Color _startColor;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _startColor = Holes[0].GetComponent<Renderer>().material.color;

        for (var i = 0; i < HolesUnderlineText.Length; i++)
        {
            HolesUnderlineText[i].enabled = false;
        }

        for (var i = 0; i < HolesText.Length; i++)
        {
            HolesText[i].text = "";
        }

        _holesRotation = new int[3,Holes.Length];

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < Holes.Length; j++)
            {
                _holesRotation[i, j] = -1;
            }
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _networkManager.CurrentRoom.TaskManipDiffUsrHoles = this;

        _currentSubTask = new SyncVarInt((int)SubTask.Second, _photonView, SUBTASKID);

        SetActiveRendererAndColliderOfTriangles(false, false, false);

        if (_networkManager.IsMasterClient && Array.Exists(SelectedHoles.OfType<int>().ToArray(), i => i == -1) &&
            Array.Exists(Numbers.OfType<int>().ToArray(), i => i == -1) &&
            Array.Exists(_holesRotation.OfType<int>().ToArray(), i => i == -1))
            InitializeHoles();
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < Holes.Length; i++)
        {
            Holes[i].SetActive(false);
            Holes[i].GetComponent<Renderer>().material.color = _startColor;
            Holes[i].transform.localRotation = Quaternion.identity;
            HolesText[i].text = "";
            HolesUnderlineText[i].enabled = false;
        }

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < Holes.Length; j++)
            {
                _holesRotation[i, j] = -1;
            }
        }

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < 3; j++)
            {
                SelectedHoles[i, j] = -1;
                Numbers[i, j] = -1;
            }
        }

        PlayerIndices[0] = 1;
        PlayerIndices[1] = 2;
        PlayerIndices[2] = 3;

        SetActiveRendererAndColliderOfTriangles(false, false, false);

        IsInitialized = false;

        if (_networkManager.IsMasterClient)
        {
            CurrentSubTask = SubTask.Second;
            InitializeHoles();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (currentScene.Type != SubSceneManager.SubSceneType.Task ||
            currentScene.TaskInfo.Interaction != SubSceneManager.Interaction.Manipulation ||
            currentScene.TaskInfo.InteractionFocus != SubSceneManager.InteractionFocus.DifferentObjects ||
            currentScene.TaskInfo.Input != SubSceneManager.Input.UserCommanded)
            return;

        ARManager.Instance.SetOcclusionWithShadowsHolePosition(transform.position);
    }

    #endregion

    #region Public methods

    public void SetupSubTask()
    {
        //Reset holes material color, renderer, number and disable them
        for (var i = 0; i < Holes.Length; i++)
        {
            Holes[i].SetActive(false);
            Holes[i].GetComponent<Renderer>().material.color = _startColor;
            HolesText[i].text = "";
            HolesUnderlineText[i].enabled = false;
        }

        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;
        int subTaskIndex = -1;
        switch (CurrentSubTask)
        {
            case SubTask.First:
                subTaskIndex = 0;
                SetActiveRendererAndColliderOfTriangles(true, false, false);
                break;
            case SubTask.Second:
                subTaskIndex = 1;
                SetActiveRendererAndColliderOfTriangles(false, true, false);
                break;
            case SubTask.Third:
                subTaskIndex = 2;
                SetActiveRendererAndColliderOfTriangles(false, false, true);
                break;
        }
        if (subTaskIndex == -1) return;

        //Set holes rotation
        for (var i = 0; i < Holes.Length; i++)
        {
            Holes[i].transform.localRotation = Quaternion.Euler(0, _holesRotation[subTaskIndex, i] * 45f, 0);
        }

        //Change selected holes color, set selected holes text number 
        InitializeSelectedHole(subTaskIndex, 1);
        InitializeSelectedHole(subTaskIndex, 2);
        InitializeSelectedHole(subTaskIndex, 3);

        if (playerIndex == PlayerIndices[subTaskIndex])
        {
            //i'm the master user, enable all holes
            for (var i = 0; i < Holes.Length; i++)
            {
                Holes[i].SetActive(true);
            }
        }
    }

    /*public void DisableCubesHighlight()
    {
        int index = -1;
        switch (type)
        {
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1:
                index = 0;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2:
                index = 1;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3:
                index = 2;
                break;
        }
        if (index == -1) return;

        Cubes[_selectedCubes[index, 0]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
        _outlines[_selectedCubes[index, 0]].FadeOutAndDisable();

        Cubes[_selectedCubes[index, 1]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
        _outlines[_selectedCubes[index, 1]].FadeOutAndDisable();

        if (_gazeConvergenceFocalPoints[index, 0] != null)
        {
            Destroy(_gazeConvergenceFocalPoints[index, 0]);
            _gazeConvergenceFocalPoints[index, 0] = null;
        }

        if (_gazeConvergenceFocalPoints[index, 1] != null)
        {
            Destroy(_gazeConvergenceFocalPoints[index, 1]);
            _gazeConvergenceFocalPoints[index, 1] = null;
        }
    }*/

    public Vector3 GetHoleEmbeddingPosition(int index)
    {
        return Holes[index].transform.position + 0.1f * transform.up;
    }

    #endregion

    #region Private methods / Photon RPCs

    private void SetActiveRendererAndColliderOfTriangles(bool firstSubTaskTrianglesEnabled, bool secondSubTaskTrianglesEnabled,
        bool thirdSubTaskTrianglesEnabled)
    {
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1.gameObject.RenderersEnabled(firstSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1.gameObject.CollidersEnabled(firstSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2.gameObject.RenderersEnabled(firstSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2.gameObject.CollidersEnabled(firstSubTaskTrianglesEnabled);

        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1.gameObject.RenderersEnabled(secondSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1.gameObject.CollidersEnabled(secondSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2.gameObject.RenderersEnabled(secondSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2.gameObject.CollidersEnabled(secondSubTaskTrianglesEnabled);

        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1.gameObject.RenderersEnabled(thirdSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1.gameObject.CollidersEnabled(thirdSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2.gameObject.RenderersEnabled(thirdSubTaskTrianglesEnabled);
        _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2.gameObject.CollidersEnabled(thirdSubTaskTrianglesEnabled);
    }

    private void InitializeHoles()
    {
        GameUtils.GenerateUniqueRandomNumbers(0, Holes.Length, out SelectedHoles[0, 0], out SelectedHoles[0, 1], out SelectedHoles[0, 2]);
        GameUtils.GenerateUniqueRandomNumbers(0, Holes.Length, out SelectedHoles[1, 0], out SelectedHoles[1, 1], out SelectedHoles[1, 2]);
        GameUtils.GenerateUniqueRandomNumbers(0, Holes.Length, out SelectedHoles[2, 0], out SelectedHoles[2, 1], out SelectedHoles[2, 2]);

        //PlayerIndices = PlayerIndices.Shuffled();
        int masterClientIndex = _networkManager.GetMasterClient().PlayerIndex;
        int[] array = new[] { PlayerIndices[masterClientIndex == 1 ? 1 : 0], PlayerIndices[masterClientIndex == 3 ? 1 : 2] };
        array = array.Shuffled();

        PlayerIndices[0] = masterClientIndex;
        PlayerIndices[1] = array[0];
        PlayerIndices[2] = array[1];

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < 3; j++)
            {
                Numbers[i,j] = Random.Range(1, 10);
            }
        }

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < Holes.Length; j++)
            {
                _holesRotation[i, j] = Random.Range(0, 8);
            }
        }

        _photonView.RPC("InitializeHolesRPC", RpcTarget.AllBuffered, MatrixSerializer.Serialize(SelectedHoles),
            MatrixSerializer.Serialize(Numbers), MatrixSerializer.Serialize(_holesRotation), PlayerIndices);
    }


    [PunRPC]
    public void InitializeHolesRPC(byte[] selectedHoles, byte[] numbers, byte[] holesRotation, int[] playerIndices)
    {
        SelectedHoles = MatrixSerializer.Deserialize<int>(selectedHoles);
        Numbers = MatrixSerializer.Deserialize<int>(numbers);
        _holesRotation = MatrixSerializer.Deserialize<int>(holesRotation);
        PlayerIndices = playerIndices;

        InitializeTriangles(0, _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1,
            _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2);
        InitializeTriangles(1, _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1,
            _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2);
        InitializeTriangles(2, _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1,
            _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2);

        IsInitialized = true;
    }

    private void InitializeSelectedHole(int subTaskIndex, int playerIndex)
    {
        if (PlayerIndices[subTaskIndex] != playerIndex)
        {
            Holes[SelectedHoles[subTaskIndex, playerIndex - 1]].GetComponent<Renderer>().material.color =
                _networkManager.GetPlayerColor(playerIndex).BrightnessOffset(0.4f);
            HolesText[SelectedHoles[subTaskIndex, playerIndex - 1]].text = Numbers[subTaskIndex, playerIndex - 1].ToString();
            if (Numbers[subTaskIndex, playerIndex - 1] == 6 || Numbers[subTaskIndex, playerIndex - 1] == 9)
                HolesUnderlineText[SelectedHoles[subTaskIndex, playerIndex - 1]].enabled = true;
            else HolesUnderlineText[SelectedHoles[subTaskIndex, playerIndex - 1]].enabled = false;
        }
    }

    private void InitializeTriangles(int subTaskIndex, TaskManipDiffUsrTriangle triangle1, TaskManipDiffUsrTriangle triangle2)
    {
        int playerIndex1 = PlayerIndices[subTaskIndex] == 1 ? 2 : 1;
        triangle1.InitializeTargetHole(playerIndex1, SelectedHoles[subTaskIndex, playerIndex1 - 1],
            Numbers[subTaskIndex, playerIndex1 - 1]);

        int playerIndex2 = PlayerIndices[subTaskIndex] == 3 ? 2 : 3;
        triangle2.InitializeTargetHole(playerIndex2, SelectedHoles[subTaskIndex, playerIndex2 - 1],
            Numbers[subTaskIndex, playerIndex2 - 1]);
    }

    #endregion
}
