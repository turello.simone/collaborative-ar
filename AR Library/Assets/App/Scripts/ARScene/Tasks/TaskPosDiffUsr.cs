﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TaskPosDiffUsr : ITaskPosition
{
    public TaskPosDiffUsr(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task, 
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskPosDiffUsrPlates == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskPosDiffUsrPlatesPrefab.name, _networkManager.CurrentRoom.PedestalPose);
            else
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.Reset();
        }
        else
            EnableObjects(true); 
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskPosDiffUsrPlates != null &&
                                         _networkManager.CurrentRoom.TaskPosDiffUsrPlates.IsInitialized);

        string textMasterPlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffUsr/Instructions_Start_Master");

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");

        //first subtask
        // in first subtask the master is always the helper / master client, so skip it
        /*if (_networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask == TaskPosDiffUsrPlates.SubTask.First)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[0])
                    .ToHex());

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[0], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[0], textMasterPlayer,
                textSlavePlayer);

            _networkManager.CurrentRoom.TaskPosDiffUsrPlates.EnableCurrentSubTaskCylinderHighlight();

            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.PositionConvergencesCompleted[
                    (int) TaskPosDiffUsrPlates.SubTask.First]);

            yield return ShowCompletionTextAndWaitNextSubTask(completionText,
                () => _networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask = TaskPosDiffUsrPlates.SubTask.Second,
                () => _networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask == TaskPosDiffUsrPlates.SubTask.Second);
        }*/

        //second subtask
        if (_networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask == TaskPosDiffUsrPlates.SubTask.Second)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[1])
                    .ToHex());

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[1], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[1], textMasterPlayer,
                textSlavePlayer);

            _networkManager.CurrentRoom.TaskPosDiffUsrPlates.EnableCurrentSubTaskCylinderHighlight();

            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.PositionConvergencesCompleted[
                    (int)TaskPosDiffUsrPlates.SubTask.Second]);

            yield return ShowCompletionTextAndWaitNextSubTask(completionText,
                () => _networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask = TaskPosDiffUsrPlates.SubTask.Third,
                () => _networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask == TaskPosDiffUsrPlates.SubTask.Third);
        }

        //third subtask
        if (_networkManager.CurrentRoom.TaskPosDiffUsrPlates.CurrentSubTask == TaskPosDiffUsrPlates.SubTask.Third)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[2])
                    .ToHex());

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[2], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskPosDiffUsrPlates.PlayerIndices[2], textMasterPlayer,
                textSlavePlayer);

            _networkManager.CurrentRoom.TaskPosDiffUsrPlates.EnableCurrentSubTaskCylinderHighlight();

            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.TaskPosDiffUsrPlates.PositionConvergencesCompleted[
                    (int)TaskPosDiffUsrPlates.SubTask.Third]);

            ShowCompletionTextAndNextSceneButton(completionText);
        }
    }


    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskPosDiffUsrPlates != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskPosDiffUsrPlates.Enable(enabled);
    }
}
