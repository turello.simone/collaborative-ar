﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TaskManipDiffUsr : ITaskManipulation
{
    public TaskManipDiffUsr(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task,
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffUsrTriangleFirst2Prefab.name, _networkManager.CurrentRoom.CandleHolderPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1.Reset();

            if (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffUsrTriangleFirst3Prefab.name, _networkManager.CurrentRoom.ChalicePose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2.Reset();



            if (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffUsrTriangleSecond1Prefab.name, _networkManager.CurrentRoom.CandleHolderPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1.Reset();

            if (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffUsrTriangleSecond3Prefab.name, _networkManager.CurrentRoom.ChalicePose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2.Reset();



            if (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffUsrTriangleThird1Prefab.name, _networkManager.CurrentRoom.CandleHolderPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1.Reset();

            if (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2 == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffUsrTriangleThird2Prefab.name, _networkManager.CurrentRoom.ChalicePose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2.Reset();



            if (_networkManager.CurrentRoom.TaskManipDiffUsrHoles == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskManipDiffUsrHolesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffUsrHoles.Reset();
        }
        else
            EnableObjects(true); 
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskManipDiffUsrHoles != null &&
                                         _networkManager.CurrentRoom.TaskManipDiffUsrHoles.IsInitialized);

        string textMasterPlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffUsr/Instructions_Start_Master");

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");

        ARManager.Instance.EnableHoleInOcclusionWithShadows(true, _networkManager.CurrentRoom.TaskManipDiffUsrHoles.OcclusionHoleRadius);

        //first subtask
        // in first subtask the master is always the helper / master client, so skip it
        /*if (_networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask == TaskManipDiffUsrHoles.SubTask.First)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[0])
                    .ToHex());

            _networkManager.CurrentRoom.TaskManipDiffUsrHoles.SetupSubTask();

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[0], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[0], textMasterPlayer,
                textSlavePlayer);

            yield return new WaitUntil(() =>
                (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled &&
                 (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1.OwnerPlayerIndex) == null ||
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1.IsEmbedded) &&
                 (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2.OwnerPlayerIndex) == null ||
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2.IsEmbedded)) ||

                (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled &&
                 (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1.IsEmbedded &&
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2.IsEmbedded))
                );

            yield return ShowCompletionTextAndWaitNextSubTask(completionText,
                () => _networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask = TaskManipDiffUsrHoles.SubTask.Second,
                () => _networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask == TaskManipDiffUsrHoles.SubTask.Second);
        }*/

        //second subtask
        if (_networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask == TaskManipDiffUsrHoles.SubTask.Second)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[1])
                    .ToHex());

            _networkManager.CurrentRoom.TaskManipDiffUsrHoles.SetupSubTask();

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[1], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[1], textMasterPlayer,
                textSlavePlayer);

            yield return new WaitUntil(() =>
                (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled &&
                 (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1.OwnerPlayerIndex) == null ||
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1.IsEmbedded) &&
                 (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2.OwnerPlayerIndex) == null ||
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2.IsEmbedded)) ||

                (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled &&
                 (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1.IsEmbedded &&
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2.IsEmbedded))
                );

            yield return ShowCompletionTextAndWaitNextSubTask(completionText,
                () => _networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask = TaskManipDiffUsrHoles.SubTask.Third,
                () => _networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask == TaskManipDiffUsrHoles.SubTask.Third);
        }

        //third subtask
        if (_networkManager.CurrentRoom.TaskManipDiffUsrHoles.CurrentSubTask == TaskManipDiffUsrHoles.SubTask.Third)
        {
            string textSlavePlayer = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffUsr/Instructions_Start_Slave");
            textSlavePlayer = textSlavePlayer.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());
            textSlavePlayer = textSlavePlayer.Replace("masterColor",
                _networkManager.GetPlayerColor(_networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[2])
                    .ToHex());

            _networkManager.CurrentRoom.TaskManipDiffUsrHoles.SetupSubTask();

            yield return ShowInstructionsAndCheckReady(
                _networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[2], textMasterPlayer, textSlavePlayer);
            ShowHint(_networkManager.CurrentRoom.TaskManipDiffUsrHoles.PlayerIndices[2], textMasterPlayer,
                textSlavePlayer);

            yield return new WaitUntil(() =>
                (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled &&
                 (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1.OwnerPlayerIndex) == null ||
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1.IsEmbedded) &&
                 (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2.OwnerPlayerIndex) == null ||
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2.IsEmbedded)) ||

                (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled &&
                 (_networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1.IsEmbedded &&
                  _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2.IsEmbedded))
                );

            ShowCompletionTextAndNextSceneButton(completionText);
        }
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleFirst1 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleFirst1.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleFirst2 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleFirst2.Enable(enabled);

        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleSecond1 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleSecond1.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleSecond2 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleSecond2.Enable(enabled);

        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleThird1 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleThird1.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleThird2 != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrTriangleThird2.Enable(enabled);

        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrHoles != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffUsrHoles.Enable(enabled);
    }
}
