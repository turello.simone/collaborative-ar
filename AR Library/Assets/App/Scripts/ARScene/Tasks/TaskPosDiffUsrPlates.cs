﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class TaskPosDiffUsrPlates : StaticObject
{
    public enum SubTask
    {
        First,
        Second,
        Third,
        Default
    }

    #region Attributes

    [Header("TaskPosSameUsrPlates properties")]
    [MustBeAssigned] public GameObject[] Cylinders;
    [MustBeAssigned] public Renderer[] BasesRenderers;
    public SubTask CurrentSubTask
    {
        get => (SubTask)_currentSubTask.Value;
        set => _currentSubTask.Value = (int)value;
    }
    private const string SUBTASKID = "SubTask";
    private SyncVarInt _currentSubTask;
    private bool[] _subTaskInitialized = { false, false, false };
    
    public bool IsInitialized
    {
        get { return _isInitialized; }
        private set { _isInitialized = value; }
    }
    private bool _isInitialized = false;

    [ReadOnly] public bool[] PositionConvergencesCompleted = { false, false, false };
    public float CompletionRequiredTime = 1f;

    private Color _cylinderHighlightCompleteColor = Color.green.BrightnessOffset(1f);

    [ReadOnly] public int[] PlayerIndices = { 1, 2, 3 };
    [SerializeField, ReadOnly] private int[,] _selectedCylinders = { { -1, -1, -1 }, { -1, -1, -1 }, { -1, -1, -1 } };
    private CustomOutline[] _basesOutlines;
    private Color _startBaseColor;
    private Color _startCylinderColor;
    private float _timer = 0;

    //DEBUG
    [SerializeField, ReadOnly] private int[] _selectedCylinders0 = {-1, -1, -1};
    [SerializeField, ReadOnly] private int[] _selectedCylinders1 = {-1, -1, -1};
    [SerializeField, ReadOnly] private int[] _selectedCylinders2 = {-1, -1, -1};

    private SyncVarBool _player1OnBase;
    private const string PLAYER1BOOL = "PLAYER1BOOL";
    private SyncVarBool _player2OnBase;
    private const string PLAYER2BOOL = "PLAYER2BOOL";
    private SyncVarBool _player3OnBase;
    private const string PLAYER3BOOL = "PLAYER3BOOL";

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _timer = 0;
        _startBaseColor = BasesRenderers[0].material.color;
        _startCylinderColor = Cylinders[0].GetComponent<Renderer>().material.color;

        _basesOutlines = new CustomOutline[BasesRenderers.Length];

        for (int i = 0; i < Cylinders.Length; i++)
        {
            Cylinders[i].GetComponent<Renderer>().enabled = false;

            if ((_basesOutlines[i] = BasesRenderers[i].GetComponent<CustomOutline>()) == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;
        }

        _player1OnBase = new SyncVarBool(false, _photonView, PLAYER1BOOL);
        _player2OnBase = new SyncVarBool(false, _photonView, PLAYER2BOOL);
        _player3OnBase = new SyncVarBool(false, _photonView, PLAYER3BOOL);
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _currentSubTask = new SyncVarInt((int) SubTask.Second, _photonView, SUBTASKID);

        _networkManager.CurrentRoom.TaskPosDiffUsrPlates = this;

        if (_networkManager.IsMasterClient &&
            Array.Exists(_selectedCylinders.OfType<int>().ToArray(), i => i == -1))
            SelectCylinders();
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < Cylinders.Length; i++)
        {
            if (_basesOutlines[i] == null)
                _basesOutlines[i] = BasesRenderers[i].gameObject.AddComponent<CustomOutline>();
            _basesOutlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _basesOutlines[i].OutlineWidth = 4f;
            _basesOutlines[i].FadeIn = true;
            _basesOutlines[i].enabled = false;

            Cylinders[i].GetComponent<Renderer>().enabled = false;
            Cylinders[i].GetComponent<Renderer>().material.DOKill();
            Cylinders[i].GetComponent<Renderer>().material.color = _startCylinderColor;

            BasesRenderers[i].material.DOKill();
            BasesRenderers[i].material.color = _startBaseColor;
        }

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < 3; j++)
            {
                _selectedCylinders[i, j] = -1;
            }
        }

        for (var i = 0; i < PositionConvergencesCompleted.Length; i++)
        {
            PositionConvergencesCompleted[i] = false;
            _subTaskInitialized[i] = false;
        }

        PlayerIndices[0] = 1;
        PlayerIndices[1] = 2;
        PlayerIndices[2] = 3;

        _timer = 0;

        IsInitialized = false;

        if (_networkManager.IsMasterClient)
        {
            _player1OnBase.Value = false;
            _player2OnBase.Value = false;
            _player3OnBase.Value = false;
            CurrentSubTask = SubTask.Second;
            SelectCylinders();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    private void Update()
    {
        //check if the scene is right
        SubSceneManager.RoomScene currentScene = _networkManager.CurrentRoom.RoomScene;
        if (_networkManager.NumberOfPlayersReady != _networkManager.NumberOfPlayers ||
            currentScene.Type != SubSceneManager.SubSceneType.Task ||
            currentScene.TaskInfo.Interaction != SubSceneManager.Interaction.Position ||
            currentScene.TaskInfo.InteractionFocus != SubSceneManager.InteractionFocus.DifferentObjects ||
            currentScene.TaskInfo.Input != SubSceneManager.Input.UserCommanded)
            return;

        int currentSubTask = (int)CurrentSubTask;

        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;
        SetSyncVarBool(playerIndex,
            !GameUIManager.Instance.InstructionsPanelEnabled && !PositionConvergencesCompleted[currentSubTask] &&
            GameUtils.IsPointWithinCollider(
                Cylinders[_selectedCylinders[currentSubTask, playerIndex - 1]].GetComponent<Collider>(),
                Camera.main.transform.position));

        if (_networkManager.IsMasterClient && CurrentSubTask != SubTask.Default &&
            !PositionConvergencesCompleted[currentSubTask] && _subTaskInitialized[currentSubTask])
        {
            if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
            {
                int masterPlayerIndex = PlayerIndices[currentSubTask];
                int slave1PlayerIndex = masterPlayerIndex == 1 ? 2 : 1;
                int slave2PlayerIndex = masterPlayerIndex == 3 ? 2 : 3;

                if (_networkManager.GetPlayerInfoFromPlayerIndex(slave1PlayerIndex) != null &&
                    GetSyncVarBool(slave1PlayerIndex).Value &&
                    _networkManager.GetPlayerInfoFromPlayerIndex(slave2PlayerIndex) != null &&
                    GetSyncVarBool(slave2PlayerIndex).Value)
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompleteCurrentPositionConvergence();
                }
                else _timer = 0;
            }
            else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
            {
                int masterPlayerIndex = PlayerIndices[currentSubTask];
                int slave1PlayerIndex = masterPlayerIndex == 1 ? 2 : 1;
                int slave2PlayerIndex = masterPlayerIndex == 3 ? 2 : 3;

                if ((_networkManager.GetPlayerInfoFromPlayerIndex(slave1PlayerIndex) != null ||
                     GetSyncVarBool(slave1PlayerIndex).Value) &&
                    (_networkManager.GetPlayerInfoFromPlayerIndex(slave2PlayerIndex) != null ||
                     GetSyncVarBool(slave2PlayerIndex).Value))
                {
                    _timer += Time.deltaTime;

                    if (_timer >= CompletionRequiredTime)
                        CompleteCurrentPositionConvergence();
                }
                else _timer = 0;
            }
        }
    }

    #endregion

    #region Public methods

    public void EnableCurrentSubTaskCylinderHighlight()
    {
        int currentSubTask = (int)CurrentSubTask;
        if (PositionConvergencesCompleted[currentSubTask] || _subTaskInitialized[currentSubTask]) return;

        _subTaskInitialized[currentSubTask] = true;

        //Reset cylinders and bases color
        for (var i = 0; i < Cylinders.Length; i++)
        {
            if(_basesOutlines[i] != null) _basesOutlines[i].enabled = false;

            Cylinders[i].GetComponent<Renderer>().enabled = false;
            Cylinders[i].GetComponent<Renderer>().material.DOKill();
            Cylinders[i].GetComponent<Renderer>().material.color = _startCylinderColor;

            BasesRenderers[i].material.DOKill();
            BasesRenderers[i].material.color = _startBaseColor;
        }

        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;

        if (playerIndex == PlayerIndices[currentSubTask])
        {
            InitializeSelectedCylinder(currentSubTask, 1);
            InitializeSelectedCylinder(currentSubTask, 2);
            InitializeSelectedCylinder(currentSubTask, 3);
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCylinders()
    {
        GameUtils.GenerateUniqueRandomNumbers(0, Cylinders.Length, out _selectedCylinders[0, 0],
            out _selectedCylinders[0, 1], out _selectedCylinders[0, 2]);
        GameUtils.GenerateUniqueRandomNumbers(0, Cylinders.Length, out _selectedCylinders[1, 0],
            out _selectedCylinders[1, 1], out _selectedCylinders[1, 2]);
        GameUtils.GenerateUniqueRandomNumbers(0, Cylinders.Length, out _selectedCylinders[2, 0],
            out _selectedCylinders[2, 1], out _selectedCylinders[2, 2]);

        //PlayerIndices = PlayerIndices.Shuffled();
        int masterClientIndex = _networkManager.GetMasterClient().PlayerIndex;
        int[] array = new[] { PlayerIndices[masterClientIndex == 1 ? 1 : 0], PlayerIndices[masterClientIndex == 3 ? 1 : 2] };
        array = array.Shuffled();

        PlayerIndices[0] = masterClientIndex;
        PlayerIndices[1] = array[0];
        PlayerIndices[2] = array[1];

        //Debug.Log("test " + PlayerIndices[0] + " " + PlayerIndices[1] + " " + PlayerIndices[2]);

        _photonView.RPC("SelectCylindersRPC", RpcTarget.AllBuffered, MatrixSerializer.Serialize(_selectedCylinders), PlayerIndices);
    }

    [PunRPC]
    public void SelectCylindersRPC(byte[] selectedCylinder, int[] playerIndices)
    {
        _selectedCylinders = MatrixSerializer.Deserialize<int>(selectedCylinder);
        PlayerIndices = playerIndices;

        //DEBUG
        _selectedCylinders0[0] = _selectedCylinders[0, 0];
        _selectedCylinders0[1] = _selectedCylinders[0, 1];
        _selectedCylinders0[2] = _selectedCylinders[0, 2];
        _selectedCylinders1[0] = _selectedCylinders[1, 0];
        _selectedCylinders1[1] = _selectedCylinders[1, 1];
        _selectedCylinders1[2] = _selectedCylinders[1, 2];
        _selectedCylinders2[0] = _selectedCylinders[2, 0];
        _selectedCylinders2[1] = _selectedCylinders[2, 1];
        _selectedCylinders2[2] = _selectedCylinders[2, 2];

        IsInitialized = true;
    }

    private void CompleteCurrentPositionConvergence()
    {
        _photonView.RPC("CompleteCurrentPositionConvergenceRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void CompleteCurrentPositionConvergenceRPC()
    {
        int currentSubTask = (int) CurrentSubTask;

        PositionConvergencesCompleted[currentSubTask] = true;

        if (_networkManager.IsMasterClient)
            OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer,
                OnlineLogger.EventType.PosCompleted);

        if (PlayerIndices[currentSubTask] != 1)
        {
            if (_basesOutlines[_selectedCylinders[currentSubTask, 0]] != null)
                _basesOutlines[_selectedCylinders[currentSubTask, 0]].FadeOutAndDisable();

            Cylinders[_selectedCylinders[currentSubTask, 0]].GetComponent<Renderer>().enabled = true;
            Cylinders[_selectedCylinders[currentSubTask, 0]].GetComponent<Renderer>().material
                .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f);

            BasesRenderers[_selectedCylinders[currentSubTask, 0]].material.DOColor(_cylinderHighlightCompleteColor, 0.5f);
        }

        if (PlayerIndices[currentSubTask] != 2)
        {
            if (_basesOutlines[_selectedCylinders[currentSubTask, 1]] != null)
                _basesOutlines[_selectedCylinders[currentSubTask, 1]].FadeOutAndDisable();

            Cylinders[_selectedCylinders[currentSubTask, 1]].GetComponent<Renderer>().enabled = true;
            Cylinders[_selectedCylinders[currentSubTask, 1]].GetComponent<Renderer>().material
                .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f);

            BasesRenderers[_selectedCylinders[currentSubTask, 1]].material.DOColor(_cylinderHighlightCompleteColor, 0.5f);
        }

        if (PlayerIndices[currentSubTask] != 3)
        {
            if (_basesOutlines[_selectedCylinders[currentSubTask, 2]] != null)
                _basesOutlines[_selectedCylinders[currentSubTask, 2]].FadeOutAndDisable();

            Cylinders[_selectedCylinders[currentSubTask, 2]].GetComponent<Renderer>().enabled = true;
            Cylinders[_selectedCylinders[currentSubTask, 2]].GetComponent<Renderer>().material
                .DOColor(_cylinderHighlightCompleteColor.WithAlphaSetTo(_startCylinderColor.a), 0.5f);

            BasesRenderers[_selectedCylinders[currentSubTask, 2]].material.DOColor(_cylinderHighlightCompleteColor, 0.5f);
        }

        if (_networkManager.IsMasterClient)
        {
            _player1OnBase.Value = false;
            _player2OnBase.Value = false;
            _player3OnBase.Value = false;
        }
    }

    private void InitializeSelectedCylinder(int subTaskIndex, int playerIndex)
    {
        if (PlayerIndices[subTaskIndex] != playerIndex)
        {
            Color color = _networkManager.GetPlayerColor(playerIndex);
            int selectedCylinderIndex = _selectedCylinders[subTaskIndex, playerIndex - 1];

            if (_basesOutlines[selectedCylinderIndex] != null)
            {
                _basesOutlines[selectedCylinderIndex].OutlineColor = color;
                _basesOutlines[selectedCylinderIndex].enabled = true;
            }

            Cylinders[selectedCylinderIndex].GetComponent<Renderer>().enabled = true;
            Cylinders[selectedCylinderIndex].GetComponent<Renderer>().material.color =
                color.BrightnessOffset(0.4f).WithAlphaSetTo(_startCylinderColor.a);

            BasesRenderers[selectedCylinderIndex].material.color = color.BrightnessOffset(0.2f);
        }
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        switch (CurrentSubTask)
        {
            case SubTask.First:
                if ((PlayerIndices[0] != 1 && gameObj == Cylinders[_selectedCylinders[0, 0]]) ||
                    (PlayerIndices[0] != 2 && gameObj == Cylinders[_selectedCylinders[0, 1]]) ||
                    (PlayerIndices[0] != 3 && gameObj == Cylinders[_selectedCylinders[0, 2]]))
                {
                    return true;
                }
                break;
            case SubTask.Second:
                if ((PlayerIndices[1] != 1 && gameObj == Cylinders[_selectedCylinders[1, 0]]) ||
                    (PlayerIndices[1] != 2 && gameObj == Cylinders[_selectedCylinders[1, 1]]) ||
                    (PlayerIndices[1] != 3 && gameObj == Cylinders[_selectedCylinders[1, 2]]))
                {
                    return true;
                }
                break;
            case SubTask.Third:
                if ((PlayerIndices[2] != 1 && gameObj == Cylinders[_selectedCylinders[2, 0]]) ||
                    (PlayerIndices[2] != 2 && gameObj == Cylinders[_selectedCylinders[2, 1]]) ||
                    (PlayerIndices[2] != 3 && gameObj == Cylinders[_selectedCylinders[2, 2]]))
                {
                    return true;
                }
                break;
        }
        return false;
    }

    private SyncVarBool GetSyncVarBool(int playerIndex)
    {
        switch (playerIndex)
        {
            case 1:
                return _player1OnBase;
            case 2:
                return _player2OnBase;
            case 3:
                return _player3OnBase;
        }

        return null;
    }

    private void SetSyncVarBool(int playerIndex, bool value)
    {
        switch (playerIndex)
        {
            case 1:
                if (_player1OnBase.Value != value) _player1OnBase.Value = value;
                break;
            case 2:
                if (_player2OnBase.Value != value) _player2OnBase.Value = value;
                break;
            case 3:
                if (_player3OnBase.Value != value) _player3OnBase.Value = value;
                break;
        }
    }
}
