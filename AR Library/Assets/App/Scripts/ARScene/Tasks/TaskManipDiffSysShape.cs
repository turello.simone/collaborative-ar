﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TaskManipDiffSysShape : DynamicObject
{
    public enum ShapeType
    {
        Rectangle,
        Square,
        Triangle
    }

    #region Properties

    [Header("TaskManipDiffSysShape properties")]
    public ShapeType Shape;
    [MustBeAssigned] public GameObject Gear;
    public float GearAdjustmentSpeed = 100f;
    [MustBeAssigned] public Renderer BodyRenderer;
    [MustBeAssigned] public Renderer NumberRenderer;

    [ReadOnly] public int Number = 0;
    public bool IsEmbedded { get; private set; }

    public float DistanceThreshold = 0.05f;
    public float RotationThreshold = 20f;

    public int OwnerPlayerIndex
    {
        get { return _ownerPlayerIndex; }
        set
        {
            if (value == _ownerPlayerIndex) return;

            _ownerPlayerIndex = value;

            if (_ownerPlayerIndex != 0)
                BodyRenderer.material.color = _networkManager.GetPlayerColor(_ownerPlayerIndex).BrightnessOffset(0.4f);
            else
                BodyRenderer.material.color = _startColor;
        }
    }
    [SerializeField, ReadOnly] private int _ownerPlayerIndex = 0;

    private PhotonView _gearPhotonView;
    private bool _isGearInteracted;
    private const float GearAngleStep = 360f / 10f;
    private Sequence _embedSequence;
    private Color _startColor;

    #endregion

    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _gearPhotonView = Gear.GetComponent<PhotonView>();
        IsEmbedded = false;
        _isGearInteracted = false;
        _startColor = BodyRenderer.material.color;
        ChangeRotationOnGrab = false;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        switch (Shape)
        {
            case ShapeType.Rectangle:
                _networkManager.CurrentRoom.TaskManipDiffSysRectangle = this;
                break;
            case ShapeType.Square:
                _networkManager.CurrentRoom.TaskManipDiffSysSquare = this;
                break;
            case ShapeType.Triangle:
                _networkManager.CurrentRoom.TaskManipDiffSysTriangle = this;
                break;
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Number = 0;

        Gear.transform.localRotation = Quaternion.identity;
        NumberRenderer.material.SetTextureOffset("_MainTex", Vector2.zero);

        IsEmbedded = false;
        _isGearInteracted = false;

        OwnerPlayerIndex = 0;

        m_PositionModel.SynchronizeEnabled = true;
        m_RotationModel.SynchronizeEnabled = true;

        _embedSequence.Kill();
    }

    protected override void Update()
    {
        base.Update();

        if (!IsEmbedded)
        {
            //Update Number
            float offset = Gear.transform.localRotation.eulerAngles.z / 360f;
            NumberRenderer.material.SetTextureOffset("_MainTex", new Vector2(0, offset));

            if (Owner == _networkManager.LocalPlayer)
            {
                float gearRot =
                    GameUtils.GetSignedAngle(Quaternion.identity, Gear.transform.localRotation, Vector3.forward); //angle in range (-180;180)
                if (gearRot < 0) gearRot += 360; //angle in range (0, 360)

                int div = Mathf.RoundToInt(gearRot / GearAngleStep);
                Number = (10 - div) % 10;

                //Auto-snap to closest number
                if (!_isGearInteracted)
                    Gear.transform.localRotation = Quaternion.RotateTowards(Gear.transform.localRotation,
                        Quaternion.Euler(0, 0, GearAngleStep * div), GearAdjustmentSpeed * Time.deltaTime);
            }
        }
    }

    #endregion


    #region Public methods 

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (Owner != _networkManager.LocalPlayer) return;

        if (isPressed)
        {
            if (interactiveObject == Gear)
            {
                _isGearInteracted = true;
            }
        }
        else
        {
            _isGearInteracted = false;
        }
    }

    public override void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        if (Owner == _networkManager.LocalPlayer && interactiveObject == Gear)
        {
            Vector2 gearCenterScreenPos = Camera.main.WorldToScreenPoint(Gear.transform.position);
            Vector2 startScreenPos = screenPos - deltaMovement;
            Vector2 destScreenPos = screenPos;

            Vector2 startVector = startScreenPos - gearCenterScreenPos;
            Vector2 destVector = destScreenPos - gearCenterScreenPos;

            float angle = Vector2.SignedAngle(startVector, destVector);

            Gear.transform.localRotation *= Quaternion.Euler(0, 0, -2 * angle);
        }
    }

    public override void Place(Vector3 restPosition, Quaternion restRotation)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        PlayerInfo oldOwner = Owner;

        base.Place(restPosition, restRotation);

        //Check embedding
        if (oldOwner.PlayerIndex == OwnerPlayerIndex)
        {
            TaskManipDiffSysHoles holes = _networkManager.CurrentRoom.TaskManipDiffSysHoles;
            Vector3 referencePosition = Vector3.zero;
            Quaternion referenceRotation = Quaternion.identity;
            int referenceNumber = -1;

            switch (Shape)
            {
                case ShapeType.Rectangle:
                    referencePosition = holes.HoleRectangle.transform.position;
                    referenceRotation = holes.HoleRectangle.transform.rotation;
                    referenceNumber = holes.RectangleNumber;
                    break;
                case ShapeType.Square:
                    referencePosition = holes.HoleSquare.transform.position;
                    referenceRotation = holes.HoleSquare.transform.rotation;
                    referenceNumber = holes.SquareNumber;
                    break;
                case ShapeType.Triangle:
                    referencePosition = holes.HoleTriangle.transform.position;
                    referenceRotation = holes.HoleTriangle.transform.rotation;
                    referenceNumber = holes.TriangleNumber;
                    break;
            }

            /*Debug.LogFormat("Shape: number {0}, distance {1}, angle {2}", Number == referenceNumber,
                Vector3.Distance(transform.position, referencePosition),
                Quaternion.Angle(transform.rotation, referenceRotation));*/

            if (Number == referenceNumber &&
                Vector3.Distance(transform.position, referencePosition) < DistanceThreshold &&
                Quaternion.Angle(transform.rotation, referenceRotation) < RotationThreshold)
            {
                Embed();
            }
        }
    }

    [PunRPC]
    public override void GrabRPC(Vector3 startPosition, Quaternion startRotation, PhotonMessageInfo info)
    {
        base.GrabRPC(startPosition, startRotation, info);

        if (Owner == _networkManager.LocalPlayer)
        {
            _gearPhotonView.RequestOwnership();
        }
    }

    #endregion

    #region Private methods / Photon RPCs

    private void Embed()
    {
        OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, OnlineLogger.EventType.ManipCompleted);
        _photonView.RPC("EmbedRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void EmbedRPC()
    {
        Debug.Log("EmbedRPC called");

        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

        IsEmbedded = true;

        m_PositionModel.SynchronizeEnabled = false;
        m_RotationModel.SynchronizeEnabled = false;

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;
        this.transform.parent = _networkManager.CurrentRoom.TaskManipDiffSysHoles.transform;

        TaskManipDiffSysHoles holes = _networkManager.CurrentRoom.TaskManipDiffSysHoles;
        Vector3 referencePosition = Vector3.zero;
        Quaternion referenceRotation = Quaternion.identity;
        Vector3 referenceHolePosition = Vector3.zero;

        switch (Shape)
        {
            case ShapeType.Rectangle:
                referencePosition = holes.HoleRectangle.transform.position;
                referenceRotation = holes.HoleRectangle.transform.rotation;
                referenceHolePosition = holes.HoleRectangle.transform.position - holes.HoleRectangle.transform.up * 0.04f;
                break;
            case ShapeType.Square:
                referencePosition = holes.HoleSquare.transform.position;
                referenceRotation = holes.HoleSquare.transform.rotation;
                referenceHolePosition = holes.HoleSquare.transform.position - holes.HoleSquare.transform.up * 0.04f;
                break;
            case ShapeType.Triangle:
                referencePosition = holes.HoleTriangle.transform.position;
                referenceRotation = holes.HoleTriangle.transform.rotation;
                referenceHolePosition = holes.HoleTriangle.transform.position - holes.HoleTriangle.transform.up * 0.04f;
                break;
        }

        _embedSequence = DOTween.Sequence();
        _embedSequence
            .Append(transform.DOMove(referencePosition, 0.5f))
            .Insert(0, transform.DORotate(referenceRotation.eulerAngles, 0.5f))
            .InsertCallback(0, () => this.gameObject.CollidersEnabled(false))
            .AppendInterval(0.2f)
            .Append(transform.DOMove(referenceHolePosition, 0.5f));
    }

    #endregion
}
