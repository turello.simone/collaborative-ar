﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TaskGazeDiffUsrCubes : StaticObject
{
    public enum SubTask
    {
        Default,
        First,
        Second,
        Third
    }

    #region Attributes

    [Header("TaskGazeDiffUsrCubes properties")]
    [MustBeAssigned] public GameObject[] Cubes;

    public SubTask CurrentSubTask
    {
        get => (SubTask)_currentSubTask.Value;
        set => _currentSubTask.Value = (int)value;
    }
    private const string SUBTASKID = "SubTask";
    private SyncVarInt _currentSubTask;

    public bool IsInitialized { get; private set; } = false;

    private Color _cubeHighlightCompleteColor = Color.green.BrightnessOffset(1f);

    [ReadOnly] public int[] PlayerIndices = { 1, 2, 3 };
    [SerializeField, ReadOnly] private int[,] _selectedCubes = { {-1, -1, -1}, {-1, -1, -1}, {-1, -1, -1}};
    private GameObject[,] _gazeConvergenceFocalPoints = new GameObject[3, 3];
    private CustomOutline[] _outlines;
    private Color _startColor;

    //DEBUG
    [SerializeField, ReadOnly] private int[] _selectedCubes0 = { -1, -1, -1 };
    [SerializeField, ReadOnly] private int[] _selectedCubes1 = { -1, -1, -1 };
    [SerializeField, ReadOnly] private int[] _selectedCubes2 = { -1, -1, -1 };

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _startColor = Cubes[0].GetComponent<Renderer>().material.color;

        _outlines = new CustomOutline[Cubes.Length];
        for (int i = 0; i < Cubes.Length; i++)
        {
            if ((_outlines[i] = Cubes[i].GetComponent<CustomOutline>()) == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _currentSubTask = new SyncVarInt((int)SubTask.Second, _photonView, SUBTASKID);

        _networkManager.CurrentRoom.TaskGazeDiffUsrCubes = this;

        if (_networkManager.IsMasterClient && Array.Exists(_selectedCubes.OfType<int>().ToArray(), i => i == -1))
            SelectCubes();
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        for (var i = 0; i < _outlines.Length; i++)
        {
            if (_outlines[i] == null)
                _outlines[i] = Cubes[i].AddComponent<CustomOutline>();
            _outlines[i].OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outlines[i].OutlineWidth = 4f;
            _outlines[i].FadeIn = true;
            _outlines[i].enabled = false;
        }

        for (var i = 0; i < Cubes.Length; i++)
        {
            Cubes[i].GetComponent<Renderer>().material.DOKill();
            Cubes[i].GetComponent<Renderer>().material.color = _startColor;
        }

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < 3; j++)
            {
                if (_gazeConvergenceFocalPoints[i, j] != null)
                {
                    Destroy(_gazeConvergenceFocalPoints[i, j]);
                    _gazeConvergenceFocalPoints[i, j] = null;
                }
            }
        }

        for (var i = 0; i < 3; i++)
        {
            for (var j = 0; j < 3; j++)
            {
                _selectedCubes[i,j] = -1;
            }
        }

        //DEBUG
        _selectedCubes0[0] = -1;
        _selectedCubes0[1] = -1;
        _selectedCubes0[2] = -1;
        _selectedCubes1[0] = -1;
        _selectedCubes1[1] = -1;
        _selectedCubes1[2] = -1;
        _selectedCubes2[0] = -1;
        _selectedCubes2[1] = -1;
        _selectedCubes2[2] = -1;

        PlayerIndices[0] = 1;
        PlayerIndices[1] = 2;
        PlayerIndices[2] = 3;

        IsInitialized = false;

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);
        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);
        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);
            CurrentSubTask = SubTask.Second;
            SelectCubes();
        }
    }

    public override void Enable(bool objEnabled)
    {
        base.Enable(objEnabled);
        if (!objEnabled) IsInitialized = false;
    }

    #endregion

    #region Public methods

    public void EnableCubesHighlight(GazeConvergenceFocalPoint.Type type)
    {
        //Reset cube material color
        foreach (var cube in Cubes)
        {
            cube.GetComponent<Renderer>().material.color = _startColor;
        }

        int playerIndex = _networkManager.LocalPlayer.PlayerIndex;
        if (playerIndex == 0) return;

        int focalPointIndex = -1;
        switch (type)
        {
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1:
                focalPointIndex = 0;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2:
                focalPointIndex = 1;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3:
                focalPointIndex = 2;
                break;
        }
        if (focalPointIndex == -1) return;

        if (playerIndex == PlayerIndices[focalPointIndex])
        {
            if (playerIndex != 1)
            {
                Cubes[_selectedCubes[focalPointIndex, 0]].GetComponent<Renderer>().material.color =
                    _networkManager.GetPlayerColor(1).Lighter().Lighter();
                if (_outlines[_selectedCubes[focalPointIndex, 0]] != null)
                {
                    _outlines[_selectedCubes[focalPointIndex, 0]].OutlineColor = _networkManager.GetPlayerColor(1);
                    _outlines[_selectedCubes[focalPointIndex, 0]].enabled = true;
                }
            }

            if (playerIndex != 2)
            {
                Cubes[_selectedCubes[focalPointIndex, 1]].GetComponent<Renderer>().material.color =
                    _networkManager.GetPlayerColor(2).Lighter().Lighter();
                if (_outlines[_selectedCubes[focalPointIndex, 1]] != null)
                {
                    _outlines[_selectedCubes[focalPointIndex, 1]].OutlineColor = _networkManager.GetPlayerColor(2);
                    _outlines[_selectedCubes[focalPointIndex, 1]].enabled = true;
                }
            }

            if (playerIndex != 3)
            {
                Cubes[_selectedCubes[focalPointIndex, 2]].GetComponent<Renderer>().material.color =
                    _networkManager.GetPlayerColor(3).Lighter().Lighter();
                if (_outlines[_selectedCubes[focalPointIndex, 2]] != null)
                {
                    _outlines[_selectedCubes[focalPointIndex, 2]].OutlineColor = _networkManager.GetPlayerColor(3);
                    _outlines[_selectedCubes[focalPointIndex, 2]].enabled = true;
                }
            }
        }
    }

    public void DisableCubesHighlight(GazeConvergenceFocalPoint.Type type)
    {
        int focalPointIndex = -1;
        switch (type)
        {
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1:
                focalPointIndex = 0;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2:
                focalPointIndex = 1;
                break;
            case GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3:
                focalPointIndex = 2;
                break;
        }
        if (focalPointIndex == -1) return;

        if (PlayerIndices[focalPointIndex] != 1)
        {
            Cubes[_selectedCubes[focalPointIndex, 0]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
            if (_outlines[_selectedCubes[focalPointIndex, 0]] != null)
                _outlines[_selectedCubes[focalPointIndex, 0]].FadeOutAndDisable();
        }

        if (PlayerIndices[focalPointIndex] != 2)
        {
            Cubes[_selectedCubes[focalPointIndex, 1]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
            if (_outlines[_selectedCubes[focalPointIndex, 1]] != null)
                _outlines[_selectedCubes[focalPointIndex, 1]].FadeOutAndDisable();
        }

        if (PlayerIndices[focalPointIndex] != 3)
        {
            Cubes[_selectedCubes[focalPointIndex, 2]].GetComponent<Renderer>().material.DOColor(_cubeHighlightCompleteColor, 0.5f);
            if (_outlines[_selectedCubes[focalPointIndex, 2]] != null)
                _outlines[_selectedCubes[focalPointIndex, 2]].FadeOutAndDisable();
        }


        if (_gazeConvergenceFocalPoints[focalPointIndex, 0] != null)
        {
            Destroy(_gazeConvergenceFocalPoints[focalPointIndex, 0]);
            _gazeConvergenceFocalPoints[focalPointIndex, 0] = null;
        }

        if (_gazeConvergenceFocalPoints[focalPointIndex, 1] != null)
        {
            Destroy(_gazeConvergenceFocalPoints[focalPointIndex, 1]);
            _gazeConvergenceFocalPoints[focalPointIndex, 1] = null;
        }

        if (_gazeConvergenceFocalPoints[focalPointIndex, 2] != null)
        {
            Destroy(_gazeConvergenceFocalPoints[focalPointIndex, 2]);
            _gazeConvergenceFocalPoints[focalPointIndex, 2] = null;
        }
    }

    #endregion


    #region Private methods / Photon RPCs

    private void SelectCubes()
    {
        GameUtils.GenerateUniqueRandomNumbers(0, Cubes.Length, out _selectedCubes[0, 0], out _selectedCubes[0, 1], out _selectedCubes[0, 2]);
        GameUtils.GenerateUniqueRandomNumbers(0, Cubes.Length, out _selectedCubes[1, 0], out _selectedCubes[1, 1], out _selectedCubes[1, 2]);
        GameUtils.GenerateUniqueRandomNumbers(0, Cubes.Length, out _selectedCubes[2, 0], out _selectedCubes[2, 1], out _selectedCubes[2, 2]);

        int masterClientIndex = _networkManager.GetMasterClient().PlayerIndex;
        int[] array = new[] {PlayerIndices[masterClientIndex == 1 ? 1 : 0], PlayerIndices[masterClientIndex == 3 ? 1 : 2]};
        array = array.Shuffled();

        PlayerIndices[0] = masterClientIndex;
        PlayerIndices[1] = array[0];
        PlayerIndices[2] = array[1];

        _photonView.RPC("SelectCubesRPC", RpcTarget.AllBuffered, MatrixSerializer.Serialize(_selectedCubes), PlayerIndices);
    }

    [PunRPC]
    public void SelectCubesRPC(byte[] selectedCubes, int[] playerIndices)
    {
        Debug.Log("SelectCubesRPC: " + playerIndices);
        _selectedCubes = MatrixSerializer.Deserialize<int>(selectedCubes);

        //DEBUG
        _selectedCubes0[0] = _selectedCubes[0, 0];
        _selectedCubes0[1] = _selectedCubes[0, 1];
        _selectedCubes0[2] = _selectedCubes[0, 2];
        _selectedCubes1[0] = _selectedCubes[1, 0];
        _selectedCubes1[1] = _selectedCubes[1, 1];
        _selectedCubes1[2] = _selectedCubes[1, 2];
        _selectedCubes2[0] = _selectedCubes[2, 0];
        _selectedCubes2[1] = _selectedCubes[2, 1];
        _selectedCubes2[2] = _selectedCubes[2, 2];

        PlayerIndices = playerIndices;

        //InitializeFocalPoints(0, GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr1);
        InitializeFocalPoints(1, GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr2);
        InitializeFocalPoints(2, GazeConvergenceFocalPoint.Type.TaskGazeDiffUsr3);

        IsInitialized = true;
    }

    private void InitializeFocalPoints(int focalPointIndex, GazeConvergenceFocalPoint.Type type)
    {
        GameObject gazeConvergenceObj1 = PlayerIndices[focalPointIndex] == 1 ? null : 
            FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                Cubes[_selectedCubes[focalPointIndex, 0]].transform.position, 0.1f, this.transform);
        GameObject gazeConvergenceObj2 = PlayerIndices[focalPointIndex] == 2 ? null :
            FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                Cubes[_selectedCubes[focalPointIndex, 1]].transform.position, 0.1f, this.transform);
        GameObject gazeConvergenceObj3 = PlayerIndices[focalPointIndex] == 3 ? null :
            FocalPointManager.Instance.CreateGazeConvergenceSphericalCollider(
                Cubes[_selectedCubes[focalPointIndex, 2]].transform.position, 0.1f, this.transform);

        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(
            type, gazeConvergenceObj1, gazeConvergenceObj2, gazeConvergenceObj3, 
            outlineEnabled: false, iconEnabled: false, arrowEnabled: false);

        _gazeConvergenceFocalPoints[focalPointIndex, 0] = gazeConvergenceObj1;
        _gazeConvergenceFocalPoints[focalPointIndex, 1] = gazeConvergenceObj2;
        _gazeConvergenceFocalPoints[focalPointIndex, 2] = gazeConvergenceObj3;
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        switch (CurrentSubTask)
        {
            case SubTask.First:
                if ((PlayerIndices[0] != 1 && gameObj == Cubes[_selectedCubes[0, 0]]) ||
                    (PlayerIndices[0] != 2 && gameObj == Cubes[_selectedCubes[0, 1]]) ||
                    (PlayerIndices[0] != 3 && gameObj == Cubes[_selectedCubes[0, 2]]))
                {
                    return true;
                }
                break;
            case SubTask.Second:
                if ((PlayerIndices[1] != 1 && gameObj == Cubes[_selectedCubes[1, 0]]) ||
                    (PlayerIndices[1] != 2 && gameObj == Cubes[_selectedCubes[1, 1]]) ||
                    (PlayerIndices[1] != 3 && gameObj == Cubes[_selectedCubes[1, 2]]))
                {
                    return true;
                }
                break;
            case SubTask.Third:
                if ((PlayerIndices[2] != 1 && gameObj == Cubes[_selectedCubes[2, 0]]) ||
                    (PlayerIndices[2] != 2 && gameObj == Cubes[_selectedCubes[2, 1]]) ||
                    (PlayerIndices[2] != 3 && gameObj == Cubes[_selectedCubes[2, 2]]))
                {
                    return true;
                }
                break;
        }
        return false;
    }
}
