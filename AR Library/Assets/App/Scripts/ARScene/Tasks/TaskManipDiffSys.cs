﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;

public class TaskManipDiffSys : ITaskManipulation
{
    public TaskManipDiffSys(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task, 
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskManipDiffSysRectangle == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffSysRectanglePrefab.name, _networkManager.CurrentRoom.CandleHolderPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffSysRectangle.Reset();

            if (_networkManager.CurrentRoom.TaskManipDiffSysSquare == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffSysSquarePrefab.name, _networkManager.CurrentRoom.ChalicePose);
            else
                _networkManager.CurrentRoom.TaskManipDiffSysSquare.Reset();

            if (_networkManager.CurrentRoom.TaskManipDiffSysTriangle == null)
                _networkManager.SpawnDynamicObject(_gameManager.TaskManipDiffSysTrianglePrefab.name, _networkManager.CurrentRoom.SkullPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffSysTriangle.Reset();

            if (_networkManager.CurrentRoom.TaskManipDiffSysHoles == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskManipDiffSysHolesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TaskManipDiffSysHoles.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskManipDiffSysHoles != null &&
                                         _networkManager.CurrentRoom.TaskManipDiffSysHoles.IsInitialized);

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffSys/Instructions_Start");
        instructions = instructions.Replace("playerColor", _networkManager.LocalPlayer.Color.ToHex());

        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        ARManager.Instance.EnableHoleInOcclusionWithShadows(true, _networkManager.CurrentRoom.TaskManipDiffSysHoles.OcclusionHoleRadius);

        yield return new WaitUntil(() =>
            (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled &&
             (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffSysRectangle.OwnerPlayerIndex) == null ||
              _networkManager.CurrentRoom.TaskManipDiffSysRectangle.IsEmbedded) &&
             (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffSysSquare.OwnerPlayerIndex) == null ||
              _networkManager.CurrentRoom.TaskManipDiffSysSquare.IsEmbedded) &&
             (_networkManager.GetPlayerInfoFromPlayerIndex(_networkManager.CurrentRoom.TaskManipDiffSysTriangle.OwnerPlayerIndex) == null ||
              _networkManager.CurrentRoom.TaskManipDiffSysTriangle.IsEmbedded)) ||

            (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled &&
             (_networkManager.CurrentRoom.TaskManipDiffSysRectangle.IsEmbedded &&
              _networkManager.CurrentRoom.TaskManipDiffSysSquare.IsEmbedded &&
              _networkManager.CurrentRoom.TaskManipDiffSysTriangle.IsEmbedded))
            );

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public override void Destroy()
    {
        base.Destroy();
        if (ARManager.Instance != null) ARManager.Instance.EnableHoleInOcclusionWithShadows(false);
    }


    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysRectangle != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysRectangle.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysSquare != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysSquare.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysTriangle != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysTriangle.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysHoles != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskManipDiffSysHoles.Enable(enabled);
    }
}
