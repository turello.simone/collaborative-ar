﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskGazeSameSys : ITaskGaze
{
    public TaskGazeSameSys(string sceneName, SubSceneManager.TaskInfo task, bool initialized) : base(sceneName, task,
        initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TaskGazeSameSysCubes == null)
                _networkManager.SpawnStaticObject(_gameManager.TaskGazeSameSysCubesPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TaskGazeSameSysCubes.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();
        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskGazeSameSysCubes != null &&
                                         _networkManager.CurrentRoom.TaskGazeSameSysCubes.IsInitialized);

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeSameSys/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);
        ShowHint(instructions);

        yield return new WaitUntil(() => _networkManager.CurrentRoom.TaskGazeSameSysCubes.CurrentRepetition ==
                                         _networkManager.CurrentRoom.TaskGazeSameSysCubes.Repetitions);

        /*bool completed;
        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameSys,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameSys);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.TaskGazeSameSys,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.TaskGazeSameSys);
        }

        _networkManager.CurrentRoom.TaskGazeSameSysCubes.DisableCubeHighlight();*/
        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }


    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TaskGazeSameSysCubes != null)
            PhotonNetworkManager.Instance.CurrentRoom.TaskGazeSameSysCubes.Enable(enabled);
    }
}
