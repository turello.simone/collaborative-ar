﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TaskManipDiffUsrTriangle : DynamicObject
{
    public enum Typology
    {
        First1,
        First2,
        Second1,
        Second2,
        Third1,
        Third2
    }


    #region Properties

    [Header("TaskManipDiffUsrTriangle properties")]
    public Typology Type;
    [MustBeAssigned] public GameObject Gear;
    public float GearAdjustmentSpeed = 100f;
    [MustBeAssigned] public Renderer BodyRenderer;
    [MustBeAssigned] public Renderer NumberRenderer;

    [ReadOnly] public int Number = 0;
    public bool IsEmbedded { get; private set; }

    public float DistanceThreshold = 0.05f;
    public float RotationThreshold = 20f;
    //public float EmbeddingRequiredTime = 1f;

    [ReadOnly] public int OwnerPlayerIndex = 0;

    [SerializeField, ReadOnly] private int _referenceHoleIndex;
    [SerializeField, ReadOnly] private int _referenceNumber;
    private PhotonView _gearPhotonView;
    private bool _isGearInteracted;
    private const float GearAngleStep = 360f / 10f;
    private Sequence _embedSequence;
    private Color _startColor;

    #endregion

    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _gearPhotonView = Gear.GetComponent<PhotonView>();
        IsEmbedded = false;
        _isGearInteracted = false;
        _startColor = BodyRenderer.material.color;
        ChangeRotationOnGrab = false;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        switch (Type)
        {
            case Typology.First1:
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst1 = this;
                break;
            case Typology.First2:
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleFirst2 = this;
                break;
            case Typology.Second1:
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond1 = this;
                break;
            case Typology.Second2:
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleSecond2 = this;
                break;
            case Typology.Third1:
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird1 = this;
                break;
            case Typology.Third2:
                _networkManager.CurrentRoom.TaskManipDiffUsrTriangleThird2 = this;
                break;
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Number = 0;

        OwnerPlayerIndex = 0;
        _referenceHoleIndex = 0;
        _referenceNumber = 0;

        Gear.transform.localRotation = Quaternion.identity;
        NumberRenderer.material.SetTextureOffset("_MainTex", Vector2.zero);
        BodyRenderer.material.color = _startColor;

        IsEmbedded = false;
        _isGearInteracted = false;

        m_PositionModel.SynchronizeEnabled = true;
        m_RotationModel.SynchronizeEnabled = true;

        _embedSequence.Kill();
    }

    protected override void Update()
    {
        base.Update();

        if (!IsEmbedded)
        {
            //Update Number
            float offset = Gear.transform.localRotation.eulerAngles.z / 360f;
            NumberRenderer.material.SetTextureOffset("_MainTex", new Vector2(0, offset));

            if (Owner == _networkManager.LocalPlayer)
            {
                float gearRot =
                    GameUtils.GetSignedAngle(Quaternion.identity, Gear.transform.localRotation, Vector3.forward); //angle in range (-180;180)
                if (gearRot < 0) gearRot += 360; //angle in range (0, 360)

                int div = Mathf.RoundToInt(gearRot / GearAngleStep);
                Number = (10 - div) % 10;

                //Auto-snap to closest number
                if (!_isGearInteracted)
                    Gear.transform.localRotation = Quaternion.RotateTowards(Gear.transform.localRotation,
                        Quaternion.Euler(0, 0, GearAngleStep * div), GearAdjustmentSpeed * Time.deltaTime);
            }
        }
    }

    #endregion


    #region Public methods 

    public void InitializeTargetHole(int ownerPlayerIndex, int referenceHoleIndex, int referenceNumber)
    {
        OwnerPlayerIndex = ownerPlayerIndex;
        _referenceHoleIndex = referenceHoleIndex;
        _referenceNumber = referenceNumber;

        //Change color
        BodyRenderer.material.color = _networkManager.GetPlayerColor(OwnerPlayerIndex).BrightnessOffset(0.4f);
    }

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (Owner != _networkManager.LocalPlayer) return;

        if (isPressed)
        {
            if (interactiveObject == Gear)
            {
                _isGearInteracted = true;
            }
        }
        else
        {
            _isGearInteracted = false;
        }
    }

    public override void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        if (Owner == _networkManager.LocalPlayer && interactiveObject == Gear)
        {
            /*float angle = Vector3.Dot(Vector3.Cross(movement, Camera.main.transform.forward), -Gear.transform.forward);

            Gear.transform.localRotation *= Quaternion.Euler(0, 0, angle);*/

            Vector2 gearCenterScreenPos = Camera.main.WorldToScreenPoint(Gear.transform.position);
            Vector2 startScreenPos = screenPos - deltaMovement;
            Vector2 destScreenPos = screenPos;

            Vector2 startVector = startScreenPos - gearCenterScreenPos;
            Vector2 destVector = destScreenPos - gearCenterScreenPos;

            float angle = Vector2.SignedAngle(startVector, destVector);

            Gear.transform.localRotation *= Quaternion.Euler(0, 0, -2 * angle);
        }
    }

    [PunRPC]
    public override void GrabRPC(Vector3 startPosition, Quaternion startRotation, PhotonMessageInfo info)
    {
        base.GrabRPC(startPosition, startRotation, info);

        if (Owner == _networkManager.LocalPlayer)
        {
            _gearPhotonView.RequestOwnership();
        }
    }

    public override void Place(Vector3 restPosition, Quaternion restRotation)
    {
        if (Owner != _networkManager.LocalPlayer)
        {
            //The object is held by someone else
            return;
        }

        PlayerInfo oldOwner = Owner;

        base.Place(restPosition, restRotation);

        //Check embedding
        if (oldOwner.PlayerIndex == OwnerPlayerIndex)
        {
            TaskManipDiffUsrHoles holes = _networkManager.CurrentRoom.TaskManipDiffUsrHoles;
            Vector3 referencePosition = holes.Holes[_referenceHoleIndex].transform.position;
            Quaternion referenceRotation = holes.Holes[_referenceHoleIndex].transform.rotation;

            /*Debug.LogFormat("Test: number {0}, distance {1}, angle {2}", Number == _referenceNumber,
                Vector3.Distance(transform.position, referencePosition),
                Quaternion.Angle(transform.rotation, referenceRotation));*/

            if (Number == _referenceNumber &&
                Vector3.Distance(transform.position, referencePosition) < DistanceThreshold &&
                Quaternion.Angle(transform.rotation, referenceRotation) < RotationThreshold)
            {
                Embed();
            }
        }
    }

    #endregion

    #region Private methods / Photon RPCs

    private void Embed()
    {
        OnlineLogger.Instance.SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, OnlineLogger.EventType.ManipCompleted);
        _photonView.RPC("EmbedRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void EmbedRPC()
    {
        Debug.Log("EmbedRPC called");

        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.Success);

        IsEmbedded = true;

        m_PositionModel.SynchronizeEnabled = false;
        m_RotationModel.SynchronizeEnabled = false;

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;

        TaskManipDiffUsrHoles holes = _networkManager.CurrentRoom.TaskManipDiffUsrHoles;
        this.transform.parent = holes.transform;

        Vector3 referencePosition = holes.Holes[_referenceHoleIndex].transform.position;
        Vector3 referenceEmbeddedPosition = holes.Holes[_referenceHoleIndex].transform.position -
                                            holes.Holes[_referenceHoleIndex].transform.up * 0.04f;
        Quaternion referenceRotation = holes.Holes[_referenceHoleIndex].transform.rotation;
        holes.Holes[_referenceHoleIndex].SetActive(true);

        _embedSequence = DOTween.Sequence();
        _embedSequence
            .Append(transform.DOMove(referencePosition, 0.5f))
            .Insert(0, transform.DORotate(referenceRotation.eulerAngles, 0.5f))
            .InsertCallback(0, () => this.gameObject.CollidersEnabled(false))
            .AppendInterval(0.2f)
            .Append(transform.DOMove(referenceEmbeddedPosition, 0.5f));
    }

    #endregion
}
