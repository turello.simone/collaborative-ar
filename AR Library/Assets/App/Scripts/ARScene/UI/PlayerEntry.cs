﻿using System;
using MyBox;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEntry : MonoBehaviour
{
    [Header("UI References")]
    [MustBeAssigned] public TextMeshProUGUI PlayerNameText;
    [MustBeAssigned] public Image PlayerColorImage;
    [MustBeAssigned] public TextMeshProUGUI PlayerInGameText;
    [MustBeAssigned] public Button MicrophoneButton;
    [MustBeAssigned] public Image MicrophoneOnImage;
    [MustBeAssigned] public Image MicrophoneOffImage;
    [MustBeAssigned] public Button SpeakerButton;
    [MustBeAssigned] public Image SpeakerOnImage;
    [MustBeAssigned] public Image SpeakerOffImage;
    [MustBeAssigned] public Button IsHelperButton;
    [MustBeAssigned] public Image AdminImage;
    [MustBeAssigned] public Image UserImage;

    private PlayerInfo _owner;

    #region UNITY

    public void OnDestroy()
    {
        _owner.PlayerStateChangedEvent -= SetPlayerState;
    }

    #endregion

    public void Initialize(PlayerInfo player)
    {
        _owner = player;
        SetPlayerState(player, player.State);
        player.PlayerStateChangedEvent += SetPlayerState;
        PlayerNameText.text = player.Name;

        Color color = player.Color;
        if (color != Color.black)
        {
            PlayerColorImage.color = color;
        }
        else
        {
            player.ColorSetEvent += Player_ColorSetEvent;
        }

        MicrophoneButton.gameObject.SetActive(false);
        SpeakerButton.gameObject.SetActive(true);
        IsHelperButton.gameObject.SetActive(false);
        SetSpeakerImage(player.IsVoiceEnabled);
    }

    public void Initialize(LocalPlayerInfo player)
    {
        _owner = player;
        SetPlayerState(player, player.State);
        player.PlayerStateChangedEvent += SetPlayerState;
        PlayerNameText.text = player.Name;

        Color color = player.Color;
        if (color != Color.black)
        {
            PlayerColorImage.color = color;
        }
        else
        {
            player.ColorSetEvent += Player_ColorSetEvent;
        }

        MicrophoneButton.gameObject.SetActive(true);
        SpeakerButton.gameObject.SetActive(true);
        IsHelperButton.gameObject.SetActive(true);

        SetMicrophoneImage(player.IsVoiceTransmissionEnabled);
        SetSpeakerImage(player.IsVoiceEnabled);
        SetIsHelperImage(player.IsMasterClient);
        PhotonNetworkManager.Instance.CurrentRoom.MasterClientSwitchedEvent += OnMasterClientSwitched;
    }

    private void Player_ColorSetEvent(PlayerInfo player, Color newValue)
    {
        if (newValue != Color.black)
        {
            PlayerColorImage.color = newValue;
            _owner.ColorSetEvent -= Player_ColorSetEvent;
        }
    }

    private void SetPlayerState(PlayerInfo player, GameUtils.PlayerState playerState)
    {
        switch (playerState)
        {
            case GameUtils.PlayerState.Synchronizing:
                PlayerInGameText.text = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PlayerStateSynchronizing");
                break;
            case GameUtils.PlayerState.Synchronized:
                PlayerInGameText.text = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PlayerStateSynchronized");
                break;
            case GameUtils.PlayerState.InGame:
                PlayerInGameText.text = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PlayerStateInGame");
                break;
            case GameUtils.PlayerState.Error:
                PlayerInGameText.text = "";
                break;

        }
    }

    private void SetMicrophoneImage(bool isOn)
    {
        MicrophoneOnImage.enabled = isOn;
        MicrophoneOffImage.enabled = !isOn;
    }

    private void SetSpeakerImage(bool isOn)
    {
        SpeakerOnImage.enabled = isOn;
        SpeakerOffImage.enabled = !isOn;
    }

    private void SetIsHelperImage(bool isHelper)
    {
        AdminImage.enabled = isHelper;
        UserImage.enabled = !isHelper;
    }

    public void OnMicrophoneButtonClicked()
    {
        if (PhotonNetworkManager.Instance.LocalPlayer == _owner)
        {
            bool oldValue = ((LocalPlayerInfo)_owner).IsVoiceTransmissionEnabled;
            ((LocalPlayerInfo)_owner).IsVoiceTransmissionEnabled = !oldValue;
            SetMicrophoneImage(!oldValue);
        }
    }

    public void OnSpeakerButtonClicked()
    {
        bool oldValue = _owner.IsVoiceEnabled;
        _owner.IsVoiceEnabled = !oldValue;
        SetSpeakerImage(!oldValue);
    }

    public void OnIsHelperButtonClicked()
    {
        if(!_owner.IsMasterClient) ((LocalPlayerInfo)_owner).BecomeMasterClient();
    }

    private void OnMasterClientSwitched(PlayerInfo player)
    {
        SetIsHelperImage(player == _owner);
    }
}
