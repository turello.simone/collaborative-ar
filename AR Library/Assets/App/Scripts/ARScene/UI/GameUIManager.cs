﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using DG.Tweening;
using MyBox;
using MyUtils;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameUIManager : SingletonInScene<GameUIManager>
{
    public enum SettingsPanel
    {
        None,
        Background,
        SettingsList,
        Settings,
        InsideRoom,
        SceneSelection,
        GameSceneVisualizationSelection,
        GameSceneSelection,
        TaskSelection,
        TrainingSceneSelection
    }

    public enum InGamePanel
    {
        Default,
        DetectSurface,
        DetectMarker,
        ConfirmObjectsPosition,
        UnequipObject,
        FreezeObject,
        UnfreezeObject,
        SettingsButton,
        //NextScene,
        //NextSubTask
    }

    public bool PlayerColorBorderEnabled
    {
        get { return _playerColorBorderEnabled;}
        set
        {
            _playerColorBorderEnabled = value;
            _borderDrawer.enabled = value;
        }
    }
    private bool _playerColorBorderEnabled = false;
    private BorderDrawer _borderDrawer;

    [Separator("InGame Panels", true)]

    [Header("Focal Point Panel")]
    [MustBeAssigned] public GameObject FocalPointPanel;

    [Header("Scene Name Panel")]
    [SerializeField, MustBeAssigned] private GameObject _sceneNamePanel;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _sceneNameText;
    [SerializeField, MustBeAssigned] private Image _userIconImage;
    [SerializeField, MustBeAssigned] private Image _userCrownIconImage;
    [SerializeField, MustBeAssigned] private GameObject _sceneNameLocVisPanel;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _sceneLocationText;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _sceneVisualizationText;

    [Header("Detect Surface Panel")]
    [SerializeField, MustBeAssigned] private GameObject _detectSurfacePanel;

    [Header("Detect Marker Panel")]
    [SerializeField, MustBeAssigned] private GameObject _detectMarkerPanel;
    [MustBeAssigned] public Slider ProgressBar;

    [Header("Confirm Objects Position Panel")]
    [SerializeField, MustBeAssigned] private GameObject _confirmObjectsPositionPanel;

    [Header("Unequip Object Panel")]
    [SerializeField, MustBeAssigned] private GameObject _unequipObjectPanel;

    [Header("Settings Button Panel")]
    [SerializeField, MustBeAssigned] private GameObject _settingsButtonPanel;

    [Header("Advanced Settings Panel")]
    [SerializeField, MustBeAssigned] private GameObject _advancedSettingsPanel;
    [SerializeField, MustBeAssigned] public Dropdown LocalPlayerDropdown;
    [SerializeField, MustBeAssigned] public Dropdown RemotePlayerDropdown;
    [SerializeField, MustBeAssigned] public Dropdown LocalGazeDropdown;
    [SerializeField, MustBeAssigned] public Dropdown RemoteGazeDropdown;
    [SerializeField, MustBeAssigned] public Dropdown LocalObjectManipDropdown;
    [SerializeField, MustBeAssigned] public Dropdown RemoteObjectManipDropdown;
    [SerializeField, MustBeAssigned] public Dropdown VisualizationPresetDropdown;
    [SerializeField, MustBeAssigned] public Toggle PlayerFocalPointEnabledToggle;

    [Header("Freeze Object Panel")]
    [SerializeField, MustBeAssigned] private GameObject _freezeObjectPanel;

    [Header("Unfreeze Object Panel")]
    [SerializeField, MustBeAssigned] private GameObject _unfreezeObjectPanel;

    [Header("Next Scene Panel")]
    [SerializeField, MustBeAssigned] private GameObject _nextScenePanel;

    [Header("Instructions Panel")]
    [SerializeField, MustBeAssigned] private GameObject _instructionsPanel;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _instructionsSceneNameText;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _instructionsText;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _readyText;
    [SerializeField, MustBeAssigned] private Button _readyButton;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _instructionsSceneLocationText;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _instructionsSceneVisualizationText;
    public bool InstructionsPanelEnabled = false;

    [Header("Next SubTask Panel")]
    [SerializeField, MustBeAssigned] private GameObject _nextSubTaskPanel;
    private Action _nextSubTaskAction;


    [Separator("Settings Panels", true)]

    [Header("Background Panel")]
    [SerializeField, MustBeAssigned] private GameObject _backgroundPanel;

    [Header("Settings List Panel")]
    [SerializeField, MustBeAssigned] private GameObject _settingsListPanel;
    [SerializeField, MustBeAssigned] private GameObject _settingsListSceneSelectionButton;

    [Header("Settings Panel")]
    [SerializeField, MustBeAssigned] private GameObject _settingsPanel;
    [SerializeField, MustBeAssigned] private Toggle _advancedSettingsToggle;
    [SerializeField, MustBeAssigned] public Toggle SimpleGameModeToggle;
    [SerializeField, MustBeAssigned] public Dropdown LightEstimationDropdown;

    [Header("Inside Room Panel")]
    [SerializeField, MustBeAssigned] private GameObject _insideRoomPanel;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _insideRoomNameText;
    [SerializeField, MustBeAssigned] private GameObject _playerEntryPrefab;
    [SerializeField, MustBeAssigned] private GameObject _insideRoomLeaveRoomButton;
    [SerializeField, MustBeAssigned] private GameObject _insideRoomEnterGameButton;
    [SerializeField, MustBeAssigned] private GameObject _insideRoomBackButton;

    [Header("Scene Selection Panel")]
    [SerializeField, MustBeAssigned] private GameObject _sceneSelectionPanel;
    [SerializeField, MustBeAssigned] private GameObject _sceneSelectionBackButton;

    [Header("Game Scene Visualization Selection Panel")]
    [SerializeField, MustBeAssigned] private GameObject _gameSceneVisualizationSelectionPanel;

    [Header("Game Scene Selection Panel")]
    [SerializeField, MustBeAssigned] private GameObject _gameSceneSelectionPanel;
    private SubSceneManager.Visualization _gameScenevisualization;

    [Header("Training Scene Selection Panel")]
    [SerializeField, MustBeAssigned] private GameObject _trainingSceneSelectionPanel;

    [Header("Task Selection Panel")]
    [SerializeField, MustBeAssigned] private GameObject _taskSelectionPanel;
    [SerializeField, MustBeAssigned] private TextMeshProUGUI _errorUninitializedText;
    [SerializeField, MustBeAssigned] private TMP_Dropdown _interactionDropdown;
    [SerializeField, MustBeAssigned] private TMP_Dropdown _locationDropdown;
    [SerializeField, MustBeAssigned] private TMP_Dropdown _visualizationSequenceDropdown;
    [SerializeField, MustBeAssigned] private TMP_Dropdown _taskInSequenceDropdown;
    private SubSceneManager.Interaction _currentInteraction;
    private SubSceneManager.Location _currentLocation;
    private SubSceneManager.VisualizationSequence _currentVisualizationSequence;
    private int _currentTaskInSequence;
    private Sequence _errorSequence;

    private EscapeGameManager _gameManager;
    private PhotonNetworkManager _networkManager;
    private Dictionary<int, GameObject> _playerListEntries;
    private Dictionary<GameObject, AiryUIAnimationManager> _panels;
    private InGamePanel _inGamePanelCurrent;
    public SettingsPanel CurrentSettingsPanel;

    #region UNITY

    public override void Awake()
    {
        base.Awake();

        _gameManager = EscapeGameManager.Instance;
        _networkManager = PhotonNetworkManager.Instance;

        _panels = new Dictionary<GameObject, AiryUIAnimationManager>();

        _panels.Add(_backgroundPanel, _backgroundPanel.GetComponent<AiryUIAnimationManager>());
        _backgroundPanel.GetComponent<AiryUIAnimatedElement>().showItemOnMenuEnable = false;

        _panels.Add(_settingsListPanel, _settingsListPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_settingsPanel, _settingsPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_insideRoomPanel, _insideRoomPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_sceneSelectionPanel, _sceneSelectionPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_gameSceneVisualizationSelectionPanel, _gameSceneVisualizationSelectionPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_gameSceneSelectionPanel, _gameSceneSelectionPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_trainingSceneSelectionPanel, _trainingSceneSelectionPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_taskSelectionPanel, _taskSelectionPanel.GetComponent<AiryUIAnimationManager>());

        _panels.Add(_settingsButtonPanel, _settingsButtonPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_detectSurfacePanel, _detectSurfacePanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_detectMarkerPanel, _detectMarkerPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_confirmObjectsPositionPanel, _confirmObjectsPositionPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_unequipObjectPanel, _unequipObjectPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_advancedSettingsPanel, _advancedSettingsPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_freezeObjectPanel.gameObject, _freezeObjectPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_unfreezeObjectPanel.gameObject, _unfreezeObjectPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_nextScenePanel.gameObject, _nextScenePanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_instructionsPanel.gameObject, _instructionsPanel.GetComponent<AiryUIAnimationManager>());
        _panels.Add(_nextSubTaskPanel.gameObject, _nextSubTaskPanel.GetComponent<AiryUIAnimationManager>());

        SetActiveInGamePanel(InGamePanel.Default);
        SetActiveSettingsPanel(SettingsPanel.Background);
        _backgroundPanel.GetComponent<AiryUIAnimatedElement>().showItemOnMenuEnable = true;

        _insideRoomBackButton.SetActive(false);
        _insideRoomEnterGameButton.SetActive(true);
        _insideRoomLeaveRoomButton.SetActive(true);
        _sceneSelectionBackButton.SetActive(false);

        _advancedSettingsToggle.isOn = false;
        OnAdvancedSettingsToggleValueChanged(false);

        _borderDrawer = Camera.main.gameObject.AddComponent<BorderDrawer>();
        _borderDrawer.borderWidth = 0.05f * Mathf.Min(Screen.width, Screen.height);
        if (_networkManager.LocalPlayer.Color == Color.black)
            _networkManager.LocalPlayer.ColorSetEvent +=
                (player, value) =>
                {
                    _borderDrawer.OutsideColor = value.WithAlphaSetTo(0.5f);
                    _userIconImage.color = value;
                    _userCrownIconImage.color = value;
                };
        else
        {
            _borderDrawer.OutsideColor = _networkManager.LocalPlayer.Color.WithAlphaSetTo(0.5f);
            _userIconImage.color = _networkManager.LocalPlayer.Color;
            _userCrownIconImage.color = _networkManager.LocalPlayer.Color;
        }
        PlayerColorBorderEnabled = false;

        _sceneNameLocVisPanel.SetActive(false);
        _sceneNameText.enabled = false;
    }

    private void Start()
    {
        _playerListEntries = new Dictionary<int, GameObject>();

        LocalPlayerDropdown.value = PlayerVisualizationToInt(_networkManager.LocalPlayerVisualizationSetting);
        RemotePlayerDropdown.value = PlayerVisualizationToInt(_networkManager.RemotePlayerVisualizationSetting);
        LocalGazeDropdown.value = GazeVisualizationToInt(_networkManager.LocalGazeVisualizationSetting);
        RemoteGazeDropdown.value = GazeVisualizationToInt(_networkManager.RemoteGazeVisualizationSetting);
        LocalObjectManipDropdown.value = ObjManipToInt(ObjectManipulation.Instance.LocalVisualization);
        RemoteObjectManipDropdown.value = ObjManipToInt(ObjectManipulation.Instance.RemoteVisualization);
        VisualizationPresetDropdown.value = 0;
        PlayerFocalPointEnabledToggle.isOn = _networkManager.PlayerFocalPointEnabled;

        _networkManager.PlayerEnteredRoomEvent += OnPlayerEnteredRoom;
        _networkManager.PlayerLeftRoomEvent += OnPlayerLeftRoom;

        if (_networkManager.CurrentRoom == null)
            _networkManager.RoomJoinedEvent += () =>
                _networkManager.CurrentRoom.MasterClientSwitchedEvent += OnMasterClientSwitched;
        else
            _networkManager.CurrentRoom.MasterClientSwitchedEvent += OnMasterClientSwitched;

        StartCoroutine(InitializeRoom());
    }

    private void Update()
    {
        if (InstructionsPanelEnabled)
        {
            _readyText.text = _networkManager.NumberOfPlayersReady + "/" + _networkManager.NumberOfPlayers + " " +
                              Lean.Localization.LeanLocalization.GetTranslationText("ARScene/Ready");
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        _networkManager.PlayerEnteredRoomEvent -= OnPlayerEnteredRoom;
        _networkManager.PlayerLeftRoomEvent -= OnPlayerLeftRoom;
        if(_networkManager.CurrentRoom != null) _networkManager.CurrentRoom.MasterClientSwitchedEvent -= OnMasterClientSwitched;
    }

    #endregion



    #region UI CALLBACKS

    public void OnSceneTrainingButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.TrainingSceneSelection);
    }
    
    public void OnSceneTrainingIntroButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneVisualizationInitialSelection = SubSceneManager.Visualization.Complete;
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Training;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.Training, SubSceneManager.Visualization.Complete);
        }
    }
    
    public void OnSceneTrainingGazeButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneVisualizationInitialSelection = SubSceneManager.Visualization.Complete;
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.TrainingGaze;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.TrainingGaze, SubSceneManager.Visualization.Complete);
        }
    }
    public void OnSceneTrainingManipButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneVisualizationInitialSelection = SubSceneManager.Visualization.Complete;
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.TrainingManip;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.TrainingManip, SubSceneManager.Visualization.Complete);
        }
    }
    public void OnSceneTrainingPosButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneVisualizationInitialSelection = SubSceneManager.Visualization.Complete;
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.TrainingPos;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.TrainingPos, SubSceneManager.Visualization.Complete);
        }
    }

    public void OnSceneTutorialButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Tutorial;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.Tutorial, _gameScenevisualization);
        }
    }

    public void OnScenePedestalButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Pedestal;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.Pedestal, _gameScenevisualization);
        }
    }

    public void OnScenePaintingButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Painting;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.Painting, _gameScenevisualization);
        }
    }

    public void OnSceneTranslationButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Translation;
        }
        else
        {
            _gameManager.StartSceneGame(SubSceneManager.SubSceneType.Translation, _gameScenevisualization);
        }
    }

    public void OnConfirmObjectsPositionButtonClicked()
    {
        _gameManager.ObjectsPositionConfirmed = true;
    }

    public void OnLeaveRoomButtonClicked()
    {
        _networkManager.LeaveRoom();
        SceneManager.LoadScene(0);
    }

    public void OnStartGameButtonClicked()
    {
        _insideRoomBackButton.SetActive(true);
        _insideRoomEnterGameButton.SetActive(false);
        _insideRoomLeaveRoomButton.SetActive(false);

        if (_networkManager.IsMasterClient && _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.InsideRoomGameStarted = true;
        }
        else
        {
            _gameManager.InsideRoomGameStarted = true;
            SetActiveSettingsPanel(SettingsPanel.None);
        }
    }

    public void OnUnequipObjectButtonClicked()
    {
        EquippableDynamicObject obj;
        if ((obj = _networkManager.LocalPlayer.ObjectHeld.GetComponent<EquippableDynamicObject>()) != null)
        {
            obj.Unequip();
        }
    }

    public void OnSettingsIconClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.SettingsList);
    }

    public void OnBackButtonClicked()
    {
        if (CurrentSettingsPanel == SettingsPanel.InsideRoom ||
            CurrentSettingsPanel == SettingsPanel.Settings ||
            CurrentSettingsPanel == SettingsPanel.SceneSelection)
        {
            SetActiveSettingsPanel(SettingsPanel.SettingsList);
        }
        else if (CurrentSettingsPanel == SettingsPanel.GameSceneVisualizationSelection ||
                 CurrentSettingsPanel == SettingsPanel.TaskSelection ||
                 CurrentSettingsPanel == SettingsPanel.TrainingSceneSelection)
        {
            SetActiveSettingsPanel(SettingsPanel.SceneSelection);
        }
        else if (CurrentSettingsPanel == SettingsPanel.GameSceneSelection)
        {
            SetActiveSettingsPanel(SettingsPanel.GameSceneVisualizationSelection);
        }
        else if (CurrentSettingsPanel == SettingsPanel.SettingsList)
        {
            SetActiveSettingsPanel(SettingsPanel.None);
        }
    }

    public void OnRoomInfoButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.InsideRoom);
    }

    public void OnSettingsButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.Settings);
    }

    public void OnAdvancedSettingsToggleValueChanged(bool newValue)
    {
        SetActive(_advancedSettingsPanel, newValue);
    }
    public void OnSimpleGameModeToggleValueChanged(bool newValue)
    {
        _networkManager.CurrentRoom.SimpleGameMode =
            newValue ? GameUtils.SimpleGameMode.Enabled : GameUtils.SimpleGameMode.Disabled;
    }

    public void OnLocalPlayerDropdownValueChanged(int value)
    {
        _networkManager.LocalPlayerVisualizationSetting = IntToPlayerVisualization(value);
    }

    public void OnRemotePlayerDropdownValueChanged(int value)
    {
        if(_networkManager.IsMasterClient)
            _networkManager.ForceRemotePlayerVisualization(IntToPlayerVisualization(value));
        else
            _networkManager.RemotePlayerVisualizationSetting = IntToPlayerVisualization(value);
    }

    public void OnLocalGazeDropdownValueChanged(int value)
    {
        _networkManager.LocalGazeVisualizationSetting = IntToGazeVisualization(value);
    }

    public void OnRemoteGazeDropdownValueChanged(int value)
    {
        if (_networkManager.IsMasterClient)
            _networkManager.ForceRemoteGazeVisualization(IntToGazeVisualization(value));
        else
            _networkManager.RemoteGazeVisualizationSetting = IntToGazeVisualization(value);
    }

    public void OnLocalObjectManipDropdownValueChanged(int value)
    {
        ObjectManipulation.Instance.LocalVisualization = IntToObjManip(value);
    }

    public void OnRemoteObjectManipDropdownValueChanged(int value)
    {
        if (_networkManager.IsMasterClient)
            _networkManager.ForceRemoteObjectManipulationVisualization(IntToObjManip(value));
        else
            ObjectManipulation.Instance.RemoteVisualization = IntToObjManip(value);
    }
    
    public void OnVisualizationPresetDropdownValueChanged(int value)
    {
        if (value == 0) return;

        _networkManager.AvatarType = GameUtils.AvatarType.Simple;

        _networkManager.LocalPlayerVisualizationSetting = GameUtils.PlayerVisualization.None;
        _networkManager.LocalGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
        ObjectManipulation.Instance.LocalVisualization = GameUtils.ObjectManipulationVisualization.Hand;

        if (_networkManager.IsMasterClient)
        {
            switch (value)
            {
                case 1:
                    _networkManager.ForceRemotePlayerVisualization(GameUtils.PlayerVisualization.Frustum);
                    _networkManager.ForceRemoteGazeVisualization(GameUtils.GazeVisualization.Cursor);
                    _networkManager.ForceRemoteObjectManipulationVisualization(GameUtils.ObjectManipulationVisualization.None);
                    _networkManager.ForcePlayerFocalPointEnabled(false);
                    break;
                case 2:
                    _networkManager.ForceRemotePlayerVisualization(GameUtils.PlayerVisualization.Name |
                                                                   GameUtils.PlayerVisualization.Avatar);
                    _networkManager.ForceRemoteGazeVisualization(GameUtils.GazeVisualization.CursorRay);
                    _networkManager.ForceRemoteObjectManipulationVisualization(
                        GameUtils.ObjectManipulationVisualization.Ray |
                        GameUtils.ObjectManipulationVisualization.Outline |
                        GameUtils.ObjectManipulationVisualization.Hand);
                    _networkManager.ForcePlayerFocalPointEnabled(false);
                    break;
                case 3:
                    _networkManager.ForceRemotePlayerVisualization(GameUtils.PlayerVisualization.Name |
                                                                   GameUtils.PlayerVisualization.Avatar);
                    _networkManager.ForceRemoteGazeVisualization(GameUtils.GazeVisualization.CursorRay);
                    _networkManager.ForceRemoteObjectManipulationVisualization(
                        GameUtils.ObjectManipulationVisualization.Ray |
                        GameUtils.ObjectManipulationVisualization.Outline |
                        GameUtils.ObjectManipulationVisualization.Hand);
                    _networkManager.ForcePlayerFocalPointEnabled(true);
                    break;
            }
        }
        else
        {
            switch (value)
            {
                case 1:
                    _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Frustum;
                    _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
                    ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.None;
                    _networkManager.PlayerFocalPointEnabled = false;
                    break;
                case 2:
                    _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                       GameUtils.PlayerVisualization.Avatar;
                    _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                    ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.Ray |
                                                                      GameUtils.ObjectManipulationVisualization.Outline |
                                                                      GameUtils.ObjectManipulationVisualization.Hand;
                    _networkManager.PlayerFocalPointEnabled = false;
                    break;
                case 3:
                    _networkManager.RemotePlayerVisualizationSetting = GameUtils.PlayerVisualization.Name |
                                                                       GameUtils.PlayerVisualization.Avatar;
                    _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                    ObjectManipulation.Instance.RemoteVisualization = GameUtils.ObjectManipulationVisualization.Ray |
                                                                      GameUtils.ObjectManipulationVisualization.Outline |
                                                                      GameUtils.ObjectManipulationVisualization.Hand;
                    _networkManager.PlayerFocalPointEnabled = true;
                    break;
            }
        }
    }

    public void OnFocalPointEnabledToggleValueChanged(bool newValue)
    {
        if (_networkManager.IsMasterClient)
            _networkManager.ForcePlayerFocalPointEnabled(newValue);
        else
            _networkManager.PlayerFocalPointEnabled = newValue;
    }

    public void OnLightEstimationDropdownValueChanged(int value)
    {
        switch (value)
        {
            case 0:
                ARManager.Instance.ARLibrary.LightEstimationMode = ARUtils.LightEstimationMode.Disabled;
                break;
            case 1:
                ARManager.Instance.ARLibrary.LightEstimationMode = ARUtils.LightEstimationMode.AmbientIntensity;
                break;
            case 2:
                ARManager.Instance.ARLibrary.LightEstimationMode = ARUtils.LightEstimationMode.EnvironmentalHDRWithoutReflections;
                break;
            case 3:
                ARManager.Instance.ARLibrary.LightEstimationMode = ARUtils.LightEstimationMode.EnvironmentalHDRWithReflections;
                break;
        }
    }

    public void OnFreezeObjectButtonClicked()
    {
        _networkManager.FreezeDynamicObject(_networkManager.LocalPlayer.ObjectHeld);
        SetActiveInGamePanel(InGamePanel.UnfreezeObject);
    }

    public void OnUnfreezeObjectButtonClicked()
    {
        _networkManager.UnfreezeDynamicObject(_networkManager.LocalPlayer.ObjectHeld);
        SetActiveInGamePanel(InGamePanel.FreezeObject);
    }

    public void OnSceneSelectionButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.SceneSelection);
        _sceneSelectionBackButton.SetActive(true);
    }

    public void OnGameScenesButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.GameSceneVisualizationSelection);
    }

    public void OnBaseVisualizationButtonClicked()
    {
        _gameScenevisualization = SubSceneManager.Visualization.Base;
        _gameManager.SubSceneVisualizationInitialSelection = SubSceneManager.Visualization.Base;
        SetActiveSettingsPanel(SettingsPanel.GameSceneSelection);
    }

    public void OnCompleteVisualizationButtonClicked()
    {
        _gameScenevisualization = SubSceneManager.Visualization.Complete;
        _gameManager.SubSceneVisualizationInitialSelection = SubSceneManager.Visualization.Complete;
        SetActiveSettingsPanel(SettingsPanel.GameSceneSelection);
    }

    public void OnTasksButtonClicked()
    {
        SetActiveSettingsPanel(SettingsPanel.TaskSelection);

        _currentInteraction = SubSceneManager.Interaction.Uninitialized;
        _currentLocation = SubSceneManager.Location.Uninitialized;
        _currentVisualizationSequence = SubSceneManager.VisualizationSequence.Uninitialized;
        _currentTaskInSequence = 0;
        _errorUninitializedText.enabled = false;
        _interactionDropdown.value = (int) SubSceneManager.Interaction.Uninitialized;
        _locationDropdown.value = (int)SubSceneManager.Location.Uninitialized;
        _visualizationSequenceDropdown.value = (int)SubSceneManager.VisualizationSequence.Uninitialized;
        _taskInSequenceDropdown.value = 0;
    }

    public void OnStartTaskButtonClicked()
    {
        //Handle dropdowns with default values
        if (_currentInteraction == SubSceneManager.Interaction.Uninitialized ||
            _currentLocation == SubSceneManager.Location.Uninitialized ||
            _currentVisualizationSequence == SubSceneManager.VisualizationSequence.Uninitialized ||
            (_currentInteraction == SubSceneManager.Interaction.Manipulation && (_currentTaskInSequence < 0 || _currentTaskInSequence > 5)))
        {
            if (_errorSequence != null && _errorSequence.IsPlaying()) _errorSequence.Kill();
            _errorSequence = DOTween.Sequence()
                .AppendCallback(() =>
                {
                    _errorUninitializedText.alpha = 0;
                    _errorUninitializedText.enabled = true;
                })
                .Append(_errorUninitializedText.DOFade(1, 0.2f))
                .AppendInterval(2f)
                .Append(_errorUninitializedText.DOFade(0, 0.2f))
                .AppendCallback(() => _errorUninitializedText.enabled = false);

            return;
        }

        SetActiveSettingsPanel(SettingsPanel.None);
        if (!_networkManager.IsMasterClient) return;

        if (_networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.Uninitialized ||
            _networkManager.CurrentRoom.RoomScene.Type == SubSceneManager.SubSceneType.InitialPrep)
        {
            _gameManager.SubSceneTypeInitialSelection = SubSceneManager.SubSceneType.Task;
            _gameManager.TaskInteractionInitialSelection = _currentInteraction;
            _gameManager.TaskLocationInitialSelection = _currentLocation;
            _gameManager.TaskVisualizationSequenceInitialSelection = _currentVisualizationSequence;
            _gameManager.TaskInSequenceInitialSelection = _currentTaskInSequence;
        }
        else
        {
            _gameManager.StartSceneTask(SubSceneManager.SubSceneType.Task, _currentInteraction, _currentLocation,
                _currentVisualizationSequence, _currentTaskInSequence);
        }
    }

    public void OnNextSceneButtonClicked()
    {
        PhotonNetworkManager.Instance.ResetPlayersReady();
        _gameManager.StartNextScene();
        SetActiveNextScenePanel(false);
    }

    public void OnInteractionDropdownValueChanged(int value)
    {
        _currentInteraction = (SubSceneManager.Interaction) value;
    }

    public void OnLocationDropdownValueChanged(int value)
    {
        _currentLocation = (SubSceneManager.Location) value;
    }
    
    public void OnVisualizationSequenceDropdownValueChanged(int value)
    {
        _currentVisualizationSequence = (SubSceneManager.VisualizationSequence) value;
    }

    public void OnTaskInSequenceDropdownValueChanged(int value)
    {
        _currentTaskInSequence = value;
    }

    public void OnReadyButtonClicked()
    {
        _networkManager.LocalPlayer.IsReady = true;
        _readyButton.interactable = false;
    }
    public void OnNextSubTaskButtonClicked()
    {
        PhotonNetworkManager.Instance.ResetPlayersReady();
        _nextSubTaskAction();
        _nextSubTaskAction = null;
        SetActiveNextSubTaskPanel(false);
    }


    #endregion


    #region NetworkManager CALLBACKS

    public void OnPlayerEnteredRoom(PlayerInfo newPlayer)
    {
        GameObject entry = Instantiate(_playerEntryPrefab);
        entry.transform.SetParent(_insideRoomPanel.transform);
        entry.transform.localScale = Vector3.one;
        entry.GetComponent<PlayerEntry>().Initialize(newPlayer);

        _playerListEntries.Add(newPlayer.ActorNumber, entry);
    }

    public void OnPlayerLeftRoom(PlayerInfo otherPlayer)
    {
        Destroy(_playerListEntries[otherPlayer.ActorNumber].gameObject);
        _playerListEntries.Remove(otherPlayer.ActorNumber);
    }

    private void OnMasterClientSwitched(PlayerInfo player)
    {
        _userCrownIconImage.enabled = (player == _networkManager.LocalPlayer);
    }

    #endregion


    #region public methods

    public void SetActiveInGamePanel(InGamePanel activePanel)
    {
        _inGamePanelCurrent = activePanel;

        SetActive(_settingsButtonPanel, true);

        _sceneNamePanel.SetActive(true);
        _userIconImage.enabled = true;
        _userCrownIconImage.enabled = _networkManager.IsMasterClient;

        SetActive(_detectSurfacePanel, activePanel.Equals(InGamePanel.DetectSurface));
        SetActive(_detectMarkerPanel, activePanel.Equals(InGamePanel.DetectMarker));
        SetActive(_confirmObjectsPositionPanel, activePanel.Equals(InGamePanel.ConfirmObjectsPosition));
        SetActive(_unequipObjectPanel, activePanel.Equals(InGamePanel.UnequipObject));
        SetActive(_freezeObjectPanel, activePanel.Equals(InGamePanel.FreezeObject));
        SetActive(_unfreezeObjectPanel, activePanel.Equals(InGamePanel.UnfreezeObject));
        //SetActive(_nextScenePanel, activePanel.Equals(InGamePanel.NextScene));
        //SetActive(_nextSubTaskPanel, activePanel.Equals(InGamePanel.NextSubTask));

        if (activePanel.Equals(InGamePanel.DetectMarker)) ProgressBar.value = 0;
    }

    public void SetActiveSettingsPanel(SettingsPanel activePanel)
    {
        CurrentSettingsPanel = activePanel;

        SetActive(_backgroundPanel, activePanel != SettingsPanel.None);

        SetActive(_settingsListPanel, activePanel.Equals(SettingsPanel.SettingsList));
        _settingsListSceneSelectionButton.SetActive(activePanel.Equals(SettingsPanel.SettingsList) &&
                                                    _networkManager.IsMasterClient &&
                                                    _networkManager.CurrentRoom != null &&
                                                    _networkManager.CurrentRoom.RoomScene.Type !=
                                                    SubSceneManager.SubSceneType.Uninitialized &&
                                                    _networkManager.CurrentRoom.RoomScene.Type !=
                                                    SubSceneManager.SubSceneType.InitialPrep);

        SetActive(_settingsPanel, activePanel.Equals(SettingsPanel.Settings));
        SetActive(_insideRoomPanel, activePanel.Equals(SettingsPanel.InsideRoom));
        SetActive(_sceneSelectionPanel, activePanel.Equals(SettingsPanel.SceneSelection));
        SetActive(_gameSceneVisualizationSelectionPanel, activePanel.Equals(SettingsPanel.GameSceneVisualizationSelection));
        SetActive(_gameSceneSelectionPanel, activePanel.Equals(SettingsPanel.GameSceneSelection));
        SetActive(_taskSelectionPanel, activePanel.Equals(SettingsPanel.TaskSelection));
        SetActive(_trainingSceneSelectionPanel, activePanel.Equals(SettingsPanel.TrainingSceneSelection));

        SimpleGameModeToggle.gameObject.SetActive(activePanel.Equals(SettingsPanel.Settings) && 
                                                  _networkManager.IsMasterClient);
    }

    public void SetInstructionsText(string text)
    {
        _instructionsText.text = text;
    }

    public void SetNextSubTaskButtonAction(Action action)
    {
        _nextSubTaskAction = action;
    }

    public void SetActiveInstructionsPanel(bool enabled)
    {
        InstructionsPanelEnabled = enabled;
        SetActive(_instructionsPanel, enabled);
        if (enabled) _readyButton.interactable = true;
    }
    
    public void SetActiveNextScenePanel(bool enabled)
    {
        SetActive(_nextScenePanel, enabled);
    }
    
    public void SetActiveNextSubTaskPanel(bool enabled)
    {
        SetActive(_nextSubTaskPanel, enabled);
    }

    public void SetSceneNameVisualizationAndLocation(string sceneName, SubSceneManager.Visualization visualization,
        SubSceneManager.Location location)
    {
        _sceneNameLocVisPanel.SetActive(true);
        _sceneNameText.enabled = true;

        _instructionsSceneNameText.text = sceneName;
        _sceneNameText.text = sceneName;

        switch (visualization)
        {
            case SubSceneManager.Visualization.Minimal:
                _sceneVisualizationText.text = "V1";
                _instructionsSceneVisualizationText.text = "V1";
                break;
            case SubSceneManager.Visualization.Base:
                _sceneVisualizationText.text = "V2";
                _instructionsSceneVisualizationText.text = "V2";
                break;
            case SubSceneManager.Visualization.Complete:
                _sceneVisualizationText.text = "V3";
                _instructionsSceneVisualizationText.text = "V3";
                break;
        }

        switch (location)
        {
            case SubSceneManager.Location.Local:
                _sceneLocationText.text = "L";
                _instructionsSceneLocationText.text = "L";
                break;
            case SubSceneManager.Location.Remote:
                _sceneLocationText.text = "R";
                _instructionsSceneLocationText.text = "R";
                break;
        }
    }

    #endregion

    #region private methods

    private void SetActive(GameObject panel, bool value)
    {
        if (!panel.activeSelf && value)
        {
            _panels[panel].ShowMenu();
        }
        else if (panel.activeSelf && !value)
        {
            _panels[panel].HideMenu();
        }
    }

    private IEnumerator InitializeRoom()
    {
        yield return new WaitUntil(() => _networkManager.CurrentRoom != null);

        _insideRoomNameText.text = _networkManager.CurrentRoom.Name;

        //Insert local player
        GameObject entry = Instantiate(_playerEntryPrefab);
        entry.transform.SetParent(_insideRoomPanel.transform);
        entry.transform.localScale = Vector3.one;
        entry.GetComponent<PlayerEntry>().Initialize(_networkManager.LocalPlayer);
        _playerListEntries.Add(_networkManager.LocalPlayer.ActorNumber, entry);

        foreach (PlayerInfo p in _networkManager.OtherPlayers.Values)
        {
            if (_playerListEntries.ContainsKey(p.ActorNumber)) continue;

            entry = Instantiate(_playerEntryPrefab);
            entry.transform.SetParent(_insideRoomPanel.transform);
            entry.transform.localScale = Vector3.one;
            entry.GetComponent<PlayerEntry>().Initialize(p);
            _playerListEntries.Add(p.ActorNumber, entry);
        }
    }

    public int PlayerVisualizationToInt(GameUtils.PlayerVisualization visualization)
    {
        int result = 0;
        if (visualization.HasFlag(GameUtils.PlayerVisualization.Name) &&
            !visualization.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
            !visualization.HasFlag(GameUtils.PlayerVisualization.Frustum))
            result = 1;
        else if (!visualization.HasFlag(GameUtils.PlayerVisualization.Name) &&
                 !visualization.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
                 visualization.HasFlag(GameUtils.PlayerVisualization.Frustum))
            result = 2;
        else if (visualization.HasFlag(GameUtils.PlayerVisualization.Name) &&
                 visualization.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
                 !visualization.HasFlag(GameUtils.PlayerVisualization.Frustum))
            result = 3;
        else if (visualization.HasFlag(GameUtils.PlayerVisualization.Name) &&
                 !visualization.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
                 visualization.HasFlag(GameUtils.PlayerVisualization.Frustum))
            result = 4;
        else if (visualization.HasFlag(GameUtils.PlayerVisualization.Name) &&
                 visualization.HasFlag(GameUtils.PlayerVisualization.Avatar) &&
                 visualization.HasFlag(GameUtils.PlayerVisualization.Frustum))
            result = 5;
        return result;
    }

    public GameUtils.PlayerVisualization IntToPlayerVisualization(int num)
    {
        GameUtils.PlayerVisualization result = GameUtils.PlayerVisualization.None;
        switch (num)
        {
            case 0:
                result = GameUtils.PlayerVisualization.None;
                break;
            case 1:
                result = GameUtils.PlayerVisualization.Name;
                break;
            case 2:
                result = GameUtils.PlayerVisualization.Frustum;
                break;
            case 3:
                result = GameUtils.PlayerVisualization.Name |
                         GameUtils.PlayerVisualization.Avatar;
                break;
            case 4:
                result = GameUtils.PlayerVisualization.Name |
                         GameUtils.PlayerVisualization.Frustum;
                break;
            case 5:
                result = GameUtils.PlayerVisualization.Name |
                         GameUtils.PlayerVisualization.Avatar |
                         GameUtils.PlayerVisualization.Frustum;
                break;
        }
        return result;
    }

    public int GazeVisualizationToInt(GameUtils.GazeVisualization visualization)
    {
        int result = 0;
        switch (visualization)
        {
            case GameUtils.GazeVisualization.None:
                result = 0;
                break;
            case GameUtils.GazeVisualization.Cursor:
                result = 1;
                break;
            case GameUtils.GazeVisualization.CursorRay:
                result = 2;
                break;
        }

        return result;
    }

    public GameUtils.GazeVisualization IntToGazeVisualization(int num)
    {
        GameUtils.GazeVisualization result = GameUtils.GazeVisualization.None;
        switch (num)
        {
            case 0:
                result = GameUtils.GazeVisualization.None;
                break;
            case 1:
                result = GameUtils.GazeVisualization.Cursor;
                break;
            case 2:
                result = GameUtils.GazeVisualization.CursorRay;
                break;
        }

        return result;
    }

    public int ObjManipToInt(GameUtils.ObjectManipulationVisualization visualization)
    {
        int result = 0;
        if (visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Outline) &&
            !visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand) &&
            !visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray))
            result = 1;
        else if (!visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Outline) &&
                 visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand) &&
                 !visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray))
            result = 2;
        else if (visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Outline) &&
                 visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand) &&
                 !visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray))
            result = 3;
        else if (visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Outline) &&
                 visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand) &&
                 visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray))
            result = 4;
        else if (!visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Outline) &&
                 visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand) &&
                 visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray))
            result = 5;
        return result;
    }

    public GameUtils.ObjectManipulationVisualization IntToObjManip(int num)
    {
        GameUtils.ObjectManipulationVisualization result = GameUtils.ObjectManipulationVisualization.None;
        switch (num)
        {
            case 0:
                result = GameUtils.ObjectManipulationVisualization.None;
                break;
            case 1:
                result = GameUtils.ObjectManipulationVisualization.Outline;
                break;
            case 2:
                result = GameUtils.ObjectManipulationVisualization.Hand;
                break;
            case 3:
                result = GameUtils.ObjectManipulationVisualization.Outline | 
                         GameUtils.ObjectManipulationVisualization.Hand;
                break;
            case 4:
                result = GameUtils.ObjectManipulationVisualization.Outline |
                         GameUtils.ObjectManipulationVisualization.Hand | 
                         GameUtils.ObjectManipulationVisualization.Ray;
                break;
            case 5:
                result = GameUtils.ObjectManipulationVisualization.Hand |
                         GameUtils.ObjectManipulationVisualization.Ray;
                break;
        }

        return result;
    }

    #endregion

}
