﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MyBox;
using UnityEngine;

public class ObjectManipulationView : MonoBehaviour
{
    [MustBeAssigned] public GameObject Hand;
    [MustBeAssigned] public GameObject Ray;
    [MustBeAssigned] public GameObject CircleProjector;

    public GameUtils.ObjectManipulationVisualization Visualization
    {
        get { return _visualization; }
        set
        {
            _visualization = value; 
            UpdateRenderers();
        }
    }

    private PlayerInfo _player;
    [SerializeField, ReadOnly] private string _playerName;
    private Color _color;
    private bool _isLocal;
    private GameUtils.ObjectManipulationVisualization _visualization;
    private GameObject _objectHeld;
    private DynamicObject _dynamicObjectHeld;
    private CustomOutline _outline;
    private LineRenderer _rayRenderer;
    private MeshRenderer _handRenderer;
    private float _outlineWidth;
    private bool _handIsHidden;
    private Projector _circleProjector;
    private float _circleAddedRadius;

    //Object Outlining under cursor variables
    public bool OutlineObjectsWithGazeCursor;
    private Dictionary<GameObject, CustomOutline> _outlines;
    private Dictionary<GameObject, bool> _objectsOutlined;
    private float _gazeCursorRadius;
    private Coroutine _addObjectOutlineCoroutine;

    #region Unity

    private void Awake()
    {
        if (Hand == null)
        {
            Debug.LogError("ObjectManipulationView: <Color=Red><a>Missing</a></Color> Hand Reference.", this);
            return;
        }
        if (Ray == null)
        {
            Debug.LogError("ObjectManipulationView: <Color=Red><a>Missing</a></Color> Ray Reference.", this);
            return;
        }

        _handRenderer = Hand.GetComponent<MeshRenderer>();
        _handRenderer.enabled = false;
        _handIsHidden = false;
        _rayRenderer = Ray.GetComponent<LineRenderer>();
        _rayRenderer.enabled = false;
        _circleProjector = CircleProjector.GetComponent<Projector>();
        _circleProjector.enabled = false;

        _outlines = new Dictionary<GameObject, CustomOutline>();
        _objectsOutlined = new Dictionary<GameObject, bool>();

    }

    private void LateUpdate()
    {
        if (_player == null) return;

        //Handle Object Outlining with cursor
        OutlineObjectsUnderCursor();

        if (_objectHeld == null) return;

        //Update position following object held
        if (_isLocal)
        {
            transform.position = _objectHeld.transform.position - Camera.main.transform.up * _dynamicObjectHeld.EncumbranceRadius;

            //Show Circle if dynamicobject state is heldOnSurface
            _circleProjector.enabled =
                (_dynamicObjectHeld.DynamicObjectState == GameUtils.DynamicObjectState.HeldOnSurface);
            //Update circle
            CircleProjector.transform.position = _objectHeld.transform.position;
            if(_dynamicObjectHeld.SurfaceHitInfo.normal != Vector3.zero)
                CircleProjector.transform.forward = -_dynamicObjectHeld.SurfaceHitInfo.normal;
        }
        else
        {
            transform.position = _objectHeld.transform.position -
                                 _player.Player.transform.up * _dynamicObjectHeld.EncumbranceRadius;
        }

        if (Visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray) && _player.Player != null)
        {
            //Update ray
            _rayRenderer.SetPosition(1, Ray.transform.InverseTransformPoint(_player.Player.transform.position));
        }

        //Hide hand if dynamicobject state is heldOnSurface or HeldOnObject
        if (_isLocal && Visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand))
        {
            if (_dynamicObjectHeld.DynamicObjectState == GameUtils.DynamicObjectState.HeldOnSurface ||
                _dynamicObjectHeld.DynamicObjectState == GameUtils.DynamicObjectState.HeldOnObject)
            {
                if (_handIsHidden == false)
                {
                    _handIsHidden = true;
                    TryEnableHand(false);
                }
            }
            else
            {
                if (_handIsHidden == true)
                {
                    _handIsHidden = false;
                    TryEnableHand(true);
                }
            }
        }
    }

    #endregion


    #region public methods 

    public void Initialize(PlayerInfo player, GameUtils.ObjectManipulationVisualization visualization, bool isLocal,
        float outlineWidth, float circleAddedRadius, bool outlineObjectsWithGazeCursor, float gazeCursorRadius)
    {
        _player = player;
        Visualization = visualization;
        _isLocal = isLocal;
        _outlineWidth = outlineWidth;
        _circleAddedRadius = circleAddedRadius;
        _gazeCursorRadius = gazeCursorRadius;
        OutlineObjectsWithGazeCursor = outlineObjectsWithGazeCursor;

        if (_player.ObjectHeld != null)
        {
            OnObjectHeldChangedEvent(_player, _player.ObjectHeld);
        }
        _player.ObjectHeldChangedEvent += OnObjectHeldChangedEvent;

        _playerName = _player.Name;

        if (_player.Color != Color.black)
        {
            _color = _player.Color;
            InitializeColor(_color);
        }
        else
        {
            _player.ColorSetEvent += OnColorSetEvent;
        }
    }

    public void Destroy()
    {
        if (_outline != null)
        {
            _outline.FadeOutAndDestroy();
            //_outline.enabled = false;
            //Object.Destroy(_outline);
            _outline = null;
        }

        _player.ObjectHeldChangedEvent -= OnObjectHeldChangedEvent;
    }

    #endregion


    #region private methods

    private void OnObjectHeldChangedEvent(PlayerInfo player, GameObject newvalue)
    {
        if (newvalue != null)
        {
            //object grabbed
            _dynamicObjectHeld = newvalue.GetComponent<DynamicObject>();
            _objectHeld = newvalue;

            if (_isLocal)
            {
                transform.parent = Camera.main.transform;
                transform.position = _objectHeld.transform.position - Camera.main.transform.up * _dynamicObjectHeld.EncumbranceRadius;
                transform.localRotation = Quaternion.identity;

                //initialize circle dimensions
                _circleProjector.orthographicSize = _dynamicObjectHeld.EncumbranceRadius + _circleAddedRadius;
                _circleProjector.farClipPlane = _dynamicObjectHeld.EncumbranceRadius + _circleAddedRadius;
            }
            else
            {
                transform.parent = _player.Player.transform;
                transform.position = _objectHeld.transform.position -
                                     _player.Player.transform.up * _dynamicObjectHeld.EncumbranceRadius;
                transform.localRotation = Quaternion.identity;
            }

        }
        else
        {
            //object placed

            transform.parent = null;
            _objectHeld = null;
            _circleProjector.enabled = false;
        }

        UpdateRenderers();

    }

    private void UpdateRenderers()
    {
        if (_objectHeld == null)
        {
            TryEnableHand(false);
            TryEnableOutline(false);
            TryEnableRay(false);
        }
        else
        {
            TryEnableHand(true);
            TryEnableOutline(true);
            TryEnableRay(true);
        }
    }

    private void TryEnableHand(bool enabled)
    {
        if (!enabled || _handIsHidden)
        {
            _handRenderer.enabled = false;
        }
        else 
        {
            if (Visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Hand))
                _handRenderer.enabled = true;
            else
                //I'm trying to enable the hand, but the visualization settings don't require it to be enabled
                //So I disable it
                _handRenderer.enabled = false;
        }
    }

    private void TryEnableOutline(bool enabled)
    {
        if (!enabled)
        {
            if(_addObjectOutlineCoroutine != null) StopCoroutine(_addObjectOutlineCoroutine);
            if (_outline != null)
            {
                _outline.FadeOutAndDestroy();
                //_outline.enabled = false;
                //Destroy(_outline);
                _outline = null;
            }
        }
        else
        {
            if (Visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Outline))
            {
                if (_outline == null)
                {
                    //Add an outline if not already existing on the object
                    if (_objectHeld != null)
                    {
                        CustomOutline outline = _objectHeld.GetComponent<CustomOutline>();
                        //if an outline exists, destroy it
                        if (outline != null)
                        {
                            Destroy(outline);
                            _addObjectOutlineCoroutine = StartCoroutine(AddObjectOutlineCoroutine(_objectHeld));
                        }
                        else
                        {
                            _outline = _objectHeld.AddComponent<CustomOutline>();
                            _outline.OutlineMode = CustomOutline.Mode.OutlineVisible;
                            _outline.OutlineColor = _color;
                            _outline.OutlineWidth = 2f;
                            _outline.enabled = true;
                        }
                    }
                }
                else
                {
                    _outline.enabled = true;
                }
            }
            else
            {
                //I'm trying to enable the outline, but the visualization settings don't require it to be enabled
                //So I disable it, if I can
                if (_outline != null)
                {
                    _outline.enabled = false;
                    Destroy(_outline);
                    _outline = null;
                }
            }
        }
    }

    private void TryEnableRay(bool enabled)
    {
        if (!enabled)
        {
            _rayRenderer.enabled = false;
        }
        else 
        {
            if (Visualization.HasFlag(GameUtils.ObjectManipulationVisualization.Ray))
                _rayRenderer.enabled = true;
            else
                //I'm trying to enable the ray, but the visualization settings don't require it to be enabled
                //So I disable it
                _rayRenderer.enabled = false;

        }
    }

    private void InitializeColor(Color color)
    {
        _handRenderer.material.color = new Color(color.r, color.g, color.b,
            _handRenderer.material.color.a);
        _rayRenderer.startColor = color;
        _rayRenderer.endColor = color;

        if (_outline != null)
        {
            _outline.OutlineColor = _color;
        }
    }

    private void OnColorSetEvent(PlayerInfo player, Color newValue)
    {
        _color = newValue;
        InitializeColor(newValue);
        _player.ColorSetEvent -= OnColorSetEvent;
    }

    private IEnumerator AddObjectOutlineCoroutine(GameObject objectHeld)
    {
        yield return 0;
        if (objectHeld.GetComponent<CustomOutline>() == null)
        {
            _outline = objectHeld.AddComponent<CustomOutline>();
            _outline.OutlineMode = CustomOutline.Mode.OutlineVisible;
            _outline.OutlineColor = _color;
            _outline.OutlineWidth = 2f;
            _outline.FadeIn = false;
            _outline.enabled = true;
        }
    }

    private void OutlineObjectsUnderCursor()
    {
        if (!_isLocal) return;
        if (ObjectManipulation.Instance.InteractionEnabled &&
            _player.GazeManager.HitType != GameUtils.GazeHitType.None && _objectHeld == null &&
            OutlineObjectsWithGazeCursor &&
            GameUIManager.Instance.CurrentSettingsPanel == GameUIManager.SettingsPanel.None)
        {
            //_objectsOutlined contains all objects hit in the previous frame
            //Set all values in _objectsOutlined to false
            foreach (var obj in _objectsOutlined.Keys.ToList())
            {
                _objectsOutlined[obj] = false;
            }

            Collider[] colliders = Physics.OverlapSphere(_player.Gaze.transform.position, _gazeCursorRadius,
                GameUtils.Mask.StaticObjects | GameUtils.Mask.DynamicObjects);
            for (int i = 0; i < colliders.Length; i++)
            {
                GameObject objectHit = colliders[i].gameObject;

                //If an object is hit, update _objectsOutlined adding it if it was not in the dictionary
                //(setting the value to true) or setting the value to true if was already there

                if (objectHit.layer == GameUtils.Layer.StaticObjects && objectHit.CompareTag("Interactive"))
                {
                    if (_objectsOutlined.ContainsKey(objectHit))
                    {
                        _objectsOutlined[objectHit] = true;
                    }
                    else
                    {
                        _objectsOutlined.Add(objectHit, true);
                        AddToOutlinedObjects(objectHit);
                    }
                }
                else if (objectHit.layer == GameUtils.Layer.DynamicObjects)
                {
                    objectHit = GameUtils.GetObjectRoot(objectHit);
                    if (objectHit.GetComponent<DynamicObject>().Owner == null)
                    {
                        if (_objectsOutlined.ContainsKey(objectHit))
                        {
                            _objectsOutlined[objectHit] = true;
                        }
                        else
                        {
                            _objectsOutlined.Add(objectHit, true);
                            AddToOutlinedObjects(objectHit);
                        }
                    }
                }
            }

            //Remove all objects with value of false
            var objectsToRemove = _objectsOutlined.Where(o => o.Value == false).ToArray();
            foreach (var obj in objectsToRemove)
            {
                RemoveFromOutlinedObjects(obj.Key);
                _objectsOutlined.Remove(obj.Key);
            }
        }
        else
        {
            if (_objectsOutlined.Count != 0)
            {
                //Gaze didn't hit anything, remove all objects
                foreach (var obj in _objectsOutlined)
                {
                    RemoveFromOutlinedObjects(obj.Key);
                }
                _objectsOutlined.Clear();
            }
        }

    }

    private void AddToOutlinedObjects(GameObject obj)
    {
        if(!GameUIManager.Instance.InstructionsPanelEnabled) Vibration.VibrateAndroid(200);

        CustomOutline oldOutline = obj.GetComponent<CustomOutline>();
        if (oldOutline == null)
        {
            CustomOutline outline = obj.AddComponent<CustomOutline>();
            outline.OutlineMode = CustomOutline.Mode.OutlineVisible;
            outline.OutlineColor = _color;
            outline.OutlineWidth = _outlineWidth;
            outline.enabled = true;

            _outlines.Add(obj, outline);
        }
    }

    private void RemoveFromOutlinedObjects(GameObject obj)
    {
        if (_outlines.TryGetValue(obj, out CustomOutline outline))
        {
            if (outline != null)
            {
                outline.FadeOutAndDestroy();
                //outline.enabled = false;
                //Destroy(outline);
            }
            _outlines.Remove(obj);
        }
    }

#endregion
}
