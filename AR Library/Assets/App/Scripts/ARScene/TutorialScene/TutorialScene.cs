﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class TutorialScene : IGameScene
{
    public TutorialScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.TutorialSceneContraption == null)
                _networkManager.SpawnStaticObject(_gameManager.TutorialSceneContraptionPrefab.name, _networkManager.CurrentRoom.ContraptionPose);
            else
                _networkManager.CurrentRoom.TutorialSceneContraption.Reset();

            if (_networkManager.CurrentRoom.TutorialSceneCylinderAzathoth == null)
                _networkManager.SpawnDynamicObject(_gameManager.TutorialSceneCylinderAzathothPrefab.name, _networkManager.CurrentRoom.AzathothCylinderPose);
            else
                _networkManager.CurrentRoom.TutorialSceneCylinderAzathoth.Reset();

            if (_networkManager.CurrentRoom.TutorialSceneCylinderCthugha == null)
                _networkManager.SpawnDynamicObject(_gameManager.TutorialSceneCylinderCthughaPrefab.name, _networkManager.CurrentRoom.CthughaCylinderPose);
            else
                _networkManager.CurrentRoom.TutorialSceneCylinderCthugha.Reset();

            if (_networkManager.CurrentRoom.TutorialSceneCylinderYogSothoth == null)
                _networkManager.SpawnDynamicObject(_gameManager.TutorialSceneCylinderYogSothothPrefab.name, _networkManager.CurrentRoom.YogSothothCylinderPose);
            else
                _networkManager.CurrentRoom.TutorialSceneCylinderYogSothoth.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TutorialScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        ObjectManipulation.Instance.InteractionEnabled = false;
        //HintManager.Instance.HintsSystemEnabled = true;
        //HintManager.Instance.AddHint("Focus your gaze on the sphere on the top of the strange contraption.");
        bool completed;
        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop,
                out completed) && !completed)
        {
            Debug.Log("EnableGazeConvergenceFocalPoint");
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop);
            yield return new WaitUntil(() => _networkManager.CurrentRoom.GetGazeConvergenceStatus(
                                                 GazeConvergenceFocalPoint.Type.ContraptionTutorialTop,
                                                 out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop);
        }

        ObjectManipulation.Instance.InteractionEnabled = true;

        /*HintManager.Instance.AddHint(
            "Find the three cylinders embedded in the vertical edges of the contraption, " +
            "open them sliding the golden ring " +
            "and place them in the respective sockets over the top of the contraption.");*/

        /*yield return new WaitUntil(() =>
            (_networkManager.InRoom &&
             _networkManager.CurrentRoom.TutorialSceneContraption.AzathothCylinderIn &&
             _networkManager.CurrentRoom.TutorialSceneContraption.CthughaCylinderIn &&
             _networkManager.CurrentRoom.TutorialSceneContraption.YogSothothCylinderIn));*/

        //HintManager.Instance.AddHint("Push the three embedded cylinders at the same time for some seconds in order to open the contraption.");

        yield return new WaitUntil(() =>
            (_networkManager.InRoom &&
             _networkManager.CurrentRoom.TutorialSceneContraption.State > TutorialSceneContraption.ContraptionState.Start));

        //HintManager.Instance.HintsSystemEnabled = false;

        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening);
            yield return new WaitUntil(() => _networkManager.CurrentRoom.GetGazeConvergenceStatus(
                                                 GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening,
                                                 out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening);
        }

        if (_networkManager.IsMasterClient &&
            _networkManager.CurrentRoom.TutorialSceneContraption.State == TutorialSceneContraption.ContraptionState.FirstOpen)
        {
            _networkManager.CurrentRoom.TutorialSceneContraption.State = TutorialSceneContraption.ContraptionState.SecondOpen;
        }

        yield return new WaitForSeconds(8f);

        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/SceneCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public override void Destroy()
    {
        base.Destroy();
        if (FocalPointManager.Instance != null) FocalPointManager.Instance.DisableCurrentGazeConvergenceFocalPoint();
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneContraption != null)
            PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneContraption.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneCylinderAzathoth != null)
            PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneCylinderAzathoth.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneCylinderCthugha != null)
            PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneCylinderCthugha.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneCylinderYogSothoth != null)
            PhotonNetworkManager.Instance.CurrentRoom.TutorialSceneCylinderYogSothoth.Enable(enabled);
    }
}
