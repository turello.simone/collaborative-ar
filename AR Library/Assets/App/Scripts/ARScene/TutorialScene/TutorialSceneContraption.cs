﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TutorialSceneContraption : StaticObject
{

    public enum ContraptionState : int
    {
        Default,
        Start,
        FirstOpen,
        SecondOpen
    }

    [Header("Contraption properties")]

    public GameObject AzathothCylinderBase;
    public GameObject CthughaCylinderBase;
    public GameObject YogSothothCylinderBase;

    [MustBeAssigned] public GameObject AzathothCylinderEmbeddingPosition;
    [MustBeAssigned] public GameObject CthughaCylinderEmbeddingPosition;
    [MustBeAssigned] public GameObject YogSothothCylinderEmbeddingPosition;

    [MustBeAssigned] public GameObject AzathothButtonTop;
    [MustBeAssigned] public GameObject CthughaButtonTop;
    [MustBeAssigned] public GameObject YogSothothButtonTop;

    [MustBeAssigned] public GameObject FocalPointOnTop;
    [MustBeAssigned] public GameObject FocalPointOpening;

    [MustBeAssigned] public Animator Animator;

    //Variables to be synchronized
    public ContraptionState State
    {
        get { return (ContraptionState)_state.Value; }
        set { _state.Value = (int)value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;
    private readonly int _stateHash = Animator.StringToHash("ContraptionState");

    //CylinderOut variables are automatically synchronized by Cylinder script 
    public bool AzathothCylinderOut
    {
        get { return _azathothCylinderOut; }
        set
        {
            _azathothCylinderOut = value;
            Animator.SetBool(_azathothCylinderOutHash, value);
        }
    }
    private bool _azathothCylinderOut;
    private readonly int _azathothCylinderOutHash = Animator.StringToHash("AzathothCylinderOut");
    private readonly int _azathothCylinderInHash = Animator.StringToHash("AzathothCylinderIn");

    public bool CthughaCylinderOut
    {
        get { return _cthughaCylinderOut; }
        set
        {
            _cthughaCylinderOut = value;
            Animator.SetBool(_cthughaCylinderOutHash, value);
        }
    }
    private bool _cthughaCylinderOut;
    private readonly int _cthughaCylinderOutHash = Animator.StringToHash("CthughaCylinderOut");
    private readonly int _cthughaCylinderInHash = Animator.StringToHash("CthughaCylinderIn");

    public bool YogSothothCylinderOut
    {
        get { return _yogSothothCylinderOut; }
        set
        {
            _yogSothothCylinderOut = value;
            Animator.SetBool(_yogSothothCylinderOutHash, value);
        }
    }
    private bool _yogSothothCylinderOut;
    private readonly int _yogSothothCylinderOutHash = Animator.StringToHash("YogSothothCylinderOut");
    private readonly int _yogSothothCylinderInHash = Animator.StringToHash("YogSothothCylinderIn");

    public SyncButton AzathothButton;
    private readonly int _azathothButtonPressedHash = Animator.StringToHash("AzathothButtonPressed");

    public SyncButton CthughaButton;
    private readonly int _cthughaButtonPressedHash = Animator.StringToHash("CthughaButtonPressed");

    public SyncButton YogSothothButton;
    private readonly int _yogSothothButtonPressedHash = Animator.StringToHash("YogSothothButtonPressed");

    public float OpeningProgress
    {
        get => _openingProgress.Value;
        set => _openingProgress.Value = value;
    }
    private SyncVarFloatNotBuffered _openingProgress;
    private readonly int _openingProgressHash = Animator.StringToHash("OpeningProgress");


    private float _openingVelocity = 0.25f;


    #region Unity

    protected override void Awake()
    {
        base.Awake();

        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _state = new SyncVarInt((int)ContraptionState.Start, _photonView, STATEID);
        OnContraptionStateChange((int)ContraptionState.Start);
        _state.ValueChanged += OnContraptionStateChange;

        AzathothButton = new SyncButton(AzathothButtonTop);
        AzathothButton.Pushed += n => Animator.SetBool(_azathothButtonPressedHash, true);
        AzathothButton.Released += n => Animator.SetBool(_azathothButtonPressedHash, false);
        Animator.SetBool(_azathothButtonPressedHash, false);

        CthughaButton = new SyncButton(CthughaButtonTop);
        CthughaButton.Pushed += n => Animator.SetBool(_cthughaButtonPressedHash, true);
        CthughaButton.Released += n => Animator.SetBool(_cthughaButtonPressedHash, false);
        Animator.SetBool(_cthughaButtonPressedHash, false);

        YogSothothButton = new SyncButton(YogSothothButtonTop);
        YogSothothButton.Pushed += n => Animator.SetBool(_yogSothothButtonPressedHash, true);
        YogSothothButton.Released += n => Animator.SetBool(_yogSothothButtonPressedHash, false);
        Animator.SetBool(_yogSothothButtonPressedHash, false);

        _openingProgress = new SyncVarFloatNotBuffered(0, _photonView);

        _networkManager.CurrentRoom.TutorialSceneContraption = this;

        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop, FocalPointOnTop);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening, FocalPointOpening);
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop, FocalPointOnTop);

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening, FocalPointOpening);

        Animator.SetInteger(_stateHash, 0);
        Animator.SetFloat(_openingProgressHash, 0);
        Animator.SetBool(_azathothCylinderOutHash, false);
        Animator.SetBool(_azathothCylinderInHash, false);
        Animator.SetBool(_cthughaCylinderOutHash, false);
        Animator.SetBool(_cthughaCylinderInHash, false);
        Animator.SetBool(_yogSothothCylinderOutHash, false);
        Animator.SetBool(_yogSothothCylinderInHash, false);
        Animator.SetBool(_azathothButtonPressedHash, false);
        Animator.SetBool(_cthughaButtonPressedHash, false);
        Animator.SetBool(_yogSothothButtonPressedHash, false);

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.ContraptionTutorialTop);
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.ContraptionTutorialOpening);
            State = ContraptionState.Start;
            AzathothButton.Release(true);
            CthughaButton.Release(true);
            YogSothothButton.Release(true);
            OpeningProgress = 0;
        }
    }

    public void Update()
    {
        if (State == ContraptionState.Start)
        {
            if (_networkManager.IsMasterClient)
            {
                //Update OpeningProgress
                int numberOfButtonsPushed = 0;
                if (AzathothButton.IsPushed)
                {
                    numberOfButtonsPushed++;
                }
                if (CthughaButton.IsPushed)
                {
                    numberOfButtonsPushed++;
                }
                if (YogSothothButton.IsPushed)
                {
                    numberOfButtonsPushed++;
                }

                if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
                {
                    switch (numberOfButtonsPushed)
                    {
                        case 0:
                            OpeningProgress = Mathf.Clamp(OpeningProgress - _openingVelocity * Time.deltaTime, 0f, 1f);
                            break;
                        case 1:
                        case 2:
                        case 3:
                            OpeningProgress = Mathf.Clamp(OpeningProgress + _openingVelocity * Time.deltaTime, 0, 1f);
                            break;
                    }

                }
                else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
                {
                    switch (numberOfButtonsPushed)
                    {
                        case 0:
                            OpeningProgress = Mathf.Clamp(OpeningProgress - _openingVelocity * Time.deltaTime, 0f, 1f);
                            break;
                        case 1:
                            if (OpeningProgress > 0.33f)
                            {
                                OpeningProgress = Mathf.Clamp(OpeningProgress - _openingVelocity * Time.deltaTime, 0.33f, 1f);
                            }
                            else
                            {
                                OpeningProgress = Mathf.Clamp(OpeningProgress + _openingVelocity * Time.deltaTime, 0, 0.33f);
                            }
                            break;
                        case 2:
                            if (OpeningProgress > 0.66f)
                            {
                                OpeningProgress = Mathf.Clamp(OpeningProgress - _openingVelocity * Time.deltaTime, 0.66f, 1f);
                            }
                            else
                            {
                                OpeningProgress = Mathf.Clamp(OpeningProgress + _openingVelocity * Time.deltaTime, 0, 0.66f);
                            }
                            break;
                        case 3:
                            OpeningProgress = Mathf.Clamp(OpeningProgress + _openingVelocity * Time.deltaTime, 0, 1f);
                            break;
                    }
                }

                if (OpeningProgress >= 1.0)
                {
                    State = ContraptionState.FirstOpen;
                }
            }
        }

        if (State == ContraptionState.Start || State == ContraptionState.FirstOpen)
        {
            Animator.SetFloat(_openingProgressHash, OpeningProgress);
        }
    }


    #endregion

    #region public methods

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (State == ContraptionState.Start)
        {
            if (interactiveObject == AzathothButtonTop)
            {
                if(isPressed) AzathothButton.Push();
                else AzathothButton.Release();
            }
            else if (interactiveObject == CthughaButtonTop)
            {
                if (isPressed) CthughaButton.Push();
                else CthughaButton.Release();
            }
            else if (interactiveObject == YogSothothButtonTop)
            {
                if (isPressed) YogSothothButton.Push();
                else YogSothothButton.Release();
            }

            if (isPressed) AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.ButtonPressed);
            else AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.ButtonPressedUp);
        }
    }

    public void CloseCylinderIn(TutorialSceneCylinder.CylinderTypeEnum type)
    {
        AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.MetalDrag);
        switch (type)
        {
            case TutorialSceneCylinder.CylinderTypeEnum.Azathoth:
                Animator.SetTrigger(_azathothCylinderInHash);
                StartCoroutine(WaitAndEnableButton(AzathothButtonTop));
                break;
            case TutorialSceneCylinder.CylinderTypeEnum.Cthugha:
                Animator.SetTrigger(_cthughaCylinderInHash);
                StartCoroutine(WaitAndEnableButton(CthughaButtonTop));
                break;
            case TutorialSceneCylinder.CylinderTypeEnum.YogSothoth:
                Animator.SetTrigger(_yogSothothCylinderInHash);
                StartCoroutine(WaitAndEnableButton(YogSothothButtonTop));
                break;
        }
    }
    #endregion

    #region private methods

    private void OnContraptionStateChange(int value)
    {
        Animator.SetInteger(_stateHash, value);

        switch ((ContraptionState) value)
        {
            case ContraptionState.Start:
                AzathothButtonTop.RenderersEnabled(false);
                AzathothButtonTop.CollidersEnabled(false);
                CthughaButtonTop.RenderersEnabled(false);
                CthughaButtonTop.CollidersEnabled(false);
                YogSothothButtonTop.RenderersEnabled(false);
                YogSothothButtonTop.CollidersEnabled(false);
                break;
            case ContraptionState.FirstOpen:
                AzathothButtonTop.CollidersEnabled(false);
                CthughaButtonTop.CollidersEnabled(false);
                YogSothothButtonTop.CollidersEnabled(false);

                Animator.SetBool(_azathothButtonPressedHash, true);
                Animator.SetBool(_cthughaButtonPressedHash, true);
                Animator.SetBool(_yogSothothButtonPressedHash, true);
                break;
            case ContraptionState.SecondOpen:
                Animator.SetBool(_azathothButtonPressedHash, true);
                Animator.SetBool(_cthughaButtonPressedHash, true);
                Animator.SetBool(_yogSothothButtonPressedHash, true);
                break;
        }
    }

    private IEnumerator WaitAndEnableButton(GameObject button)
    {
        yield return new WaitForSeconds(1);
        button.RenderersEnabled(true);
        button.CollidersEnabled(true);
    }

    #endregion

}
