﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class TutorialSceneCylinder : DynamicObject
{

    public enum CylinderState
    {
        Default,
        Embedded1,
        Closed,
        Open,
        Embedded2
    }

    public enum CylinderTypeEnum
    {
        Azathoth,
        Cthugha,
        YogSothoth
    }

    #region Properties

    [Header("Cylinder properties")]
    public CylinderTypeEnum CylinderType;
    [MustBeAssigned] public GameObject Ring;
    public float RingRotationSpeed = 0.001f;
    [MustBeAssigned] public Collider BodyCollider;
    [MustBeAssigned] public Animator Animator;

    //Variables to be synchronized

    public CylinderState State
    {
        get { return (CylinderState)_state.Value; }
        set { _state.Value = (int)value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;

    public float UnlockProgress
    {
        get => _unlockProgress.Value;
        set => _unlockProgress.Value = value;
    }
    private SyncVarFloatNotBuffered _unlockProgress;
    private readonly int _unlockProgressHash = Animator.StringToHash("UnlockProgress");

    private readonly int _embedTriggerHash = Animator.StringToHash("Embed");

    private Coroutine _checkEmbeddingCoroutine;

    #endregion

    #region Unity

    protected override void Awake()
    {
        base.Awake();

        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();

        _unlockProgress = new SyncVarFloatNotBuffered(0, _photonView);

        AnchorOnSpawn = false;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        _state = new SyncVarInt((int)CylinderState.Embedded1, _photonView, STATEID);
        OnCylinderStateChange((int)CylinderState.Embedded1);
        _state.ValueChanged += OnCylinderStateChange;

        switch (CylinderType)
        {
            case CylinderTypeEnum.Azathoth:
                _networkManager.CurrentRoom.TutorialSceneCylinderAzathoth = this;
                break;
            case CylinderTypeEnum.Cthugha:
                _networkManager.CurrentRoom.TutorialSceneCylinderCthugha = this;
                break;
            case CylinderTypeEnum.YogSothoth:
                _networkManager.CurrentRoom.TutorialSceneCylinderYogSothoth = this;
                break;
        }
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        m_PositionModel.SynchronizeEnabled = true;
        m_RotationModel.SynchronizeEnabled = true;

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();
        Animator.SetFloat(_unlockProgressHash, 0);

        if (_photonView.IsMine)
            UnlockProgress = 0;

        if (_networkManager.IsMasterClient)
        {
            State = CylinderState.Embedded1;
        }
    }

    protected override void Update()
    {
        base.Update();

        if (State == CylinderState.Closed && Owner == _networkManager.LocalPlayer && UnlockProgress >= 0.8f)
        {
            Sequence s = DOTween.Sequence();
            s.Append(DOTween.To((x) => UnlockProgress = x, UnlockProgress, 1.0f, 0.5f))
                .AppendCallback(() => _photonView.RPC("OpenRPC", RpcTarget.AllBuffered));
            State = CylinderState.Open;
        }

        if (State >= CylinderState.Closed && UnlockProgress < 1)
        {
            Animator.SetFloat(_unlockProgressHash, UnlockProgress);
        }
    }

    #endregion

    #region public methods 

    public override void Grab()
    {
        if (State == CylinderState.Embedded1 || State == CylinderState.Closed || State == CylinderState.Open)
        {
            base.Grab();
        }
    }

    public override void Place(Vector3 restPosition, Quaternion restRotation)
    {
        if (Owner == _networkManager.LocalPlayer)
        {
            base.Place(restPosition, restRotation);
            BodyCollider.enabled = true;
        }
    }

    public override void SwipeOnInteractiveObject(GameObject interactiveObject, Vector2 screenPos, Vector2 deltaMovement)
    {
        if (Owner == _networkManager.LocalPlayer && State == CylinderState.Closed && interactiveObject == Ring)
        {
            Vector3 movement = Camera.main.transform.TransformDirection(deltaMovement.x,
                deltaMovement.y, 0);

            UnlockProgress =
                Mathf.Clamp(
                    UnlockProgress +
                    Vector3.Dot(Vector3.Cross(movement, Camera.main.transform.forward), -Ring.transform.forward) *
                    RingRotationSpeed, 0.0f, 1.0f);
            AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.MetalClunk);
        }
    }

    #endregion

    #region private / protected methods

    protected override void OnPlayerSynchronization()
    {
        if (State == CylinderState.Closed || State == CylinderState.Open || State == CylinderState.Embedded2)
        {
            base.OnPlayerSynchronization();
        }
    }

    private IEnumerator CheckEmbeddingPosition()
    {
        float distanceThreshold = 0.05f;
        float rotationThreshold = 0.1f;
        float timer = 0f;
        float embeddingRequiredTime = 1f;
        GameObject reference = null;

        switch (CylinderType)
        {
            case CylinderTypeEnum.Azathoth:
                reference = _networkManager.CurrentRoom.TutorialSceneContraption.AzathothCylinderEmbeddingPosition;
                break;
            case CylinderTypeEnum.Cthugha:
                reference = _networkManager.CurrentRoom.TutorialSceneContraption.CthughaCylinderEmbeddingPosition;
                break;
            case CylinderTypeEnum.YogSothoth:
                reference = _networkManager.CurrentRoom.TutorialSceneContraption.YogSothothCylinderEmbeddingPosition;
                break;
        }

        while (true)
        {
            if (Owner == _networkManager.LocalPlayer && Vector3.Distance(transform.position, reference.transform.position) < distanceThreshold &&
                Mathf.Abs(Vector3.Dot(transform.up, reference.transform.up) - 1) < rotationThreshold)
            {
                timer += Time.deltaTime;

                if (timer >= embeddingRequiredTime)
                {
                    State = CylinderState.Embedded2;
                    break;
                }
            }
            else
            {
                timer = 0f;
            }
            yield return 0;

        }

    }

    private IEnumerator FinalPosition()
    {
        m_PositionModel.SynchronizeEnabled = false;
        m_RotationModel.SynchronizeEnabled = false;

        DynamicObjectState = GameUtils.DynamicObjectState.Placed;
        Owner = null;
        this.transform.parent = _networkManager.CurrentRoom.TutorialSceneContraption.transform;

        Vector3 startPos = transform.position;
        Quaternion startRot = transform.rotation;
        float speed = 2f;
        float percentage = 0f;

        GameObject reference = null;

        switch (CylinderType)
        {
            case CylinderTypeEnum.Azathoth:
                reference = _networkManager.CurrentRoom.TutorialSceneContraption.AzathothCylinderEmbeddingPosition;
                break;
            case CylinderTypeEnum.Cthugha:
                reference = _networkManager.CurrentRoom.TutorialSceneContraption.CthughaCylinderEmbeddingPosition;
                break;
            case CylinderTypeEnum.YogSothoth:
                reference = _networkManager.CurrentRoom.TutorialSceneContraption.YogSothothCylinderEmbeddingPosition;
                break;
        }

        while (percentage < 1)
        {
            percentage += Time.deltaTime * speed;
            transform.position = Vector3.Lerp(startPos, reference.transform.position, percentage);
            transform.rotation = Quaternion.Lerp(startRot, reference.transform.rotation, percentage);
            Dummy.localPosition = Vector3.Lerp(-RotationCenter, Vector3.zero, percentage);
            yield return 0;
        }

        Animator.SetTrigger(_embedTriggerHash);

        yield return new WaitForSeconds(1);
        _networkManager.CurrentRoom.TutorialSceneContraption.CloseCylinderIn(CylinderType);
        yield return new WaitForSeconds(1);
        gameObject.RenderersEnabled(false);
        gameObject.CollidersEnabled(false);
    }


    private void OnCylinderStateChange(int value)
    {
        switch ((CylinderState) value)
        {
            case CylinderState.Embedded1:
                gameObject.RenderersEnabled(true);
                gameObject.CollidersEnabled(true);

                Dummy.localPosition = -RotationCenter;
                switch (CylinderType)
                {
                    case CylinderTypeEnum.Azathoth:
                        transform.parent = _networkManager.CurrentRoom.TutorialSceneContraption.AzathothCylinderBase.transform;
                        transform.localPosition = Vector3.zero;
                        transform.localRotation = Quaternion.identity;
                        _networkManager.CurrentRoom.TutorialSceneContraption.AzathothCylinderOut = false;
                        break;
                    case CylinderTypeEnum.Cthugha:
                        transform.parent = _networkManager.CurrentRoom.TutorialSceneContraption.CthughaCylinderBase.transform;
                        transform.localPosition = Vector3.zero;
                        transform.localRotation = Quaternion.identity;
                        _networkManager.CurrentRoom.TutorialSceneContraption.CthughaCylinderOut = false;
                        break;
                    case CylinderTypeEnum.YogSothoth:
                        transform.parent = _networkManager.CurrentRoom.TutorialSceneContraption.YogSothothCylinderBase.transform;
                        transform.localPosition = Vector3.zero;
                        transform.localRotation = Quaternion.identity;
                        _networkManager.CurrentRoom.TutorialSceneContraption.YogSothothCylinderOut = false;
                        break;
                }
                break;
            case CylinderState.Closed:
                if(Owner != _networkManager.LocalPlayer)
                    transform.parent = null;
                switch (CylinderType)
                {
                    case CylinderTypeEnum.Azathoth:
                        _networkManager.CurrentRoom.TutorialSceneContraption.AzathothCylinderOut = true;
                        break;
                    case CylinderTypeEnum.Cthugha:
                        _networkManager.CurrentRoom.TutorialSceneContraption.CthughaCylinderOut = true;
                        break;
                    case CylinderTypeEnum.YogSothoth:
                        _networkManager.CurrentRoom.TutorialSceneContraption.YogSothothCylinderOut = true;
                        break;
                }
                break;
            case CylinderState.Open:
                _checkEmbeddingCoroutine = StartCoroutine(CheckEmbeddingPosition());
                //disable ring collider
                Ring.GetComponent<Collider>().enabled = false;
                break;
            case CylinderState.Embedded2:
                Animator.SetFloat(_unlockProgressHash, 1);
                StopCoroutine(_checkEmbeddingCoroutine);
                StartCoroutine(FinalPosition());
                break;
        }
    }

    #endregion

    #region PUN RPCs

    [PunRPC]
    public override void GrabRPC(Vector3 startPosition, Quaternion startRotation, PhotonMessageInfo info)
    {
        base.GrabRPC(startPosition, startRotation, info);

        if (State == CylinderState.Embedded1)
        {
            State = CylinderState.Closed;
        }

        if (Owner == _networkManager.LocalPlayer)
        {
            BodyCollider.enabled = false;
        }
    }

    [PunRPC]
    public override void PlaceRPC(Vector3 restPosition, Quaternion restRotation)
    {
        base.PlaceRPC(restPosition, restRotation);
        BodyCollider.enabled = true;
    }

    [PunRPC]
    public void OpenRPC()
    {
        Animator.SetFloat(_unlockProgressHash, 1);
    }

    #endregion

}
