﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyBox;
using MyUtils;
using UnityEngine;

public class SubSceneManager : SingletonInScene<SubSceneManager>
{
    #region enums and structs

    public struct RoomScene
    {
        public SubSceneType Type;
        public TaskInfo TaskInfo; //only useful when Type == Task

        public RoomScene(SubSceneType type)
        {
            Type = type;
            TaskInfo = new TaskInfo();
        }

        public RoomScene(SubSceneType type, TaskInfo taskInfo)
        {
            Type = type;
            TaskInfo = taskInfo;
        }

        public static readonly byte RegistrationTypeCode = 254;

        public static object Deserialize(byte[] data)
        {
            var scene = new RoomScene((SubSceneType) data[0],
                new TaskInfo((Interaction) data[1], (InteractionFocus) data[2], (Input) data[3],
                    (Visualization) data[4], (Location) data[5], (VisualizationSequence) data[6]));
            return scene;
        }

        public static byte[] Serialize(object roomScene)
        {
            var scene = (RoomScene) roomScene;
            return new byte[]
            {
                (byte) scene.Type,
                (byte) scene.TaskInfo.Interaction,
                (byte) scene.TaskInfo.InteractionFocus,
                (byte) scene.TaskInfo.Input,
                (byte) scene.TaskInfo.Visualization,
                (byte) scene.TaskInfo.Location,
                (byte) scene.TaskInfo.VisualizationSequence
            };
        }

    }

    public enum SubSceneType : byte
    {
        Uninitialized,
        InitialPrep,
        Training,
        TrainingGaze,
        TrainingManip,
        TrainingPos,
        Tutorial,
        Pedestal,
        Painting,
        Translation,
        Task
    }

    public enum Interaction : byte
    {
        Uninitialized,
        Gaze,
        Manipulation,
        Position
    }

    public enum InteractionFocus : byte
    {
        Uninitialized,
        SameObject,
        DifferentObjects
    }

    public enum Input : byte
    {
        Uninitialized,
        SystemCommanded,
        UserCommanded
    }

    public enum Visualization : byte
    {
        Uninitialized,
        Minimal,
        Base,
        Complete
    }

    public enum Location : byte
    {
        Uninitialized,
        Local,
        Remote
    }

    public enum VisualizationSequence : byte
    {
        Uninitialized,
        V1_123,
        V2_132,
        V3_213,
        V4_231,
        V5_312,
        V6_321,
        V7_23,
        V8_32
    }

    public struct TaskInfo
    {
        public Interaction Interaction;
        public InteractionFocus InteractionFocus;
        public Input Input;
        public Visualization Visualization;
        public Location Location;
        public VisualizationSequence VisualizationSequence;

        public TaskInfo(Interaction interaction, InteractionFocus interactionFocus, Input input,
            Visualization visualization, Location location, VisualizationSequence visualizationSequence)
        {
            Interaction = interaction;
            InteractionFocus = interactionFocus;
            Input = input;
            Visualization = visualization;
            Location = location;
            VisualizationSequence = visualizationSequence;
        }
    }

    #endregion

    private readonly Visualization[] _visualizations = {Visualization.Minimal, Visualization.Base, Visualization.Complete};
    private readonly Visualization[] _visualizationsShort = {Visualization.Base, Visualization.Complete};

    private readonly SubSceneType[] _gameSceneSequence =
    {
        SubSceneType.Tutorial,
        SubSceneType.Pedestal,
        SubSceneType.Painting,
        SubSceneType.Translation
    };

    private readonly TaskInfo[] _gazeTaskSequence =
    {
        new TaskInfo(Interaction.Gaze, InteractionFocus.SameObject, Input.SystemCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
        new TaskInfo(Interaction.Gaze, InteractionFocus.DifferentObjects, Input.SystemCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
        new TaskInfo(Interaction.Gaze, InteractionFocus.DifferentObjects, Input.UserCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
    };

    private readonly TaskInfo[] _manipulationTaskSequence =
    {
        new TaskInfo(Interaction.Manipulation, InteractionFocus.DifferentObjects, Input.SystemCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
        new TaskInfo(Interaction.Manipulation, InteractionFocus.DifferentObjects, Input.UserCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
    };

    private readonly TaskInfo[] _positionTaskSequence =
    {
        new TaskInfo(Interaction.Position, InteractionFocus.SameObject, Input.SystemCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
        new TaskInfo(Interaction.Position, InteractionFocus.DifferentObjects, Input.SystemCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
        new TaskInfo(Interaction.Position, InteractionFocus.DifferentObjects, Input.UserCommanded,
            Visualization.Uninitialized, Location.Uninitialized, VisualizationSequence.Uninitialized),
    };

    private ISubScene _currentSubScene;
    private List<RoomScene> _initializedScenes;

    #region Unity

    public override void Awake()
    {
        base.Awake();

        _currentSubScene = null;
        _initializedScenes = new List<RoomScene>();
    }

    #endregion

    #region public methods

    public void LoadSubScene(RoomScene scene)
    {
        UnloadCurrentSubScene();
        DisableAllSceneObjectsExceptCurrent(scene);

        string sceneName;
        switch (scene.Type)
        {
            case SubSceneType.Training:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/IntroName");
                _currentSubScene = new TrainingScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.TrainingGaze:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/GazeName");
                _currentSubScene = new TrainingGazeScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.TrainingManip:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/ManipName");
                _currentSubScene = new TrainingManipScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.TrainingPos:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TrainingScene/PosName");
                _currentSubScene = new TrainingPosScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.Tutorial:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TutorialScene/Name");
                _currentSubScene = new TutorialScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.Pedestal:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PedestalScene/Name");
                _currentSubScene = new PedestalScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.Painting:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PaintingScene/Name");
                _currentSubScene = new PaintingScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.Translation:
                sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TranslationScene/Name");
                _currentSubScene = new TranslationScene(sceneName, scene.TaskInfo.Visualization, scene.TaskInfo.Location,
                    IsSceneInitialized(scene));
                break;
            case SubSceneType.Task:
                LoadTask(scene);
                break;
        }

    }

    public void UnloadCurrentSubScene()
    {
        if (_currentSubScene != null)
        {
            _currentSubScene.Destroy();
            _currentSubScene = null;
        }
    }

    public void DisableAllSceneObjectsExceptCurrent(RoomScene scene)
    {
        if (scene.Type != SubSceneType.Training) TrainingScene.EnableObjects(false);
        if (scene.Type != SubSceneType.TrainingGaze) TrainingGazeScene.EnableObjects(false);
        if (scene.Type != SubSceneType.TrainingManip) TrainingManipScene.EnableObjects(false);
        if (scene.Type != SubSceneType.TrainingPos) TrainingPosScene.EnableObjects(false);

        if (scene.Type != SubSceneType.Tutorial) TutorialScene.EnableObjects(false);
        if (scene.Type != SubSceneType.Pedestal) PedestalScene.EnableObjects(false);
        if (scene.Type != SubSceneType.Painting) PaintingScene.EnableObjects(false);
        if (scene.Type != SubSceneType.Translation) TranslationScene.EnableObjects(false);

        if (scene.Type != SubSceneType.Task || 
            scene.TaskInfo.Interaction != Interaction.Gaze ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.SameObject ||
            scene.TaskInfo.Input != Input.SystemCommanded) 
            TaskGazeSameSys.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Gaze ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.SameObject ||
            scene.TaskInfo.Input != Input.UserCommanded) 
            TaskGazeSameUsr.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Gaze ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.DifferentObjects ||
            scene.TaskInfo.Input != Input.SystemCommanded)
            TaskGazeDiffSys.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Gaze ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.DifferentObjects ||
            scene.TaskInfo.Input != Input.UserCommanded)
            TaskGazeDiffUsr.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Manipulation ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.DifferentObjects ||
            scene.TaskInfo.Input != Input.SystemCommanded)
            TaskManipDiffSys.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Manipulation ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.DifferentObjects ||
            scene.TaskInfo.Input != Input.UserCommanded)
            TaskManipDiffUsr.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Position ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.SameObject ||
            scene.TaskInfo.Input != Input.SystemCommanded)
            TaskPosSameSys.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Position ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.SameObject ||
            scene.TaskInfo.Input != Input.UserCommanded)
            TaskPosSameUsr.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Position ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.DifferentObjects ||
            scene.TaskInfo.Input != Input.SystemCommanded)
            TaskPosDiffSys.EnableObjects(false);
        if (scene.Type != SubSceneType.Task ||
            scene.TaskInfo.Interaction != Interaction.Position ||
            scene.TaskInfo.InteractionFocus != InteractionFocus.DifferentObjects ||
            scene.TaskInfo.Input != Input.UserCommanded)
            TaskPosDiffUsr.EnableObjects(false);
    }

    /*public TaskInfo GetFirstInTaskSequence(Interaction interaction, Location location, VisualizationSequence visualizationSequence)
    {
        return GetTaskInSequence(interaction, location, visualizationSequence, 0);
    }*/

    public RoomScene GetNextSceneInSequence(RoomScene currentScene)
    {
        if (currentScene.Type == SubSceneType.Tutorial ||
            currentScene.Type == SubSceneType.Pedestal ||
            currentScene.Type == SubSceneType.Painting ||
            currentScene.Type == SubSceneType.Translation)
        {
            int currentIndex = _gameSceneSequence.IndexOfItem(currentScene.Type);

            if (currentIndex == -1 || (currentIndex + 1) >= _gameSceneSequence.Length)
            {
                return new RoomScene();
            }
            else
            {
                RoomScene scene = new RoomScene(_gameSceneSequence[currentIndex + 1]);
                scene.TaskInfo.Visualization = currentScene.TaskInfo.Visualization;
                return scene;
            }
        }
        else if (currentScene.Type == SubSceneType.Task)
        {
            int numberOfTasksInInteraction = 0;
            switch (currentScene.TaskInfo.Interaction)
            {
                case Interaction.Gaze:
                    numberOfTasksInInteraction = _gazeTaskSequence.Length;
                    break;
                case Interaction.Manipulation:
                    numberOfTasksInInteraction = _manipulationTaskSequence.Length;
                    break;
                case Interaction.Position:
                    numberOfTasksInInteraction = _positionTaskSequence.Length;
                    break;
            }

            if (currentScene.TaskInfo.VisualizationSequence < VisualizationSequence.V7_23)
            {
                for (int index = 0; index < (_visualizations.Length * numberOfTasksInInteraction) - 1; index++)
                {
                    TaskInfo taskInfo = GetTaskInSequence(currentScene.TaskInfo.Interaction, currentScene.TaskInfo.Location,
                        currentScene.TaskInfo.VisualizationSequence, index);

                    if (taskInfo.InteractionFocus == currentScene.TaskInfo.InteractionFocus &&
                        taskInfo.Input == currentScene.TaskInfo.Input &&
                        taskInfo.Visualization == currentScene.TaskInfo.Visualization)
                    {
                        return new RoomScene(SubSceneType.Task,
                            GetTaskInSequence(currentScene.TaskInfo.Interaction, currentScene.TaskInfo.Location,
                                currentScene.TaskInfo.VisualizationSequence, index + 1));
                    }
                }
            }
            else
            {
                for (int index = 0; index < (_visualizationsShort.Length * numberOfTasksInInteraction) - 1; index++)
                {
                    TaskInfo taskInfo = GetTaskInSequence(currentScene.TaskInfo.Interaction, currentScene.TaskInfo.Location,
                        currentScene.TaskInfo.VisualizationSequence, index);

                    if (taskInfo.InteractionFocus == currentScene.TaskInfo.InteractionFocus &&
                        taskInfo.Input == currentScene.TaskInfo.Input &&
                        taskInfo.Visualization == currentScene.TaskInfo.Visualization)
                    {
                        return new RoomScene(SubSceneType.Task,
                            GetTaskInSequence(currentScene.TaskInfo.Interaction, currentScene.TaskInfo.Location,
                                currentScene.TaskInfo.VisualizationSequence, index + 1));
                    }
                }
            }

        }

        return new RoomScene();
    }

    #endregion

    #region private methods

    private void LoadTask(RoomScene scene)
    {
        string sceneName;
        switch (scene.TaskInfo.Interaction)
        {
            case Interaction.Gaze:
                if (scene.TaskInfo.InteractionFocus == InteractionFocus.SameObject)
                {
                    if (scene.TaskInfo.Input == Input.SystemCommanded)
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeSameSys/Name");
                        _currentSubScene = new TaskGazeSameSys(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                    else
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeSameUsr/Name", "");
                        _currentSubScene = new TaskGazeSameUsr(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                }
                else if (scene.TaskInfo.InteractionFocus == InteractionFocus.DifferentObjects)
                {
                    if (scene.TaskInfo.Input == Input.SystemCommanded)
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffSys/Name");
                        _currentSubScene = new TaskGazeDiffSys(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                    else
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskGazeDiffUsr/Name");
                        _currentSubScene = new TaskGazeDiffUsr(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                }
                break;
            case Interaction.Manipulation:
                if (scene.TaskInfo.InteractionFocus == InteractionFocus.SameObject)
                {
                    Debug.LogError("TaskManager/CreateTask: Cannot create a Manipulation Task with Same Object");
                }
                else if (scene.TaskInfo.InteractionFocus == InteractionFocus.DifferentObjects)
                {
                    if (scene.TaskInfo.Input == Input.SystemCommanded)
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffSys/Name");
                        _currentSubScene = new TaskManipDiffSys(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                    else
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskManipDiffUsr/Name");
                        _currentSubScene = new TaskManipDiffUsr(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                }
                break;
            case Interaction.Position:
                if (scene.TaskInfo.InteractionFocus == InteractionFocus.SameObject)
                {
                    if (scene.TaskInfo.Input == Input.SystemCommanded)
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosSameSys/Name");
                        _currentSubScene = new TaskPosSameSys(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                    else
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosSameUsr/Name");
                        _currentSubScene = new TaskPosSameUsr(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                }
                else if (scene.TaskInfo.InteractionFocus == InteractionFocus.DifferentObjects)
                {
                    if (scene.TaskInfo.Input == Input.SystemCommanded)
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffSys/Name");
                        _currentSubScene = new TaskPosDiffSys(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                    else
                    {
                        sceneName = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/TaskPosDiffUsr/Name");
                        _currentSubScene = new TaskPosDiffUsr(sceneName, scene.TaskInfo, IsSceneInitialized(scene));
                    }
                }
                break;
        }
    }

    private bool IsSceneInitialized(RoomScene scene)
    {
        bool initialized = _initializedScenes.Contains(scene);
        if(!initialized) _initializedScenes.Add(scene);
        return initialized;
    }

    public TaskInfo GetTaskInSequence(Interaction interaction, Location location,
        VisualizationSequence visualizationSequence, int index)
    {
        TaskInfo taskInfo = new TaskInfo();
        int[] sequence = new int[] { };
        switch (visualizationSequence)
        {
            case VisualizationSequence.V1_123:
                sequence = new[] {0, 1, 2};
                break;
            case VisualizationSequence.V2_132:
                sequence = new[] {0, 2, 1};
                break;
            case VisualizationSequence.V3_213:
                sequence = new[] { 1, 0, 2 };
                break;
            case VisualizationSequence.V4_231:
                sequence = new[] { 1, 2, 0 };
                break;
            case VisualizationSequence.V5_312:
                sequence = new[] { 2, 0, 1 };
                break;
            case VisualizationSequence.V6_321:
                sequence = new[] { 2, 1, 0 };
                break;
            case VisualizationSequence.V7_23:
                sequence = new[] { 1, 2 };
                break;
            case VisualizationSequence.V8_32:
                sequence = new[] { 2, 1 };
                break;
        }

        switch (interaction)
        {
            case Interaction.Gaze:
                taskInfo = _gazeTaskSequence[index % _gazeTaskSequence.Length];
                taskInfo.Visualization = _visualizations[sequence[index / _gazeTaskSequence.Length]];
                break;
            case Interaction.Manipulation:
                taskInfo = _manipulationTaskSequence[index % _manipulationTaskSequence.Length];
                taskInfo.Visualization = _visualizations[sequence[index / _manipulationTaskSequence.Length]];
                break;
            case Interaction.Position:
                taskInfo = _positionTaskSequence[index % _positionTaskSequence.Length];
                taskInfo.Visualization = _visualizations[sequence[index / _positionTaskSequence.Length]];
                break;
        }

        taskInfo.Location = location;
        taskInfo.VisualizationSequence = visualizationSequence;

        return taskInfo;
    }

    #endregion
}
