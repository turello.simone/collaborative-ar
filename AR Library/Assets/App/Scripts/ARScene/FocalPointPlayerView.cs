﻿using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;
using UnityEngine.UI;

public class FocalPointPlayerView : MonoBehaviour
{
    [MustBeAssigned] public Image Icon;
    [MustBeAssigned] public LineRenderer Ray;
    [MustBeAssigned] public Projector Cursor;

    public bool IsOccluded
    {
        set
        {
            _isOccluded = value;
            if (_color != Color.black)
            {
                if (value)
                {
                    Icon.color = _color.WithAlphaSetTo(0.5f);
                }
                else
                {
                    Icon.color = _color;
                }
            }
        }
        get { return _isOccluded; }
    }
    private bool _isOccluded = false;

    private Color _color = Color.black;
    private bool _isDynamic = false;
    private DynamicObject _dynamicObj;
    private float _distance;

    public void Initialize(Color color, float distance, bool isDynamic = false)
    {
        _color = color;

        Icon.transform.position = transform.position + Vector3.up * distance;
        Icon.color = color;
        Icon.enabled = true;

        Ray.SetPosition(1, Ray.transform.InverseTransformPoint(transform.position + Vector3.up * distance));
        Ray.startColor = color.WithAlphaSetTo(Ray.startColor.a);
        Ray.endColor = color.WithAlphaSetTo(Ray.endColor.a);
        Ray.enabled = true;

        // Projector material is like renderer.shaderMaterial, in order to change local material color make a copy of the shared one.
        Material cursorMaterial = new Material(Cursor.material);
        cursorMaterial.color = color;
        Cursor.material = cursorMaterial;
        Cursor.enabled = true;

        IsOccluded = false;

        _isDynamic = isDynamic;
        _distance = distance;
    }

    private void Awake()
    {
        Icon.enabled = false;
        Ray.enabled = false;
        Cursor.enabled = false;
        Icon.GetComponent<Canvas>().worldCamera = Camera.main;
    }

    private void Update()
    {
        Icon.transform.LookAt(Camera.main.transform, Camera.main.transform.up);

        if (_isDynamic)
        {
            Icon.transform.position = transform.position + Vector3.up * _distance;
            Ray.SetPosition(1, Ray.transform.InverseTransformPoint(transform.position + Vector3.up * _distance));

            /*if (_dynamicObj == null)
                _dynamicObj = GameUtils.GetObjectRoot(transform.parent.gameObject).GetComponent<DynamicObject>();

            if (_dynamicObj != null && _dynamicObj.DynamicObjectState != GameUtils.DynamicObjectState.Placed)
            {
                Icon.transform.position = transform.position + Vector3.up * _distance;
                Ray.SetPosition(1, Ray.transform.InverseTransformPoint(transform.position + Vector3.up * _distance));
            }*/
        }
    }

    public void OnIconClick()
    {
        if(PhotonNetworkManager.Instance.LocalPlayer.Color == _color) PhotonNetworkManager.Instance.DestroyPlayerFocalPoint();
    }
}
