﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using MyBox;
using MyUtils;
using UnityEngine;

public class FocalPointManager : SingletonInScene<FocalPointManager>
{
    public float OutlineWidth = 2f;
    [MustBeAssigned] public GameObject GazeConvergenceFocalPointArrowPrefab;
    [MustBeAssigned] public GameObject GazeConvergenceFocalPointIconPrefab;
    [MustBeAssigned] public GameObject GazeConvergencePrefab;
    public Color GazeConvergenceFocalPointColor;
    [MustBeAssigned] public GameObject PlayerFocalPointArrowPrefab;
    [MustBeAssigned] public GameObject PlayerFocalPointIconPrefab;
    [MustBeAssigned] public GameObject PlayerFocalPointViewPrefab;
    public float PlayerFocalIconTargetDistance = 0.1f;
    //public float PlayerFocalPointDuration = 5f;
    [MustBeAssigned] public GameObject FreezedObjectFocalPointArrowPrefab;
    [MustBeAssigned] public GameObject FreezedObjectFocalPointIconPrefab;

    private GameObject _focalPointPanel;
    private Dictionary<int, FocalPointPlayer> _playerFocalPoints;
    private GazeConvergenceFocalPoint _activeGazeConvergenceFocalPoint;
    private Dictionary<GazeConvergenceFocalPoint.Type, GazeConvergenceFocalPoint> _gazeConvergenceFocalPoints;
    private FocalPoint _freezedObjectFocalPoint;

    public override void Awake()
    {
        base.Awake();
        _playerFocalPoints = new Dictionary<int, FocalPointPlayer>();
        _gazeConvergenceFocalPoints = new Dictionary<GazeConvergenceFocalPoint.Type, GazeConvergenceFocalPoint>();
        _focalPointPanel = GameUIManager.Instance.FocalPointPanel;
    }

    private void Update ()
    {
        /*foreach (var gazeConvergenceFocalPoint in _gazeConvergenceFocalPoints.Values)
        {
            gazeConvergenceFocalPoint.Update();
        }*/
        if (_activeGazeConvergenceFocalPoint != null) _activeGazeConvergenceFocalPoint.Update();

        for (var i = 0; i < _playerFocalPoints.Count; i++)
        {
            
            if (PhotonNetworkManager.Instance.GetPlayerInfoFromActorNumber(_playerFocalPoints.ElementAt(i).Key) != null)
                _playerFocalPoints.ElementAt(i).Value.Update();
            else
            {
                _playerFocalPoints.ElementAt(i).Value.Destroy();
                _playerFocalPoints.Remove(_playerFocalPoints.ElementAt(i).Key);
                i--;
            }
        }

        /*foreach (var playerFocalPointPair in _playerFocalPoints)
        {
            if(PhotonNetworkManager.Instance.GetPlayerInfoFromActorNumber(playerFocalPointPair.Key) != null)
                playerFocalPointPair.Value.Update();
            else
            {
                playerFocalPointPair.Value.Destroy();
                _playerFocalPoints.Remove(playerFocalPointPair.Key);
            }
        }*/

        if (_freezedObjectFocalPoint != null)
        {
            _freezedObjectFocalPoint.Update();
        }
    }

    public void SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type type, GameObject target,
        float waitTimeOnFocalPoint = 1f, bool outlineEnabled = true, bool iconEnabled = true, bool arrowEnabled = true)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("FocalPointManager/SubscribeGazeConvergenceFocalPoint: Type not defined.");
            return;
        }
        if (target == null)
        {
            Debug.LogError("FocalPointManager/SubscribeGazeConvergenceFocalPoint: Target GameObject is null.");
            return;
        }
        GazeConvergenceFocalPoint focalPoint;
        if (_gazeConvergenceFocalPoints.TryGetValue(type, out focalPoint))
        {
            Debug.LogError("FocalPointManager/SubscribeGazeConvergenceFocalPoint: GazeConvergenceFocalPoint with this type was added already.");
            return;
        }
        GameObject focalPointArrowUI = Instantiate(GazeConvergenceFocalPointArrowPrefab, _focalPointPanel.transform);
        GameObject focalPointIconUI = Instantiate(GazeConvergenceFocalPointIconPrefab, _focalPointPanel.transform);
        focalPoint = new GazeConvergenceFocalPoint(type, focalPointArrowUI, focalPointIconUI,
            GazeConvergenceFocalPointColor, OutlineWidth, waitTimeOnFocalPoint, targetPlayer1: target,
            targetPlayer2: target, targetPlayer3: target, outlineEnabled: outlineEnabled, iconEnabled: iconEnabled,
            arrowEnabled: arrowEnabled);
        _gazeConvergenceFocalPoints.Add(type, focalPoint);
    }

    public void SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type type, GameObject targetPlayer1,
        GameObject targetPlayer2, GameObject targetPlayer3, float waitTimeOnFocalPoint = 1f, bool outlineEnabled = true,
        bool iconEnabled = true, bool arrowEnabled = true)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("FocalPointManager/SubscribeGazeConvergenceFocalPoint: Type not defined.");
            return;
        }
        GazeConvergenceFocalPoint focalPoint;
        if (_gazeConvergenceFocalPoints.TryGetValue(type, out focalPoint))
        {
            Debug.LogError("FocalPointManager/SubscribeGazeConvergenceFocalPoint: GazeConvergenceFocalPoint with this type was added already.");
            return;
        }
        GameObject focalPointArrowUI = Instantiate(GazeConvergenceFocalPointArrowPrefab, _focalPointPanel.transform);
        GameObject focalPointIconUI = Instantiate(GazeConvergenceFocalPointIconPrefab, _focalPointPanel.transform);
        focalPoint = new GazeConvergenceFocalPoint(type, focalPointArrowUI, focalPointIconUI,
            GazeConvergenceFocalPointColor, OutlineWidth, waitTimeOnFocalPoint, targetPlayer1: targetPlayer1,
            targetPlayer2: targetPlayer2, targetPlayer3: targetPlayer3, outlineEnabled: outlineEnabled, iconEnabled: iconEnabled,
            arrowEnabled: arrowEnabled);
        _gazeConvergenceFocalPoints.Add(type, focalPoint);
    }

    public void DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type type)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("FocalPointManager/RemoveGazeConvergenceFocalPoint: Type not defined.");
            return;
        }
        GazeConvergenceFocalPoint focalPoint;
        if (!_gazeConvergenceFocalPoints.TryGetValue(type, out focalPoint))
        {
            Debug.Log("FocalPointManager/RemoveGazeConvergenceFocalPoint: No GazeConvergenceFocalPoint with this type found.");
            return;
        }

        if (focalPoint == _activeGazeConvergenceFocalPoint)
        {
            _activeGazeConvergenceFocalPoint = null;
        }
        _gazeConvergenceFocalPoints.Remove(type);
        focalPoint.Destroy();
    }


    /// <summary>
    /// Only one GazeConvergenceFocalPoint can be active at the same time.
    /// If there is one already active, the old one is removed and replaced with the new one.
    /// </summary>
    /// <param name="type"></param>
    public void EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type type)
    {
        if (!Enum.IsDefined(typeof(GazeConvergenceFocalPoint.Type), type))
        {
            Debug.LogError("FocalPointManager/EnableGazeConvergenceFocalPoint: Type not defined.");
            return;
        }
        GazeConvergenceFocalPoint focalPoint;
        if (!_gazeConvergenceFocalPoints.TryGetValue(type, out focalPoint))
        {
            Debug.LogError("FocalPointManager/EnableGazeConvergenceFocalPoint: No GazeConvergenceFocalPoint with this type found.");
            return;
        }

        if (_activeGazeConvergenceFocalPoint != null)
        {
            DestroyGazeConvergenceFocalPoint(_activeGazeConvergenceFocalPoint.GazeConvergenceType);
        }

        focalPoint.Enabled = true;
        _activeGazeConvergenceFocalPoint = focalPoint;
        PhotonNetworkManager.Instance.LocalPlayer.IsGazeOnFocalPoint = false;
    }

    public GameObject CreateGazeConvergenceSphericalCollider(Vector3 position, float radius, Transform parent, bool colliderEnabled = false)
    {
        GameObject gazeConvergence = Instantiate(GazeConvergencePrefab, position, Quaternion.identity, parent);
        gazeConvergence.transform.localScale = new Vector3(radius, radius, radius);
        gazeConvergence.GetComponent<Collider>().enabled = colliderEnabled;
        return gazeConvergence;
    }

    public void DisableCurrentGazeConvergenceFocalPoint()
    {
        if (_activeGazeConvergenceFocalPoint != null)
        {
            _activeGazeConvergenceFocalPoint.Enabled = false;
            _activeGazeConvergenceFocalPoint = null;
        }
    }


    public void CreateFreezedObjectFocalPoint(GameObject target)
    {
        if (_freezedObjectFocalPoint != null)
        {
            Debug.LogError("FocalPointManager/CreateFreezedObjectFocalPoint: A FreezedObjectFocalPoint already exists.");
            return;
        }
        if (target == null)
        {
            Debug.LogError("FocalPointManager/CreateFreezedObjectFocalPoint: Target GameObject is null.");
            return;
        }
        GameObject focalPointArrowUI = Instantiate(FreezedObjectFocalPointArrowPrefab, _focalPointPanel.transform);
        GameObject focalPointIconUI = Instantiate(FreezedObjectFocalPointIconPrefab, _focalPointPanel.transform);
        _freezedObjectFocalPoint = new FocalPoint(target, focalPointArrowUI, focalPointIconUI,
            PhotonNetworkManager.Instance.LocalPlayer.Color, OutlineWidth, position: null, enabled: true,
            outlineEnabled: true, replaceOutline: false, iconEnabled: false);
    }

    public void CreatePlayerFocalPoint(PlayerInfo player, Vector3 position, Quaternion rotation, [CanBeNull] GameObject target = null)
    {
        if (player == null)
        {
            Debug.LogError("FocalPointManager/CreatePlayerFocalPoint: PlayerInfo is null.");
            return;
        }
        FocalPointPlayer focalPoint;
        if (_playerFocalPoints.TryGetValue(player.ActorNumber, out focalPoint))
        {
            DestroyPlayerFocalPoint(player);
            StartCoroutine(CreatePlayerFocalPointCoroutine(player, position, rotation, target));
        }
        else
        {
            GameObject focalPointArrowUI = Instantiate(PlayerFocalPointArrowPrefab, _focalPointPanel.transform);
            GameObject focalPointIconUI = Instantiate(PlayerFocalPointIconPrefab, _focalPointPanel.transform);
            GameObject focalPointView = Instantiate(PlayerFocalPointViewPrefab);
            focalPoint = new FocalPointPlayer(target, rotation, focalPointArrowUI, focalPointIconUI, focalPointView,
                player.Color, OutlineWidth, PlayerFocalIconTargetDistance, position);
            _playerFocalPoints.Add(player.ActorNumber, focalPoint);
            //StartCoroutine(DestroyPlayerFocalPointCoroutine(player, focalPoint));
        }
    }

    private IEnumerator CreatePlayerFocalPointCoroutine(PlayerInfo player, Vector3 position, Quaternion rotation, GameObject target)
    {
        yield return 0;
        GameObject focalPointArrowUI = Instantiate(PlayerFocalPointArrowPrefab, _focalPointPanel.transform);
        GameObject focalPointIconUI = Instantiate(PlayerFocalPointIconPrefab, _focalPointPanel.transform);
        GameObject focalPointView = Instantiate(PlayerFocalPointViewPrefab);
        FocalPointPlayer focalPoint = new FocalPointPlayer(target, rotation, focalPointArrowUI, focalPointIconUI, focalPointView,
            player.Color, OutlineWidth, PlayerFocalIconTargetDistance, position);
        _playerFocalPoints.Add(player.ActorNumber, focalPoint);
        //StartCoroutine(DestroyPlayerFocalPointCoroutine(player, focalPoint));
    }

    /*private IEnumerator DestroyPlayerFocalPointCoroutine(PlayerInfo player, FocalPoint focalPoint)
    {
        yield return new WaitForSeconds(PlayerFocalPointDuration);
        if (focalPoint != null && !focalPoint.IsDestroyed)
        {
            DestroyPlayerFocalPoint(player);
        }
    }*/

    public void DestroyPlayerFocalPoint(PlayerInfo player)
    {
        if (player == null)
        {
            return;
        }

        FocalPointPlayer focalPoint;
        if (!_playerFocalPoints.TryGetValue(player.ActorNumber, out focalPoint))
        {
            return;
        }
        focalPoint.Destroy();
        _playerFocalPoints.Remove(player.ActorNumber);
    }

    public void DestroyFreezedObjectFocalPoint()
    {
        if (_freezedObjectFocalPoint == null)
        {
            return;
        }

        _freezedObjectFocalPoint.Destroy();
        _freezedObjectFocalPoint = null;
    }

    public void AddOutlineNextFrame(FocalPoint focalPoint, Color color, float outlineWidth)
    {
        StartCoroutine(AddOutlineNextFrameCoroutine(focalPoint, color, outlineWidth));
    }

    private IEnumerator AddOutlineNextFrameCoroutine(FocalPoint focalPoint, Color color, float outlineWidth)
    {
        yield return 0;
        if (focalPoint.TargetGameObject.GetComponent<CustomOutline>() == null)
        {
            focalPoint.Outline = focalPoint.TargetGameObject.AddComponent<CustomOutline>();
            focalPoint.Outline.OutlineMode = CustomOutline.Mode.OutlineVisible;
            focalPoint.Outline.OutlineColor = color;
            focalPoint.Outline.OutlineWidth = outlineWidth;
            focalPoint.Outline.FadeIn = false;
            focalPoint.OutlineEnabled = true;
        }
    }
}
