﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocalPointPlayer : FocalPoint
{
    private FocalPointPlayerView _focalPointPlayerView;

    public FocalPointPlayer(GameObject target, Quaternion focalPointRotation, GameObject focalPointArrowUI,
        GameObject focalPointIconUI, GameObject focalPointPlayerView, Color color, float outlineWidth,
        float iconTargetDistance, Vector3? position = null, bool outlineEnabled = true, bool replaceOutline = true,
        bool arrowEnabled = true) : base(target, focalPointArrowUI, focalPointIconUI, color, outlineWidth, position,
        true, outlineEnabled, replaceOutline, false, arrowEnabled)
    {
        focalPointPlayerView.transform.position = TargetGameObject.transform.TransformPoint(_relativePosition);
        focalPointPlayerView.transform.rotation = focalPointRotation;
        //focalPointPlayerView.transform.up = focalPointNormal;
        _focalPointPlayerView = focalPointPlayerView.GetComponent<FocalPointPlayerView>();

        if (TargetGameObject.layer == GameUtils.Layer.DynamicObjects)
        {
            _focalPointPlayerView.Initialize(color, iconTargetDistance, true);
            focalPointPlayerView.transform.parent = TargetGameObject.transform.GetChild(0);
        }
        else
        {
            _focalPointPlayerView.Initialize(color, iconTargetDistance);
            focalPointPlayerView.transform.parent = TargetGameObject.transform;
        }
    }

    public override void Destroy()
    {
        base.Destroy();
        Object.Destroy(_focalPointPlayerView.gameObject);
    }

    public override void Update()
    {
        base.Update();

        //Check if icon is obstructed by a dynamic or a static object; if so, change alpha
        Ray ray = new Ray(_focalPointPlayerView.gameObject.transform.position,
                Camera.main.transform.position - _focalPointPlayerView.gameObject.transform.position);
        if (Physics.Raycast(ray, out var hitInfo, ObjectManipulation.Instance.MaxInteractionDistance,
            GameUtils.Mask.DynamicObjects | GameUtils.Mask.StaticObjects))
        {
            /*Debug.LogFormat("Test {0}, {1}, {2}", hitInfo.collider.gameObject.name,
                !hitInfo.collider.transform.IsChildOf(TargetGameObject.transform),
                hitInfo.collider.gameObject.tag != "Interactive");*/
            _focalPointPlayerView.IsOccluded = !hitInfo.collider.transform.IsChildOf(TargetGameObject.transform) &&
                                               hitInfo.collider.gameObject.tag != "Interactive";
        }
        else
        {
            _focalPointPlayerView.IsOccluded = false;
        }
    }
}
