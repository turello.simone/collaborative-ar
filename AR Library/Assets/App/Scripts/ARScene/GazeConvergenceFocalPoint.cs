﻿using UnityEngine;
using System.Collections;
using System.Linq;
using Boo.Lang;
using MyBox;

public class GazeConvergenceFocalPoint : FocalPoint
{
    public enum Type
    {
        ContraptionTutorialTop,
        ContraptionTutorialOpening,
        PedestalTop,
        PaintingMoon,
        PaintingOpening,
        ContraptionFinalOpening,
        TaskGazeSameSys,
        TaskGazeDiffSys,
        TaskGazeSameUsr1,
        TaskGazeSameUsr2,
        TaskGazeSameUsr3,
        TaskGazeDiffUsr1,
        TaskGazeDiffUsr2,
        TaskGazeDiffUsr3
    }

    public override bool Enabled
    {
        get { return base.Enabled; }
        set
        {
            base.Enabled = value;

            if (_targetGameObjectCollider != null) _targetGameObjectCollider.enabled = value;
            if (_targetGameObjectRenderer != null) _targetGameObjectRenderer.enabled = value;

        }
    }
    public Type GazeConvergenceType;


    private PhotonNetworkManager _networkManager;
    private ObjectManipulation _objectManipulation;
    private Collider _targetGameObjectCollider;
    private Renderer _targetGameObjectRenderer;

    private bool _player1HasTarget;
    private bool _player2HasTarget;
    private bool _player3HasTarget;
    private float _progressTime;
    private float _waitTimeOnFocalPoint;

    public GazeConvergenceFocalPoint(Type type, GameObject focalPointArrowUI,
        GameObject focalPointIconUI, Color color, float outlineWidth, float waitTimeOnFocalPoint,
        GameObject targetPlayer1 = null, GameObject targetPlayer2 = null, GameObject targetPlayer3 = null,
        bool outlineEnabled = true, bool iconEnabled = true, bool arrowEnabled = true) : 
        base(targetPlayer1, targetPlayer2, targetPlayer3, focalPointArrowUI, focalPointIconUI, color, outlineWidth, enabled: false,
        replaceOutline: false, outlineEnabled: outlineEnabled, iconEnabled: iconEnabled, arrowEnabled: arrowEnabled)
    {
        _networkManager = PhotonNetworkManager.Instance;
        GazeConvergenceType = type;
        _waitTimeOnFocalPoint = waitTimeOnFocalPoint;

        _progressTime = 0.0f;
        _objectManipulation = ObjectManipulation.Instance;

        _player1HasTarget = (targetPlayer1 != null);
        _player2HasTarget = (targetPlayer2 != null);
        _player3HasTarget = (targetPlayer3 != null);

        if (targetPlayer1 == null && targetPlayer2 == null && targetPlayer3 == null)
        {
            Debug.LogError("GazeConvergenceFocalPoint: No target defined.");
            return;
        }
    }

    protected override void Initialize(PlayerInfo player, Color newValue)
    {
        base.Initialize(player, newValue);

        if (_temp.TargetPlayer1 != null)
        {
            if(_temp.TargetPlayer1.HasComponent<Renderer>()) _temp.TargetPlayer1.GetComponent<Renderer>().enabled = false;
            if(_temp.TargetPlayer1.HasComponent<Collider>()) _temp.TargetPlayer1.GetComponent<Collider>().enabled = false;
        }
        if (_temp.TargetPlayer2 != null)
        {
            if (_temp.TargetPlayer2.HasComponent<Renderer>()) _temp.TargetPlayer2.GetComponent<Renderer>().enabled = false;
            if (_temp.TargetPlayer2.HasComponent<Collider>()) _temp.TargetPlayer2.GetComponent<Collider>().enabled = false;
        }
        if (_temp.TargetPlayer3 != null)
        {
            if (_temp.TargetPlayer3.HasComponent<Renderer>()) _temp.TargetPlayer3.GetComponent<Renderer>().enabled = false;
            if (_temp.TargetPlayer3.HasComponent<Collider>()) _temp.TargetPlayer3.GetComponent<Collider>().enabled = false;
        }

        if (TargetGameObject != null)
        {
            _targetGameObjectRenderer = TargetGameObject.GetComponent<Renderer>();
            if (_targetGameObjectRenderer != null) _targetGameObjectRenderer.enabled = Enabled;

            _targetGameObjectCollider = TargetGameObject.GetComponent<Collider>();
            if (_targetGameObjectCollider != null) _targetGameObjectCollider.enabled = Enabled;
        }
    }

    public override void Update()
    {

        base.Update();

        if (!Enabled || IsDestroyed || !_networkManager.InRoom)
        {
            return;
        }

        if ((_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceType, out bool c) && c) ||
            GameUIManager.Instance.InstructionsPanelEnabled)
        {
            _networkManager.LocalPlayer.IsGazeOnFocalPoint = false;
            return;
        }

        //Check if local player's gaze is on this focal point and then if there are other objects (closer) in the way
        if (TargetGameObject != null && _targetGameObjectCollider != null)
        {
            Ray screenRay = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            RaycastHit hitInfoFocalPoint;
            RaycastHit hitInfoObjects;
            if (_targetGameObjectCollider.Raycast(screenRay, out hitInfoFocalPoint, _objectManipulation.MaxInteractionDistance) &&
                (!Physics.Raycast(screenRay, out hitInfoObjects, _objectManipulation.MaxInteractionDistance,
                     GameUtils.Mask.StaticObjects | GameUtils.Mask.DynamicObjects) ||
                 hitInfoObjects.distance >= hitInfoFocalPoint.distance))
            {
                _progressTime += Time.deltaTime;
                if (_progressTime >= _waitTimeOnFocalPoint)
                {
                    _networkManager.LocalPlayer.IsGazeOnFocalPoint = true;
                }
            }
            else
            {
                _progressTime = 0.0f;
                _networkManager.LocalPlayer.IsGazeOnFocalPoint = false;
            }
        }


        if (_networkManager.IsMasterClient)
        {
            PlayerInfo player1 = null;
            PlayerInfo player2 = null;
            PlayerInfo player3 = null;

            if (_player1HasTarget)
                player1 = _networkManager.GetPlayerInfoFromPlayerIndex(1);
            if (_player2HasTarget)
                player2 = _networkManager.GetPlayerInfoFromPlayerIndex(2);
            if (_player3HasTarget)
                player3 = _networkManager.GetPlayerInfoFromPlayerIndex(3);

            bool completed;
            //Check if all players have gaze on focal point

            //CHEATING VERSION: checks only online players
            if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Enabled)
            {
                if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceType, out completed) &&
                    !completed &&
                    (player1 == null || player1.IsGazeOnFocalPoint) &&
                    (player2 == null || player2.IsGazeOnFocalPoint) &&
                    (player3 == null || player3.IsGazeOnFocalPoint))
                {
                    _networkManager.CurrentRoom.SetGazeConvergenceStatusCompleted(GazeConvergenceType);
                }
            }
            
            //CORRECT VERSION
            else if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
            {
                if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceType, out completed) &&
                    !completed &&
                    (!_player1HasTarget || (player1 != null && player1.IsGazeOnFocalPoint)) &&
                    (!_player2HasTarget || (player2 != null && player2.IsGazeOnFocalPoint)) &&
                    (!_player3HasTarget || (player3 != null && player3.IsGazeOnFocalPoint)))
                {
                    _networkManager.CurrentRoom.SetGazeConvergenceStatusCompleted(GazeConvergenceType);
                }
            }
        }

    }

    public override void Destroy()
    {
        base.Destroy();
        _networkManager.LocalPlayer.IsGazeOnFocalPoint = false;
    }
}
