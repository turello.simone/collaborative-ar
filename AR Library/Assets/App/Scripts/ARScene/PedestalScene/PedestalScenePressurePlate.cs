﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class PedestalScenePressurePlate : StaticObject
{
    public enum PressurePlateState
    {
        Default,
        Disabled,
        Enabled,
        Pressed
    }

    public enum PressurePlateType
    {
        One,
        Two,
        Three
    }


    #region Attributes

    [Header("PressurePlate properties")]

    public PressurePlateType Type;

    [MustBeAssigned] public GameObject Beam;
    [MustBeAssigned] public GameObject Plate;
    [MustBeAssigned] public Collider PositionCollider;

    public PressurePlateState State
    {
        get { return (PressurePlateState) _state.Value; }
        set { _state.Value = (int) value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;

    private MeshRenderer _beamRenderer;
    private MeshRenderer _plateRenderer;
    private Color _color;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _beamRenderer = Beam.GetComponent<MeshRenderer>();
        _plateRenderer = Plate.GetComponent<MeshRenderer>();

        _state = new SyncVarInt((int) PressurePlateState.Disabled, _photonView, STATEID);
        OnPressurePlateStateChange((int)PressurePlateState.Disabled);
        _state.ValueChanged += OnPressurePlateStateChange;
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        switch (Type)
        {
            case PressurePlateType.One:
                _networkManager.CurrentRoom.PedestalScenePressurePlate1 = this;
                _color = _networkManager.GetPlayerColor(1);
                break;
            case PressurePlateType.Two:
                _networkManager.CurrentRoom.PedestalScenePressurePlate2 = this;
                _color = _networkManager.GetPlayerColor(2);
                break;
            case PressurePlateType.Three:
                _networkManager.CurrentRoom.PedestalScenePressurePlate3 = this;
                _color = _networkManager.GetPlayerColor(3);
                break;
        }

        //_beamRenderer.material.SetColor("_Color", _color);
        _beamRenderer.material.color = _color;
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        if (_networkManager.IsMasterClient)
        {
            State = PressurePlateState.Disabled;
        }
    }

    #endregion


    #region Private methods

    private void OnPressurePlateStateChange(int value)
    {
        switch ((PressurePlateState) value)
        {
            case PressurePlateState.Disabled:
                _beamRenderer.enabled = false;
                _plateRenderer.material.SetColor("_EmissionColor", Color.black);
                break;
            case PressurePlateState.Enabled:
                _beamRenderer.enabled = false;
                _plateRenderer.material.SetColor("_EmissionColor", _color);
                break;
            case PressurePlateState.Pressed:
                _beamRenderer.enabled = true;
                _plateRenderer.material.SetColor("_EmissionColor", _color);
                break;
        }
    }

    #endregion

    public override bool IsUseful(GameObject gameObj)
    {
        return State >= PressurePlateState.Enabled;
    }
}
