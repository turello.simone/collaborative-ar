﻿using System;
using System.Collections;
using System.Linq;
using Lean.Pool;
using MyBox;
using Photon.Pun;
using UnityEngine;

public class PedestalScenePedestal : StaticObject
{
    public enum PedestalState
    {
        Default,
        NotAligned,
        Aligned,
        PipesNotFull,
        PipesFull,
        TopOpen
    }

    #region Attributes

    [Header("Pedestal properties")]

    [MustBeAssigned] public GameObject FocalPointOnTop;

    [MustBeAssigned] public GameObject BotBlock;
    [MustBeAssigned] public GameObject MidBlock;
    [MustBeAssigned] public GameObject TopBlock;

    [MustBeAssigned] public GameObject[] BotHandles;
    [MustBeAssigned] public GameObject[] MidHandles;
    [MustBeAssigned] public GameObject[] TopHandles;

    [MustBeAssigned] public Material Pipe1Material;
    [MustBeAssigned] public Material Pipe2Material;
    [MustBeAssigned] public Material Pipe3Material;

    public PedestalState State
    {
        get { return (PedestalState)_state.Value; }
        set { _state.Value = (int)value; }
    }
    private const string STATEID = "State";
    private SyncVarInt _state;

    [MustBeAssigned] public Animator Animator;

    /// <summary>
    /// Blocks rotation speed in degrees
    /// </summary>
    public float RotationSpeed = 60f;
    /// <summary>
    /// Max rotation error for the positioning of the rotating blocks, in degrees.
    /// </summary>
    public float MaxRotationError = 5f;
    public float AlignTimeNeeded = 1f;
    public float AlignAnimationTime = 5f;
    public float PressureTimeNeededTillFull = 10f;
    public float PressureTimeNeededOnceFull = 1f;

    private GameObject _interactedBlock;
    private Quaternion _interactedBlockStartLocalRot;
    private Vector3 _startVectorTowardsPlayer;

    private Quaternion _botBlockStartLocalRot;
    private PhotonView _botBlockPhotonView;
    private Quaternion _midBlockStartLocalRot;
    private PhotonView _midBlockPhotonView;
    private Quaternion _topBlockStartLocalRot;
    private PhotonView _topBlockPhotonView;
    private Quaternion _blocksCorrectLocalRot;
    private float _timer = 0f;

    public float ProgressPipe1
    {
        get => _progressPipe1.Value;
        set => _progressPipe1.Value = value;
    }
    private SyncVarFloatNotBuffered _progressPipe1;
    public float ProgressPipe2
    {
        get => _progressPipe2.Value;
        set => _progressPipe2.Value = value;
    }
    private SyncVarFloatNotBuffered _progressPipe2;
    public float ProgressPipe3
    {
        get => _progressPipe3.Value;
        set => _progressPipe3.Value = value;
    }
    private SyncVarFloatNotBuffered _progressPipe3;

    private Material _botBlockPipe1Material;
    private Material _botBlockPipe2Material;
    private Material _botBlockPipe3Material;
    private Material _midBlockPipe1Material;
    private Material _midBlockPipe2Material;
    private Material _midBlockPipe3Material;
    private Material _topBlockPipe1Material;
    private Material _topBlockPipe2Material;
    private Material _topBlockPipe3Material;

    #endregion


    #region Unity / Initialization

    protected override void Awake()
    {
        base.Awake();

        _botBlockPhotonView = BotBlock.GetComponent<PhotonView>();
        _midBlockPhotonView = MidBlock.GetComponent<PhotonView>();
        _topBlockPhotonView = TopBlock.GetComponent<PhotonView>();

        _botBlockStartLocalRot = Quaternion.Euler(-90, 0, 120);
        _midBlockStartLocalRot = Quaternion.Euler(-90, 0, -120);
        _topBlockStartLocalRot = Quaternion.Euler(-90, 0, 120);

        _blocksCorrectLocalRot = Quaternion.Euler(-90f, 0f, 0f);

        _state = new SyncVarInt((int) PedestalState.NotAligned, _photonView, STATEID);
        OnPedestalStateChange((int)PedestalState.NotAligned);
        _state.ValueChanged += OnPedestalStateChange;

        _progressPipe1 = new SyncVarFloatNotBuffered(0, _photonView);
        _progressPipe2 = new SyncVarFloatNotBuffered(0, _photonView);
        _progressPipe3 = new SyncVarFloatNotBuffered(0, _photonView);

        _botBlockPipe1Material = BotBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe1Material.name));
        _botBlockPipe2Material = BotBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe2Material.name));
        _botBlockPipe3Material = BotBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe3Material.name));

        _midBlockPipe1Material = MidBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe1Material.name));
        _midBlockPipe2Material = MidBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe2Material.name));
        _midBlockPipe3Material = MidBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe3Material.name));

        _topBlockPipe1Material = TopBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe1Material.name));
        _topBlockPipe2Material = TopBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe2Material.name));
        _topBlockPipe3Material = TopBlock.GetComponent<MeshRenderer>().materials.First(material => material.name.Contains(Pipe3Material.name));

        Animator.enabled = false;
        Animator.gameObject.AddComponent<AnimatorStateManager>().SaveState();
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _networkManager.CurrentRoom.PedestalScenePedestal = this;

        _botBlockPipe1Material.color = _networkManager.GetPlayerColor(1);
        _botBlockPipe1Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(1));
        _midBlockPipe1Material.color = _networkManager.GetPlayerColor(1);
        _midBlockPipe1Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(1));
        _topBlockPipe1Material.color = _networkManager.GetPlayerColor(1);
        _topBlockPipe1Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(1));

        _botBlockPipe2Material.color = _networkManager.GetPlayerColor(2);
        _botBlockPipe2Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(2));
        _midBlockPipe2Material.color = _networkManager.GetPlayerColor(2);
        _midBlockPipe2Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(2));
        _topBlockPipe2Material.color = _networkManager.GetPlayerColor(2);
        _topBlockPipe2Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(2));

        _botBlockPipe3Material.color = _networkManager.GetPlayerColor(3);
        _botBlockPipe3Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(3));
        _midBlockPipe3Material.color = _networkManager.GetPlayerColor(3);
        _midBlockPipe3Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(3));
        _topBlockPipe3Material.color = _networkManager.GetPlayerColor(3);
        _topBlockPipe3Material.SetColor("_EmissionColor", _networkManager.GetPlayerColor(3));

        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PedestalTop, FocalPointOnTop);
    }

    [PunRPC]
    public override void ResetRPC()
    {
        base.ResetRPC();

        BotBlock.GetComponent<PhotonTransformViewClassic>().m_RotationModel.SynchronizeEnabled = true;
        MidBlock.GetComponent<PhotonTransformViewClassic>().m_RotationModel.SynchronizeEnabled = true;
        TopBlock.GetComponent<PhotonTransformViewClassic>().m_RotationModel.SynchronizeEnabled = true;

        FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PedestalTop);
        FocalPointManager.Instance.SubscribeGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PedestalTop, FocalPointOnTop);

        _botBlockPipe1Material.SetTextureOffset("_MainTex", Vector2.zero);
        _midBlockPipe1Material.SetTextureOffset("_MainTex", Vector2.zero);
        _topBlockPipe1Material.SetTextureOffset("_MainTex", Vector2.zero);

        _botBlockPipe2Material.SetTextureOffset("_MainTex", Vector2.zero);
        _midBlockPipe2Material.SetTextureOffset("_MainTex", Vector2.zero);
        _topBlockPipe2Material.SetTextureOffset("_MainTex", Vector2.zero);

        _botBlockPipe3Material.SetTextureOffset("_MainTex", Vector2.zero);
        _midBlockPipe3Material.SetTextureOffset("_MainTex", Vector2.zero);
        _topBlockPipe3Material.SetTextureOffset("_MainTex", Vector2.zero);

        Animator.gameObject.GetComponent<AnimatorStateManager>().RestoreState();

        BotBlock.transform.localRotation = _botBlockStartLocalRot;
        MidBlock.transform.localRotation = _midBlockStartLocalRot;
        TopBlock.transform.localRotation = _topBlockStartLocalRot;

        if (_networkManager.IsMasterClient)
        {
            _networkManager.CurrentRoom.ResetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PedestalTop);
            State = PedestalState.NotAligned;
            ProgressPipe1 = 0;
            ProgressPipe2 = 0;
            ProgressPipe3 = 0;
        }
    }

    private void Update()
    {
        if (State == PedestalState.NotAligned)
        {
            if (_interactedBlock != null)
            {
                Vector3 vectorTowardsPlayer = _networkManager.LocalPlayer.Player.transform.position -
                                              _interactedBlock.transform.position;
                vectorTowardsPlayer = Vector3.ProjectOnPlane(vectorTowardsPlayer, transform.up);

                /*Debug.Log("Block rotation: " +
                          Quaternion.FromToRotation(_startVectorTowardsPlayer, vectorTowardsPlayer).eulerAngles + " " +
                          _interactedBlockStartLocalRot.eulerAngles);*/
                Quaternion oldRot = _interactedBlock.transform.localRotation;

                _interactedBlock.transform.localRotation = _interactedBlockStartLocalRot;
                //_interactedBlock.transform.Rotate(Quaternion.FromToRotation(_startVectorTowardsPlayer, vectorTowardsPlayer).eulerAngles, Space.World);
                _interactedBlock.transform.RotateAround(_interactedBlock.transform.position, transform.up,
                    Vector3.SignedAngle(_startVectorTowardsPlayer, vectorTowardsPlayer, transform.up));

                if(Quaternion.Angle(oldRot, _interactedBlock.transform.localRotation) > 20f * Time.deltaTime)
                    AudioManager.Instance.PlaySoundEffect(AudioManager.SFXType.StoneScraping);
            }

            if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
            {
                if (_interactedBlock != BotBlock && _botBlockPhotonView.IsMine)
                {
                    BotBlock.transform.localRotation = Quaternion.RotateTowards(BotBlock.transform.localRotation,
                        _botBlockStartLocalRot, Time.deltaTime * RotationSpeed);
                }
                if (_interactedBlock != MidBlock && _midBlockPhotonView.IsMine)
                {
                    MidBlock.transform.localRotation = Quaternion.RotateTowards(MidBlock.transform.localRotation,
                        _midBlockStartLocalRot, Time.deltaTime * RotationSpeed);
                }
                if (_interactedBlock != TopBlock && _topBlockPhotonView.IsMine)
                {
                    TopBlock.transform.localRotation = Quaternion.RotateTowards(TopBlock.transform.localRotation,
                        _topBlockStartLocalRot, Time.deltaTime * RotationSpeed);
                }
            }

            if (_networkManager.IsMasterClient)
            {
                if (Quaternion.Angle(BotBlock.transform.localRotation, _blocksCorrectLocalRot) <= MaxRotationError &&
                    Quaternion.Angle(MidBlock.transform.localRotation, _blocksCorrectLocalRot) <= MaxRotationError &&
                    Quaternion.Angle(TopBlock.transform.localRotation, _blocksCorrectLocalRot) <= MaxRotationError)
                {
                    _timer += Time.deltaTime;
                    if (_timer >= AlignTimeNeeded)
                    {
                        State = PedestalState.Aligned;
                    }
                }
                else
                {
                    _timer = 0f;
                }
            }
        }
        else if (State == PedestalState.PipesNotFull)
        {
            if (_networkManager.IsMasterClient)
            {
                //Handle Pressure Plates activation
                PlayerInfo player1 = null;
                PlayerInfo player2 = null;
                PlayerInfo player3 = null;

                switch (_networkManager.LocalPlayer.PlayerIndex)
                {
                    case 1:
                        player1 = _networkManager.LocalPlayer;
                        break;
                    case 2:
                        player2 = _networkManager.LocalPlayer;
                        break;
                    case 3:
                        player3 = _networkManager.LocalPlayer;
                        break;
                }

                foreach (PlayerInfo player in _networkManager.OtherPlayers.Values)
                {
                    switch (player.PlayerIndex)
                    {
                        case 1:
                            player1 = player;
                            break;
                        case 2:
                            player2 = player;
                            break;
                        case 3:
                            player3 = player;
                            break;
                    }
                }

                if (_networkManager.CurrentRoom.SimpleGameMode == GameUtils.SimpleGameMode.Disabled)
                {
                    if (player1 != null &&
                        GameUtils.IsPointWithinCollider(_networkManager.CurrentRoom.PedestalScenePressurePlate1.PositionCollider,
                            player1.Player.transform.position))
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate1.State = PedestalScenePressurePlate.PressurePlateState.Pressed;
                        ProgressPipe1 = Mathf.Clamp(ProgressPipe1 + (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                    else
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate1.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                        ProgressPipe1 = Mathf.Clamp(ProgressPipe1 - (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }

                    if (player2 != null &&
                        GameUtils.IsPointWithinCollider(_networkManager.CurrentRoom.PedestalScenePressurePlate2.PositionCollider,
                            player2.Player.transform.position))
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate2.State = PedestalScenePressurePlate.PressurePlateState.Pressed;
                        ProgressPipe2 = Mathf.Clamp(ProgressPipe2 + (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                    else
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate2.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                        ProgressPipe2 = Mathf.Clamp(ProgressPipe2 - (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }

                    if (player3 != null &&
                        GameUtils.IsPointWithinCollider(_networkManager.CurrentRoom.PedestalScenePressurePlate3.PositionCollider,
                            player3.Player.transform.position))
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate3.State = PedestalScenePressurePlate.PressurePlateState.Pressed;
                        ProgressPipe3 = Mathf.Clamp(ProgressPipe3 + (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                    else
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate3.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                        ProgressPipe3 = Mathf.Clamp(ProgressPipe3 - (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                }
                else
                {
                    if (GameUtils.IsPointWithinCollider(_networkManager.CurrentRoom.PedestalScenePressurePlate1.PositionCollider,
                        _networkManager.LocalPlayer.Player.transform.position))
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate1.State = PedestalScenePressurePlate.PressurePlateState.Pressed;
                        ProgressPipe1 = Mathf.Clamp(ProgressPipe1 + (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                    else
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate1.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    }

                    if (GameUtils.IsPointWithinCollider(_networkManager.CurrentRoom.PedestalScenePressurePlate2.PositionCollider,
                        _networkManager.LocalPlayer.Player.transform.position))
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate2.State = PedestalScenePressurePlate.PressurePlateState.Pressed;
                        ProgressPipe2 = Mathf.Clamp(ProgressPipe2 + (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                    else
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate2.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    }

                    if (GameUtils.IsPointWithinCollider(_networkManager.CurrentRoom.PedestalScenePressurePlate3.PositionCollider,
                        _networkManager.LocalPlayer.Player.transform.position))
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate3.State = PedestalScenePressurePlate.PressurePlateState.Pressed;
                        ProgressPipe3 = Mathf.Clamp(ProgressPipe3 + (Time.deltaTime / PressureTimeNeededTillFull), 0, 1);
                    }
                    else
                    {
                        _networkManager.CurrentRoom.PedestalScenePressurePlate3.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    }
                }

                //Check if the pipes are totally filled (for a certain amount of time)
                if (Math.Abs(ProgressPipe1 - 1) < Mathf.Epsilon && 
                    Math.Abs(ProgressPipe2 - 1) < Mathf.Epsilon && 
                    Math.Abs(ProgressPipe3 - 1) < Mathf.Epsilon)
                {
                    _timer += Time.deltaTime;
                    if (_timer >= PressureTimeNeededOnceFull)
                    {
                        State = PedestalState.PipesFull;
                    }
                }
                else
                {
                    _timer = 0f;
                }
            }

            //set progression to textures
            _botBlockPipe1Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe1 * (-0.5f)));
            _midBlockPipe1Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe1 * (-0.5f)));
            _topBlockPipe1Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe1 * (-0.5f)));

            _botBlockPipe2Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe2 * (-0.5f)));
            _midBlockPipe2Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe2 * (-0.5f)));
            _topBlockPipe2Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe2 * (-0.5f)));

            _botBlockPipe3Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe3 * (-0.5f)));
            _midBlockPipe3Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe3 * (-0.5f)));
            _topBlockPipe3Material.SetTextureOffset("_MainTex", new Vector2(0, ProgressPipe3 * (-0.5f)));
        }
    }

    #endregion


    #region Private methods

    public override void HoldInteractiveObject(GameObject interactiveObject, bool isPressed)
    {
        if (isPressed)
        {
            _interactedBlock = interactiveObject.transform.parent.gameObject;

            if (_interactedBlock == BotBlock)
            {
                if(!_botBlockPhotonView.IsMine) _botBlockPhotonView.RequestOwnership();
            }
            else if (_interactedBlock == MidBlock)
            {
                if (!_midBlockPhotonView.IsMine) _midBlockPhotonView.RequestOwnership();
            }
            else if (_interactedBlock == TopBlock)
            {
                if (!_topBlockPhotonView.IsMine) _topBlockPhotonView.RequestOwnership();
            }

            _interactedBlockStartLocalRot = _interactedBlock.transform.localRotation;
            _startVectorTowardsPlayer = _networkManager.LocalPlayer.Player.transform.position -
                                          _interactedBlock.transform.position;
            _startVectorTowardsPlayer = Vector3.ProjectOnPlane(_startVectorTowardsPlayer, transform.up);
        }
        else
        {
            _interactedBlock = null;
        }
    }

    #endregion


    #region Private methods

    private void OnPedestalStateChange(int value)
    {
        switch ((PedestalState) value)
        {
            case PedestalState.NotAligned:
                Animator.enabled = false;

                _timer = 0f;

                //Enable Colliders to all handles
                for (var i = 0; i < BotHandles.Length; i++)
                {
                    BotHandles[i].CollidersEnabled(true);
                }
                for (var i = 0; i < MidHandles.Length; i++)
                {
                    MidHandles[i].CollidersEnabled(true);
                }
                for (var i = 0; i < TopHandles.Length; i++)
                {
                    TopHandles[i].CollidersEnabled(true);
                }

                break;
            case PedestalState.Aligned:
                _timer = 0f;

                //Disable Colliders to all handles
                for (var i = 0; i < BotHandles.Length; i++)
                {
                    BotHandles[i].CollidersEnabled(false);
                }
                for (var i = 0; i < MidHandles.Length; i++)
                {
                    MidHandles[i].CollidersEnabled(false);
                }
                for (var i = 0; i < TopHandles.Length; i++)
                {
                    TopHandles[i].CollidersEnabled(false);
                }
                //start coroutine to correctly align locally the rotation of the blocks
                StartCoroutine(AlignBlocks());
                break;
            case PedestalState.PipesNotFull:
                if (_networkManager.IsMasterClient)
                {
                    _networkManager.CurrentRoom.PedestalScenePressurePlate1.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    _networkManager.CurrentRoom.PedestalScenePressurePlate2.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    _networkManager.CurrentRoom.PedestalScenePressurePlate3.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                }
                break;
            case PedestalState.PipesFull:
                if (_networkManager.IsMasterClient)
                {
                    _networkManager.CurrentRoom.PedestalScenePressurePlate1.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    _networkManager.CurrentRoom.PedestalScenePressurePlate2.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                    _networkManager.CurrentRoom.PedestalScenePressurePlate3.State = PedestalScenePressurePlate.PressurePlateState.Enabled;
                }
                break;
            case PedestalState.TopOpen:
                Animator.SetTrigger("Open");
                break;
        }
    }

    private IEnumerator AlignBlocks()
    {
        BotBlock.GetComponent<PhotonTransformViewClassic>().m_RotationModel.SynchronizeEnabled = false;
        MidBlock.GetComponent<PhotonTransformViewClassic>().m_RotationModel.SynchronizeEnabled = false;
        TopBlock.GetComponent<PhotonTransformViewClassic>().m_RotationModel.SynchronizeEnabled = false;

        Quaternion botBlockStartRot = BotBlock.transform.localRotation;
        Quaternion midBlockStartRot = MidBlock.transform.localRotation;
        Quaternion topBlockStartRot = TopBlock.transform.localRotation;
        float timeNeededToAlign = 0.5f;
        float percentage = 0f;

        while (percentage < 1)
        {
            percentage += Time.deltaTime / timeNeededToAlign;
            BotBlock.transform.localRotation = Quaternion.Lerp(botBlockStartRot, _blocksCorrectLocalRot, percentage);
            MidBlock.transform.localRotation = Quaternion.Lerp(midBlockStartRot, _blocksCorrectLocalRot, percentage);
            TopBlock.transform.localRotation = Quaternion.Lerp(topBlockStartRot, _blocksCorrectLocalRot, percentage);
            yield return 0;
        }

        Animator.enabled = true;
        Animator.SetTrigger("Align");

        yield return new WaitForSeconds(AlignAnimationTime);

        if (_networkManager.IsMasterClient) State = PedestalState.PipesNotFull;
    }

    #endregion

}
