﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedestalScene : IGameScene
{
    public PedestalScene(string sceneName, SubSceneManager.Visualization visualization, SubSceneManager.Location location,
        bool initialized) : base(sceneName, visualization, location, initialized) { }

    protected override void SpawnObjects()
    {
        if (_networkManager.IsMasterClient)
        {
            if (_networkManager.CurrentRoom.PedestalScenePedestal == null)
                _networkManager.SpawnStaticObject(_gameManager.PedestalScenePedestalPrefab.name, _networkManager.CurrentRoom.PedestalPose);
            else
                _networkManager.CurrentRoom.PedestalScenePedestal.Reset();

            if (_networkManager.CurrentRoom.PedestalScenePressurePlate1 == null)
                _networkManager.SpawnStaticObject(_gameManager.PedestalScenePressurePlate1Prefab.name, _networkManager.CurrentRoom.PressurePlate1Pose);
            else
                _networkManager.CurrentRoom.PedestalScenePressurePlate1.Reset();

            if (_networkManager.CurrentRoom.PedestalScenePressurePlate2 == null)
                _networkManager.SpawnStaticObject(_gameManager.PedestalScenePressurePlate2Prefab.name, _networkManager.CurrentRoom.PressurePlate2Pose);
            else
                _networkManager.CurrentRoom.PedestalScenePressurePlate2.Reset();

            if (_networkManager.CurrentRoom.PedestalScenePressurePlate3 == null)
                _networkManager.SpawnStaticObject(_gameManager.PedestalScenePressurePlate3Prefab.name, _networkManager.CurrentRoom.PressurePlate3Pose);
            else
                _networkManager.CurrentRoom.PedestalScenePressurePlate3.Reset();
        }
        else
            EnableObjects(true);
    }

    protected override IEnumerator Progression()
    {
        yield return base.Progression();

        string instructions = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/PedestalScene/Instructions_Start");
        yield return ShowInstructionsAndCheckReady(instructions);

        ObjectManipulation.Instance.InteractionEnabled = true;

        yield return new WaitUntil(() =>
            _networkManager.CurrentRoom.PedestalScenePedestal.State >= PedestalScenePedestal.PedestalState.PipesFull);

        bool completed;
        if (_networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PedestalTop,
                out completed) && !completed)
        {
            FocalPointManager.Instance.EnableGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PedestalTop);
            yield return new WaitUntil(() =>
                _networkManager.CurrentRoom.GetGazeConvergenceStatus(GazeConvergenceFocalPoint.Type.PedestalTop,
                    out completed) && completed);
            FocalPointManager.Instance.DestroyGazeConvergenceFocalPoint(GazeConvergenceFocalPoint.Type.PedestalTop);
        }

        if (_networkManager.IsMasterClient &&
            _networkManager.CurrentRoom.PedestalScenePedestal.State == PedestalScenePedestal.PedestalState.PipesFull)
        {
            _networkManager.CurrentRoom.PedestalScenePedestal.State = PedestalScenePedestal.PedestalState.TopOpen;
        }

        yield return new WaitForSeconds(7f);
        string completionText = Lean.Localization.LeanLocalization.GetTranslationText("ARScene/SceneCompleted");
        ShowCompletionTextAndNextSceneButton(completionText);
    }

    public override void Destroy()
    {
        base.Destroy();
        if (FocalPointManager.Instance != null) FocalPointManager.Instance.DisableCurrentGazeConvergenceFocalPoint();
        if (ObjectManipulation.Instance != null) ObjectManipulation.Instance.InteractionEnabled = false;
    }

    public static void EnableObjects(bool enabled)
    {
        if (PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePedestal != null)
            PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePedestal.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePressurePlate1 != null)
            PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePressurePlate1.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePressurePlate2 != null)
            PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePressurePlate2.Enable(enabled);
        if (PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePressurePlate3 != null)
            PhotonNetworkManager.Instance.CurrentRoom.PedestalScenePressurePlate3.Enable(enabled);
    }

}
