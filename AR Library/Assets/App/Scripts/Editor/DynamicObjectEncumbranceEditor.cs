﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DynamicObject), true)]
public class DynamicObjectEncumbranceEditor : Editor
{
    private DynamicObject _dynObj;
    private bool _handlesEnabled;

    void OnEnable()
    {
        _dynObj = (target as DynamicObject);
        _handlesEnabled = false;
    }

    void OnDisable()
    {
        if (_handlesEnabled) Tools.hidden = false;
    }

    public void OnSceneGUI()
    {
        if (!_handlesEnabled) return;

        EditorGUI.BeginChangeCheck();

        Handles.color = Color.blue;
        float radius = Handles.RadiusHandle(Quaternion.identity, _dynObj.transform.position + _dynObj.RotationCenter, _dynObj.EncumbranceRadius);
        Vector3 pos = Handles.PositionHandle(_dynObj.transform.position + _dynObj.RotationCenter, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Changed Encumbrance Radius");
            _dynObj.EncumbranceRadius = radius;
            _dynObj.RotationCenter = pos - _dynObj.transform.position;
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (!_handlesEnabled)
        {
            if (GUILayout.Button("Edit Encumbrance Mesh"))
            {
                _handlesEnabled = true;
                Tools.hidden = true;
                SceneView.RepaintAll();
            }
        }
        else
        {
            if (GUILayout.Button("Confirm Encumbrance Mesh"))
            {
                _handlesEnabled = false;
                Tools.hidden = false;
                SceneView.RepaintAll();
            }
        }
    }

}