﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using MyBox;
using MyUtils;
using UnityEngine;
using UnityEngine.Networking;

public class OnlineLogger : SingletonInScene<OnlineLogger>
{
    [Serializable]
    public struct TaskCompletionFormData
    {
        public string URL;
        public string RoomNameField;
        public string SubSceneTypeField;
        public string SubSceneInteractionField;
        public string SubSceneInteractionFocusField;
        public string SubSceneInputField;
        public string SubSceneVisualizationField;
        public string SubSceneLocationField;
        public string SubSceneVisualizationSequenceField;
        public string CompletionTimeField;
        public string PlayerNameField;
        public string PlayerActorNumberField;
        public string PlayerIndexField;
        public string PlayerIsHelperField;
    }

    public TaskCompletionFormData TaskCompletionForm;
    
    public enum EventType
    {
        RoomCreated,
        PlayerEntered,
        PlayerLeft,
        PlayerStateChanged,
        PlayerFocalPointCreated,
        PlayerFocalPointDestroyed,
        StaticObjectSpawned,
        DynamicObjectSpawned,
        RoomSceneChanged,
        SceneStarted,
        SceneCompleted,
        ObjectGrabbed,
        ObjectPlaced,
        GazeCompleted,
        ManipCompleted,
        PosCompleted
    }

    [Serializable]
    public struct EventsFormData
    {
        public string URL;
        public string RoomNameField;
        public string PlayerNameField;
        public string PlayerActorNumberField;
        public string PlayerIndexField;
        public string PlayerIsHelperField;
        public string PlayerStateField;
        public string SubSceneTypeField;
        public string SubSceneInteractionField;
        public string SubSceneInteractionFocusField;
        public string SubSceneInputField;
        public string SubSceneVisualizationField;
        public string SubSceneLocationField;
        public string SubSceneVisualizationSequenceField;
        public string EventField;
        public string TargetGameObjectField;
        public string TargetIsUsefulField;
    }
    public EventsFormData GameEventsForm;

    private PhotonNetworkManager _networkManager;

    #region unity

    private void Start()
    {
        _networkManager = PhotonNetworkManager.Instance;

        if (_networkManager.LocalPlayer.PlayerIndex != 0)
        {
            LocalPlayer_ColorSetEvent(_networkManager.LocalPlayer, _networkManager.LocalPlayer.Color);
        }
        else
        {
            _networkManager.LocalPlayer.ColorSetEvent += LocalPlayer_ColorSetEvent;
        }

        _networkManager.PlayerEnteredRoomEvent += _networkManager_PlayerEnteredRoomEvent;
        _networkManager.PlayerLeftRoomEvent += _networkManager_PlayerLeftRoomEvent;

        _networkManager.StaticObjectSpawnedEvent += _networkManager_StaticObjectSpawnedEvent;
        _networkManager.DynamicObjectSpawnedEvent += _networkManager_DynamicObjectSpawnedEvent;
    }

    private void _networkManager_DynamicObjectSpawnedEvent(GameObject gameObject)
    {
        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.DynamicObjectSpawned, gameObject);
    }

    private void _networkManager_StaticObjectSpawnedEvent(GameObject gameObject)
    {
        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.StaticObjectSpawned, gameObject);
    }

    private void LocalPlayer_ColorSetEvent(PlayerInfo player, Color newValue)
    {
        _networkManager.LocalPlayer.PlayerStateChangedEvent += LocalPlayer_PlayerStateChangedEvent;
        _networkManager.CurrentRoom.RoomSceneChangedEvent += CurrentRoom_RoomSceneChangedEvent;
        _networkManager.LocalPlayer.ObjectHeldChangedEvent += LocalPlayer_ObjectHeldChangedEvent;

        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.RoomCreated);
    }
    private void LocalPlayer_PlayerStateChangedEvent(PlayerInfo player, GameUtils.PlayerState newValue)
    {
        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.PlayerStateChanged);
    }
    private void CurrentRoom_RoomSceneChangedEvent(SubSceneManager.RoomScene newValue)
    {
        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.RoomSceneChanged);
    }
    private void LocalPlayer_ObjectHeldChangedEvent(PlayerInfo player, GameObject newValue)
    {
        if (_networkManager.IsMasterClient)
        {
            if(newValue == null)
                SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.ObjectPlaced);
            else
                SendGameEvent(_networkManager.CurrentRoom, _networkManager.LocalPlayer, EventType.ObjectGrabbed, newValue);
        }
    }

    private void _networkManager_PlayerEnteredRoomEvent(PlayerInfo player)
    {
        if (player.PlayerIndex != 0)
        {
            OtherPlayer_ColorSetEvent(player, player.Color);
        }
        else
        {
            player.ColorSetEvent += OtherPlayer_ColorSetEvent;
        }
    }
    private void OtherPlayer_ColorSetEvent(PlayerInfo player, Color newValue)
    {
        player.PlayerStateChangedEvent += OtherPlayer_PlayerStateChangedEvent;
        player.ObjectHeldChangedEvent += OtherPlayer_ObjectHeldChangedEvent;

        if(_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, player, EventType.PlayerEntered);
    }
    private void OtherPlayer_PlayerStateChangedEvent(PlayerInfo player, GameUtils.PlayerState newValue)
    {
        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, player, EventType.PlayerStateChanged);
    }
    private void _networkManager_PlayerLeftRoomEvent(PlayerInfo player)
    {
        if (_networkManager.IsMasterClient)
            SendGameEvent(_networkManager.CurrentRoom, player, EventType.PlayerLeft);
    }
    private void OtherPlayer_ObjectHeldChangedEvent(PlayerInfo player, GameObject newValue)
    {
        if (_networkManager.IsMasterClient)
        {
            if (newValue == null)
                SendGameEvent(_networkManager.CurrentRoom, player, EventType.ObjectPlaced);
            else
                SendGameEvent(_networkManager.CurrentRoom, player, EventType.ObjectGrabbed, newValue);
        }
    }

    #endregion

    #region public methods

    public void SendTaskCompletion(string roomName, SubSceneManager.RoomScene roomScene, float completionTime, PlayerInfo masterPlayer = null)
    {
        Debug.Log("OnlineLogger/SendTaskCompletion: " + roomName + ", " + roomScene.Type + " (" +
                  roomScene.TaskInfo.Interaction + " " + roomScene.TaskInfo.InteractionFocus + " " +
                  roomScene.TaskInfo.Input + " " + roomScene.TaskInfo.Visualization + " " +
                  roomScene.TaskInfo.Location + "), " + completionTime + ".");
        StartCoroutine(PostTaskCompletion(roomName, roomScene, completionTime, masterPlayer));
    }

    private IEnumerator PostTaskCompletion(string roomName, SubSceneManager.RoomScene roomScene, float completionTime,
        PlayerInfo masterPlayer = null)
    {
        WWWForm form = new WWWForm();
        form.AddField(TaskCompletionForm.RoomNameField, roomName);
        form.AddField(TaskCompletionForm.SubSceneTypeField, roomScene.Type.ToString());
        form.AddField(TaskCompletionForm.SubSceneInteractionField, roomScene.TaskInfo.Interaction.ToString());
        form.AddField(TaskCompletionForm.SubSceneInteractionFocusField, roomScene.TaskInfo.InteractionFocus.ToString());
        form.AddField(TaskCompletionForm.SubSceneInputField, roomScene.TaskInfo.Input.ToString());
        form.AddField(TaskCompletionForm.SubSceneVisualizationField, roomScene.TaskInfo.Visualization.ToString());
        form.AddField(TaskCompletionForm.SubSceneLocationField, roomScene.TaskInfo.Location.ToString());
        form.AddField(TaskCompletionForm.SubSceneVisualizationSequenceField, roomScene.TaskInfo.VisualizationSequence.ToString());
        form.AddField(TaskCompletionForm.CompletionTimeField, completionTime.ToString("F2", new CultureInfo("it-IT")));
        form.AddField(TaskCompletionForm.PlayerNameField, masterPlayer != null ? masterPlayer.Name : "null");
        form.AddField(TaskCompletionForm.PlayerActorNumberField, masterPlayer != null ? masterPlayer.ActorNumber.ToString() : "null");
        form.AddField(TaskCompletionForm.PlayerIndexField, masterPlayer != null ? masterPlayer.PlayerIndex.ToString() : "null");
        form.AddField(TaskCompletionForm.PlayerIsHelperField, masterPlayer != null ? masterPlayer.IsMasterClient.ToString() : "null");

        using (UnityWebRequest www = UnityWebRequest.Post(TaskCompletionForm.URL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError(www.error);
            }
            else
            {
                Debug.Log("OnlineLogger/SendTaskCompletion: Form upload complete!");
            }
        }
    }

    public void SendGameEvent(CurrentRoomInfo roomInfo, PlayerInfo player, EventType eventType, 
        GameObject target = null, bool isTargetUseful = false)
    {
        Debug.Log("OnlineLogger/SendGameEvent: " + player.Name + ", " + eventType + ".");
        StartCoroutine(PostGameEvent(roomInfo, player, eventType, target, isTargetUseful));

    }

    private IEnumerator PostGameEvent(CurrentRoomInfo roomInfo, PlayerInfo player, EventType eventType,
        GameObject target = null, bool isTargetUseful = false)
    { 
        string roomName = roomInfo.Name;
        string playerName = player.Name;
        string playerActorNumber = player.ActorNumber.ToString();
        string playerIndex = "null";
        string playerIsHelper = "null";
        string playerState = "null";

        if (!eventType.Equals(EventType.PlayerLeft))
        {
            playerIndex = player.PlayerIndex.ToString();
            playerIsHelper = player.IsMasterClient.ToString();
            playerState = player.State.ToString();
        }

        string subSceneType = roomInfo.RoomScene.Type.ToString();
        string subSceneInteraction = roomInfo.RoomScene.TaskInfo.Interaction.ToString();
        string subSceneInteractionFocus = roomInfo.RoomScene.TaskInfo.InteractionFocus.ToString();
        string subSceneInput = roomInfo.RoomScene.TaskInfo.Input.ToString();
        string subSceneVisualization = roomInfo.RoomScene.TaskInfo.Visualization.ToString();
        string subSceneLocation = roomInfo.RoomScene.TaskInfo.Location.ToString();
        string subSceneVisualizationSequence = roomInfo.RoomScene.TaskInfo.VisualizationSequence.ToString();
        string eventName = eventType.ToString();
        string targetGameObject = "null";
        string targetIsUseful = "null";

        switch (eventType)
        {
            case EventType.RoomCreated:
                playerIsHelper = "null";
                break;
            case EventType.PlayerEntered:
                playerIsHelper = "null";
                break;
            case EventType.PlayerLeft:
                break;
            case EventType.PlayerStateChanged:
                break;
            case EventType.PlayerFocalPointCreated:
                targetGameObject = target == null ? "null" : target.name;
                targetIsUseful = isTargetUseful.ToString();
                break;
            case EventType.StaticObjectSpawned:
                targetGameObject = target == null ? "null" : target.name;
                break;
            case EventType.DynamicObjectSpawned:
                targetGameObject = target == null ? "null" : target.name;
                break;
            case EventType.RoomSceneChanged:
                break;
            case EventType.SceneStarted:
                break;
            case EventType.SceneCompleted:
                break;
            case EventType.ObjectGrabbed:
                targetGameObject = target == null ? "null" : target.name;
                break;
            case EventType.ObjectPlaced:
                break;
            case EventType.PlayerFocalPointDestroyed:
                break;
            case EventType.GazeCompleted:
                break;
            case EventType.ManipCompleted:
                break;
            case EventType.PosCompleted:
                break;
        }


        WWWForm form = new WWWForm();
        form.AddField(GameEventsForm.RoomNameField, roomName);
        form.AddField(GameEventsForm.PlayerNameField, playerName);
        form.AddField(GameEventsForm.PlayerActorNumberField, playerActorNumber);
        form.AddField(GameEventsForm.PlayerIndexField, playerIndex);
        form.AddField(GameEventsForm.PlayerIsHelperField, playerIsHelper);
        form.AddField(GameEventsForm.PlayerStateField, playerState);
        form.AddField(GameEventsForm.SubSceneTypeField, subSceneType);
        form.AddField(GameEventsForm.SubSceneInteractionField, subSceneInteraction);
        form.AddField(GameEventsForm.SubSceneInteractionFocusField, subSceneInteractionFocus);
        form.AddField(GameEventsForm.SubSceneInputField, subSceneInput);
        form.AddField(GameEventsForm.SubSceneVisualizationField, subSceneVisualization);
        form.AddField(GameEventsForm.SubSceneLocationField, subSceneLocation);
        form.AddField(GameEventsForm.SubSceneVisualizationSequenceField, subSceneVisualizationSequence);
        form.AddField(GameEventsForm.EventField, eventName);
        form.AddField(GameEventsForm.TargetGameObjectField, targetGameObject);
        form.AddField(GameEventsForm.TargetIsUsefulField, targetIsUseful);

        using (UnityWebRequest www = UnityWebRequest.Post(GameEventsForm.URL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                //if (www.responseCode == 429)
                    //Debug.LogErrorFormat("Error: {0} {1} {2}", www.responseCode, www.error, string.Join(Environment.NewLine, www.GetResponseHeaders()));
                //else
                    Debug.LogError(www.error);
            }
            else
            {
                Debug.Log("OnlineLogger/SendGameEvent: Form upload complete!");
            }
        }
    }
    #endregion
}
