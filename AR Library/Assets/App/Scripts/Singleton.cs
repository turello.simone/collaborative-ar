﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyUtils
{
    public class Singleton<T> : MonoBehaviour where T : Component
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                    if (_instance == null)
                    {
                        Debug.LogError("Singleton Instance caused: " + typeof(T).Name + " not found on scene");

                        var gameObj = new GameObject();
                        gameObj.name = typeof(T).Name;
                        _instance = gameObj.AddComponent<T>();
                    }
                }

                return _instance;
            }
        }

        public virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
