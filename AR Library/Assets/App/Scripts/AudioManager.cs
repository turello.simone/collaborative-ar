﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class AudioManager : MyUtils.Singleton<AudioManager>
{
    public enum MusicType
    {
    }

    public enum SFXType
    {
        Undefined,
        Success,
        PopStart,
        PopClose,
        PingGazeConvergence,
        BoopPlayerFocalPoint,
        LowBoopPlayerFocalPoint,
        ButtonPressed,
        ButtonPressedUp,
        MetalClunk,
        MetalDrag,
        StoneScraping
    }

    [Serializable]
    public struct SfxClip
    {
        public AudioClip AudioClip;
        public float Volume;
        public SFXType Type;
        public float Cooldown;
        [NonSerialized] public float LastPlayTime;
    }

    /*[Header("Music")]
    [MustBeAssigned] public AudioMixer MusicMixer;
    [MustBeAssigned] public AudioSource MusicAudioSource;*/
    [Header("Sound Effects")]
    [MustBeAssigned] public AudioMixer SFXMixer;
    [MustBeAssigned] public AudioSource SFXSource;

    public SfxClip[] SfxClips;

    private PhotonNetworkManager _networkManager;

    private void Start()
    {
        _networkManager = PhotonNetworkManager.Instance;
        _networkManager.LocalPlayer.ObjectHeldChangedEvent += (player, value) =>
        {
            Vibration.VibrateAndroid(200);
            if (value != null)
                PlaySoundEffect(SFXType.PopStart);
            else
                PlaySoundEffect(SFXType.PopClose);
        };
    }

    public void PlaySoundEffect(int index)
    {
        if (index >= 0 && SfxClips.Length > index)
        {
            SfxClip sfxClip = SfxClips[index];
            SFXSource.PlayOneShot(sfxClip.AudioClip, sfxClip.Volume);
        }
    }
    
    public void PlaySoundEffect(SFXType sfxType)
    {
        List<int> sounds = new List<int>();

        for (var i = 0; i < SfxClips.Length; i++)
        {
            if (SfxClips[i].Type.Equals(sfxType))
            {
                sounds.Add(i);
            }
        }

        if (Time.time < SfxClips[sounds.First()].LastPlayTime + SfxClips[sounds.First()].Cooldown) return;

        if (sounds.Count == 1)
        {
            SfxClips[sounds.First()].LastPlayTime = Time.time;
            SFXSource.PlayOneShot(SfxClips[sounds.First()].AudioClip, SfxClips[sounds.First()].Volume);
        }
        else if (sounds.Count > 1)
        {
            SfxClip clip = SfxClips[Random.Range(0, sounds.Count)];
            clip.LastPlayTime = Time.time;
            SFXSource.PlayOneShot(clip.AudioClip, clip.Volume);
        }
    }
}
