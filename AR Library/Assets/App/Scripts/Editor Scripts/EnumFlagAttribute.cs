﻿using UnityEngine;
/// <summary>
/// Attribute declaration for EnumFlagDrawer, to be used on Enum with [Flags] set.
/// </summary>
public class EnumFlagAttribute : PropertyAttribute
{
}