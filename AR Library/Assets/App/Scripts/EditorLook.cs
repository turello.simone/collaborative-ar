﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class EditorLook : MonoBehaviour
{
    public GameObject target;

    public GameObject reference;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target.transform, reference.transform.up);
    }
}
