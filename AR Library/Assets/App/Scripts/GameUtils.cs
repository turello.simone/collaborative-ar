﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public static class GameUtils {

    public delegate void StaticObjectSpawned(GameObject gameObject);
    public delegate void DynamicObjectSpawned(GameObject gameObject);
    public delegate void MeshReceived(string meshObjectName, Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale);

    public delegate void Connected();
    public delegate void Disconnected(string message);
    public delegate void LobbyJoined();
    public delegate void LobbyLeft();
    public delegate void RoomCreated();
    public delegate void RoomCreatedFailed(string message);
    public delegate void RoomJoined();
    public delegate void RoomJoinedFailed(string message);
    public delegate void RoomLeft();
    public delegate void RoomListUpdated(Dictionary<string, RoomInfo> listUpdated);
    public delegate void PlayerEnteredRoom(PlayerInfo player);
    public delegate void PlayerLeftRoom(PlayerInfo player);
    public delegate void PlayerColorSet(PlayerInfo player);

    public const string PLAYER_STATE = "PlayerState";
    public const string PLAYER_COLOR1 = "PlayerColor1";
    public const string PLAYER_COLOR2 = "PlayerColor2";
    public const string PLAYER_COLOR3 = "PlayerColor3";
    public const string GAZE_ON_FOCALPOINT = "GazeOnFocalPoint";
    public const string READY = "Ready";
    public const string HELPER = "Helper";
    public const string ROOM_SCENE = "RoomScene";
    public const string CONTRAPTION_POSITION = "ContraptionPosition";
    public const string CONTRAPTION_ROTATION = "ContraptionRotation";
    public const string PEDESTAL_POSITION = "PedestalPosition";
    public const string PEDESTAL_ROTATION = "PedestalRotation";
    public const string PRESSUREPLATE1_POSITION = "PressurePlate1Position";
    public const string PRESSUREPLATE1_ROTATION = "PressurePlate1Rotation";
    public const string PRESSUREPLATE2_POSITION = "PressurePlate2Position";
    public const string PRESSUREPLATE2_ROTATION = "PressurePlate2Rotation";
    public const string PRESSUREPLATE3_POSITION = "PressurePlate3Position";
    public const string PRESSUREPLATE3_ROTATION = "PressurePlate3Rotation";
    public const string FLASHLIGHT1_POSITION = "Flashlight1Position";
    public const string FLASHLIGHT1_ROTATION = "Flashlight1Rotation";
    public const string FLASHLIGHT2_POSITION = "Flashlight2Position";
    public const string FLASHLIGHT2_ROTATION = "Flashlight2Rotation";
    public const string FLASHLIGHT3_POSITION = "Flashlight3Position";
    public const string FLASHLIGHT3_ROTATION = "Flashlight3Rotation";
    public const string RIDDLE_POSITION = "RiddlePosition";
    public const string RIDDLE_ROTATION = "RiddleRotation";
    public const string PAINTING_POSITION = "PaintingPosition";
    public const string PAINTING_ROTATION = "PaintingRotation";
    public const string CANDLEHOLDER_POSITION = "CandleHolderPosition";
    public const string CANDLEHOLDER_ROTATION = "CandleHolderRotation";
    public const string CHALICE_POSITION = "ChalicePosition";
    public const string CHALICE_ROTATION = "ChaliceRotation";
    public const string DAGGER_POSITION = "DaggerPosition";
    public const string DAGGER_ROTATION = "DaggerRotation";
    public const string SKULL_POSITION = "SkullPosition";
    public const string SKULL_ROTATION = "SkullRotation";
    public const string FOCALPOINT = "FocalPoint";
    public const string ASPECT_RATIO = "AspectRatio";
    public const string FIELD_OF_VIEW = "FieldOfView";
    public const string SIMPLE_GAMEMODE = "SimpleGameMode";

    [Flags]
    public enum PlayerVisualization
    {
        None = 0,
        Name = 1 << 0,
        Frustum = 1 << 1,
        Avatar = 1 << 2
    }

    //public enum PlayerVisualization : int { None, NameOnly, FrustumOnly, NameAvatar, NameFrustum, NameAvatarFrustum }
    public enum AvatarType : int { Simple, Oculus }

    public enum GazeVisualization : int { None, Cursor, CursorRay }
    public enum SimpleGameMode : int { Uninitialized, Enabled, Disabled }

    [Flags]
    public enum ObjectManipulationVisualization
    {
        None = 0,
        Hand = 1 << 0,
        Outline = 1 << 1,
        Ray = 1 << 2
    }

    public enum GazeHitType : int { None, Surface, Object }

    public enum DynamicObjectState : int { HeldInAir, HeldOnSurface, HeldOnObject, Placed, Equipped }

    public enum PlayerState : int { Error, Synchronizing, Synchronized, InGame }

    public enum RotAxis : int { Right, Up, Forward }

    public static class Mask
    {
        public static int Default = LayerMask.GetMask("Default");
        public static int Everything = ~0;
        public static int Players = LayerMask.GetMask("Players");
        public static int DynamicObjects = LayerMask.GetMask("Dynamic Objects");
        public static int StaticObjects = LayerMask.GetMask("Static Objects");
        public static int FocalPoints = LayerMask.GetMask("Focal Points");
        public static int DetectedSurfaces = LayerMask.GetMask("Detected Surfaces");
        public static int Effects = LayerMask.GetMask("Effects");
        public static int PositionColliders = LayerMask.GetMask("Position Colliders");
        public static int Special = LayerMask.GetMask("Special");
    }

    public static class Layer
    {
        public static int Default = LayerMask.NameToLayer("Default");
        public static int Players = LayerMask.NameToLayer("Players");
        public static int DynamicObjects = LayerMask.NameToLayer("Dynamic Objects");
        public static int StaticObjects = LayerMask.NameToLayer("Static Objects");
        public static int FocalPoints = LayerMask.NameToLayer("Focal Points");
        public static int DetectedSurfaces = LayerMask.NameToLayer("Detected Surfaces");
        public static int Effects = LayerMask.NameToLayer("Effects");
        public static int PositionColliders = LayerMask.NameToLayer("Position Colliders");
        public static int Special = LayerMask.NameToLayer("Special");
    }

    /*public static Color GetColor(int colorChoice)
    {
        switch (colorChoice)
        {
            case 0: return Color.black;
            case 1: return Color.green;
            case 2: return Color.red;
            case 3: return Color.cyan;
            default: return Color.black;
        }
    }*/

    /// <summary>
    ///     Giving that "start" is part of a DynamicObject or a StaticObject, returns the root of "start" (the root must have
    ///     a DynamicObject / StaticObject script attached).
    /// </summary>
    public static GameObject GetObjectRoot(GameObject start)
    {
        if (start.layer == Layer.StaticObjects)
        {
            Transform t = start.transform;
            while (t != null)
            {
                if (t.GetComponent<StaticObject>() != null)
                {
                    return t.gameObject;
                }
                t = t.parent;
            }
        }
        else if (start.layer == Layer.DynamicObjects)
        {
            Transform t = start.transform;
            while (t != null)
            {
                if (t.GetComponent<DynamicObject>() != null)
                {
                    return t.gameObject;
                }
                t = t.parent;
            }
        }

        return null;
    }

    public static void CollidersEnabled(this GameObject gameObject, bool enabled)
    {
        foreach (var collider in gameObject.GetComponentsInChildren<Collider>())
        {
            collider.enabled = enabled;
        }
    }

    public static void RenderersEnabled(this GameObject gameObject, bool enabled)
    {
        foreach (var renderer in gameObject.GetComponentsInChildren<Renderer>())
        {
            renderer.enabled = enabled;
        }
    }

    public static void RenderersAndCollidersEnabled(this GameObject gameObject, bool enabled)
    {
        foreach (var renderer in gameObject.GetComponentsInChildren<Renderer>())
        {
            renderer.enabled = enabled;
        }

        foreach (var collider in gameObject.GetComponentsInChildren<Collider>())
        {
            collider.enabled = enabled;
        }
    }

    public static void EnabledWithScale(this GameObject gameObject, bool enabled)
    {
        if (enabled)
            gameObject.transform.localScale = Vector3.one;
        else
            gameObject.transform.localScale = Vector3.zero;
    }


    public static bool IsPointWithinCollider(Collider collider, Vector3 point)
    {
        Vector3 closest = collider.ClosestPoint(point);
        // Because closest = point if point is inside
        return closest == point;
    }

    public static void GenerateUniqueRandomNumbers(int min, int max, out int n1, out int n2, out int n3)
    {
        n1 = Random.Range(min, max);
        while ((n2 = Random.Range(min, max)) == n1) ;
        while ((n3 = Random.Range(min, max)) == n1 || n3 == n2) ;
    }

    public static void GenerateUniqueRandomNumbers(int min, int max, out int n1, out int n2)
    {
        n1 = Random.Range(min, max);
        while ((n2 = Random.Range(min, max)) == n1) ;
    }
    
    public static int[] GenerateUniqueRandomNumbers(int min, int max, int count)
    {
        if (max <= min) return null;
        int c = max - min;
        if (count > c) return null;

        List<int> possible = Enumerable.Range(min, c).ToList();
        List<int> listNumbers = new List<int>();
        for (int i = 0; i < count; i++)
        {
            int index = Random.Range(0, possible.Count);
            listNumbers.Add(possible[index]);
            possible.RemoveAt(index);
        }

        return listNumbers.ToArray();
    }

    public static T[] Shuffled<T>(this T[] array)
    {
        /*To initialize an array a of n elements to a randomly shuffled copy of source, both 0 - based:
        for i from 0 to n − 1 do
            j ← random integer such that 0 ≤ j ≤ i
        if j ≠ i
            a[i] ← a[j]
        a[j] ← source[i]*/

        /*T[] shuffled = new T[array.Length];

        for (int i = 0; i < array.Length; i++)
        {
            int j = Random.Range(0, i + 1);
            if (j != i)
            {
                shuffled[i] = shuffled[j];
            }
            shuffled[j] = array[i];
        }

        return shuffled;*/

        /*--To shuffle an array a of n elements(indices 0..n - 1):
        for i from n−1 downto 1 do
            j ← random integer such that 0 ≤ j ≤ i
            exchange a[j] and a[i]*/

        /*for (int i = array.Length - 1; i > 0 ; i--)
        {
            int j = Random.Range(0, i + 1);

            T temp = array[j];
            array[j] = array[i];
            array[i] = temp;
        }

        return array;*/

        /*--To shuffle an array a of n elements(indices 0..n - 1):
        for i from 0 to n−2 do
            j ← random integer such that i ≤ j < n
            exchange a[i] and a[j]*/

        for (int i = 0; i <= array.Length - 2; i++)
        {
            int j = Random.Range(i, array.Length);

            T temp = array[j];
            array[j] = array[i];
            array[i] = temp;
        }

        return array;
    }

    public static float GetSignedAngle(Quaternion a, Quaternion b, Vector3 axis)
    {
        float angle = 0f;
        Vector3 angleAxis = Vector3.zero;
        (b * Quaternion.Inverse(a)).ToAngleAxis(out angle, out angleAxis);
        if (Vector3.Angle(axis, angleAxis) > 90f)
        {
            angle = -angle;
        }
        return Mathf.DeltaAngle(0f, angle);
    }

    public static bool IsUseful(this GameObject gameObject)
    {
        if (gameObject == null) return false;

        GameObject parent = GetObjectRoot(gameObject);

        if (parent.GetComponent<StaticObject>())
        {
            return parent.GetComponent<StaticObject>().IsUseful(gameObject);
        }
        else if (parent.GetComponent<DynamicObject>())
        {
            return parent.GetComponent<DynamicObject>().IsUseful(gameObject);
        }

        return false;
    }

    public static void Mute(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
        }
    }

    public static void Unmute(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        }
    }
}
