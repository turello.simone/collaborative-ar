using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = System.Object;

namespace UnityStandardAssets.Utility
{
    [RequireComponent(typeof (TextMeshProUGUI))]
    public class MyFPSCounter : MonoBehaviour
    {
        const float fpsMeasurePeriod = 0.5f;
        private int m_FpsAccumulator = 0;
        private float m_FpsNextPeriod = 0;
        private int m_CurrentFps;
        const string display = "FPS: {0}";
        private TextMeshProUGUI m_Text;

        private void Awake()
        {
            if (FindObjectsOfType<MyFPSCounter>().Length > 1)
            {
                Destroy(gameObject.transform.parent.gameObject);
            }
        }

        private void Start()
        {
            m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
            m_Text = GetComponent<TextMeshProUGUI>();
            DontDestroyOnLoad(this.transform.parent);
        }


        private void Update()
        {
            // measure average frames per second
            m_FpsAccumulator++;
            if (Time.realtimeSinceStartup > m_FpsNextPeriod)
            {
                m_CurrentFps = (int) (m_FpsAccumulator/fpsMeasurePeriod);
                m_FpsAccumulator = 0;
                m_FpsNextPeriod += fpsMeasurePeriod;
                m_Text.text = string.Format(display, m_CurrentFps);
            }
        }
    }
}
