﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using DigitalRubyShared;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using Photon.Pun;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = System.Random;
using TouchPhase = DigitalRubyShared.TouchPhase;

// Handle InstantPreview input in the Editor
#if UNITY_EDITOR
using Input = GoogleARCore.InstantPreviewInput;
#endif

public class ARController : MonoBehaviour
{
    [Tooltip("Chosen AR platform.")]
    public ARUtils.ARPlatform Platform = ARUtils.ARPlatform.None;

    [Tooltip("Enable material for ar-detected surfaces.")]
    public bool EnableSurfaceMaterial = true;

    public enum SurfaceRenderEnum : int { Visualization, Occlusion, OcclusionWithShadows };
    [Tooltip("Whether to create scene surfaces to overlay the ar-detected surfaces.")]
    public SurfaceRenderEnum UseOverlaySurface = SurfaceRenderEnum.Visualization;

    [Tooltip("Material used to render the overlay surfaces.")]
    public Material SurfaceVisualizationMaterial;

    [Tooltip("Material used for overlay surface occlusion.")]
    public Material SurfaceOcclusionMaterial;

    [Tooltip("Material used for overlay surface occlusion with shadows.")]
    public Material SurfaceOcclusionWithShadowsMaterial;

    [Tooltip("Material used for shared planes.")]
    public Material SharedPlaneMaterial;

    public enum AnchorTypeEnum : int { WorldPosition, Trackable, Session };
    [Tooltip("Anchor type used.")]
    public AnchorTypeEnum AnchorType = AnchorTypeEnum.Trackable;

    public enum MeshSelectionEnum : int { Pointer, Andy, DynamicObject };
    [Tooltip("Mesh to be spawned on tap.")]
    public MeshSelectionEnum SelectedMesh = MeshSelectionEnum.Pointer;

    [Tooltip("Pointer prefab.")]
    public GameObject PointerPrefab;

    [Tooltip("Andy prefab.")]
    public GameObject AndyPrefab;

    [Tooltip("Andy prefab.")]
    public GameObject DynamicObject;

    [Tooltip("Enable light estimation.")]
    public ARUtils.LightEstimationMode LightEstimationMode = ARUtils.LightEstimationMode.AmbientIntensity;

    [Tooltip("Enable image tracking.")]
    public bool EnableImageTracking = false;

    [Tooltip("Tracked Images Database1.")]
    public Object ImageTrackingDatabase1 = null;

    [Tooltip("Tracked Images Database2.")]
    public Object ImageTrackingDatabase2 = null;

    [Tooltip("Enable automatic sending of detected meshes.")]
    public bool EnableAutoSendMeshes = false;

    [Tooltip("Movement speed of dynamic objects during manipulation (zoom/pinch).")]
    public float DynamicObjectScaleSpeed;

    [Tooltip("Rotation speed of dynamic objects during manipulation (swipe).")]
    public float DynamicObjectRotationSpeed;

    public float lightIntensity;
    public Color colorCorrection;

    public Canvas Canvas;

    public float MeshesSendRate;

    public static ARLibrary Library;
    public PhotonNetworkManager ArNetworkManager;
    private Light _light;
    private GameObject _axis;
    private Dictionary<string, GameObject> _meshesReceived;
    private GameObject _combinedMesh;
    private GameObject _objectHeld;
    private GameObject _interactedObject;

    private TapGestureRecognizer _tapGestureRecognizer;
    private ScaleGestureRecognizer _scaleGestureRecognizer;
    private OneTouchScaleGestureRecognizer _rotateGestureRecognizer;

    private int _anchorNum = 0;


    // Use this for initialization
    void Awake ()
    {
        ArNetworkManager = PhotonNetworkManager.Instance;
        ArNetworkManager.AnchorId = "MarkerSessionAnchor";
        ArNetworkManager.StaticObjectSpawnedEvent += StaticObjectSpawnedCallback;
        //ArNetworkManager.MeshReceivedEvent += MeshReceivedCallback;
        //ArNetworkManager.MeshReceivedEvent += MeshReceivedCallback2;

        _combinedMesh = new GameObject();
        _combinedMesh.AddComponent<MeshFilter>();
        _combinedMesh.AddComponent<MeshRenderer>();
        _combinedMesh.AddComponent<DetectedPlaneVisualizer>();
        _combinedMesh.name = "CombinedMesh";

        _light = GetComponentInChildren<Light>();
        Material chosenMaterial = null;
        switch (UseOverlaySurface)
        {
            case SurfaceRenderEnum.Occlusion:
                chosenMaterial = SurfaceOcclusionMaterial;
                _combinedMesh.GetComponent<MeshRenderer>().material = SurfaceOcclusionMaterial;
                break;
            case SurfaceRenderEnum.OcclusionWithShadows:
                chosenMaterial = SurfaceOcclusionWithShadowsMaterial;
                _combinedMesh.GetComponent<MeshRenderer>().material = SurfaceOcclusionWithShadowsMaterial;
                break;
            case SurfaceRenderEnum.Visualization:
                chosenMaterial = SurfaceVisualizationMaterial;
                _combinedMesh.GetComponent<MeshRenderer>().material = SharedPlaneMaterial;
                break;
        }
        if (Platform == ARUtils.ARPlatform.ARCore)
        {
            Library = ARManager.Instance.ARLibrary;

            //Library = new ARCoreLibrary(EnableSurfaceMaterial, chosenMaterial, EnableLightEstimation, EnableImageTracking, ImageTrackingDatabase);
        }
        Library.ConfigSession(false, EnableSurfaceMaterial, new [] {chosenMaterial}, LightEstimationMode, EnableImageTracking, ImageTrackingDatabase1);
        Library.ImageAnchorCreatedEvent += ImageAnchorCreatedCallback;
        Library.SurfaceDetectedEvent += surfaceGameObject => { Debug.Log("Surface " + surfaceGameObject.ToString() + " detected."); };
        Library.SurfaceUpdatedEvent += surfaceGameObject => { Debug.Log("Surface " + surfaceGameObject.ToString() + " updated."); };
        Library.SurfaceRemovedEvent += () => { Debug.Log("Surface removed."); };

        QuitOnConnectionErrors();

        _meshesReceived = new Dictionary<string, GameObject>();

        //_axis = Object.Instantiate(Resources.Load("Axis", typeof(GameObject)), new Vector3(), new Quaternion()) as GameObject;
        //_axis.name = "Axis";

    }

    void Start()
    {
        if (Platform == ARUtils.ARPlatform.ARCore || Platform == ARUtils.ARPlatform.ARKit)
        {
            //Initialize gesture recognizers

            //Create tap recognizer
            _tapGestureRecognizer = new TapGestureRecognizer();
            _tapGestureRecognizer.StateUpdated += TapGestureCallback;
            FingersScript.Instance.AddGesture(_tapGestureRecognizer);

            //Create zoom/pinch recognizer
            _scaleGestureRecognizer = new ScaleGestureRecognizer();
            _scaleGestureRecognizer.StateUpdated += ScaleGestureCallback;
            FingersScript.Instance.AddGesture(_scaleGestureRecognizer);

            //Create swipe recognizer
            _rotateGestureRecognizer = new OneTouchScaleGestureRecognizer();
            _rotateGestureRecognizer.StateUpdated += RotateGestureCallback;
            FingersScript.Instance.AddGesture(_rotateGestureRecognizer);


            //_tapGestureRecognizer.RequireGestureRecognizerToFail = _rotateGestureRecognizer;

        }
    }

    // Update is called once per frame
    void Update () {
        Library.GetLightEstimation(out lightIntensity, out colorCorrection);
        _light.color = Color.white * colorCorrection;
        _light.intensity = lightIntensity*2.0f;

        /*if (_objectHeld != null)
        {
            Ray ray = new Ray(_objectHeld.transform.position - _objectHeldCenterToPivotVector, Camera.main.transform.forward);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, 0.02f, 1 << 9))
            {
                //A static Object was hit
                _objectHeld.transform.position = hitInfo.point;
                _objectHeld.transform.up = hitInfo.normal;
                _objectHeldCenterToPivotVector.

            }
        }*/
    }

    private void QuitOnConnectionErrors()
    {

#if UNITY_ANDROID
        ARUtils.ARSessionStatus status = Library.GetSessionStatus();

        if (status.isError)
        {
            StartCoroutine(CodelabUtils.ToastAndExit(status.description, 5));
        }
#endif
    }

    void ImageAnchorCreatedCallback(string anchorId, string imageName)
    {
        Pose pose;
        Library.GetAnchorPose(anchorId, out pose);
        Debug.Log("Image Anchor Mesh: " + pose.position + " " + pose.rotation);
        GameObject andy = SpawnSelectedMeshLocal(PointerPrefab, new Color(1,1,0), pose);
        Library.AttachGameObjectToAnchor(anchorId, andy, true);
        StartCoroutine(CreateMarkerSessionAnchor());
    }

    private IEnumerator CreateMarkerSessionAnchor()
    {
        yield return new WaitForSeconds(3);

        Pose pose;
        Library.GetAnchorPose("000_Anchor", out pose);
        GameObject andy = SpawnSelectedMeshLocal(PointerPrefab, Color.blue, pose);
        Library.CreateSessionAnchor("MarkerSessionAnchor", pose);
        Library.AttachGameObjectToAnchor("MarkerSessionAnchor", andy, true);

        Library.AttachGameObjectToAnchor("MarkerSessionAnchor", _combinedMesh, true);
        ArNetworkManager.MeshReceivedEvent += MeshReceivedCallback;
        //Start timer for sending meshes
        StartCoroutine(SendMeshes());

    }

    private void TapGestureCallback(GestureRecognizer gesture)
    {
        if (gesture.State == GestureRecognizerState.Ended)
        {
            Debug.Log("Tapped at " + gesture.FocusX + ", " + gesture.FocusY);
            Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
            if (_objectHeld == null)
            { //I'm not holding a dynamic object
                //Check collision with dynamic objects
                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(screenRay, out hitInfo, 3.0f, 1 << 8))
                {
                    _objectHeld = hitInfo.transform.parent.parent.parent.gameObject;
                    ArNetworkManager.GrabDynamicObject(_objectHeld);
                    ArNetworkManager.LocalPlayer.GazeManager.Hidden = true;
                }
                else
                {
                    //Try to spawn an object
                    if (Library.RaycastFromCamera(touchPosition.x, touchPosition.y, out hitInfo))
                    {
                        SpawnSelectedMeshNetwork(new Pose(hitInfo.point, Quaternion.LookRotation(Vector3.forward, hitInfo.normal)));
                    }
                }
            }
            else
            { //I'm holding a dynamic object
                //Try to put down the object
                RaycastHit hitInfo;

                if (Library.RaycastFromCamera(touchPosition.x, touchPosition.y, out hitInfo))
                {
                    ArNetworkManager.PlaceDynamicObject(_objectHeld, hitInfo.point, Quaternion.identity);

                    _objectHeld = null;
                    ArNetworkManager.LocalPlayer.GazeManager.Hidden = false;
                }

            }
        }
    }

    private void ScaleGestureCallback(GestureRecognizer gesture)
    {
        if (_objectHeld != null && gesture.State == GestureRecognizerState.Executing)
        {
            //ScaleMultiplier assumes values between 0.25 and 1.75
            //f(x) = -(4/3) * x + (4/3)
            //f(x) rescales values between -1 and 1
            float movementOffset = (-(4.0f / 3.0f) * _scaleGestureRecognizer.ScaleMultiplier + (4.0f / 3.0f)) * DynamicObjectScaleSpeed;

            Debug.Log("ScaleMultiplier: " + _scaleGestureRecognizer.ScaleMultiplier + ", movementOffset: " + movementOffset + ", Focus: " + _scaleGestureRecognizer.FocusX + ", " + _scaleGestureRecognizer.FocusY);

            ArNetworkManager.TranslateDynamicObjectTowardsCameraForward(_objectHeld, movementOffset);
        }

    }

    private void RotateGestureCallback(GestureRecognizer gesture)
    {
        if(_objectHeld != null)
        {
            if (gesture.State == GestureRecognizerState.Began)
            {
                Vector3 touchPosition = new Vector3(gesture.FocusX, gesture.FocusY, 0);
                Ray screenRay = Camera.main.ScreenPointToRay(touchPosition);
                RaycastHit hitInfo;
                float distance = float.MaxValue;
                _interactedObject = null;
                foreach (Collider coll in _objectHeld.transform.GetComponentsInChildren<Collider>())
                {
                    if (coll.Raycast(screenRay, out hitInfo, 2.0f) && hitInfo.distance < distance)
                    {
                        distance = hitInfo.distance;
                        if (coll.gameObject.CompareTag("Interactive"))
                        {
                            _interactedObject = hitInfo.transform.gameObject;
                        }
                        else
                        {
                            _interactedObject = null;
                        }
                    }
                }

                if (_interactedObject != null)
                {
                    Animator animator = _interactedObject.GetComponent<Animator>();
                    animator.speed = 0f;
                    animator.Play("CylinderRotation");
                    //Animation animation = _interactedObject.GetComponent<Animation>();
                    /*_percentAnimation = 0.0f;
                    animator.SetFloat("PercentageTime", _percentAnimation);
                    animation.Play();
                    animation.Sample(); //forces the pose to be calculated
                    animation.Stop(); // actually commits the pose without waiting for end of frame*/

                }
            }
            else if (gesture.State == GestureRecognizerState.Executing)
            {
                if (_interactedObject != null)
                {
                    Animator animator = _interactedObject.GetComponent<Animator>();
                    //Animation animation = _interactedObject.GetComponent<Animation>();

                    Vector3 movement = Camera.main.transform.TransformDirection(_rotateGestureRecognizer.DeltaX,
                        _rotateGestureRecognizer.DeltaY, 0);

                    float percentAnimation = Mathf.Clamp(
                        animator.GetFloat("PercentageTime") +
                        Vector3.Dot(Vector3.Cross(movement, Camera.main.transform.forward), _interactedObject.transform.up) *
                        0.001f, 0.0f, 1.0f);

                    animator.SetFloat("PercentageTime", percentAnimation);
                    /*animation.Play();
                    animation.Sample(); //forces the pose to be calculated
                    animation.Stop(); // actually commits the pose without waiting for end of frame*/
                }
                else
                {
                    float rotationX = -_rotateGestureRecognizer.DeltaX * DynamicObjectRotationSpeed;
                    float rotationY = _rotateGestureRecognizer.DeltaY * DynamicObjectRotationSpeed;
                    Debug.Log("RotationX: " + rotationX + ", RotationY: " + rotationY +
                              ", DeltaX: " + _rotateGestureRecognizer.DeltaX + ", DeltaY: " + _rotateGestureRecognizer.DeltaY);

                    ArNetworkManager.RotateDynamicObject(_objectHeld, GameUtils.RotAxis.Up, rotationX);
                    ArNetworkManager.RotateDynamicObject(_objectHeld, GameUtils.RotAxis.Right, rotationY);
                }
            }
            else if (gesture.State == GestureRecognizerState.Ended)
            {
                if (_interactedObject != null)
                {
                    Animator animator = _interactedObject.GetComponent<Animator>();
                    animator.Play("Idle");

                }
            }


        }

    }

    private GameObject SpawnSelectedMeshLocal(GameObject prefab, Color color, Pose pose)
    {
        GameObject instance;
        instance = Object.Instantiate(prefab, pose.position, pose.rotation) as GameObject;
        instance.GetComponentInChildren<MeshRenderer>().material.color = color;
        Object.DontDestroyOnLoad(instance);
        return instance;
    }

    private void SpawnSelectedMeshNetwork(Pose pose)
    {
        switch (SelectedMesh)
        {
            case MeshSelectionEnum.Pointer:
                ArNetworkManager.SpawnStaticObject("Pointer", pose);
                break;
            case MeshSelectionEnum.Andy:
                ArNetworkManager.SpawnStaticObject("Andy", pose);
                break;
            case MeshSelectionEnum.DynamicObject:
                ArNetworkManager.SpawnDynamicObject("DynamicObject", pose);
                break;
        }
    }


    private void StaticObjectSpawnedCallback(GameObject gameObject)
    {
        string anchorId = "anchor" + _anchorNum;
        Ray ray = new Ray(gameObject.transform.position, gameObject.transform.up);
        Ray inverseRay = new Ray(gameObject.transform.position, -gameObject.transform.up);
        Pose hitPose;
        RaycastHit hitInfo;
        if (Library.Raycast(inverseRay, out hitInfo, 0.25f))
        {
            if (gameObject.name == "Pointer")
            {
                //Attach object received from network to the marker anchor, in its real position
                Library.AttachGameObjectToAnchor("MarkerSessionAnchor", gameObject, true);
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = new Color(1, 0.5f, 0);

                //Attach a copy of object received from network to a session anchor, in its real position
                GameObject gameObjectCopy = Instantiate(Resources.Load(gameObject.name), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                Pose pose = new Pose(gameObject.transform.position, gameObject.transform.rotation);
                Library.CreateSessionAnchor(anchorId, pose);
                Library.AttachGameObjectToAnchor(anchorId, gameObjectCopy, true);
                gameObjectCopy.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
                _anchorNum++;

                anchorId = "anchor" + _anchorNum;
                //Create a new object laying on the true local detected plane and attach it to a trackable anchor
                Library.CreateAnchorFromRay(anchorId, inverseRay, out hitPose, 0.25f);
                GameObject gameObjectCopy2 = Instantiate(Resources.Load(gameObject.name), hitPose.position, gameObject.transform.rotation) as GameObject;
                Library.AttachGameObjectToAnchor(anchorId, gameObjectCopy2, true);
                gameObjectCopy2.GetComponentInChildren<MeshRenderer>().material.color = Color.green;

                _anchorNum++;
            }
            else if(gameObject.name == "Andy")
            {
                Library.CreateAnchorFromRay(anchorId, inverseRay, out hitPose, 0.25f);
                gameObject.transform.position = hitPose.position;
                Library.AttachGameObjectToAnchor(anchorId, gameObject, true);
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.green;

                _anchorNum++;
            }

        }
        else if (Library.Raycast(ray, out hitInfo, 0.25f))
        {
            if (gameObject.name == "Pointer")
            {
                //Attach object received from network to the marker anchor, in its real position
                Library.AttachGameObjectToAnchor("MarkerSessionAnchor", gameObject, true);
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = new Color(1, 0.5f, 0);

                //Attach a copy of object received from network to a session anchor, in its real position
                GameObject gameObjectCopy = Instantiate(Resources.Load(gameObject.name), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                Pose pose = new Pose(gameObject.transform.position, gameObject.transform.rotation);
                Library.CreateSessionAnchor(anchorId, pose);
                Library.AttachGameObjectToAnchor(anchorId, gameObjectCopy, true);
                gameObjectCopy.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
                _anchorNum++;

                anchorId = "anchor" + _anchorNum;
                //Create a new object laying on the true local detected plane and attach it to a trackable anchor
                Library.CreateAnchorFromRay(anchorId, ray, out hitPose, 0.25f);
                GameObject gameObjectCopy2 = Instantiate(Resources.Load(gameObject.name), hitPose.position, gameObject.transform.rotation) as GameObject;
                Library.AttachGameObjectToAnchor(anchorId, gameObjectCopy2, true);
                gameObjectCopy2.GetComponentInChildren<MeshRenderer>().material.color = Color.green;

                _anchorNum++;
            }
            else if (gameObject.name == "Andy")
            {
                Library.CreateAnchorFromRay(anchorId, ray, out hitPose, 0.25f);
                gameObject.transform.position = hitPose.position;
                Library.AttachGameObjectToAnchor(anchorId, gameObject, true);
                gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.green;

                _anchorNum++;
            }
        }
        else
        {
            //Create a session anchor
            hitPose = new Pose(gameObject.transform.position, gameObject.transform.rotation);
            Library.CreateSessionAnchor(anchorId, hitPose);
            Library.AttachGameObjectToAnchor(anchorId, gameObject, true);
            gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            _anchorNum++;
        }
    }

    private void MeshReceivedCallback(string meshObjectName, Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        /*GameObject meshReceived;
        Debug.Log("MeshReceivedCallback: _meshesReceived: " + _meshesReceived);
        if (_meshesReceived.TryGetValue(meshObjectName + "_session", out meshReceived) == false)
        {
            Debug.Log("MeshReceivedCallback: new plane received: " + meshObjectName + "_session");
            meshReceived = new GameObject();
            meshReceived.name = meshObjectName + "_session";
            meshReceived.AddComponent<MeshFilter>();
            meshReceived.AddComponent<MeshRenderer>().material = SharedPlaneMaterial;
            //meshReceived.AddComponent<MeshRenderer>().material.color = Color.blue;

            Pose pose = new Pose(Vector3.zero, Quaternion.identity);
            string anchorId = "anchor" + _anchorNum;
            Library.CreateSessionAnchor(anchorId, pose);
            Library.AttachGameObjectToAnchor(anchorId, meshReceived, true);

            _anchorNum++;

            _meshesReceived.Add(meshObjectName + "_session", meshReceived);
        }

        Debug.Log("MeshReceivedCallback: updating: " + meshObjectName + "_session");

        meshReceived.GetComponent<MeshFilter>().mesh = mesh;
        meshReceived.transform.position = position;
        meshReceived.transform.rotation = rotation;
        meshReceived.transform.localScale = scale;*/

       GameObject meshReceived_image;
        if (_meshesReceived.TryGetValue(meshObjectName + "_image", out meshReceived_image) == false)
        {
            Debug.Log("MeshReceivedCallback: new plane received: " + meshObjectName + "_image");
            meshReceived_image = new GameObject();
            meshReceived_image.name = meshObjectName + "_image";
            meshReceived_image.AddComponent<MeshFilter>();
            //meshReceived_image.AddComponent<MeshRenderer>().materials[0] = SharedPlaneMaterial;
            
            switch (UseOverlaySurface)
            {
                case SurfaceRenderEnum.Occlusion:
                    meshReceived_image.AddComponent<MeshRenderer>().material = SurfaceOcclusionMaterial;
                    break;
                case SurfaceRenderEnum.OcclusionWithShadows:
                    meshReceived_image.AddComponent<MeshRenderer>().material = SurfaceOcclusionWithShadowsMaterial;
                    break;
                case SurfaceRenderEnum.Visualization:
                    meshReceived_image.AddComponent<MeshRenderer>().material = SharedPlaneMaterial;
                    break;
            }

            if (!EnableSurfaceMaterial)
            {
                meshReceived_image.GetComponent<MeshRenderer>().enabled = false;
            }
            //meshReceived_image.AddComponent<MeshRenderer>().material = SharedPlaneMaterial;
            //meshReceived_image.AddComponent<MeshRenderer>().material.color = new Color(1, 0.5f, 0);
            meshReceived_image.transform.position = position;
            meshReceived_image.transform.rotation = rotation;
            meshReceived_image.transform.localScale = scale;

            Library.AttachGameObjectToAnchor("MarkerSessionAnchor", meshReceived_image, true);

            _anchorNum++;

            _meshesReceived.Add(meshObjectName + "_image", meshReceived_image);
        }

        Debug.Log("MeshReceivedCallback: updating: " + meshObjectName + "_image");

        meshReceived_image.GetComponent<MeshFilter>().mesh = mesh;
        /*meshReceived_image.transform.position = position;
        meshReceived_image.transform.rotation = rotation;
        meshReceived_image.transform.localScale = scale;*/
    }

    private void MeshReceivedCallback2(string meshObjectName, Mesh mesh, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        Debug.Log("MeshReceivedCallback2: creating new object.");

        GameObject meshReceived = new GameObject();
        meshReceived.name = meshObjectName;
        meshReceived.AddComponent<MeshFilter>().mesh = mesh;
        meshReceived.AddComponent<MeshRenderer>().material = SurfaceVisualizationMaterial;
        meshReceived.transform.position = position;
        meshReceived.transform.rotation = rotation;
        meshReceived.transform.localScale = scale;

        Pose pose = new Pose(position, rotation);
        string anchorId = "anchor" + _anchorNum;
        Library.CreateSessionAnchor(anchorId, pose);
        Library.AttachGameObjectToAnchor(anchorId, meshReceived, true);
        _anchorNum++;

    }


    private void SendDetectedMeshes(object sender, ElapsedEventArgs e)
    {
        Debug.Log("Sending detected meshes.");
        int i = 0;
        foreach (var surface in Library.DetectedSurfacesList)
        {
            ArNetworkManager.SendMesh(surface.name + i, surface.GetComponent<MeshFilter>().mesh, surface.transform);
            i++;
        }
    }

    IEnumerator SendMeshes()
    {
        Debug.Log("SendMeshes Coroutine: waiting.");

        yield return new WaitForSeconds(MeshesSendRate);
        Debug.Log("SendMeshes Coroutine: waited.");

        if (ArNetworkManager.IsMasterClient && EnableAutoSendMeshes)
        {
            Debug.Log("SendMeshes Coroutine: Sending detected meshes.");

            CombineInstance[] combine = new CombineInstance[Library.DetectedSurfacesList.Count];

            int i = 0;
            foreach (var surface in Library.DetectedSurfacesList)
            {
                combine[i].mesh = surface.GetComponent<MeshFilter>().mesh;
                combine[i].transform = surface.transform.localToWorldMatrix;

                i++;
            }

            _combinedMesh.transform.GetComponent<MeshFilter>().mesh = new Mesh();
            _combinedMesh.transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);

            ArNetworkManager.SendMesh(_combinedMesh.name, _combinedMesh.GetComponent<MeshFilter>().mesh, _combinedMesh.transform);

            /*int i = 0;
            foreach (var surface in Library.DetectedSurfacesList)
            {
                ArNetworkManager.SendMeshEvent(surface.name + i, surface.GetComponent<MeshFilter>().mesh, surface.transform);
                i++;
            }*/
        }
        StartCoroutine(SendMeshes());
    }


    public void WorldPositionToggle(bool value)
    {
        if (value)
        {
            AnchorType = AnchorTypeEnum.WorldPosition;
        }
    }
    public void TrackableToggle(bool value)
    {
        if (value)
        {
            AnchorType = AnchorTypeEnum.Trackable;
        }
    }
    public void SessionToggle(bool value)
    {
        if (value)
        {
            AnchorType = AnchorTypeEnum.Session;
        }
    }

    public void SurfaceDropdownValueChanged(int value)
    {
        switch (value)
        {
            case 0:
                EnableSurfaceMaterial = false;
                Library.EnableSurfaceMaterial = false;

                _combinedMesh.GetComponent<MeshRenderer>().enabled = false;
                foreach (var m in _meshesReceived.Values)
                {
                    m.GetComponent<MeshRenderer>().enabled = false;
                }

                break;
            case 1:
                if (!EnableSurfaceMaterial)
                {
                    EnableSurfaceMaterial = true;
                    Library.EnableSurfaceMaterial = true;

                    _combinedMesh.GetComponent<MeshRenderer>().enabled = true;
                    foreach (var m in _meshesReceived.Values)
                    {
                        m.GetComponent<MeshRenderer>().enabled = true;
                    }
                }

                UseOverlaySurface = SurfaceRenderEnum.Visualization;
                Library.SurfaceMaterials = new []{SurfaceVisualizationMaterial};

                _combinedMesh.GetComponent<MeshRenderer>().material = SharedPlaneMaterial;
                foreach (var m in _meshesReceived.Values)
                {
                    m.GetComponent<MeshRenderer>().material = SharedPlaneMaterial;
                }

                break;
            case 2:
                if (!EnableSurfaceMaterial)
                {
                    EnableSurfaceMaterial = true;
                    Library.EnableSurfaceMaterial = true;
                    _combinedMesh.GetComponent<MeshRenderer>().enabled = true;
                    foreach (var m in _meshesReceived.Values)
                    {
                        m.GetComponent<MeshRenderer>().enabled = true;
                    }
                }

                UseOverlaySurface = SurfaceRenderEnum.Occlusion;
                Library.SurfaceMaterials = new []{SurfaceOcclusionMaterial};

                _combinedMesh.GetComponent<MeshRenderer>().material = SurfaceOcclusionMaterial;
                foreach (var m in _meshesReceived.Values)
                {
                    m.GetComponent<MeshRenderer>().material = SurfaceOcclusionMaterial;
                }
                break;
            case 3:
                if (!EnableSurfaceMaterial)
                {
                    EnableSurfaceMaterial = true;
                    Library.EnableSurfaceMaterial = true;
                    _combinedMesh.GetComponent<MeshRenderer>().enabled = true;
                    foreach (var m in _meshesReceived.Values)
                    {
                        m.GetComponent<MeshRenderer>().enabled = true;
                    }
                }
                UseOverlaySurface = SurfaceRenderEnum.OcclusionWithShadows;
                Library.SurfaceMaterials = new []{SurfaceOcclusionWithShadowsMaterial};

                _combinedMesh.GetComponent<MeshRenderer>().material = SurfaceOcclusionWithShadowsMaterial;
                foreach (var m in _meshesReceived.Values)
                {
                    m.GetComponent<MeshRenderer>().material = SurfaceOcclusionWithShadowsMaterial;
                }
                break;
        }
    }

    public void LightEstimationToggle(bool value)
    {
        LightEstimationMode = value ? ARUtils.LightEstimationMode.AmbientIntensity : ARUtils.LightEstimationMode.Disabled;
        Library.LightEstimationMode = LightEstimationMode;
    }

    public void ResetAnchorsButton()
    {
        var anchorList = new List<string>(Library.AnchoredObjectsDictionary.Keys);
        foreach (var anchorId in anchorList)
        {
            List<GameObject> list;
            Library.DestroyAnchor(anchorId, out list);
            foreach (var obj in list)
            {
                Destroy(obj);
            }
        }
        Debug.Log("Destroyed " + anchorList.Count + " Anchors.");


    }

    public void RemoveAnchoredObjectsButton()
    {
        var anchorList = new List<string>(Library.AnchoredObjectsDictionary.Keys);
        foreach (var anchorId in anchorList)
        {
            List<GameObject> list;
            Library.DetachAllGameObjectsFromAnchor(anchorId, out list);
            foreach (var obj in list)
            {
                Destroy(obj);
            }
        }
    }

    public void ResetSessionButton()
    {
        if (Canvas != null) Canvas.enabled = false;
        Library.ResetSession();
        if (Canvas != null) Canvas.enabled = true;
    }

    public void ImageTrackingToggle(bool value)
    {
        EnableImageTracking = value;
        Library.EnableImageTracking = value;
    }

    public void ChangeImageDatabaseButton()
    {
        if (Library.ImageTrackingDatabase == ImageTrackingDatabase1)
        {
            Library.ImageTrackingDatabase = ImageTrackingDatabase2;
        }
        else
        {
            Library.ImageTrackingDatabase = ImageTrackingDatabase1;
        }
    }

    public void SetLightDirectionButton()
    {
        _light.transform.rotation = Camera.main.transform.rotation;
        _light.transform.Rotate(Camera.main.transform.right, 180, Space.World);


    }

    public void SendDetectedMeshButton()
    {
        Debug.Log("Sending detected meshes.");

        int i = 0;
        foreach (var surface in Library.DetectedSurfacesList)
        {
            ArNetworkManager.SendMesh(surface.name + i, surface.GetComponent<MeshFilter>().mesh, surface.transform);
            i++;
        }
    }

    public void MeshSelectionDropdownOnValueChanged(int value)
    {
        switch (value)
        {
            case 0:
                SelectedMesh = MeshSelectionEnum.Pointer;
                break;
            case 1:
                SelectedMesh = MeshSelectionEnum.Andy;
                break;
            case 2:
                SelectedMesh = MeshSelectionEnum.DynamicObject;
                break;
        }
    }

}
