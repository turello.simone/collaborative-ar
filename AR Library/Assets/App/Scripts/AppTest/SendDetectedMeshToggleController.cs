﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendDetectedMeshToggleController : MonoBehaviour {

    public ARController ArController;

    public Toggle SendDetectedMeshToggle;

    public Button SendDetectedMeshButton;

    // Use this for initialization
    void Start () {
        SendDetectedMeshToggle.isOn = ArController.EnableAutoSendMeshes;
        SendDetectedMeshButton.enabled = !SendDetectedMeshToggle.isOn;
    }

    public void OnValueChanged(bool value)
    {
        ArController.EnableAutoSendMeshes = value;
        SendDetectedMeshToggle.isOn = value;
        SendDetectedMeshButton.interactable = !value;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
