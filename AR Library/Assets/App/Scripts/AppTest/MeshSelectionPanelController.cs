﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeshSelectionPanelController : MonoBehaviour {

    public ARController ARController;
    public Dropdown SelectedMeshDropdown;

    // Use this for initialization
    void Start () {
        switch (ARController.SelectedMesh)
        {
            case ARController.MeshSelectionEnum.Pointer:
                SelectedMeshDropdown.value = 0;
                break;
            case ARController.MeshSelectionEnum.Andy:
                SelectedMeshDropdown.value = 1;
                break;
            case ARController.MeshSelectionEnum.DynamicObject:
                SelectedMeshDropdown.value = 2;
                break;
        }

    }

}
