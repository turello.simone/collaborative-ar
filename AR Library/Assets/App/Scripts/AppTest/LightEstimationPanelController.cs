﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightEstimationPanelController : MonoBehaviour
{
    public ARController ArController;
    public Toggle LightEstimationToggle;
    public Image IntensityImage;
    public Image ColorCorrectionImage;

	// Use this for initialization
	void Start () {
	    LightEstimationToggle.isOn = ArController.LightEstimationMode != ARUtils.LightEstimationMode.Disabled;
    }

    // Update is called once per frame
    void Update ()
    {
        float denormalizedLightIntensity = ArController.lightIntensity;
        IntensityImage.color = new Color(denormalizedLightIntensity, denormalizedLightIntensity, denormalizedLightIntensity);
        ColorCorrectionImage.color = ArController.colorCorrection;
    }
}
