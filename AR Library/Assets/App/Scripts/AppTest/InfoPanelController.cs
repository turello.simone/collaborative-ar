﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanelController : MonoBehaviour {

    public Text SessionStatusText;
    public Text CameraStatusText;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    SessionStatusText.text = ARController.Library.GetSessionStatus().description;
	    CameraStatusText.text = ARController.Library.GetCameraTrackingStatus().ToString();

	}
}
