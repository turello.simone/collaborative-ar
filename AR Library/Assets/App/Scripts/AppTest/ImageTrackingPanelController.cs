﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageTrackingPanelController : MonoBehaviour
{
    public ARController ArController;

    public Toggle ImageTrackingToggle;

	// Use this for initialization
	void Start ()
	{
	    ImageTrackingToggle.isOn = ArController.EnableImageTracking;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
