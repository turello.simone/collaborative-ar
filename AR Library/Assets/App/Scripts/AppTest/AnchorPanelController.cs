﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnchorPanelController : MonoBehaviour {

    public ARController ArController;
    public Toggle WorldPositionToggle;
    public Toggle TrackableToggle;
    public Toggle SessionToggle;

    // Use this for initialization
    void Start () {
        switch (ArController.AnchorType)
        {
            case ARController.AnchorTypeEnum.WorldPosition:
                WorldPositionToggle.isOn = true;
                TrackableToggle.isOn = false;
                SessionToggle.isOn = false;
                break;
            case ARController.AnchorTypeEnum.Trackable:
                WorldPositionToggle.isOn = false;
                TrackableToggle.isOn = true;
                SessionToggle.isOn = false;
                break;
            case ARController.AnchorTypeEnum.Session:
                WorldPositionToggle.isOn = false;
                TrackableToggle.isOn = false;
                SessionToggle.isOn = true;
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
