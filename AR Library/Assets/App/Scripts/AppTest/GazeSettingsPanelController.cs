﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeSettingsPanelController : MonoBehaviour
{

    public Dropdown GazeDropdown;
    public Toggle LocalGazePointerToggle;
    private PhotonNetworkManager _networkManager;

    // Use this for initialization
    void Start()
    {
        _networkManager = PhotonNetworkManager.Instance;
        LocalGazePointerToggle.isOn =
            (_networkManager.LocalGazeVisualizationSetting != GameUtils.GazeVisualization.None);
        switch (_networkManager.RemoteGazeVisualizationSetting)
        {
            case GameUtils.GazeVisualization.None:
                GazeDropdown.value = 0;
                break;
            case GameUtils.GazeVisualization.Cursor:
                GazeDropdown.value = 1;
                break;
            case GameUtils.GazeVisualization.CursorRay:
                GazeDropdown.value = 2;
                break;
        }

    }

    public void DropdownOnValueChanged(int value)
    {
        switch (value)
        {
            case 0:
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.None;
                break;
            case 1:
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
                break;
            case 2:
                _networkManager.RemoteGazeVisualizationSetting = GameUtils.GazeVisualization.CursorRay;
                break;
        }
    }

    public void LocalGazePointerOnValueChanged(bool value)
    {
        LocalGazePointerToggle.isOn = value;
        if (value)
        {
            _networkManager.LocalGazeVisualizationSetting = GameUtils.GazeVisualization.Cursor;
        }
        else
        {
            _networkManager.LocalGazeVisualizationSetting = GameUtils.GazeVisualization.None;
        }

    }
}
