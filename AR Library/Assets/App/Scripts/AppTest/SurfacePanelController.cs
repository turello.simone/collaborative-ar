﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SurfacePanelController : MonoBehaviour {

    public ARController ArController;
    public Dropdown SurfaceDropdown;

    // Use this for initialization
    void Start ()
    {
        if (!ArController.EnableSurfaceMaterial)
        {
            SurfaceDropdown.value = 0;
        }
        else
        {
            switch (ArController.UseOverlaySurface)
            {
                case ARController.SurfaceRenderEnum.Visualization:
                    SurfaceDropdown.value = 1;
                    break;
                case ARController.SurfaceRenderEnum.Occlusion:
                    SurfaceDropdown.value = 2;
                    break;
                case ARController.SurfaceRenderEnum.OcclusionWithShadows:
                    SurfaceDropdown.value = 3;
                    break;
            }
        }

    }

}
