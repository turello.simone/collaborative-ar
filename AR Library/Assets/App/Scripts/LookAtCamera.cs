﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    public bool isCanvas = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt(Camera.main.transform, Camera.main.transform.up);
	    if (isCanvas)
	    {
            transform.Rotate(0, 180f, 0);
	    }

	}
}
