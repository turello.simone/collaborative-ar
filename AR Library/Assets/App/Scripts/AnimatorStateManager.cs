﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
public class AnimatorStateManager : MonoBehaviour
{
    private Animator _animator;

    private Dictionary<int, AnimatorStateInfo> _stateInfoByLayer;
    private Dictionary<AnimatorControllerParameter, object> _parameterValues;

    public void Awake()
    {
        _stateInfoByLayer = new Dictionary<int, AnimatorStateInfo>();
        _parameterValues = new Dictionary<AnimatorControllerParameter, object>();
        _animator = GetComponent<Animator>();
    }

    public void RestoreState()
    {
        if (!_animator)
        {
            return;
        }

        foreach (KeyValuePair<int, AnimatorStateInfo> layerAndStateInfo in _stateInfoByLayer)
        {
            int layer = layerAndStateInfo.Key;
            AnimatorStateInfo stateInfo = layerAndStateInfo.Value;
            _animator.Play(stateInfo.shortNameHash, layer, stateInfo.normalizedTime);
        }

        foreach (KeyValuePair<AnimatorControllerParameter, object> parameterAndValue in _parameterValues)
        {
            AnimatorControllerParameter parameter = parameterAndValue.Key;
            object value = parameterAndValue.Value;
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Bool:
                    _animator.SetBool(parameter.name, (bool)value);
                    break;
                case AnimatorControllerParameterType.Float:
                    _animator.SetFloat(parameter.name, (float)value);
                    break;
                case AnimatorControllerParameterType.Int:
                    _animator.SetInteger(parameter.name, (int)value);
                    break;
                case AnimatorControllerParameterType.Trigger:
                    if ((bool)value)
                    {
                        _animator.SetTrigger(parameter.name);
                    }
                    break;
                default:
                    continue;
            }
        }

        //ResetInternalState();
    }

    public void SaveState()
    {
        ResetInternalState();
        if (!_animator)
        {
            return;
        }

        for (int i = 0; i < _animator.layerCount; ++i)
        {
            _stateInfoByLayer[i] = _animator.GetCurrentAnimatorStateInfo(i);
        }

        foreach (AnimatorControllerParameter parameter in _animator.parameters)
        {
            object value;
            switch (parameter.type)
            {
                case AnimatorControllerParameterType.Bool:
                case AnimatorControllerParameterType.Trigger:
                    value = _animator.GetBool(parameter.name);
                    break;
                case AnimatorControllerParameterType.Float:
                    value = _animator.GetFloat(parameter.name);
                    break;
                case AnimatorControllerParameterType.Int:
                    value = _animator.GetInteger(parameter.name);
                    break;
                default:
                    continue;
            }

            _parameterValues[parameter] = value;
        }
    }

    private void ResetInternalState()
    {
        _stateInfoByLayer.Clear();
        _parameterValues.Clear();
    }
}