﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainMenuBackgroundPanel : MonoBehaviour {

    [SerializeField, ReadOnly] private string _connectionStatusMessage;

    [Header("UI References")]
    public TextMeshProUGUI ConnectionStatusText;

    private PhotonNetworkManager _networkManager;

    #region UNITY

    public void Start()
    {
        _networkManager = PhotonNetworkManager.Instance;
        _connectionStatusMessage = Lean.Localization.LeanLocalization.GetTranslationText("MainMenu/ConnectionStatus");
    }

    public void Update()
    {
        ConnectionStatusText.text = Lean.Localization.LeanLocalization.GetTranslationText("MainMenu/ConnectionStatus") +
                                    " " + _networkManager.NetworkClientState;
    }

    public void OnItalianFlagClicked()
    {
        Lean.Localization.LeanLocalization.CurrentLanguage = "Italian";
    }
    
    public void OnEnglishFlagClicked()
    {
        Lean.Localization.LeanLocalization.CurrentLanguage = "English";
    }

    #endregion

}
