﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomEntry : MonoBehaviour
{
    public TextMeshProUGUI RoomNameText;
    public TextMeshProUGUI RoomPlayersText;
    public Button JoinRoomButton;

    private MainMenuMainPanel _mainPanel;

    public void Start()
    {
        JoinRoomButton.onClick.AddListener(() =>
        {
            _mainPanel.OnJoinButtonClicked(RoomNameText.text);
        });
    }

    public void Initialize(RoomInfo roomInfo, MainMenuMainPanel mainPanel)
    {
        _mainPanel = mainPanel;
        RoomNameText.text = roomInfo.Name;
        RoomPlayersText.text = roomInfo.PlayerCount + " / " + roomInfo.MaxPlayers;
    }
}