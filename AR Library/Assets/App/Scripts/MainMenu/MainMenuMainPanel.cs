﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuMainPanel : MonoBehaviour
{
    [Header("Login Panel")]
    public GameObject LoginPanel;
    public TMP_InputField PlayerNameInput;

    [Header("Selection Panel")]
    public GameObject SelectionPanel;

    [Header("Create Room Panel")]
    public GameObject CreateRoomPanel;
    public TMP_InputField RoomNameInputField;
    public TMP_InputField RoomPasswordInputField;
    public TextMeshProUGUI CreateRoomErrorText;

    [Header("Room List Panel")]
    public GameObject RoomListPanel;
    public GameObject RoomListContent;
    public GameObject RoomListEntryPrefab;

    [Header("Join Room Panel")]
    public GameObject JoinRoomPanel;
    public TextMeshProUGUI CurrentRoomNameText;
    public TMP_InputField PasswordInputField;
    public TextMeshProUGUI JoinRoomErrorText;

    private Dictionary<string, GameObject> _roomListEntries;
    private PhotonNetworkManager _networkManager;

    #region UNITY

    public void Awake()
    {
        _roomListEntries = new Dictionary<string, GameObject>();
        _networkManager = PhotonNetworkManager.Instance;
        if (_networkManager.IsConnected)
        {
            SetActivePanel(SelectionPanel.name);
        }
        else
        {
            SetActivePanel(LoginPanel.name);
        }
    }

    public void OnEnable()
    {
        _networkManager.ConnectedEvent += OnConnected;
        //_networkManager.RoomJoinedEvent += OnJoinedRoom;
        _networkManager.RoomJoinedFailedEvent += OnJoinRoomFailed;
        _networkManager.RoomCreatedFailedEvent += OnCreateRoomFailed;
        _networkManager.LobbyLeftEvent += OnLeftLobby;
        _networkManager.DisconnectedEvent += OnDisconnected;
        _networkManager.RoomListUpdatedEvent += OnRoomListUpdate;
    }

    public void OnDisable()
    {
        _networkManager.ConnectedEvent -= OnConnected;
        //_networkManager.RoomJoinedEvent -= OnJoinedRoom;
        _networkManager.RoomJoinedFailedEvent -= OnJoinRoomFailed;
        _networkManager.RoomCreatedFailedEvent -= OnCreateRoomFailed;
        _networkManager.LobbyLeftEvent -= OnLeftLobby;
        _networkManager.DisconnectedEvent -= OnDisconnected;
        _networkManager.RoomListUpdatedEvent -= OnRoomListUpdate;
    }


    #endregion


    #region NetworkManager CALLBACKS

    public void OnConnected()
    {
        this.SetActivePanel(SelectionPanel.name);
    }

    private void OnDisconnected(string message)
    {
        this.SetActivePanel(LoginPanel.name);
    }

    /*public void OnJoinedRoom()
    {
        //SceneManager.LoadScene(1);
    }*/

    public void OnCreateRoomFailed(string message)
    {
        CreateRoomErrorText.text = message;
        CreateRoomErrorText.enabled = true;
    }

    public void OnJoinRoomFailed(string message)
    {
        JoinRoomErrorText.text = message;
        JoinRoomErrorText.enabled = true;
    }

    public void OnLeftLobby()
    {
        ClearRoomListView();
    }

    public void OnRoomListUpdate(Dictionary<string, RoomInfo> roomList)
    {
        ClearRoomListView();
        UpdateRoomListView(roomList);
    }

    #endregion


    #region UI CALLBACKS

    public void OnLoginButtonClicked()
    {
        string playerName = PlayerNameInput.text;

        if (!playerName.Equals(""))
        {
            PlayerNameInput.GetComponent<PlayerNameInputField>().SetPlayerName(playerName);
            _networkManager.Connect(playerName);
        }
        else
        {
            Debug.LogError("Player Name is invalid.");
        }
    }

    public void OnGoToCreateRoomButtonClicked()
    {
        CreateRoomErrorText.enabled = false;
        SetActivePanel(CreateRoomPanel.name);
    }


    public void OnRoomListButtonClicked()
    {
        if (!_networkManager.InLobby)
        {
            _networkManager.JoinLobby();
        }

        SetActivePanel(RoomListPanel.name);
    }

    public void OnCreateRoomButtonClicked()
    {
        CreateRoomErrorText.enabled = false;
        string roomName = RoomNameInputField.text;
        if (RoomNameInputField.text.Equals(string.Empty))
        {
            CreateRoomErrorText.text = "Insert a room name.";
            CreateRoomErrorText.enabled = true;
            return;
        }
        /*else if(RoomPasswordInputField.text.Equals(string.Empty))
        {
            CreateRoomErrorText.text = "Insert a password.";
            CreateRoomErrorText.enabled = true;
            return;
        }*/
        _networkManager.CreateRoom(roomName, RoomPasswordInputField.text);
    }

    public void OnBackButtonClicked()
    {
        if (_networkManager.InLobby)
        {
            _networkManager.LeaveLobby();
        }

        SetActivePanel(SelectionPanel.name);
    }

    public void OnJoinButtonClicked(string roomName)
    {
        JoinRoomErrorText.enabled = false;
        CurrentRoomNameText.text = roomName;
        PasswordInputField.text = "";
        SetActivePanel(JoinRoomPanel.name);
    }

    public void OnJoinRoomButtonClicked()
    {
        _networkManager.JoinRoom(CurrentRoomNameText.text, PasswordInputField.text);
    }


    #endregion


    #region public methods

    public void SetActivePanel(string activePanel)
    {
        LoginPanel.SetActive(activePanel.Equals(LoginPanel.name));
        SelectionPanel.SetActive(activePanel.Equals(SelectionPanel.name));
        CreateRoomPanel.SetActive(activePanel.Equals(CreateRoomPanel.name));
        RoomListPanel.SetActive(activePanel.Equals(RoomListPanel.name));    // UI should call OnRoomListButtonClicked() to activate this
        JoinRoomPanel.SetActive(activePanel.Equals(JoinRoomPanel.name));
    }

    #endregion


    #region private methods

    private void ClearRoomListView()
    {
        foreach (GameObject entry in _roomListEntries.Values)
        {
            Destroy(entry.gameObject);
        }

        _roomListEntries.Clear();
    }

    private void UpdateRoomListView(Dictionary<string, RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList.Values)
        {
            GameObject entry = Instantiate(RoomListEntryPrefab);
            entry.transform.SetParent(RoomListContent.transform);
            entry.transform.localScale = Vector3.one;
            entry.GetComponent<RoomEntry>().Initialize(info, this);

            _roomListEntries.Add(info.Name, entry);
        }
    }

    #endregion

}
