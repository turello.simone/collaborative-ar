﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyBox;
using UnityEngine;
using Object = UnityEngine.Object;

public class ARManager : MonoBehaviour {

    public enum SurfaceRenderType : int { None, Visualization, VisualizationProximity, Occlusion, OcclusionWithShadows, Shadows };

    [Tooltip("Material used to render the overlay surfaces.")]
    public Material SurfaceVisualizationMaterial;

    [Tooltip("Material used for overlay surface (proximity version).")]
    public Material SurfaceVisualizationProximityMaterial;

    [Tooltip("Material used for overlay surface occlusion.")]
    public Material SurfaceOcclusionMaterial;

    [Tooltip("Material used for overlay surface occlusion with shadows.")]
    public Material SurfaceOcclusionWithShadowsMaterial;

    [Tooltip("Material used for overlay surface shadows.")]
    public Material SurfaceShadowsMaterial;

    [Tooltip("Tracked Images Database for ARCore.")]
    public Object ARCoreImageTrackingDatabase = null;

    public ARUtils.ARPlatform Platform
    {
        get { return _arPlatform; }
    }

    public ARLibrary ARLibrary
    {
        get { return _arLibraryInstance; }
    }

    public SurfaceRenderType SurfaceType1 { get; private set; }
    public SurfaceRenderType SurfaceType2 { get; private set; }

    private bool _visualizationProximityMaterialActive = false;
    private int _visualizationProximityMaterialIndex;

    private bool _occlusionWithShadowsMaterialActive = false;
    private int _occlusionWithShadowsMaterialIndex;

    #region Singleton

    //Singleton
    private static ARManager _instance = null;
    private static readonly object Padlock = new object();

    public static ARManager Instance
    {
        get
        {
            lock (Padlock)
            {
                /*if (_instance == null)
                {
                    // Search for existing instance.
                    _instance = FindObjectOfType(typeof(ARManager)) as ARManager;
                    InitializeARLibrary();
                }*/
                return _instance;
            }
        }
    }
    //static reference to ARLibrary
    private static ARLibrary _arLibraryInstance = null;
    private static ARUtils.ARPlatform _arPlatform = ARUtils.ARPlatform.None;

    private static void InitializeARLibrary()
    {
#if UNITY_EDITOR
        _arLibraryInstance = _instance.gameObject.AddComponent<ARLibraryTest>();
        _arPlatform = ARUtils.ARPlatform.UnityEditor;
#elif UNITY_ANDROID
        _arLibraryInstance = _instance.gameObject.AddComponent<ARCoreLibrary>();
        _arPlatform = ARUtils.ARPlatform.ARCore;
#elif UNITY_IOS
#elif WINDOWS_UWP
#endif
    }

    #endregion

    #region Unity

    private void Awake ()
    {
        lock (Padlock)
        {
            if (_instance == null)
            {
                _instance = this;
                InitializeARLibrary();
            }
        }

        SurfaceType1 = SurfaceRenderType.None;
        SurfaceType2 = SurfaceRenderType.None;

        SurfaceOcclusionWithShadowsMaterial.SetInt("_HoleEnabled", 0);
        SurfaceOcclusionWithShadowsMaterial.SetFloat("_HoleRadius", 0);
    }

    private void OnDestroy ()
    {
        _instance = null;
    }

    #endregion

    #region public methods

    public void SetOverlaySurfaceMaterials(SurfaceRenderType surfaceType1, SurfaceRenderType surfaceType2 = SurfaceRenderType.None)
    {
        if (!Enum.IsDefined(typeof(SurfaceRenderType), surfaceType1) ||
            !Enum.IsDefined(typeof(SurfaceRenderType), surfaceType1))
        {
            Debug.LogError("ARManager/SetOverlaySurfaceMaterials: SurfaceRenderType not valid.");
            return;
        }

        if (surfaceType1 == surfaceType2)
        {
            surfaceType2 = SurfaceRenderType.None;
        }

        SurfaceType1 = surfaceType1;
        SurfaceType2 = surfaceType2;

        Material[] materials = null;
        int arrayLength = 0;

        if (surfaceType1 != SurfaceRenderType.None)
        {
            arrayLength++;
        }
        if (surfaceType2 != SurfaceRenderType.None)
        {
            arrayLength++;
        }

        if (arrayLength == 0)
        {
            ARLibrary.SurfaceMaterials = null;
            ARLibrary.EnableSurfaceMaterial = false;
            return;
        }

        materials = new Material[arrayLength];

        int index = 0;
        switch (surfaceType1)
        {
            case SurfaceRenderType.Visualization:
                materials[index] = SurfaceVisualizationMaterial;
                index++;
                break;
            case SurfaceRenderType.VisualizationProximity:
                materials[index] = SurfaceVisualizationProximityMaterial;
                _visualizationProximityMaterialIndex = index;
                index++;
                break;
            case SurfaceRenderType.Occlusion:
                materials[index] = SurfaceOcclusionMaterial;
                index++;
                break;
            case SurfaceRenderType.OcclusionWithShadows:
                materials[index] = SurfaceOcclusionWithShadowsMaterial;
                index++;
                break;
            case SurfaceRenderType.Shadows:
                materials[index] = SurfaceShadowsMaterial;
                index++;
                break;
        }

        switch (surfaceType2)
        {
            case SurfaceRenderType.Visualization:
                materials[index] = SurfaceVisualizationMaterial;
                break;
            case SurfaceRenderType.VisualizationProximity:
                materials[index] = SurfaceVisualizationProximityMaterial;
                _visualizationProximityMaterialIndex = index;
                break;
            case SurfaceRenderType.Occlusion:
                materials[index] = SurfaceOcclusionMaterial;
                break;
            case SurfaceRenderType.OcclusionWithShadows:
                materials[index] = SurfaceOcclusionWithShadowsMaterial;
                _occlusionWithShadowsMaterialIndex = index;
                break;
            case SurfaceRenderType.Shadows:
                materials[index] = SurfaceShadowsMaterial;
                break;
        }

        ARLibrary.SurfaceMaterials = materials;
        ARLibrary.EnableSurfaceMaterial = true;

        _visualizationProximityMaterialActive = (surfaceType1 == SurfaceRenderType.VisualizationProximity ||
                                                 surfaceType2 == SurfaceRenderType.VisualizationProximity);
        _occlusionWithShadowsMaterialActive = (surfaceType1 == SurfaceRenderType.OcclusionWithShadows ||
                                                 surfaceType2 == SurfaceRenderType.OcclusionWithShadows);

    }

    public void SetImageTrackingDatabase()
    {
        switch (_arPlatform)
        {
            case ARUtils.ARPlatform.None:
                break;
            case ARUtils.ARPlatform.ARKit:
                break;
            case ARUtils.ARPlatform.ARCore:
                ARLibrary.ImageTrackingDatabase = ARCoreImageTrackingDatabase;
                break;
            case ARUtils.ARPlatform.WindowsMR:
                break;
        }
    }

    public void SetVisualizationProximityPosition(Vector3 position)
    {
        if (_visualizationProximityMaterialActive)
        {
            ARLibrary.SurfaceMaterials[_visualizationProximityMaterialIndex].SetVector("_Position", position);
        }
    }

    public void SetVisualizationProximityRadius(float radius)
    {
        if (_visualizationProximityMaterialActive)
        {
            ARLibrary.SurfaceMaterials[_visualizationProximityMaterialIndex].SetFloat("_Radius", radius);
        }
    }

    public void EnableHoleInOcclusionWithShadows(bool holeEnabled, float radius = 0)
    {
        if (_occlusionWithShadowsMaterialActive)
        {
            ARLibrary.SurfaceMaterials[_occlusionWithShadowsMaterialIndex].SetInt("_HoleEnabled", holeEnabled ? 1 : 0);
            if (holeEnabled) ARLibrary.SurfaceMaterials[_occlusionWithShadowsMaterialIndex].SetFloat("_HoleRadius", radius);
        }
    }

    public void SetOcclusionWithShadowsHolePosition(Vector3 position)
    {
        if (_occlusionWithShadowsMaterialActive)
        {
            ARLibrary.SurfaceMaterials[_occlusionWithShadowsMaterialIndex].SetVector("_HolePosition", position);
        }
    }

    #endregion

}
