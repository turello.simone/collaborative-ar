﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ARUtils {

    public delegate void ImageAnchorCreated(string anchorId, string imageName);
    public delegate void SurfaceDetected(GameObject surfaceGameObject);
    public delegate void SurfaceUpdated(GameObject surfaceGameObject);
    public delegate void SurfaceRemoved();


    /// <summary>
    /// AR platform enum.
    /// </summary>
    public enum ARPlatform : int
    {
        None = 0,
        ARKit = 1,
        ARCore = 2,
        WindowsMR = 3,
        UnityEditor = 4
    }

    /// <summary>
    /// Session state.
    /// </summary>
    public struct ARSessionStatus
    {
        public bool isError;
        public string description;
    }

    /// <summary>
    /// Tracking state for AR frames.
    /// </summary>
    public enum ARTrackingStatus : int
    {
        NotAvailable = 0,
        LimitedTracking = 1,
        NormalTracking = 2
    }

    public enum LightEstimationMode
    {
        Disabled,
        AmbientIntensity,
        EnvironmentalHDRWithoutReflections,
        EnvironmentalHDRWithReflections
    }

    public static Pose ToAnchorSpace(this Pose poseRelativeToWorld, string anchorId)
    {
        Pose anchorPose;
        ARManager.Instance.ARLibrary.GetAnchorPose(anchorId, out anchorPose);
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);
        Pose resultPose = Pose.identity;
        resultPose.position = worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(poseRelativeToWorld.position);
        resultPose.rotation = Quaternion.Inverse(anchorPose.rotation) * poseRelativeToWorld.rotation;
        return resultPose;
    }

    public static Vector3 ToAnchorSpace(this Vector3 positionRelativeToWorld, string anchorId)
    {
        Pose anchorPose;
        ARManager.Instance.ARLibrary.GetAnchorPose(anchorId, out anchorPose);
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);
        return worldToMarkerSpaceMatrix.inverse.MultiplyPoint3x4(positionRelativeToWorld);
    }

    public static Quaternion ToAnchorSpace(this Quaternion rotationRelativeToWorld, string anchorId)
    {
        Pose anchorPose;
        ARManager.Instance.ARLibrary.GetAnchorPose(anchorId, out anchorPose);
        return Quaternion.Inverse(anchorPose.rotation) * rotationRelativeToWorld;
    }

    public static Pose ToWorldSpace(this Pose poseRelativeToAnchor, string anchorId)
    {
        Pose anchorPose;
        ARManager.Instance.ARLibrary.GetAnchorPose(anchorId, out anchorPose);
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);
        Pose resultPose = Pose.identity;
        resultPose.position = worldToMarkerSpaceMatrix.MultiplyPoint3x4(poseRelativeToAnchor.position);
        resultPose.rotation = anchorPose.rotation * poseRelativeToAnchor.rotation;
        return resultPose;
    }

    public static Vector3 ToWorldSpace(this Vector3 positionRelativeToAnchor, string anchorId)
    {
        Pose anchorPose;
        ARManager.Instance.ARLibrary.GetAnchorPose(anchorId, out anchorPose);
        Matrix4x4 worldToMarkerSpaceMatrix = new Matrix4x4();
        worldToMarkerSpaceMatrix.SetTRS(anchorPose.position, anchorPose.rotation, Vector3.one);
        return worldToMarkerSpaceMatrix.MultiplyPoint3x4(positionRelativeToAnchor);
    }

    public static Quaternion ToWorldSpace(this Quaternion rotationRelativeToAnchor, string anchorId)
    {
        Pose anchorPose;
        ARManager.Instance.ARLibrary.GetAnchorPose(anchorId, out anchorPose);
        return anchorPose.rotation * rotationRelativeToAnchor;
    }
}
