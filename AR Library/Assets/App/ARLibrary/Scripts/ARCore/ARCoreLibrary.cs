﻿using System;
using GoogleARCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using Object = UnityEngine.Object;
using Transform = UnityEngine.Transform;

public class ARCoreLibrary : MonoBehaviour, ARLibrary
{
    public event ARUtils.ImageAnchorCreated ImageAnchorCreatedEvent;
    public event ARUtils.SurfaceDetected SurfaceDetectedEvent;
    public event ARUtils.SurfaceUpdated SurfaceUpdatedEvent;
    public event ARUtils.SurfaceRemoved SurfaceRemovedEvent;

    private GameObject arCoreDeviceObject;
    private GameObject arCoreEnvironmentalLightObject;
    private ARCoreSession arSession;
    private GameObject detectedPlaneGeneratorObject;
    private ARCoreDetectedPlaneGenerator detectedPlaneGenerator;
    private Dictionary<AugmentedImage, string> _augmentedImageAnchorDictionary;
    private Dictionary<string, Anchor> _anchorsDictionary;
    private Dictionary<string, List<GameObject>> _anchoredObjectsDictionary;
    public Dictionary<string, List<GameObject>> AnchoredObjectsDictionary
    {
        get
        {
            return _anchoredObjectsDictionary;
        }
    }
    private List<GameObject> _detectedSurfacesList;
    public List<GameObject> DetectedSurfacesList
    {
        get
        {
            return _detectedSurfacesList;
        }
    }
    private bool _enableSurfaceCollider;
    public bool EnableSurfaceCollider
    {
        get { return _enableSurfaceCollider; }
        set
        {
            _enableSurfaceCollider = value;
            ARCoreDetectedPlaneVisualizer.CollidersEnabled = value;
        }
    }
    private bool _enableSurfaceMaterial;
    public bool EnableSurfaceMaterial
    {
        get { return _enableSurfaceMaterial; }

        set
        {
            if (_enableSurfaceMaterial != value)
            {
                _enableSurfaceMaterial = value;
                ARCoreDetectedPlaneVisualizer.RenderersEnabled = value;
            }
        }
    }
    private Material[] _surfaceMaterials = null;
    public Material[] SurfaceMaterials
    {
        get { return _surfaceMaterials.ToArray(); }

        set
        {
            detectedPlaneGenerator.DetectedPlanePrefab.GetComponent<MeshRenderer>().materials = value;
            foreach (GameObject surfaceObject in DetectedSurfacesList)
            {
                surfaceObject.GetComponent<MeshRenderer>().materials = value;
            }

            if (value == null)
                _surfaceMaterials = null;
            else
                _surfaceMaterials = detectedPlaneGenerator.DetectedPlanePrefab.GetComponent<MeshRenderer>().sharedMaterials;
        }
    }
    private bool _enableImageTracking = false;
    public bool EnableImageTracking
    {
        get { return _enableImageTracking; }

        set
        {
            if (_enableImageTracking != value)
            {
                _enableImageTracking = value;
                if (arSession != null && arSession.SessionConfig != null && _imageTrackingDatabase != null)
                {

                    if (value == false)
                    {
                        arSession.SessionConfig.AugmentedImageDatabase = null;

                        Dictionary<AugmentedImage, string> dictionaryCopy = new Dictionary<AugmentedImage, string>(_augmentedImageAnchorDictionary);
                        foreach (var pair in dictionaryCopy)
                        {
                            DestroyAnchorAndGameObjects(pair.Value);
                        }

                    }
                    else
                    {
                        arSession.SessionConfig.AugmentedImageDatabase = _imageTrackingDatabase;
                    }
                }
            }
        }
    }
    private AugmentedImageDatabase _imageTrackingDatabase;
    public Object ImageTrackingDatabase
    {
        get
        {
            return _imageTrackingDatabase;
        }

        set
        {
            if (_imageTrackingDatabase != (AugmentedImageDatabase) value)
            {
                _imageTrackingDatabase = (AugmentedImageDatabase)value;
                if (arSession != null && arSession.SessionConfig != null && EnableImageTracking)
                {
                    Debug.Log("Image Database set in Session config");
                    arSession.SessionConfig.AugmentedImageDatabase = _imageTrackingDatabase;
                    
                    Dictionary<AugmentedImage, string> dictionaryCopy = new Dictionary<AugmentedImage, string>(_augmentedImageAnchorDictionary);
                    foreach (var pair in dictionaryCopy)
                    {
                        DestroyAnchorAndGameObjects(pair.Value);
                    }

                }
            }
        }
    }
    private ARUtils.LightEstimationMode _lightEstimationMode;
    public ARUtils.LightEstimationMode LightEstimationMode
    {
        get { return _lightEstimationMode; }

        set
        {
            if (_lightEstimationMode != value)
            {
                _lightEstimationMode = value;
                if (arSession != null && arSession.SessionConfig != null)
                {
                    switch (_lightEstimationMode)
                    {
                        case ARUtils.LightEstimationMode.Disabled:
                            arSession.SessionConfig.LightEstimationMode = GoogleARCore.LightEstimationMode.Disabled;
                            break;
                        case ARUtils.LightEstimationMode.AmbientIntensity:
                            arSession.SessionConfig.LightEstimationMode = GoogleARCore.LightEstimationMode.AmbientIntensity;
                            break;
                        case ARUtils.LightEstimationMode.EnvironmentalHDRWithoutReflections:
                            arSession.SessionConfig.LightEstimationMode = GoogleARCore.LightEstimationMode.EnvironmentalHDRWithoutReflections;
                            break;
                        case ARUtils.LightEstimationMode.EnvironmentalHDRWithReflections:
                            arSession.SessionConfig.LightEstimationMode = GoogleARCore.LightEstimationMode.EnvironmentalHDRWithReflections;
                            break;
                    }
                }
            }
        }
    }

    private LightEstimate _lightEstimate;
    /// <summary>
    /// A list to hold new augmented images ARCore began tracking in the current frame. This object is used across
    /// the application to avoid per-frame allocations.
    /// </summary>
    private List<AugmentedImage> augmentedImages = new List<AugmentedImage>();

    public void Awake()
    {
        Camera currentCamera = Camera.main;
        if (currentCamera)
        {
            currentCamera.gameObject.SetActive(false);
        }

        SessionInit();
    }

    private void SessionInit()
    {
        _detectedSurfacesList = new List<GameObject>();
        _anchorsDictionary = new Dictionary<string, Anchor>();
        _anchoredObjectsDictionary = new Dictionary<string, List<GameObject>>();
        _augmentedImageAnchorDictionary = new Dictionary<AugmentedImage, string>();

        // instantiate ARCore-Device prefab 
        arCoreDeviceObject = Object.Instantiate(Resources.Load("ARCore/ARCore Device", typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
        arCoreDeviceObject.name = "ARCore Device";
        arCoreDeviceObject.transform.parent = this.transform;
        arSession = arCoreDeviceObject.GetComponent<ARCoreSession>();

        // instantiate ARCore plane generator
        detectedPlaneGeneratorObject = new GameObject();
        detectedPlaneGeneratorObject.name = "Detected Plane Generator Object";
        detectedPlaneGeneratorObject.transform.parent = this.transform;
        detectedPlaneGenerator = detectedPlaneGeneratorObject.AddComponent<ARCoreDetectedPlaneGenerator>();
        detectedPlaneGenerator.PlaneDetectedEvent += gameObject =>
        {
            //Debug.Log("ARCoreLibrary: Plane detected, setting callbacks.");
            _detectedSurfacesList.Add(gameObject);
            Debug.Log("ARCoreLibrary: added a plane to the list. DetectedSurfaceList dimension: " + _detectedSurfacesList.Count + ".");
            if (SurfaceDetectedEvent != null) SurfaceDetectedEvent(gameObject);
            ARCoreDetectedPlaneVisualizer visualizer = gameObject.GetComponent<ARCoreDetectedPlaneVisualizer>();
            visualizer.PlaneUpdatedEvent += surfaceGameObject =>
            {
                if (SurfaceUpdatedEvent != null) SurfaceUpdatedEvent(surfaceGameObject);
            };
            visualizer.PlaneRemovedEvent += () =>
            {
                Debug.Log("ARCoreLibrary: removed a plane from the list. DetectedSurfaceList dimension: " + _detectedSurfacesList.Count + ".");
                if (SurfaceRemovedEvent != null) SurfaceRemovedEvent();
            };
        };

        //Instantiate ARCore Environmental Light prefab 
        arCoreEnvironmentalLightObject = Object.Instantiate(Resources.Load("ARCore/ARCore Environmental Light", typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
        arCoreEnvironmentalLightObject.name = "ARCore Environmental Light";
        arCoreEnvironmentalLightObject.transform.parent = this.transform;

        //Initialize parameters
        EnableSurfaceCollider = false;
        EnableSurfaceMaterial = false;
        _surfaceMaterials = null;
        _lightEstimationMode = ARUtils.LightEstimationMode.Disabled;
        _enableImageTracking = false;
        _imageTrackingDatabase = null;
        if (arSession != null && arSession.SessionConfig != null)
        {
            arSession.SessionConfig.LightEstimationMode = GoogleARCore.LightEstimationMode.Disabled;
            arSession.SessionConfig.AugmentedImageDatabase = null;
        }
    }

    public void ConfigSession(bool enableSurfaceCollider = false, bool enableSurfaceMaterial = false,
        Material[] surfaceMaterials = null,
        ARUtils.LightEstimationMode lightEstimationMode = ARUtils.LightEstimationMode.Disabled,
        bool enableImageTracking = false, Object imageTrackingDatabase = null)
    {
        EnableSurfaceCollider = enableSurfaceCollider;

        // Image tracking and light estimation initialization
        EnableImageTracking = enableImageTracking;
        ImageTrackingDatabase = imageTrackingDatabase;
        LightEstimationMode = lightEstimationMode;

        // Surface material initialization
        EnableSurfaceMaterial = enableSurfaceMaterial;
        SurfaceMaterials = surfaceMaterials;
    }

    public ARUtils.ARSessionStatus GetSessionStatus()
    {
        ARUtils.ARSessionStatus status;
        SessionStatus sessionStatus = Session.Status;

        if (sessionStatus == SessionStatus.ErrorSessionConfigurationNotSupported)
        {
            status.isError = true;
            status.description = "Invalid ARCore configuration.";
        }
        else if (sessionStatus == SessionStatus.ErrorPermissionNotGranted)
        {
            status.isError = true;
            status.description = "Camera permission is needed to run this application.";
        }
        else if (sessionStatus == SessionStatus.ErrorApkNotAvailable)
        {
            status.isError = true;
            status.description = "ARCore service APK is not available on the device.";
        }
        else if (sessionStatus == SessionStatus.FatalError)
        {
            status.isError = true;
            status.description = "ARCore encountered a problem connecting. Please restart the app.";
        }
        else if (sessionStatus == SessionStatus.Initializing)
        {
            status.isError = false;
            status.description = "The ARCore session is initializing.";
        }
        else if (sessionStatus == SessionStatus.None)
        {
            status.isError = false;
            status.description = "The ARCore session has not been initialized.";
        }
        else if (sessionStatus == SessionStatus.NotTracking)
        {
            status.isError = false;
            status.description = "The ARCore session is paused.";
        }
        else if (sessionStatus == SessionStatus.LostTracking)
        {
            status.isError = false;
            status.description = "The ARCore session has lost tracking and is attempting to recover.";
        }
        else
        {
            status.isError = false;
            status.description = "The ARCore session is tracking.";
        }

        return status;
    }

    public ARUtils.ARTrackingStatus GetCameraTrackingStatus()
    {
        SessionStatus sessionStatus = Session.Status;

        ARUtils.ARTrackingStatus status = ARUtils.ARTrackingStatus.NormalTracking;

        if (sessionStatus == SessionStatus.LostTracking || sessionStatus == SessionStatus.NotTracking)
        {
            status = ARUtils.ARTrackingStatus.LimitedTracking;
        }

        if (sessionStatus.IsError() || sessionStatus == SessionStatus.Initializing || sessionStatus == SessionStatus.None)
        {
            status = ARUtils.ARTrackingStatus.NotAvailable;
        }

        return status;
    }

    public void Update()
    {
        // The session status must be Tracking in order to access the Frame.
        if (Session.Status != SessionStatus.Tracking)
        {
            int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
            return;
        }
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        if (EnableImageTracking)
        {
            Session.GetTrackables<AugmentedImage>(augmentedImages);
            foreach (var image in augmentedImages)
            {
                //Debug.Log("Image \"" + image.Name + "\" found.");
                if (!_augmentedImageAnchorDictionary.ContainsKey(image) && image.TrackingState == TrackingState.Tracking)
                {
                    Debug.Log("Image \"" + image.Name + "\" is new and tracked, creating image anchor.");

                    //Create an anchor on centerpose of the tracked image
                    Anchor anchor = image.CreateAnchor(image.CenterPose);
                    string anchorId = image.Name + "_Anchor";
                    anchor.name = anchorId;
                    anchor.transform.parent = this.transform;
                    _anchorsDictionary.Add(anchorId, anchor);
                    _anchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
                    _augmentedImageAnchorDictionary.Add(image, anchorId);

                    Debug.Log("Image anchor \"" + anchorId + "\" created.");

                    if (ImageAnchorCreatedEvent != null) ImageAnchorCreatedEvent(anchorId, image.Name);
                }
            }
        }

        if (LightEstimationMode != ARUtils.LightEstimationMode.Disabled && Frame.LightEstimate.State == LightEstimateState.Valid)
        {
            _lightEstimate = Frame.LightEstimate;
        }

    }



    public bool CreateAnchorFromCamera(string anchorId, float xPos, float yPos, out Pose hitPose, float maxDistance = Mathf.Infinity)
    {
        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            hitPose = new Pose();
            return false;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds |
                                          TrackableHitFlags.PlaneWithinPolygon;
        if (Frame.Raycast(xPos, yPos, raycastFilter, out hit))
        {
            if (!float.IsPositiveInfinity(maxDistance) && hit.Distance > maxDistance)
            {
                hitPose = Pose.identity;
                return false;
            }

            Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);
            anchor.name = anchorId;
            anchor.transform.parent = this.transform;
            _anchorsDictionary.Add(anchorId, anchor);
            _anchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
            hitPose = hit.Pose;
            return true;
        }
        else
        {
            hitPose = new Pose();
            return false;
        }
    }

    public bool CreateAnchorFromRay(string anchorId, Ray ray, out Pose hitPose, float maxDistance = Mathf.Infinity)
    {
        Debug.Log("CreateAnchorFromRay: " + anchorId);
        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            hitPose = new Pose();
            return false;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | 
                                          TrackableHitFlags.PlaneWithinPolygon;
        if (Frame.Raycast(ray.origin, ray.direction, out hit, maxDistance, raycastFilter))
        {
            Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);
            anchor.name = anchorId;
            anchor.transform.parent = this.transform;
            _anchorsDictionary.Add(anchorId, anchor);
            _anchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
            hitPose = hit.Pose;
            return true;
        }
        else
        {
            hitPose = new Pose();
            return false;
        }

    }

    public bool CreateAnchorFromPose(string anchorId, Pose referencePose, out Pose hitPose, float maxDistance)
    {
        Debug.Log("CreateAnchorFromPose: " + anchorId);
        hitPose = new Pose();

        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            return false;
        }

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds |
                                          TrackableHitFlags.PlaneWithinPolygon;

        if (Frame.Raycast(referencePose.position, referencePose.up, out hit, maxDistance, raycastFilter) ||
            Frame.Raycast(referencePose.position, -referencePose.up, out hit, maxDistance, raycastFilter))
        {
            hitPose = new Pose(hit.Pose.position, Quaternion.LookRotation(Vector3.ProjectOnPlane(referencePose.forward, hit.Pose.up), hit.Pose.up));

            Anchor anchor = hit.Trackable.CreateAnchor(hitPose);
            anchor.name = anchorId;
            anchor.transform.parent = this.transform;
            _anchorsDictionary.Add(anchorId, anchor);
            _anchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
            return true;
        }
        else
        {
            return false;
        }
    }


    public bool CreateSessionAnchor(string anchorId, Pose pose)
    {
        Debug.Log("CreateSessionAnchor: " + anchorId + ", " + pose);
        if (_anchorsDictionary.ContainsKey(anchorId) || Session.Status == SessionStatus.NotTracking)
        {
            pose = new Pose();
            return false;
        }

        //Debug.Log("ARCoreLibrary/CreateSessionAnchor: Session " + Session.Status);
        Anchor anchor = Session.CreateAnchor(pose);
        //Debug.Log("ARCoreLibrary/CreateSessionAnchor: anchor " + anchor.ToString() + (anchor == null));
        anchor.name = anchorId;
        anchor.transform.parent = this.transform;
        _anchorsDictionary.Add(anchorId, anchor);
        _anchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
        return true;
    }

    public bool RaycastFromCamera(float xPos, float yPos, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity)
    {
        hitInfo = new RaycastHit();

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds |
                                          TrackableHitFlags.PlaneWithinPolygon;

        bool result = Frame.Raycast(xPos, yPos, raycastFilter, out hit);

        if (result)
        {
            if (!float.IsPositiveInfinity(maxDistance) && hit.Distance > maxDistance)
            {
                result = false;
            }
            else
            {
                hitInfo.point = hit.Pose.position;
                hitInfo.normal = hit.Pose.up;
                hitInfo.distance = hit.Distance;
            }
        }
        return result;
    }

    public bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity)
    {
        hitInfo = new RaycastHit();

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds |
                                          TrackableHitFlags.PlaneWithinPolygon;

        bool result = Frame.Raycast(ray.origin, ray.direction, out hit, maxDistance, raycastFilter);

        if (result)
        {
            hitInfo.point = hit.Pose.position;
            hitInfo.normal = hit.Pose.up;
            hitInfo.distance = hit.Distance;
        }

        return result;
    }

    public bool RaycastFromPose(Pose referencePose, out RaycastHit hitInfo, float maxDistance)
    {
        hitInfo = new RaycastHit();

        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds |
                                          TrackableHitFlags.PlaneWithinPolygon;

        if (Frame.Raycast(referencePose.position, referencePose.up, out hit, maxDistance, raycastFilter) ||
            Frame.Raycast(referencePose.position, -referencePose.up, out hit, maxDistance, raycastFilter))
        {
            hitInfo.point = hit.Pose.position;
            hitInfo.normal = hit.Pose.up;
            hitInfo.distance = hit.Distance;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool DestroyAnchor(string anchorId)
    {
        Anchor anchor;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor))
        {
            _anchorsDictionary.Remove(anchorId);
            _anchoredObjectsDictionary.Remove(anchorId);
            if (_augmentedImageAnchorDictionary.ContainsValue(anchorId))
            {
                _augmentedImageAnchorDictionary.Remove(_augmentedImageAnchorDictionary
                    .FirstOrDefault(x => x.Value == anchorId).Key);
            }
            anchor.NativeSession.AnchorApi.Detach(anchor.NativeHandle);
            foreach (Transform child in anchor.transform)
            {
                child.parent = null;
            }
            Object.Destroy(anchor.gameObject);
            return true;
        }
        else
        {
            return false;
        }
    }


    public bool DestroyAnchor(string anchorId, out List<GameObject> gameObjects)
    {
        Anchor anchor;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor) && _anchoredObjectsDictionary.TryGetValue(anchorId, out gameObjects))
        {
            _anchorsDictionary.Remove(anchorId);
            _anchoredObjectsDictionary.Remove(anchorId);
            if (_augmentedImageAnchorDictionary.ContainsValue(anchorId))
            {
                _augmentedImageAnchorDictionary.Remove(_augmentedImageAnchorDictionary
                    .FirstOrDefault(x => x.Value == anchorId).Key);
            }
            anchor.NativeSession.AnchorApi.Detach(anchor.NativeHandle);
            foreach (Transform child in anchor.transform)
            {
                child.parent = null;
            }
            Object.Destroy(anchor.gameObject);
            return true;
        }
        else
        {
            gameObjects = new List<GameObject>();
            return false;
        }
    }

    public bool DestroyAnchorAndGameObjects(string anchorId)
    {
        Anchor anchor;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor) && _anchoredObjectsDictionary.ContainsKey(anchorId))
        {
            _anchorsDictionary.Remove(anchorId);
            _anchoredObjectsDictionary.Remove(anchorId);
            if (_augmentedImageAnchorDictionary.ContainsValue(anchorId))
            {
                _augmentedImageAnchorDictionary.Remove(_augmentedImageAnchorDictionary
                    .FirstOrDefault(x => x.Value == anchorId).Key);
            }
            anchor.NativeSession.AnchorApi.Detach(anchor.NativeHandle);
            Object.Destroy(anchor.gameObject); //This destroys the anchor gameobject and its hierarchy

            return true;
        }
        else
        {
            return false;
        }
    }

    public bool AttachGameObjectToAnchor(string anchorId, GameObject gameObject, bool worldPositionStays)
    {
        Anchor anchor;
        List<GameObject> list;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor) && _anchoredObjectsDictionary.TryGetValue(anchorId, out list))
        {
            list.Add(gameObject);
            gameObject.transform.SetParent(anchor.transform, worldPositionStays);
            return true;
        }

        return false;
    }

    public bool DetachGameObjectFromAnchor(string anchorId, GameObject gameObject)
    {
        List<GameObject> list;
        if (!_anchoredObjectsDictionary.TryGetValue(anchorId, out list))
        {
            return false;
        }
        if (list.Contains(gameObject))
        {
            gameObject.transform.parent = null;
            list.Remove(gameObject);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool DetachAllGameObjectsFromAnchor(string anchorId, out List<GameObject> gameObjects)
    {
        List<GameObject> list;
        if (_anchoredObjectsDictionary.TryGetValue(anchorId, out list))
        {
            foreach (var gameObject in list)
            {
                gameObject.transform.parent = null;
            }
            gameObjects = new List<GameObject>(list);
            list.Clear();

            return true;
        }
        else
        {
            gameObjects = null;
            return false;
        }
    }

    public bool GetAnchorPose(string anchorId, out Pose anchorPose)
    {
        Anchor anchor;
        if (anchorId != null && _anchorsDictionary.TryGetValue(anchorId, out anchor))
        {
            anchorPose.position = anchor.transform.position;
            anchorPose.rotation = anchor.transform.rotation;
            return true;
        }
        else
        {
            anchorPose = new Pose(Vector3.zero,Quaternion.identity);
            return false;
        }
    }

    public bool GetLightEstimation(out float lightIntensity, out Color colorCorrection)
    {
        // Normalize pixel intensity by middle gray in gamma space.
        const float middleGray = 0.466f;
        float normalizedIntensity = _lightEstimate.PixelIntensity / middleGray;

        if (LightEstimationMode != ARUtils.LightEstimationMode.Disabled)
        {
            // Pixel intensity.
            lightIntensity = normalizedIntensity;

            // Color Correction.
            colorCorrection = _lightEstimate.ColorCorrection * normalizedIntensity;
            return true;
        }
        else
        {
            lightIntensity = 1f / middleGray;
            colorCorrection = Color.white * normalizedIntensity;
            return false;
        }
    }

    public void ResetSession()
    {
        StartCoroutine(ResetSessionCoroutine());
    }

    public IEnumerator ResetSessionCoroutine()
    {
        //Save session configuration parameters
        bool enableSurfaceCollider = _enableSurfaceCollider;
        bool enableSurfaceMaterial = _enableSurfaceMaterial;
        Material[] surfaceMaterials = _surfaceMaterials;
        ARUtils.LightEstimationMode lightEstimationMode = _lightEstimationMode;
        bool enableImageTracking = _enableImageTracking;
        Object imageTrackingDatabase = _imageTrackingDatabase;

        //Remove all anchors
        List<string> anchorIds = new List<string>(_anchorsDictionary.Keys);
        foreach (var anchorId in anchorIds)
        {
            DestroyAnchorAndGameObjects(anchorId);
        }
        //Disable session and destroy prefabs
        arSession.enabled = false;
        Object.Destroy(detectedPlaneGeneratorObject);
        Object.Destroy(arCoreDeviceObject);
        Object.Destroy(arCoreEnvironmentalLightObject);
        //Object.Destroy(sessionOriginObject);

        yield return 0;

        //Initialize new session
        SessionInit();

        //Set session configuration parameters
        ConfigSession(enableSurfaceCollider, enableSurfaceMaterial, surfaceMaterials, lightEstimationMode, enableImageTracking, imageTrackingDatabase);

    }

}
