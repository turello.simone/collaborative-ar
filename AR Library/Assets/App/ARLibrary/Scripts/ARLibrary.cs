﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ARLibrary
{
    event ARUtils.ImageAnchorCreated ImageAnchorCreatedEvent;
    event ARUtils.SurfaceDetected SurfaceDetectedEvent;
    event ARUtils.SurfaceUpdated SurfaceUpdatedEvent;
    event ARUtils.SurfaceRemoved SurfaceRemovedEvent;

    Dictionary<string,List<GameObject>> AnchoredObjectsDictionary { get; }
    List<GameObject> DetectedSurfacesList { get; }
    bool EnableSurfaceCollider { get; set; }
    bool EnableSurfaceMaterial { get; set; }
    Material[] SurfaceMaterials { get; set; } //Get returns a copy of the array (the copy contains the sharedMaterials set on surfaces objects)
    bool EnableImageTracking { get; set; }
    Object ImageTrackingDatabase { get; set; }
    ARUtils.LightEstimationMode LightEstimationMode { get; set; }
    bool GetLightEstimation(out float lightIntensity, out Color colorCorrection);

    ARUtils.ARSessionStatus GetSessionStatus();
    ARUtils.ARTrackingStatus GetCameraTrackingStatus();

    void ConfigSession(bool enableSurfaceCollider = false, bool enableSurfaceMaterial = false,
        Material[] surfaceMaterials = null,
        ARUtils.LightEstimationMode lightEstimationMode = ARUtils.LightEstimationMode.Disabled,
        bool enableImageTracking = false, Object imageTrackingDatabase = null);
    bool RaycastFromCamera(float xPos, float yPos, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity);
    bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance);
    bool RaycastFromPose(Pose referencePose, out RaycastHit hitInfo, float maxDistance);
    bool CreateAnchorFromCamera(string anchorId, float xPos, float yPos, out Pose hitPose, float maxDistance = Mathf.Infinity);
    bool CreateAnchorFromRay(string anchorId, Ray ray, out Pose hitPose, float maxDistance);
    bool CreateAnchorFromPose(string anchorId, Pose referencePose, out Pose hitPose, float maxDistance);
    bool CreateSessionAnchor(string anchorId, Pose pose);
    bool DestroyAnchor(string anchorId);
    bool DestroyAnchor(string anchorId, out List<GameObject> gameObjects);
    bool DestroyAnchorAndGameObjects(string anchorId);
    bool AttachGameObjectToAnchor(string anchorId, GameObject gameObject, bool worldPositionStays);
    bool DetachGameObjectFromAnchor(string anchorId, GameObject gameObject);
    bool DetachAllGameObjectsFromAnchor(string anchorId, out List<GameObject> gameObjects);
    bool GetAnchorPose(string anchorId, out Pose anchorPose);
    void ResetSession();

}
