﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ARLibraryTest : MonoBehaviour, ARLibrary {

    public event ARUtils.ImageAnchorCreated ImageAnchorCreatedEvent;
#pragma warning disable 67
    public event ARUtils.SurfaceDetected SurfaceDetectedEvent;
    public event ARUtils.SurfaceUpdated SurfaceUpdatedEvent;
    public event ARUtils.SurfaceRemoved SurfaceRemovedEvent;
#pragma warning restore 67

    public Dictionary<string, List<GameObject>> AnchoredObjectsDictionary { get; private set; }
    public List<GameObject> DetectedSurfacesList { get; private set; }
    private bool _enableSurfaceCollider = false;
    public bool EnableSurfaceCollider
    {
        get { return _enableSurfaceCollider; }
        set
        {
            _enableSurfaceCollider = value;
        }
    }
    private bool _enableSurfaceMaterial = false;
    public bool EnableSurfaceMaterial
    {
        get { return _enableSurfaceMaterial; }
        set
        {
            _enableSurfaceMaterial = value;
            foreach (var surf in DetectedSurfacesList)
            {
                surf.GetComponent<Renderer>().enabled = value;
            }
        }
    }
    private Material[] _surfaceMaterials;
    public Material[] SurfaceMaterials
    {
        get { return _surfaceMaterials.ToArray(); }
        set
        {
            foreach (var surf in DetectedSurfacesList)
            {
                surf.GetComponent<Renderer>().materials = value;
                _surfaceMaterials = surf.GetComponent<Renderer>().sharedMaterials;
            }
            if (DetectedSurfacesList == null || DetectedSurfacesList.Count == 0)
            {
                _surfaceMaterials = value;
            }
        }
    }
    private bool _enableImageTracking = false;
    public bool EnableImageTracking
    {
        get { return _enableImageTracking; }
        set { _enableImageTracking = value; }
    }
    private Object _imageTrackingDatabase = null;
    public Object ImageTrackingDatabase
    {
        get { return _imageTrackingDatabase; }
        set { _imageTrackingDatabase = value; }
    }
    private ARUtils.LightEstimationMode _lightEstimationMode;
    public ARUtils.LightEstimationMode LightEstimationMode
    {
        get { return _lightEstimationMode; }
        set { _lightEstimationMode = value; }
    }

    private Dictionary<string, GameObject> _anchorsDictionary;
    private bool _first = true;
    private Transform _arDummy;

    private void Awake()
    {
        AnchoredObjectsDictionary = new Dictionary<string, List<GameObject>>();
        DetectedSurfacesList = new List<GameObject>();
        _anchorsDictionary = new Dictionary<string, GameObject>();
        _arDummy = new GameObject("ARLibraryTest_Dummy").transform;

        GameObject environmentalLightObject = Object.Instantiate(Resources.Load("ARTest/ARTest Environmental Light", typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
        environmentalLightObject.name = "ARCore Environmental Light";
        environmentalLightObject.transform.parent = this.transform;
    }

    private void Update()
    {
        if (EnableImageTracking && _first)
        {
            _first = false;
            StartCoroutine(CreateSurfaceAndImageAnchor());
        }
    }

    private IEnumerator CreateSurfaceAndImageAnchor()
    {
        yield return new WaitForSeconds(1.0f);

        GameObject plane = CreatePlane(Vector3.zero, 5, Vector3.up);
        plane.transform.Rotate(2f,0f,0f);
        DetectedSurfacesList.Add(plane);
        GameObject plane2 = CreatePlane(new Vector3(0,5,5), 5, -Vector3.forward);
        DetectedSurfacesList.Add(plane2);

        foreach (var surf in DetectedSurfacesList)
        {
            Debug.Log("ARLibraryTest: added a plane to the list. DetectedSurfaceList dimension: " + DetectedSurfacesList.Count + ".");
            if (SurfaceDetectedEvent != null) SurfaceDetectedEvent(surf);
        }

        yield return new WaitForSeconds(1.0f);

        GameObject anchor = new GameObject();
        string anchorId = "Pebbles_Anchor";
        anchor.name = anchorId;
        anchor.transform.position = new Vector3(0, 0.005f, 0.3f);
        anchor.transform.rotation = Quaternion.Euler(0f, 30f, 0f);
        anchor.transform.parent = this.transform;
        _anchorsDictionary.Add(anchorId, anchor);
        AnchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
        Debug.Log("ARLibraryTest: Image anchor \"" + anchorId + "\" created.");

        if (ImageAnchorCreatedEvent != null) ImageAnchorCreatedEvent(anchorId, "Pebbles");
    }

    private GameObject CreatePlane(Vector3 center, float extend, Vector3 normal)
    {
        GameObject plane = new GameObject("DetectedSurface", typeof(MeshFilter), typeof(MeshRenderer));
        BoxCollider boxCollider = plane.AddComponent<BoxCollider>();
        boxCollider.center = new Vector3(0, -0.01f, 0);
        boxCollider.size = new Vector3(extend * 2, 0.02f, extend * 2);
        Mesh m_Mesh = new Mesh();
        m_Mesh.name = "Surface";

        List<Vector3> m_MeshVertices = new List<Vector3>();
        /*m_MeshVertices.Add(new Vector3(center.x - extend, center.y, center.z + extend));
        m_MeshVertices.Add(new Vector3(center.x + extend, center.y, center.z + extend));
        m_MeshVertices.Add(new Vector3(center.x + extend, center.y, center.z - extend));
        m_MeshVertices.Add(new Vector3(center.x - extend, center.y, center.z - extend));*/
        m_MeshVertices.Add(new Vector3(-extend, 0, +extend));
        m_MeshVertices.Add(new Vector3(+extend, 0, +extend));
        m_MeshVertices.Add(new Vector3(+extend, 0, -extend));
        m_MeshVertices.Add(new Vector3(-extend, 0, -extend));

        List<Color> m_MeshColors = new List<Color>();
        List<int> m_MeshIndices = new List<int>();
        List<Vector3> m_MeshNormals = new List<Vector3>();
        m_MeshNormals.Add(Vector3.up);
        m_MeshNormals.Add(Vector3.up);
        m_MeshNormals.Add(Vector3.up);
        m_MeshNormals.Add(Vector3.up);

        //Vector3 m_PlaneCenter = center;
        Vector3 m_PlaneCenter = Vector3.zero;

        int planePolygonCount = m_MeshVertices.Count;

        // The following code converts a polygon to a mesh with two polygons, inner
        // polygon renders with 100% opacity and fade out to outter polygon with opacity 0%, as shown below.
        // The indices shown in the diagram are used in comments below.
        // _______________     0_______________1
        // |             |      |4___________5|
        // |             |      | |         | |
        // |             | =>   | |         | |
        // |             |      | |         | |
        // |             |      |7-----------6|
        // ---------------     3---------------2


        // Fill transparent color to vertices 0 to 3.
        for (int i = 0; i < planePolygonCount; ++i)
        {
            m_MeshColors.Add(Color.clear);
        }

        // Feather distance 0.2 meters.
        const float featherLength = 0.2f;

        // Feather scale over the distance between plane center and vertices.
        const float featherScale = 0.2f;

        // Add vertex 4 to 7.
        for (int i = 0; i < planePolygonCount; ++i)
        {
            Vector3 v = m_MeshVertices[i];

            // Vector from plane center to current point
            Vector3 d = v - m_PlaneCenter;

            float scale = 1.0f - Mathf.Min(featherLength / d.magnitude, featherScale);
            m_MeshVertices.Add((scale * d) + m_PlaneCenter);

            m_MeshColors.Add(Color.white);
            m_MeshNormals.Add(Vector3.up);

        }

        int firstOuterVertex = 0;
        int firstInnerVertex = planePolygonCount;

        // Generate triangle (4, 5, 6) and (4, 6, 7).
        for (int i = 0; i < planePolygonCount - 2; ++i)
        {
            m_MeshIndices.Add(firstInnerVertex);
            m_MeshIndices.Add(firstInnerVertex + i + 1);
            m_MeshIndices.Add(firstInnerVertex + i + 2);
        }

        // Generate triangle (0, 1, 4), (4, 1, 5), (5, 1, 2), (5, 2, 6), (6, 2, 3), (6, 3, 7)
        // (7, 3, 0), (7, 0, 4)
        for (int i = 0; i < planePolygonCount; ++i)
        {
            int outerVertex1 = firstOuterVertex + i;
            int outerVertex2 = firstOuterVertex + ((i + 1) % planePolygonCount);
            int innerVertex1 = firstInnerVertex + i;
            int innerVertex2 = firstInnerVertex + ((i + 1) % planePolygonCount);

            m_MeshIndices.Add(outerVertex1);
            m_MeshIndices.Add(outerVertex2);
            m_MeshIndices.Add(innerVertex1);

            m_MeshIndices.Add(innerVertex1);
            m_MeshIndices.Add(outerVertex2);
            m_MeshIndices.Add(innerVertex2);
        }

        m_Mesh.SetVertices(m_MeshVertices);
        m_Mesh.SetIndices(m_MeshIndices.ToArray(), MeshTopology.Triangles, 0);
        m_Mesh.SetColors(m_MeshColors);
        m_Mesh.SetNormals(m_MeshNormals);
        plane.GetComponent<MeshFilter>().mesh = m_Mesh;

        plane.GetComponent<MeshRenderer>().enabled = EnableSurfaceMaterial;
        plane.GetComponent<MeshRenderer>().materials = _surfaceMaterials;
        _surfaceMaterials = plane.GetComponent<MeshRenderer>().sharedMaterials;

        plane.layer = GameUtils.Layer.DetectedSurfaces;
        plane.transform.position = center;
        plane.transform.up = normal;

        return plane;
    }

    public bool GetLightEstimation(out float lightIntensity, out Color colorCorrection)
    {
        lightIntensity = 0.5f;
        colorCorrection = Color.white;

        return _lightEstimationMode != ARUtils.LightEstimationMode.Disabled;
    }

    public ARUtils.ARSessionStatus GetSessionStatus()
    {
        ARUtils.ARSessionStatus status;

        status.isError = false;
        status.description = "The windows simulated session is tracking.";
        return status;
    }

    public ARUtils.ARTrackingStatus GetCameraTrackingStatus()
    {
        return ARUtils.ARTrackingStatus.NormalTracking;
    }

    public void ConfigSession(bool enableSurfaceCollider = false, bool enableSurfaceMaterial = false,
        Material[] surfaceMaterials = null,
        ARUtils.LightEstimationMode lightEstimationMode = ARUtils.LightEstimationMode.Disabled,
        bool enableImageTracking = false, Object imageTrackingDatabase = null)
    {
        EnableSurfaceCollider = enableSurfaceCollider;

        // Image tracking and light estimation initialization
        EnableImageTracking = enableImageTracking;
        ImageTrackingDatabase = imageTrackingDatabase;
        LightEstimationMode = lightEstimationMode;

        // Surface material initialization
        EnableSurfaceMaterial = enableSurfaceMaterial;
        SurfaceMaterials = surfaceMaterials;
    }

    public bool RaycastFromCamera(float xPos, float yPos, out RaycastHit hitInfo, float maxDistance = Mathf.Infinity)
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(xPos, yPos));
        ray.origin = Camera.main.transform.position;

        bool res = Physics.Raycast(ray, out hitInfo, maxDistance, GameUtils.Mask.DetectedSurfaces);
        if (res)
        {
            Debug.DrawLine(ray.origin, hitInfo.point);
        }
        return res;
    }

    public bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance)
    {
        bool res = Physics.Raycast(ray, out hitInfo, maxDistance, GameUtils.Mask.DetectedSurfaces);
        if (res)
        {
            Debug.DrawLine(ray.origin, hitInfo.point);
        }
        return res;
    }

    public bool RaycastFromPose(Pose referencePose, out RaycastHit hitInfo, float maxDistance)
    {
        Ray ray = new Ray(referencePose.position, referencePose.up);
        Ray inverseRay = new Ray(referencePose.position, -referencePose.up);

        bool res = Physics.Raycast(inverseRay, out hitInfo, maxDistance, GameUtils.Mask.DetectedSurfaces) ||
                   Physics.Raycast(ray, out hitInfo, maxDistance, GameUtils.Mask.DetectedSurfaces);

        if (res)
        {
            Debug.DrawLine(ray.origin, hitInfo.point);
        }
        return res;
    }

    public bool CreateAnchorFromCamera(string anchorId, float xPos, float yPos, out Pose hitPose,
        float maxDistance = Mathf.Infinity)
    {
        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            hitPose = new Pose();
            return false;
        }

        RaycastHit hit;
        hitPose = Pose.identity;
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(xPos, yPos));
        ray.origin = Camera.main.transform.position;
        bool res = Physics.Raycast(ray, out hit, maxDistance, GameUtils.Mask.DetectedSurfaces);
        if (res)
        {
            _arDummy.position = hit.point;
            _arDummy.up = hit.normal;
            hitPose.position = _arDummy.position;
            hitPose.rotation = _arDummy.rotation;

            GameObject anchor = new GameObject();
            anchor.name = anchorId;
            anchor.transform.position = hitPose.position;
            anchor.transform.rotation = hitPose.rotation;
            anchor.transform.parent = this.transform;
            _anchorsDictionary.Add(anchorId, anchor);
            AnchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
            Debug.Log("ARLibraryTest: CreateAnchorFromCamera " + anchorId);
            Debug.DrawLine(ray.origin, hit.point);
        }
        return res;
    }

    public bool CreateAnchorFromRay(string anchorId, Ray ray, out Pose hitPose, float maxDistance)
    {
        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            hitPose = new Pose();
            return false;
        }

        RaycastHit hit;
        hitPose = Pose.identity;
        bool res = Physics.Raycast(ray, out hit, maxDistance, GameUtils.Mask.DetectedSurfaces);
        if (res)
        {
            _arDummy.position = hit.point;
            _arDummy.up = hit.normal;
            hitPose.position = _arDummy.position;
            hitPose.rotation = _arDummy.rotation;

            GameObject anchor = new GameObject();
            anchor.name = anchorId;
            anchor.transform.position = hitPose.position;
            anchor.transform.rotation = hitPose.rotation;
            anchor.transform.parent = this.transform;
            _anchorsDictionary.Add(anchorId, anchor);
            AnchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
            Debug.Log("ARLibraryTest: CreateAnchor " + anchorId);
            Debug.DrawLine(ray.origin, hit.point);
        }
        return res;
    }

    public bool CreateAnchorFromPose(string anchorId, Pose referencePose, out Pose hitPose, float maxDistance)
    {
        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            hitPose = new Pose();
            return false;
        }

        RaycastHit hit;
        hitPose = Pose.identity;
        Ray ray = new Ray(referencePose.position, referencePose.up);
        Ray inverseRay = new Ray(referencePose.position, -referencePose.up);

        if (Physics.Raycast(inverseRay, out hit, maxDistance, GameUtils.Mask.DetectedSurfaces) ||
            Physics.Raycast(ray, out hit, maxDistance, GameUtils.Mask.DetectedSurfaces))
        {
            _arDummy.position = hit.point;
            _arDummy.rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(referencePose.forward, hit.normal), hit.normal);
            hitPose = new Pose(_arDummy.position, _arDummy.rotation);

            GameObject anchor = new GameObject();
            anchor.name = anchorId;
            anchor.transform.position = hitPose.position;
            anchor.transform.rotation = hitPose.rotation;
            anchor.transform.parent = this.transform;

            _anchorsDictionary.Add(anchorId, anchor);
            AnchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
            Debug.Log("ARLibraryTest: CreateAnchor " + anchorId);
            Debug.DrawLine(ray.origin, hit.point);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CreateSessionAnchor(string anchorId, Pose pose)
    {
        if (_anchorsDictionary.ContainsKey(anchorId))
        {
            return false;
        }
        GameObject anchor = new GameObject();
        anchor.name = anchorId;
        anchor.transform.position = pose.position;
        anchor.transform.rotation = pose.rotation;
        anchor.transform.parent = this.transform;
        _anchorsDictionary.Add(anchorId, anchor);
        AnchoredObjectsDictionary.Add(anchorId, new List<GameObject>());
        Debug.Log("ARLibraryTest: CreateSessionAnchor " + anchorId + ", " + anchor.transform.position);
        return true;
    }

    public bool DestroyAnchor(string anchorId)
    {
        GameObject anchor;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor))
        {
            _anchorsDictionary.Remove(anchorId);
            AnchoredObjectsDictionary.Remove(anchorId);
            foreach (Transform child in anchor.transform)
            {
                child.parent = null;
            }
            Object.Destroy(anchor);
            Debug.Log("ARLibraryTest: RemoveAnchor " + anchorId);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool DestroyAnchor(string anchorId, out List<GameObject> gameObjects)
    {
        GameObject anchor;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor) && AnchoredObjectsDictionary.TryGetValue(anchorId, out gameObjects))
        {
            _anchorsDictionary.Remove(anchorId);
            AnchoredObjectsDictionary.Remove(anchorId);
            foreach (Transform child in anchor.transform)
            {
                child.parent = null;
            }
            Object.Destroy(anchor);
            Debug.Log("ARLibraryTest: RemoveAnchor " + anchorId);
            return true;
        }
        else
        {
            gameObjects = new List<GameObject>();
            return false;
        }
    }

    public bool DestroyAnchorAndGameObjects(string anchorId)
    {
        GameObject anchor;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor) && AnchoredObjectsDictionary.ContainsKey(anchorId))
        {
            _anchorsDictionary.Remove(anchorId);
            AnchoredObjectsDictionary.Remove(anchorId);
            Object.Destroy(anchor); //This destroys the anchor gameobject and its hierarchy
            Debug.Log("ARLibraryTest: RemoveAnchorAndGameObjects " + anchorId);

            return true;
        }
        else
        {
            return false;
        }
    }

    public bool AttachGameObjectToAnchor(string anchorId, GameObject gameObject, bool worldPositionStays)
    {
        GameObject anchor;
        List<GameObject> list;
        if (_anchorsDictionary.TryGetValue(anchorId, out anchor) && AnchoredObjectsDictionary.TryGetValue(anchorId, out list))
        {
            list.Add(gameObject);
            gameObject.transform.SetParent(anchor.transform, worldPositionStays);
            return true;
        }

        return false;
    }

    public bool DetachGameObjectFromAnchor(string anchorId, GameObject gameObject)
    {
        List<GameObject> list;
        if (!AnchoredObjectsDictionary.TryGetValue(anchorId, out list))
        {
            return false;
        }
        if (list.Contains(gameObject))
        {
            gameObject.transform.parent = null;
            list.Remove(gameObject);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool DetachAllGameObjectsFromAnchor(string anchorId, out List<GameObject> gameObjects)
    {
        List<GameObject> list;
        if (AnchoredObjectsDictionary.TryGetValue(anchorId, out list))
        {
            foreach (var gameObject in list)
            {
                gameObject.transform.parent = null;
            }
            gameObjects = new List<GameObject>(list);
            list.Clear();

            return true;
        }
        else
        {
            gameObjects = null;
            return false;
        }
    }

    public bool GetAnchorPose(string anchorId, out Pose anchorPose)
    {
        GameObject anchor;
        if (anchorId != null && _anchorsDictionary.TryGetValue(anchorId, out anchor))
        {
            anchorPose.position = anchor.transform.position;
            anchorPose.rotation = anchor.transform.rotation;
            return true;
        }
        else
        {
            anchorPose = new Pose(Vector3.zero, Quaternion.identity);
            return false;
        }
    }

    public void ResetSession()
    {
        //Remove all anchors
        List<string> anchorIds = new List<string>(_anchorsDictionary.Keys);
        foreach (var anchorId in anchorIds)
        {
            DestroyAnchorAndGameObjects(anchorId);
        }
    }

}
