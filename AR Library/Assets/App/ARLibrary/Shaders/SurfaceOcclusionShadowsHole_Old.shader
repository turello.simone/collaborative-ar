﻿Shader "AR/SurfaceOcclusionShadowsHole (Old)"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_ShadowIntensity("Shadow Intensity", Range(0, 1)) = 1

		_HoleEnabled("Hole enabled", int) = 0
		_HolePosition("Hole center position", Vector) = (0,0,0,0)
		_HoleRadius("Hole Radius", Float) = 1

	}


	SubShader
	{

		Tags
		{
			"Queue" = "Geometry-1000" 
		}

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }
			Cull Back
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

				#include "UnityCG.cginc"
				#include "AutoLight.cginc"

				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_fwdbase

				uniform fixed4 _Color;
				uniform float _ShadowIntensity;

				int _HoleEnabled;
				float4 _HolePosition;
				float _HoleRadius;
				
				struct v2f
				{
					float4 pos : SV_POSITION;
					float3 worldPos : TEXCOORD0;
					LIGHTING_COORDS(0, 1)
				};

				v2f vert(appdata_base v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					if (_HoleEnabled == 1) o.worldPos = mul(unity_ObjectToWorld, v.vertex);
					TRANSFER_VERTEX_TO_FRAGMENT(o);

					return o;
				}

				fixed4 frag(v2f i) : COLOR
				{
					if (_HoleEnabled == 1) {
						half d = distance(i.worldPos, _HolePosition) - _HoleRadius;
						clip(d);
					}

					float attenuation = LIGHT_ATTENUATION(i);
					return fixed4(0,0,0,(1 - attenuation)*_ShadowIntensity) * _Color;
				}
			
			ENDCG
		}


	}

	Fallback "VertexLit"
}