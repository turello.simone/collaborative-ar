﻿Shader "AR/SurfaceGrid Proximity"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _GridColor ("Grid Color", Color) = (1.0, 1.0, 0.0, 1.0)
        _UvRotation ("UV Rotation", float) = 30
		_Position("Position", vector) = (0,0,0,0) // World space position of the spherical mask center
		_Radius("Radius", float) = 1.0 // Radius of the spherical mask
		_Softness("Softness", float) = 0.5 // Softness of the sphere edge
    }

    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        ZTest on
        ZWrite off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
                fixed4 color : COLOR;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD1;
                fixed4 color : COLOR;
				float3 worldPos : TEXCOORD0;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _GridColor;
            fixed _UvRotation;

			float4 _Position;
			half _Radius;
			half _Softness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                fixed cosr = cos(_UvRotation);
                fixed sinr = sin(_UvRotation);
                fixed2x2 uvrotation = fixed2x2(cosr, -sinr, sinr, cosr);

                const float3 arbitrary = float3(1.0, 1.0, 0.0);
                float3 vec_u = normalize(cross(v.normal, arbitrary));
                float3 vec_v = normalize(cross(v.normal, vec_u));

				// Project vertices in world frame onto vec_u and vec_v.
				float2 plane_uv = float2(dot(v.vertex.xyz, vec_u), dot(v.vertex.xyz, vec_v));
				float2 uv = plane_uv * _MainTex_ST.xy;
				o.uv = mul(uvrotation, uv);
				o.color = v.color;

				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);

				// Calculate distance from the spherical mask center
				half d = distance(i.worldPos, _Position);

				half sphere = saturate((d - _Radius) / -_Softness);
				clip(sphere);
                //return fixed4(_GridColor.rgb, min(col.r, sphere) * i.color.a);
                return fixed4(_GridColor.rgb, col.r * i.color.a * sphere);
            }
            ENDCG
        }
    }
}